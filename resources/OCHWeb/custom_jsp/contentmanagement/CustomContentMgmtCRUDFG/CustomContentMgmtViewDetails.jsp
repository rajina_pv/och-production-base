<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CustomContentMgmtCRUDFG.CONT_TYPE', 'FormField', pageContext)}" var="contType"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomContentMgmtCRUDFG.VIEW_MODE', 'FormField', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomContentMgmtCRUDFG.CALL_MODE', 'FormField', pageContext)}" var="callMode"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomContentMgmtCRUDFG.PHOTO_KEY','formfield', pageContext)}" var="photoKey"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomContentMgmtCRUDFG.FILEOUTPUTSTREAM','formfield', pageContext)}" var="fileOut"></feba2:Set>


<feba2:Set value="${feba2:equals(callMode,'5') or feba2:equals(callMode,'6') or feba2:equals(callMode,'7') or feba2:equals(callMode,'8') or feba2:equals(callMode,'9')}" var="isOption"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'APPROVALS')}" var="isApproval"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'CONF')}" var="isConf"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'VIEW_DETAILS')}" var="viewDetails"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'CONF') or feba2:equalsIgnoreCase(viewMode,'APPROVALS')}" var="isConfOrIsApprovals"></feba2:Set>
<feba2:Set value="${!viewDetails}" var="Authorization.visible"></feba2:Set>

<feba2:Set value="${feba2:equals(contType,'MRC')}" var="isMerchant"></feba2:Set>
<feba2:Set value="${feba2:equals(contType,'PRO')}" var="isPromo"></feba2:Set>
<feba2:Set value="${feba2:equals(contType,'SPL')}" var="isSplash"></feba2:Set>


<feba2:Set value="${!isViewDetails}" var="Authorization1.visible"></feba2:Set>

<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('isConfOrIsApprovals#Preview Confirmation Details||LC64@@!isConf#View Content Details||LC8284',pageContext)}"/>
<feba2:Page action="Finacle" name="CustomContentMgmtCRUDFG" forcontrolIDs="">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar29368658" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow" id="Image33330951" name="Image33330951" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC228" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
            <feba2:title id="title26742489" text="${feba2:select('isConfOrIsApprovals#Preview Confirmation Details||LC64@@!isConf#View Content Details||LC8284',pageContext)}" headinglevel="h1" upperCase="false" visible="true" />
             </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay25251687" style="" /> 
  <feba2:Section id="DispForm" identifier="DisplayForm" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="Caption30764024" text="${feba2:select('isViewDetails#Content Management||LC9974@@!isViewDetails#Preview Confirmation Details||LC64',pageContext)}" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:RowSetHeader identifier="RowSetHeader1" style="notopborder">
            <feba2:caption id="Caption24223076" text="Content Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1590" />
          </feba2:RowSetHeader>
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Start Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.START_DATE" name="CustomContentMgmtCRUDFG.START_DATE" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="End Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.END_DATE" name="CustomContentMgmtCRUDFG.END_DATE" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Content Type:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtCRUDFG.CONT_TYPE" key="CODE_TYPE=CTYP" name="CustomContentMgmtCRUDFG.CONT_TYPE" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="56" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Header:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isPromo}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.HEADER" name="CustomContentMgmtCRUDFG.HEADER" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isPromo}" />
            </feba2:Col>
          </feba2:row> 
           <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Description:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isPromo}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.DESCRIPTION" name="CustomContentMgmtCRUDFG.DESCRIPTION" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isPromo}"/>
            </feba2:Col>            
          </feba2:row>  
           <feba2:row identifier="Ra6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Text Type:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isPromo}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtCRUDFG.TEXT_TYPE" key="CODE_TYPE=STXT" name="CustomContentMgmtCRUDFG.TEXT_TYPE" visible="${isPromo}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="56" />
            </feba2:Col>
          </feba2:row>    
                   
           <feba2:row identifier="Ra7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Merchant Id:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtCRUDFG.MERCHANT_ID" key="CODE_TYPE=MRTY" name="CustomContentMgmtCRUDFG.MERCHANT_ID" visible="${isMerchant}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="" />
            </feba2:Col>
          </feba2:row>   
           <feba2:row identifier="Ra8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Andriod App URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.AND_APP_URL" name="CustomContentMgmtCRUDFG.AND_APP_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}"/>
            </feba2:Col>            
          </feba2:row>
          <feba2:row identifier="Ra9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Play Store URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.PLAY_STORE_URL" name="CustomContentMgmtCRUDFG.PLAY_STORE_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}"  />
            </feba2:Col>            
          </feba2:row>
          <feba2:row identifier="Ra10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="IOS App URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.IOS_APP_URL" name="CustomContentMgmtCRUDFG.IOS_APP_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}"/>
            </feba2:Col>            
          </feba2:row>  
          <feba2:row identifier="Ra10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="IOS Store URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.IOS_STORE_URL" name="CustomContentMgmtCRUDFG.IOS_STORE_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}"/>
            </feba2:Col>            
          </feba2:row> 
            <feba2:row identifier="Ra12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="File Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.FILE_NAME" name="CustomContentMgmtCRUDFG.FILE_NAME" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>          
           <feba2:row identifier="Ra12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="" isDownloadLink="false" isMenu="false" style="simpletext" visible="false" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field alt="front image" id="CustomContentMgmtCRUDFG.FILEOUTPUTSTREAM" name="CustomContentMgmtCRUDFG.FILEOUTPUTSTREAM" tagHelper="CardImageTagHelper" visible="false" />          
             </feba2:Col>
          </feba2:row> 
            <feba2:row identifier="Ra13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Priority:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtCRUDFG.PRIORITY" key="CODE_TYPE=PRI" name="CustomContentMgmtCRUDFG.PRIORITY" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="56" />
            </feba2:Col>
          </feba2:row>                    
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="Authorization" displaymode="N" identifier="InputForm_Authorization" style="section_grayborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="ApproverDetails18837206" name="ApproverDetails18837206" title="ApproverDetails" visible="${isConfOrIsApprovals}" hideAdditionalDetails="false" isRemarksRequired="true" tagHelper="ApproversDetailsTagHelper">
                <feba2:map>
                  <feba2:param name="isMandatory" value="false" />
                  <feba2:param name="isConfidential" value="N" />
                </feba2:map>
              </feba2:field>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="Authorization26807469" name="Authorization26807469" title="Authorization" visible="${isConfOrIsApprovals}" tagHelper="AuthenticationTagHelper" />             
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C10" style="">
              <feba2:field caption="Submit" id="USER_SUBMIT" name="USER_SUBMIT" style="formbtn_last" visible="${isConfOrIsApprovals}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC21" />
            </feba2:Col>
            <feba2:Col identifier="C11" style="">
              <feba2:field caption="Back" id="BACK" name="BACK" style="formbtn_last" visible="${!isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>
           <!--<feba2:Col identifier="C12" style="">
              <feba2:field caption="Back" id="BACK_TO_RETRIEVE_LIST" name="BACK_TO_RETRIEVE_LIST" style="formbtn_last" visible="${isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>-->
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomContentMgmtCRUDFG" />
 <feba2:hidden name="CustomContentMgmtCRUDFG.REPORTTITLE" id="CustomContentMgmtCRUDFG.REPORTTITLE" value="CustomContentMgmtViewDetails" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

