<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld"%>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set
	value="${feba2:getValue('CorpAdminFWFG.OPERATION_MODE', 'formfield', pageContext)}"
	var="sMode"></feba2:Set>
<feba2:Set
	value="${feba2:getValue('CorpAdminFWFG.OPERATION_MODE','formfield', pageContext)}"
	var="opModeIndicator"></feba2:Set>
<feba2:Set value="${feba2:equals(opModeIndicator,'INSERT')}"
	var="opMode"></feba2:Set>
<feba2:Set
	value="${feba2:select('opMode#Create Standard Text||LC7245@@!opMode#Modify Standard Text||LC7246',pageContext)}"
	var="displayMode"></feba2:Set>
<feba2:Set
	value="${feba2:select('opMode#true@@!opMode#false',pageContext)}"
	var="visible"></feba2:Set>
<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:title pageTitle="true" text="${displayMode}" />
<feba2:Page action="Finacle" name="CorpAdminFWFG"
	forcontrolIDs="Bank ID:=CorpAdminFWFG.BANK_ID@@Text Type:=CorpAdminFWFG.TEXT_TYPE@@Text Name:=CorpAdminFWFG.TEXT_NAME@@Standard Text:=CorpAdminFWFG.STANDARD_TEXT@@Display Order:=CorpAdminFWFG.DISPLAY_ORDER@@Created By:=CorpAdminFWFG.TSTM_COOKIE_R_CRE_ID@@Created On:=CorpAdminFWFG.TSTM_COOKIE_R_CRE_TIME@@Modified By:=CorpAdminFWFG.TSTM_COOKIE_R_MOD_ID@@Modified On:=CorpAdminFWFG.TSTM_COOKIE_R_MOD_TIME@@">
	<%
		if (!isGroupletView) {
	%>

	<%@include file="/jsp/header.jsp"%>
	<%
		if (pageContext.getAttribute("isRMUser").toString().equals("true")) {
	%>
	<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%
		} else {
	%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
	<%
		}
	%>
	<%
		}
	%>
	<feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb"
		style="section">
		<feba2:SubSectionSet identifier="SubSectionSet1" logical="true"
			style="width100percent">
			<feba2:SubSection identifier="SubSection1" logical="true"
				style="width100percent">
				<feba2:rowset identifier="Rowset1" logical="true"
					style="width100percent">
					<feba2:row identifier="Ra1" style="">
						<feba2:Col identifier="C1" style="">
							<feba2:moduleTopBar id="mtb" breadcrumb="true"
								helpImage="btn-help.gif" printImage="btn-print.gif"
								visible="true" />
						</feba2:Col>
					</feba2:row>
				</feba2:rowset>
			</feba2:SubSection>
		</feba2:SubSectionSet>
	</feba2:Section>
	<feba2:Section id="PgHeading" identifier="PageHeader" style="section">
		<feba2:SubSectionSet identifier="SubSectionSet1" logical="true"
			style="width100percent">
			<feba2:SubSection identifier="SubSection1" logical="true"
				style="width100percent">
				<feba2:rowset identifier="Rowset1" logical="true"
					style="width100percent">
					<feba2:row identifier="Ra1">
						<feba2:Col identifier="C1" style="width10">
							<feba2:field alt="Arrow Image" id="ArrowImage" name="a"
								src="arrow-pageheading.gif" isMenu="false"
								tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="pageheadingcaps">
							<feba2:title id="PageHeader" text="${displayMode}"
								headinglevel="h1" upperCase="false" visible="true" />
						</feba2:Col>
					</feba2:row>
				</feba2:rowset>
			</feba2:SubSection>
		</feba2:SubSectionSet>
	</feba2:Section>
	<feba2:ErrorDisplay id="ErrorDisplay5644939" style="alerttable" />
	<feba2:Section id="NavMenu" displaymode="N"
		identifier="NavigationMenuControl" style="greybottomleft">
		<feba2:SubSectionSet identifier="SubSectionSet1"
			style="greybottomright">
			<feba2:SubSection identifier="SubSection1" style="greytopleft">
				<feba2:rowset identifier="Rowset1" style="greytopright">
					<feba2:row identifier="Ra1" style="greybg_margin">
						<feba2:Col identifier="C1" style="">
							<feba2:field caption="Option" id="TradeFG.OPTION_ID"
								name="TradeFG.OPTION_ID"
								title="Financial Transaction Maintenance" buttonCaption="OK"
								tagHelper="AccessControlComboBoxTagHelper" visible="true"
								captionLC="LC31" titleLC="LC32" buttonCaptionLC="LC33" />
						</feba2:Col>
					</feba2:row>
				</feba2:rowset>
			</feba2:SubSection>
		</feba2:SubSectionSet>
	</feba2:Section>
	<feba2:Section id="InputForm" displaymode="N" identifier="InputForm"
		style="section_blackborder">
		<feba2:SectHeader identifier="Header">
			<feba2:caption id="caption" style="whtboldtxt" text="${displayMode}"
				isDownloadLink="false" isMenu="false" visible="true" />
		</feba2:SectHeader>
		<feba2:SubSectionSet identifier="SubSectionSet1" logical="true"
			style="width100percent">
			<feba2:SubSection identifier="SubSection1" style="width100percent">
				<feba2:rowset identifier="Rowset1" style="width100percent">
					<feba2:row identifier="Ra1" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol forControlId="CorpAdminFWFG.BANK_ID"
								id="BankID" name="b" text="Bank ID:"
								displayColonAfterDateFormat="true" style="simpletext"
								visible="true" textLC="LC44" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.BANK_ID" maxlength="11"
								name="CorpAdminFWFG.BANK_ID" disable="false"
								isDownloadLink="false" isMenu="false" readonly="false"
								style="searchsimpletext" tagHelper="OutputTextTagHelper"
								visible="true" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Ra2" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol forControlId="CorpAdminFWFG.TEXT_TYPE"
								id="TEXT_TYPE" name="t" required="${opMode}" text="Text Type:"
								displayColonAfterDateFormat="true" style="simpletext"
								visible="true" textLC="LC7248" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.TEXT_TYPE1" key="CODE_TYPE=STXT"
								name="CorpAdminFWFG.TEXT_TYPE" visible="${!opMode}"
								displayEvenIfAbsent="false" resourceName="CommonCode"
								style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" />
							<feba2:field combomode="complex" id="CorpAdminFWFG.TEXT_TYPE"
								key="CODE_TYPE=STXT" name="CorpAdminFWFG.TEXT_TYPE"
								select="true" title="Text Type" visible="${opMode}" all="false"
								disable="false" displayMode="N" empty="false"
								filter="CommonCodeFilter" preselect="false"
								style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper"
								titleLC="LC7255" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Ra3" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol forControlId="CorpAdminFWFG.TEXT_NAME"
								id="TEXT_NAME" name="t" required="${opMode}" text="Text Name:"
								displayColonAfterDateFormat="true" style="simpletext"
								visible="true" textLC="LC7250" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.TEXT_NAME1"
								name="CorpAdminFWFG.TEXT_NAME" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
							<feba2:field id="CorpAdminFWFG.TEXT_NAME" maxlength="35"
								name="CorpAdminFWFG.TEXT_NAME" style="querytextboxmedium"
								title="Text Name" visible="${opMode}" copyAllowed="true"
								disable="false" pasteAllowed="true" readonly="false"
								tagHelper="TextTagHelper" titleLC="LC7251" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Ra4" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol forControlId="CorpAdminFWFG.STANDARD_TEXT"
								id="STANDARD_TEXT" name="s" required="true"
								text="Standard Text:" displayColonAfterDateFormat="true"
								style="simpletext" visible="true" textLC="LC7252" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.STANDARD_TEXT"
								name="CorpAdminFWFG.STANDARD_TEXT" tabindex="0"
								title="Standard Text" copyAllowed="true" disable="false"
								pasteAllowed="true" readonly="false" richText="N"
								style="addressboxfixwidth" tagHelper="TextAreaTagHelper"
								visible="true" titleLC="LC7253" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Ra5" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol forControlId="CorpAdminFWFG.DISPLAY_ORDER"
								id="DISPLAY_ORDER" name="d" required="true"
								text="Display Order:" displayColonAfterDateFormat="true"
								style="simpletext" visible="true" textLC="LC72510" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.DISPLAY_ORDER1"
								name="CorpAdminFWFG.DISPLAY_ORDER" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
							<feba2:field fldFormatter="FEBAAIntFieldFormatter"
								id="CorpAdminFWFG.DISPLAY_ORDER" maxlength="35"
								name="CorpAdminFWFG.DISPLAY_ORDER" style="querytextboxmedium"
								title="Display Order" visible="${opMode}" copyAllowed="true"
								disable="false" pasteAllowed="true" readonly="false"
								tagHelper="TextTagHelper" titleLC="LC72511" />
						</feba2:Col>
					</feba2:row>
				</feba2:rowset>
			</feba2:SubSection>
			<feba2:SubSection identifier="SubSection2" style="width100percent">
				<feba2:SubSectHeader identifier="Header2">
					<feba2:caption id="history" text="History" visible="${!opMode}"
						isDownloadLink="false" isMenu="false" style="simpletext"
						textLC="LC57" />
				</feba2:SubSectHeader>
				<feba2:rowset identifier="Rowset5" style="width100percent">
					<feba2:row identifier="Re1" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTM_COOKIE_R_CRE_ID"
								id="TSTM_COOKIE_R_CRE_ID" name="c" text="Created By:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC58" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.TSTM_COOKIE_R_CRE_ID"
								name="CorpAdminFWFG.TSTM_COOKIE_R_CRE_ID" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Re2" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTM_COOKIE_R_CRE_TIME"
								id="TSTM_COOKIE_R_CRE_TIME" name="c" text="Created On:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC59" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field fldFormatter="HistoryDateFormatter"
								id="CorpAdminFWFG.TSTM_COOKIE_R_CRE_TIME"
								name="CorpAdminFWFG.TSTM_COOKIE_R_CRE_TIME" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Re3" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTM_COOKIE_R_MOD_ID"
								id="TSTM_COOKIE_R_MOD_ID" name="m" text="Modified By:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC60" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.TSTM_COOKIE_R_MOD_ID"
								name="CorpAdminFWFG.TSTM_COOKIE_R_MOD_ID" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Re4" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTM_COOKIE_R_MOD_TIME"
								id="TSTM_COOKIE_R_MOD_TIME" name="m" text="Modified On:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC61" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field fldFormatter="HistoryDateFormatter"
								id="CorpAdminFWFG.TSTM_COOKIE_R_MOD_TIME"
								name="CorpAdminFWFG.TSTM_COOKIE_R_MOD_TIME" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Re5" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTX_COOKIE_R_CRE_ID"
								id="TSTX_COOKIE_R_CRE_ID" name="c" text="Created By:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC58" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.TSTX_COOKIE_R_CRE_ID"
								name="CorpAdminFWFG.TSTX_COOKIE_R_CRE_ID" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Re6" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTX_COOKIE_R_CRE_TIME"
								id="TSTX_COOKIE_R_CRE_TIME" name="c" text="Created On:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC59" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field fldFormatter="HistoryDateFormatter"
								id="CorpAdminFWFG.TSTX_COOKIE_R_CRE_TIME"
								name="CorpAdminFWFG.TSTX_COOKIE_R_CRE_TIME" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Re7" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTX_COOKIE_R_MOD_ID"
								id="TSTX_COOKIE_R_MOD_ID" name="m" text="Modified By:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC60" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field id="CorpAdminFWFG.TSTX_COOKIE_R_MOD_ID"
								name="CorpAdminFWFG.TSTX_COOKIE_R_MOD_ID" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
					<feba2:row identifier="Re8" style="formrow">
						<feba2:Col identifier="C1" style="querytextleft">
							<feba2:labelforcontrol
								forControlId="CorpAdminFWFG.TSTX_COOKIE_R_MOD_TIME"
								id="TSTX_COOKIE_R_MOD_TIME" name="m" text="Modified On:"
								visible="${!opMode}" displayColonAfterDateFormat="true"
								style="simpletext" textLC="LC61" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="querytextright">
							<feba2:field fldFormatter="HistoryDateFormatter"
								id="CorpAdminFWFG.TSTX_COOKIE_R_MOD_TIME"
								name="CorpAdminFWFG.TSTX_COOKIE_R_MOD_TIME" visible="${!opMode}"
								disable="false" isDownloadLink="false" isMenu="false"
								readonly="false" style="searchsimpletext"
								tagHelper="OutputTextTagHelper" />
						</feba2:Col>
					</feba2:row>
				</feba2:rowset>
			</feba2:SubSection>
		</feba2:SubSectionSet>
	</feba2:Section>
	<feba2:Section id="NavPanel" displaymode="N"
		identifier="NavigationPanel" style="section">
		<feba2:SubSectionSet identifier="SubSectionSet1" logical="true"
			style="width100percent">
			<feba2:SubSection identifier="SubSection1" logical="true"
				style="width100percent">
				<feba2:rowset identifier="Rowset1" style="right">
					<feba2:row identifier="Ra1" style="">
						<feba2:Col identifier="C1" style="">
							<feba2:field caption="Continue" id="CONTINUE" name="CONTINUE"
								title="Continue" visible="${opMode}" disable="false"
								isDownloadAction="false" style="formbtn"
								tagHelper="ButtonTagHelper" captionLC="LC62" titleLC="LC62" />
						</feba2:Col>
						<feba2:Col identifier="C2" style="">
							<feba2:field caption="Continue" id="CONTINUEMODIFY"
								name="CONTINUE" title="Continue" visible="${!opMode}"
								disable="false" isDownloadAction="false" style="formbtn"
								tagHelper="ButtonTagHelper" captionLC="LC62" titleLC="LC62" />
						</feba2:Col>
						<feba2:Col identifier="C3" style="">
							<feba2:field caption="Reset" id="RESET" name="RESET"
								style="formbtn_last" title="Reset" disable="false"
								isDownloadAction="false" tagHelper="ButtonTagHelper"
								visible="true" captionLC="LC2" titleLC="LC2" />
						</feba2:Col>
						<feba2:Col identifier="C4" style="">
							<feba2:field caption="Back" id="MAIN_BACK" name="MAIN_BACK"
								title="Back" disable="false" isDownloadAction="false"
								style="formbtn" tagHelper="ButtonTagHelper" visible="true"
								captionLC="LC4" titleLC="LC4" />
						</feba2:Col>
					</feba2:row>
				</feba2:rowset>
			</feba2:SubSection>
		</feba2:SubSectionSet>
	</feba2:Section>
	<feba2:hidden id="CorpAdminFWFG.FMID" name="CorpAdminFWFG.FMID"
		value="STD" />
	<feba2:hidden id="CorpAdminFWFG.OPERATION_MODE"
		name="CorpAdminFWFG.OPERATION_MODE" value="${sMode}" />
	<feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__"
		value="CorpAdminFWFG" />
	<feba2:hidden name="CorpAdminFWFG.REPORTTITLE"
		id="CorpAdminFWFG.REPORTTITLE" value="StdTextMnt" />
	<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
	<%}%>
</feba2:Page>

