<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:title pageTitle="true" text = "Preview Confirmation Details" textLC = "LC64" />
<feba2:Page action="Finacle" name="CustomLoanInterestDetailsFG" forcontrolIDs="">
<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:Set value="${feba2:getValue('CustomLoanInterestDetailsFG.VIEW_MODE', 'FormField', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'CONF') or feba2:equalsIgnoreCase(viewMode,'APPROVALS')}" var="isConfOrIsApprovals"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'CONF')}" var="isConf"></feba2:Set>

<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="arrow" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="title30000602" text="Preview Confirmation Details" upperCase="True" headinglevel="h1" visible="true" textLC="LC64" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay22424518" style="alerttable" />
  <feba2:Section id="InputForm" displaymode="N" identifier="InputForm" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="header" style="whtboldtxt" text="Loan Interest Details" isDownloadLink="false" isMenu="false" visible="true" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanInterestDetailsFG.TENOR" id="tenor" name="tenor" required="true" text="Tenor:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC464" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanInterestDetailsFG.TENOR" key="CODE_TYPE=CTN" name="CustomLoanInterestDetailsFG.TENOR" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanInterestDetailsFG.INTEREST" id="interest" name="interest" required="true" text="Interest:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC466" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanInterestDetailsFG.INTEREST" name="CustomLoanInterestDetailsFG.INTEREST" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" maxlength="11" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="Authorization1" displaymode="N" identifier="InputForm_Authorization" style="section_grayborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="ApproverDetails17428736" name="ApproverDetails17428736" visible="true" hideAdditionalDetails="false" isRemarksRequired="true" tagHelper="ApproversDetailsTagHelper">
                <feba2:map>
                  <feba2:param name="isMandatory" value="false" />
                  <feba2:param name="isConfidential" value="N" />
                </feba2:map>
              </feba2:field>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="Authorization222442" name="Authorization222442" visible="false" tagHelper="AuthenticationTagHelper" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Submit" id="Button8957047" name="LOAN_SUBMIT" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" visible="${isConfOrIsApprovals}" captionLC="LC21" />
            </feba2:Col>
             <feba2:Col identifier="C2" style="">
              <feba2:field caption="Back" id="BACK" name="BACK" style="formbtn_last" visible="${!isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>
            <feba2:Col identifier="C3" style="">
              <feba2:field caption="Back" id="BACK_TO_ADD_INTEREST" name="BACK_TO_ADD_INTEREST" style="formbtn_last" visible="${isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanInterestDetailsFG" />
 <feba2:hidden name="CustomLoanInterestDetailsFG.REPORTTITLE" id="CustomLoanInterestDetailsFG.REPORTTITLE" value="CustomLoanTenorCreatePreview" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

