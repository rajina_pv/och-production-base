<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CustomLoanInterestDetailsFG.VIEW_MODE','formfield', pageContext)}" var="opModeIndicator"></feba2:Set>
<feba2:Set value="${feba2:equals(opModeIndicator,'ADD_INTEREST')}" var="opMode"></feba2:Set>
<feba2:Set value="${feba2:equals(opModeIndicator,'UPDATE_INTEREST')}" var="updateInt"></feba2:Set>
<feba2:Set value="${feba2:select('opMode#Add New||LC459@@!opMode#Update||LC460',pageContext)}" var="displayMode"></feba2:Set>
<feba2:Set value="${feba2:select('opMode#I@@!opMode#O',pageContext)}" var="Usage"></feba2:Set>
<feba2:Set value="${feba2:select('opMode#true@@!opMode#false',pageContext)}" var="req"></feba2:Set>
<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${displayMode}"/>
<feba2:Page action="Finacle" name="CustomLoanInterestDetailsFG" forcontrolIDs="Tenor:=CustomLoanInterestDetailsFG.TENOR@@Interest:=CustomLoanInterestDetailsFG.INTEREST@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="arrow" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeader" text="${displayMode}" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay22424518" style="alerttable" />
  <feba2:Section id="InputForm" displaymode="N" identifier="InputForm" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="header" style="whtboldtxt" text="${displayMode}" isDownloadLink="false" isMenu="false" visible="true" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanInterestDetailsFG.TENOR" id="tenor" name="tenor" required="true" text="Tenor:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC464" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="complex" id="CustomLoanInterestDetailsFG.TENOR" key="CODE_TYPE=CTN" name="CustomLoanInterestDetailsFG.TENOR" select="true" title="Tenor" visible="true" all="false" disable="${updateInt}" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanInterestDetailsFG.INTEREST" id="interest" name="interest" required="true" text="Interest:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC466" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanInterestDetailsFG.INTEREST" name="CustomLoanInterestDetailsFG.INTEREST" style="querytextboxmedium" title="Interest" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC467" />
            </feba2:Col>
          </feba2:row>
          
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Continue" id="CONTINUE" name="CONTINUE" title="Continue" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" visible="true" captionLC="LC62" titleLC="LC62" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field id="Back17125369" linkstyle="submit" name="PREV_SCREEN__" style="submit" tagHelper="BackTagHelper" title="Back" visible="true" titleLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:hidden id="CustomLoanInterestDetailsFG.VIEW_MODE" name="CustomLoanInterestDetailsFG.VIEW_MODE" value="${opModeIndicator}" />
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanInterestDetailsFG" />
 <feba2:hidden name="CustomLoanInterestDetailsFG.REPORTTITLE" id="CustomLoanInterestDetailsFG.REPORTTITLE" value="CustomLoanTenorCreate" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

