<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.USER_TYPE', 'FormField', pageContext)}" var="userType"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'1')}" var="isRet"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'2')}" var="isCorp"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'4')}" var="isBnkUsr"></feba2:Set>
<feba2:Set value="${feba2:select('isBnkUsr#multipart/form-data',pageContext)}" var="encType"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.VIEW_MODE', 'FormField', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isCorp,'true') and feba2:equalsIgnoreCase(viewMode,'CREATE')}" var="isCorpAndisCreate"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isCorp,'true') and feba2:equalsIgnoreCase(viewMode,'UPDATE')}" var="isCorpAndisUpdate"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isRet,'true') and feba2:equalsIgnoreCase(viewMode,'CREATE')}" var="isRetAndisCreate"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isRet,'true') and feba2:equalsIgnoreCase(viewMode,'UPDATE')}" var="isRetAndisUpdate"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isBnkUsr,'true') and feba2:equalsIgnoreCase(viewMode,'CREATE')}" var="isBnkUsrAndisCreate"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isBnkUsr,'true') and feba2:equalsIgnoreCase(viewMode,'UPDATE')}" var="isBnkUsrAndisUpdate"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.RET_BRAND_ID','formfield', pageContext)}" var="brandId"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.BAY_USER_ID','formfield', pageContext)}" var="corpId"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isCorp,'true') and feba2:equalsIgnoreCase(viewMode,'COPYREL')}" var="isCorpAndisCopy"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isRet,'true') and feba2:equalsIgnoreCase(viewMode,'COPYREL')}" var="isRetAndisCopy"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.CUST_ID','formfield', pageContext)}" var="custId"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isCorp,'true') and feba2:equalsIgnoreCase(viewMode,'COPYREL')}" var="isCorpAndisCopy"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(isRet,'true') and feba2:equalsIgnoreCase(viewMode,'COPYREL')}" var="isRetAndisCopy"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.IS_CRP_SYSTEM','formfield', pageContext)}" var="crpSystemParam"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(crpSystemParam,'Y')}" var="isCRPSystem"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('isRet#RetailUserMaintenance@@isCorp#CorporateUserMaintenance@@isBnkUsr#BankUserMaintenance',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('isRetAndisCreate#Create Retail User ID||LC1789@@isCorpAndisCreate#Create Corporate User ID||LC1790@@isBnkUsrAndisCreate#Create Bank User ID||LC1791@@isRetAndisUpdate#Modify Retail User ID||LC1792@@isCorpAndisUpdate#Modify Corporate User ID||LC1793@@isBnkUsrAndisUpdate#Modify Bank User ID||LC1794@@isCorpAndisCopy#Copy Corporate Relationship Details||LC1795@@isRetAndisCopy#Copy Retail Relationship Details||LC1796',pageContext)}"/>
<feba2:Page action="Finacle" name="RMUserMaintenanceCRUDFG" forcontrolIDs="Bank Id:=RMUserMaintenanceCRUDFG.BANK_ID@@Bank User Type:=RMUserMaintenanceCRUDFG.RM_USER_TYPE@@Branch Id:=RMUserMaintenanceCRUDFG.BRANCH_ID@@Corporate Id:=RMUserMaintenanceCRUDFG.BAY_USER_ID@@Corporate ID:=RMUserMaintenanceCRUDFG.BAY_USER_ID@@Corporate Type:=RMUserMaintenanceCRUDFG.CORP_USER_TYPE@@CIF ID:=RMUserMaintenanceCRUDFG.CUST_ID@@CIF ID:=RMUserMaintenanceCRUDFG.CUST_ID@@Retail User Id:||LC1806=RMUserMaintenanceCRUDFG.CORP_USER@@Corporate User Id:||LC1807=RMUserMaintenanceCRUDFG.CORP_USER@@Bank User Id:||LC1808=RMUserMaintenanceCRUDFG.CORP_USER@@Retail User:||LC1812=RMUserMaintenanceCRUDFG.CORP_USER@@Corporate User:||LC1813=RMUserMaintenanceCRUDFG.CORP_USER@@Bank User:||LC1686=RMUserMaintenanceCRUDFG.CORP_USER@@First Name:=RMUserMaintenanceCRUDFG.C_F_NAME@@Middle Name:=RMUserMaintenanceCRUDFG.C_M_NAME@@Last Name:=RMUserMaintenanceCRUDFG.C_L_NAME@@Salutation:=RMUserMaintenanceCRUDFG.SALUTATION@@Nickname:=RMUserMaintenanceCRUDFG.NICKNAME@@Category Code:=RMUserMaintenanceCRUDFG.CATEGORY_CODE@@Location:=RMUserMaintenanceCRUDFG.LOCATION@@Location Access Indicator:=RMUserMaintenanceCRUDFG.LOC_ACC_IND@@Division Access Indicator:=RMUserMaintenanceCRUDFG.DIV_ACC_IND@@Amount Format:=RMUserMaintenanceCRUDFG.AMT_FMT_CD@@Calendar Type:=RMUserMaintenanceCRUDFG.CAL_TYPE@@Date Format:=RMUserMaintenanceCRUDFG.DT_FMT@@Language ID:=RMUserMaintenanceCRUDFG.LANG_ID@@Administrator Id:=RMUserMaintenanceCRUDFG.ADM_ID@@Primary Division ID:=RMUserMaintenanceCRUDFG.P_DIV_ID@@Multi Currency Transaction Allowed:=RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED@@Workflow Rule Authority:=RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH@@Can Authorize Without Rules:=RMUserMaintenanceCRUDFG.AUTH_USER@@Access Scheme:=RMUserMaintenanceCRUDFG.ACCESS_SCHEME@@Entry Limit Scheme:=RMUserMaintenanceCRUDFG.RANGE_LIMIT_SCHEME@@Transaction Limit Scheme:=RMUserMaintenanceCRUDFG.LIMIT_SCHEME@@Transaction Authorization Scheme:=RMUserMaintenanceCRUDFG.TRAN_AUTH_SCHEME@@Authentication Scheme:=RMUserMaintenanceCRUDFG.AUTHENTICATION_SCHEME@@Page Scheme:=RMUserMaintenanceCRUDFG.PAGE_SCHEME@@Authentication Mode:=RMUserMaintenanceCRUDFG.AUTHENTICATION_MODE@@Authentication Mode:=RMUserMaintenanceCRUDFG.BANK_AUTH_MODE@@Secondary Authentication Mode:=RMUserMaintenanceCRUDFG.SECONDARY_AUTHENTICATION_MODE@@Authorization Mode:=RMUserMaintenanceCRUDFG.AUTHORIZATION_MODE@@Channel Ids:=RMUserMaintenanceCRUDFG.CHANNEL_ID_LIST@@Accessible Financial Transaction Types:=RMUserMaintenanceCRUDFG.USER_TXN_TYPES@@Authorization Mode Precedence:=RMUserMaintenanceCRUDFG.RET_AUTH_MODE_PREC@@Customer Assist Menu Profile:=RMUserMaintenanceCRUDFG.CUST_ASST_MENU_PRF@@Virtual Login Allowed:=RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED@@2FA Registration Required=RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED@@Account Format:=RMUserMaintenanceCRUDFG.ACC_FMT@@Date Of Birth:=RMUserMaintenanceCRUDFG.DATE_OF_BIRTH@@Number Of Household Members:=RMUserMaintenanceCRUDFG.NUM_HOUSEHOLD@@Customer Status:=RMUserMaintenanceCRUDFG.CUST_STATUS@@Gender:=RMUserMaintenanceCRUDFG.C_GENDER@@Anniversary Date:=RMUserMaintenanceCRUDFG.ANNIVERSARY_DATE@@Occupation:=RMUserMaintenanceCRUDFG.OCCUPATION@@Marital Status:=RMUserMaintenanceCRUDFG.MARITAL_STATUS@@Income Range:=RMUserMaintenanceCRUDFG.INC_RNG_CD@@Residential Status:=RMUserMaintenanceCRUDFG.C_RES_STATUS@@Educational Level:=RMUserMaintenanceCRUDFG.EDUCATIONAL_LEVEL@@Address (Line 1):=RMUserMaintenanceCRUDFG.C_ADDR1@@Address (Line 2):=RMUserMaintenanceCRUDFG.C_ADDR2@@Address (Line 3):=RMUserMaintenanceCRUDFG.C_ADDR3@@Address Type:=RMUserMaintenanceCRUDFG.CORRESPONDENCE_ADDRESS@@City:=RMUserMaintenanceCRUDFG.C_CITY@@State:=RMUserMaintenanceCRUDFG.C_STATE_DESC@@Country:=RMUserMaintenanceCRUDFG.C_CNTRY@@Zip Code:=RMUserMaintenanceCRUDFG.C_ZIP@@Phone Number:=RMUserMaintenanceCRUDFG.C_PHONE_NO@@Mobile Phone Number:=RMUserMaintenanceCRUDFG.C_M_PHONE_NO@@Fax Number:=RMUserMaintenanceCRUDFG.C_FAX_NO@@Email ID:=RMUserMaintenanceCRUDFG.C_EMAIL_ID@@Passport Number:=RMUserMaintenanceCRUDFG.PASSPORT_NUMBER@@Passport Issue Date:=RMUserMaintenanceCRUDFG.PASSPORT_ISSUE_DATE@@Passport Details:=RMUserMaintenanceCRUDFG.PASSPORT_DETAILS@@Passport Expiry Date:=RMUserMaintenanceCRUDFG.PASSPORT_EXPIRY_DATE@@Permanent Account Number/National ID:=RMUserMaintenanceCRUDFG.PAN_NATIONAL_ID@@Register For Alerts:=RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG@@Scheme ID:=RMUserMaintenanceCRUDFG.SCHEME_ID@@Start Time:=RMUserMaintenanceCRUDFG.DDT_FROM@@End Time:=RMUserMaintenanceCRUDFG.DDT_TO@@" enctype="${encType}">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (!pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%}%>
<%@include file="/jsp/sidebar.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%} %>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar17474161" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="arrow" id="Image17529214" name="Image17529214" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC1788" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="title16538590" text="${feba2:select('isRetAndisCreate#Create Retail User ID||LC1789@@isCorpAndisCreate#Create Corporate User ID||LC1790@@isBnkUsrAndisCreate#Create Bank User ID||LC1791@@isRetAndisUpdate#Modify Retail User ID||LC1792@@isCorpAndisUpdate#Modify Corporate User ID||LC1793@@isBnkUsrAndisUpdate#Modify Bank User ID||LC1794@@isCorpAndisCopy#Copy Corporate Relationship Details||LC1795@@isRetAndisCopy#Copy Retail Relationship Details||LC1796',pageContext)}" upperCase="True" headinglevel="h1" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay25519112" style="" />
  <feba2:Set value="${!isBnkUsr}" var="DispFormCollapsible.SubSection2.visible" />
  <feba2:Set value="${!isBnkUsr}" var="DispFormCollapsible.SubSection4.visible" />
  <feba2:Set value="${!isBnkUsr}" var="DispFormCollapsible.SubSection5.visible" />
  <feba2:Set value="${!isCorpAndisCopy and !isRetAndisCopy}" var="DispFormCollapsible.SubSection2.visible" />
  <feba2:Set value="${!isCorpAndisCopy and !isRetAndisCopy}" var="DispFormCollapsible.SubSection3.visible" />
  <feba2:Set value="${!isCorpAndisCopy and !isRetAndisCopy}" var="DispFormCollapsible.SubSection4.visible" />
  <feba2:Section id="DispFormCollapsible" identifier="DisplayForm_CollapsibleBlackHeader" style="section_blackborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection collapsible="true" identifier="SubSection1" style="width100percent">
        <feba2:SubSectHeader identifier="Header1" style="header">
          <feba2:caption id="Caption30687916" style="whiteboldlink" text="User Details" title="User Details" isDownloadLink="false" isMenu="false" visible="true" textLC="LC1591" titleLC="LC1591" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.BANK_ID" id="LabelForControl25619547" name="LabelForControl25619547" text="Bank Id:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.BANK_ID" name="RMUserMaintenanceCRUDFG.BANK_ID" title="Bank Id" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" maxlength="11" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.RM_USER_TYPE" id="LabelForControl8799624" name="LabelForControl8799624" required="True" text="Bank User Type:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1478" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.RM_USER_TYPE" key="CODE_TYPE=BUT" name="RMUserMaintenanceCRUDFG.RM_USER_TYPE" title="Bank User Type" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" select="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC839" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.BRANCH_ID" id="LabelForControl12578196" name="LabelForControl12578196" required="false" text="Branch Id:" visible="${isCRPSystem}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1798" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="horizontal" name="compsite" visible="true">
                <feba2:field disable="true" id="RMUserMaintenanceCRUDFG.BRANCH_ID" name="RMUserMaintenanceCRUDFG.BRANCH_ID" readonly="readonly" style="querytextboxmedium" title="Branch Id" visible="${isCRPSystem}" copyAllowed="true" pasteAllowed="true" tagHelper="TextTagHelper" titleLC="LC1799" maxlength="32" />
                <feba2:field caption="Look Up" function="onclick=&quot;goToLink('applicationmaintenance/BranchMaintenanceList.html')&quot;" id="InquiryFWFG.LOOK_UP__" name="InquiryFWFG.LOOK_UP__" visible="${isCRPSystem}" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC1430" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.BAY_USER_ID" id="LabelForControl33170118" name="LabelForControl33170118" required="True" text="Corporate Id:" visible="${isCorpAndisCreate}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1800" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="horizontal" name="compsite" visible="true">
                <feba2:field disable="true" id="RMUserMaintenanceCRUDFG.BAY_USER_ID" name="RMUserMaintenanceCRUDFG.BAY_USER_ID" readonly="readonly" style="querytextboxmedium" title="Corporate Id" visible="${isCorpAndisCreate}" copyAllowed="true" pasteAllowed="true" tagHelper="TextTagHelper" titleLC="LC1801" maxlength="32" />
                <feba2:field caption="Look Up" id="CORPORATE_LOOK_UP" name="CORPORATE_LOOK_UP" visible="${isCorpAndisCreate}" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC1430" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.BAY_USER_ID" id="LabelForControl3481631" name="LabelForControl3481631" required="false" text="Corporate ID:" visible="${isCorpAndisUpdate or isCorpAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC783" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="OutputTextbox5260286" name="RMUserMaintenanceCRUDFG.BAY_USER_ID" title="Corporate Id" visible="${isCorpAndisUpdate or isCorpAndisCopy}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1801" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CORP_USER_TYPE" id="LabelForControl29705964" name="LabelForControl29705964" text="Corporate Type:" visible="${isCorp}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1802" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CORP_USER_TYPE" name="RMUserMaintenanceCRUDFG.CORP_USER_TYPE" title="Corporate Type" visible="${isCorp}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1803" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CUST_ID" id="LabelForControl25511311" name="LabelForControl25511311" required="True" text="CIF ID:" visible="${isRetAndisCreate}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1804" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield id="TextboxButton20629704" align="horizontal" name="compsite" visible="true">
                <feba2:field id="RMUserMaintenanceCRUDFG.CUST_ID" name="RMUserMaintenanceCRUDFG.CUST_ID" readonly="readonly" style="querytextboxmedium" title="CIF ID" visible="${isRetAndisCreate}" copyAllowed="true" disable="false" pasteAllowed="true" tagHelper="TextTagHelper" titleLC="LC1805" maxlength="64" />
                <feba2:field caption="Look Up" id="RETAIL_CIF_LOOKUP" name="RETAIL_CIF_LOOKUP" visible="${isRetAndisCreate}" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC1430" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CUST_ID" id="LabelForControl27927754" name="LabelForControl27927754" required="false" text="CIF ID:" visible="${isRetAndisUpdate or isCorpAndisCopy or isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1804" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="OutputTextbox21722640" name="RMUserMaintenanceCRUDFG.CUST_ID" title="CIF ID" visible="${isRetAndisUpdate or isCorpAndisCopy or isRetAndisCopy}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1805" maxlength="64" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CORP_USER" id="LabelForControl27733830" name="LabelForControl27733830" required="True" text="${feba2:select('isRet#Retail User Id:||LC1806@@isCorp#Corporate User Id:||LC1807@@isBnkUsr#Bank User Id:||LC1808',pageContext)}" visible="${feba2:equalsIgnoreCase(isCorpAndisCreate,'true') or feba2:equalsIgnoreCase(isRetAndisCreate,'true') or feba2:equalsIgnoreCase(isBnkUsrAndisCreate,'true') or feba2:equalsIgnoreCase(isCorpAndisCopy,'true') or feba2:equalsIgnoreCase(isRetAndisCopy,'true')}" displayColonAfterDateFormat="true" style="simpletext" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CORP_USER" name="RMUserMaintenanceCRUDFG.CORP_USER" style="querytextboxmedium" title="${feba2:select('isRet#Retail User ID||LC1809@@isCorp#Corporate User ID||LC1810@@isBnkUsr#Bank User Id||LC1811',pageContext)}" visible="${feba2:equalsIgnoreCase(isCorpAndisCreate,'true') or feba2:equalsIgnoreCase(isRetAndisCreate,'true') or feba2:equalsIgnoreCase(isBnkUsrAndisCreate,'true') or feba2:equalsIgnoreCase(isCorpAndisCopy,'true') or feba2:equalsIgnoreCase(isRetAndisCopy,'true') }" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CORP_USER" id="LabelForControl23796547" name="LabelForControl23796547" required="false" text="${feba2:select('isRet#Retail User:||LC1812@@isCorp#Corporate User:||LC1813@@isBnkUsr#Bank User:||LC1686',pageContext)}" visible="${isCorpAndisUpdate or isRetAndisUpdate or isBnkUsrAndisUpdate}" displayColonAfterDateFormat="true" style="simpletext" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="OutputTextbox5934341" name="RMUserMaintenanceCRUDFG.CORP_USER" title="User ID" visible="${isCorpAndisUpdate or isRetAndisUpdate or isBnkUsrAndisUpdate}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC785" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol id="LabelForControl33017519" name="LabelForControl33017519" required="True" text="Customer Type:" title="Customer Type" visible="false" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1814" titleLC="LC1815" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="ComboBox1182667" name="ComboBox1182667" select="True" title="Customer Type" visible="false" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" key="CODE_TYPE=SIC" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1815" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol id="LabelForControl17612221" name="LabelForControl17612221" required="False" text="Customer Type:" title="Customer Type" visible="false" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1814" titleLC="LC1815" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="OutputTextbox7676460" name="OutputTextbox7676460" title="Customer Type" visible="false" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1815" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra17" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_F_NAME" id="LabelForControl29892258" name="LabelForControl29892258" required="True" text="First Name:" visible="${!isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC877" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_F_NAME" maxlength="100" name="RMUserMaintenanceCRUDFG.C_F_NAME" style="querytextboxmedium" title="First name" visible="${!isCorpAndisCopy and !isRetAndisCopy}" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" titleLC="LC845" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra18" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_M_NAME" id="LabelForControl28136633" name="LabelForControl28136633" text="Middle Name:" visible="${!isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1816" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_M_NAME" maxlength="100" name="RMUserMaintenanceCRUDFG.C_M_NAME" style="querytextboxmedium" title="Middle Name" visible="${!isCorpAndisCopy and !isRetAndisCopy}" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" titleLC="LC1817" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra19" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_L_NAME" id="LabelForControl20749327" name="LabelForControl20749327" required="True" text="Last Name:" visible="${!isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC878" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_L_NAME" maxlength="100" name="RMUserMaintenanceCRUDFG.C_L_NAME" style="querytextboxmedium" title="Last Name" visible="${!isCorpAndisCopy and !isRetAndisCopy}" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" titleLC="LC834" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra20" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.SALUTATION" id="LabelForControl22501758" name="LabelForControl22501758" required="True" text="Salutation:" visible="${!isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1818" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.SALUTATION" key="CODE_TYPE=DSAL" name="RMUserMaintenanceCRUDFG.SALUTATION" select="True" title="Salutation" visible="${!isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1819" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra21" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.NICKNAME" id="LabelForControl4009649" name="LabelForControl4009649" text="Nickname:" visible="${!isBnkUsr}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1346" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.NICKNAME" maxlength="30" name="RMUserMaintenanceCRUDFG.NICKNAME" style="querytextboxmedium" title="Nick Name" visible="${!isBnkUsr}" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" titleLC="LC1345" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra24" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CATEGORY_CODE" id="LabelForControl8413236" name="LabelForControl8413236" required="True" text="Category Code:" visible="${isRet and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1820" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CATEGORY_CODE" key="CODE_TYPE=CAT" name="RMUserMaintenanceCRUDFG.CATEGORY_CODE" select="True" title="Category Code" visible="${isRet and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1821" maxlength="8" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra25" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.LOCATION" id="LabelForControl9815756" name="LabelForControl9815756" text="Location:" visible="${isCorp}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1325" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field filter="LocationFilter" id="RMUserMaintenanceCRUDFG.LOCATION" key="${corpId}" name="RMUserMaintenanceCRUDFG.LOCATION" select="True" title="Location" visible="${isCorp}" all="false" disable="false" displayMode="N" empty="false" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1330" maxlength="64" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset identifier="Rowset2" style="width100percent">
          <feba2:row identifier="Rb1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.LOC_ACC_IND" id="LabelForControl9189705" name="LabelForControl9189705" required="True" text="Location Access Indicator:" visible="${isCorp}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1822" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield id="RadioCaption4771275" align="Horizontal" name="compsite" visible="true">
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.LOC_ACC_IND" name="RMUserMaintenanceCRUDFG.LOC_ACC_IND" preselect="False" title="Global" value="G" visible="${isCorp}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC1823" maxlength="1" />
                <feba2:caption id="Caption30526552" name="Caption30526552" text="Global" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1823" />
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.LOC_ACC_IND" name="RMUserMaintenanceCRUDFG.LOC_ACC_IND" title="Local" value="L" visible="${isCorp}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC1824" maxlength="1" />
                <feba2:caption id="Caption16096016" name="Caption16096016" text="Local" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1824" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.DIV_ACC_IND" id="LabelForControl17098443" name="LabelForControl17098443" required="True" text="Division Access Indicator:" visible="${isCorp}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1825" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="Horizontal" name="compsite" visible="true">
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.DIV_ACC_IND" name="RMUserMaintenanceCRUDFG.DIV_ACC_IND" preselect="False" title="Global" value="G" visible="${isCorp}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC1823" maxlength="1" />
                <feba2:caption id="Caption265455" name="Caption265455" text="Global" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1823" />
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.DIV_ACC_IND" name="RMUserMaintenanceCRUDFG.DIV_ACC_IND" preselect="False" title="Local" value="L" visible="${isCorp}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC1824" maxlength="1" />
                <feba2:caption id="Caption29229984" name="Caption29229984" text="Local" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1824" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.AMT_FMT_CD" id="LabelForControl3045317" name="LabelForControl3045317" required="True" text="Amount Format:" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1348" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AMT_FMT_CD" key="CODE_TYPE=AFT" name="RMUserMaintenanceCRUDFG.AMT_FMT_CD" select="True" title="Amount Format" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1347" maxlength="2" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CAL_TYPE" id="LabelForControl2003243" name="LabelForControl2003243" required="True" text="Calendar Type:" visible="${!isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1337" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CAL_TYPE" key="CODE_TYPE=CALT" name="RMUserMaintenanceCRUDFG.CAL_TYPE" select="True" title="Calendar Type" visible="${!isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1336" maxlength="35" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.DT_FMT" id="LabelForControl17899801" name="LabelForControl17899801" required="True" text="Date Format:" visible="${!isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1339" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="Horizontal" name="composite" visible="true">
                <feba2:field id="RMUserMaintenanceCRUDFG.DT_FMT" key="CODE_TYPE=DFT" name="RMUserMaintenanceCRUDFG.DT_FMT" select="True" title="Date Format" visible="${!isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1338" maxlength="30" />
                <feba2:caption id="Caption12238130" style="queryitalictext" text="(If calendar type is Hijri, valid date formats are dd/MM/yyyy or MM/dd/yyyy)" visible="${!isCorpAndisCopy and !isRetAndisCopy}" isDownloadLink="false" isMenu="false" textLC="LC1340" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.LANG_ID" id="LabelForControl2016541" name="LabelForControl2016541" required="True" text="Language ID:" visible="${!isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC461" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.LANG_ID" key="CODE_TYPE=LANG" name="RMUserMaintenanceCRUDFG.LANG_ID" select="True" title="Language Id" visible="${!isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1826" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.ADM_ID" id="LabelForControl17511920" name="LabelForControl17511920" required="True" text="Administrator Id:" visible="${!isBnkUsr}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1827" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.ADM_ID" key="CODE_TYPE=RMRL" name="RMUserMaintenanceCRUDFG.ADM_ID" select="True" title="Administrator Id" visible="${!isBnkUsr}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1828" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.P_DIV_ID" id="LabelForControl11791390" name="LabelForControl11791390" text="Primary Division ID:" visible="${isCorp and !isCorpAndCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1829" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field filter="DivisionFilter" id="RMUserMaintenanceCRUDFG.P_DIV_ID" key="${corpId}" name="RMUserMaintenanceCRUDFG.P_DIV_ID" preselect="True" select="True" title="Primary Division Id" visible="${isCorp and !isCorpAndCopy}" all="false" disable="false" displayMode="N" empty="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1830" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED" id="LabelForControl15924253" name="LabelForControl15924253" required="True" text="Multi Currency Transaction Allowed:" visible="${isCRPSystem}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1831" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield id="RadioCaption18946631" visible="true" align="Horizontal" name="compsite">
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED" name="RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED" title="Yes" value="Y" visible="${isCRPSystem}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC53" maxlength="1" />
                <feba2:caption id="Caption15457397" name="Caption15457397" text="Yes" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC53" />
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED" name="RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED" title="No" value="N" visible="${isCRPSystem}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC54" maxlength="1" />
                <feba2:caption id="Caption19232958" name="Caption19232958" text="No" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC54" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH" id="LabelForControl2106139" name="LabelForControl2106139" text="Workflow Rule Authority:" visible="${isCRPSystem}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1832" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield visible="true" align="Horizontal" name="compsite">
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH" name="RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH" title="Yes" value="Y" visible="${isCRPSystem}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC53" maxlength="1" />
                <feba2:caption id="Caption20238816" name="Caption20238816" text="Yes" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC53" />
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH" name="RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH" title="No" value="N" visible="${isCRPSystem}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC54" maxlength="1" />
                <feba2:caption id="Caption16704147" name="Caption16704147" text="No" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC54" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.AUTH_USER" id="LabelForControl12954500" name="LabelForControl12954500" required="True" text="Can Authorize Without Rules:" visible="${isCRPSystem}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1833" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield visible="true" align="Horizontal" name="compsite">
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.AUTH_USER" name="RMUserMaintenanceCRUDFG.AUTH_USER" preselect="False" title="Yes" value="Y" visible="${isCRPSystem}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC53" maxlength="1" />
                <feba2:caption id="Caption20596627" name="Caption20596627" text="Yes" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC53" />
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.AUTH_USER" name="RMUserMaintenanceCRUDFG.AUTH_USER" title="No" value="N" visible="${isCRPSystem}" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC54" maxlength="1" />
                <feba2:caption id="Caption30309562" name="Caption30309562" text="No" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC54" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.ACCESS_SCHEME" id="LabelForControl6117973" name="LabelForControl6117973" required="True" text="Access Scheme:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1081" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.ACCESS_SCHEME" key="CODE_TYPE=CAMS" name="RMUserMaintenanceCRUDFG.ACCESS_SCHEME" select="True" title="Access Scheme" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1092" maxlength="56" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.RANGE_LIMIT_SCHEME" id="LabelForControl6117965" name="LabelForControl6117965" required="True" text="Entry Limit Scheme:" visible="${!isBnkUsr}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1834" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.RANGE_LIMIT_SCHEME" key="CODE_TYPE=RLS" name="RMUserMaintenanceCRUDFG.RANGE_LIMIT_SCHEME" select="True" title="Entry Limit Scheme" visible="${!isBnkUsr}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1835" maxlength="10" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset identifier="Rowset3" style="width100percent">
          <feba2:row identifier="Rc1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.LIMIT_SCHEME" id="LabelForControl18595566" name="LabelForControl18595566" required="True" text="Transaction Limit Scheme:" visible="${!isBnkUsr}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1836" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.LIMIT_SCHEME" key="CODE_TYPE=LSH" name="RMUserMaintenanceCRUDFG.LIMIT_SCHEME" select="True" title="Transaction Limit Scheme" visible="${!isBnkUsr}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1837" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.TRAN_AUTH_SCHEME" id="LabelForControl3897195" name="LabelForControl3897195" required="True" text="Transaction Authorization Scheme:" visible="${!isBnkUsr and !isCorpAndCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1838" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.TRAN_AUTH_SCHEME" key="CODE_TYPE=ASC" name="RMUserMaintenanceCRUDFG.TRAN_AUTH_SCHEME" select="True" title="Transaction Authorization Scheme" visible="${!isBnkUsr and !isCorpAndCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1839" maxlength="56" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.AUTHENTICATION_SCHEME" id="LabelForControl8799623" name="LabelForControl8799623" required="True" text="Authentication Scheme:" visible="${isBnkUsr or isRet}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC101" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AUTHENTICATION_SCHEME" key="CODE_TYPE=CAUS" name="RMUserMaintenanceCRUDFG.AUTHENTICATION_SCHEME" select="True" title="Authentication Scheme" visible="${isBnkUsr or isRet}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC102" maxlength="56" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.PAGE_SCHEME" id="LabelForControl4075003" name="LabelForControl4075003" required="false" text="Page Scheme:" visible="${isCorp and !isCorpAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1840" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PAGE_SCHEME" key="CODE_TYPE=PGS" name="RMUserMaintenanceCRUDFG.PAGE_SCHEME" select="True" title="Page Scheme" visible="${isCorp and !isCorpAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1841" maxlength="100" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.AUTHENTICATION_MODE" id="LabelForControl28539421" name="LabelForControl28539421" required="True" text="Authentication Mode:" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC103" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AUTHENTICATION_MODE" key="CODE_TYPE=ATN" name="RMUserMaintenanceCRUDFG.AUTHENTICATION_MODE" select="True" title="Authentication Mode" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC104" maxlength="4" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.BANK_AUTH_MODE" id="LabelForControl2872959" name="LabelForControl2872959" required="True" text="Authentication Mode:" visible="${isBnkUsr}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC103" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.BANK_AUTH_MODE" key="CODE_TYPE=BATN" name="RMUserMaintenanceCRUDFG.BANK_AUTH_MODE" select="True" title="Authentication Mode" visible="${isBnkUsr}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC104" maxlength="4" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.SECONDARY_AUTHENTICATION_MODE" id="LabelForControl1184520" name="LabelForControl1184520" required="False" text="Secondary Authentication Mode:" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1842" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.SECONDARY_AUTHENTICATION_MODE" key="CODE_TYPE=ATN" name="RMUserMaintenanceCRUDFG.SECONDARY_AUTHENTICATION_MODE" select="True" title="Secondary Authorization Mode" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1843" maxlength="4" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.AUTHORIZATION_MODE" id="LabelForControl3859690" name="LabelForControl3859690" required="True" text="Authorization Mode:" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1844" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AUTHORIZATION_MODE" key="CODE_TYPE=AUT" name="RMUserMaintenanceCRUDFG.AUTHORIZATION_MODE" select="True" title="Authorization Mode" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1845" maxlength="4" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CHANNEL_ID_LIST" id="LabelForControl24085034" name="LabelForControl24085034" required="false" text="Channel Ids:" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy and !isCorpAndisUpdate and !isRetAndisUpdate}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1846" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="simple" id="RMUserMaintenanceCRUDFG.CHANNEL_ID_LIST" key="CODE_TYPE=AIS|ENTITY=AIS_FOR_CHANNEL_ID|SCENARIO=GEN" multiple="Any" name="RMUserMaintenanceCRUDFG.CHANNEL_ID_LIST" title="List of Channel Ids" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy and !isCorpAndisUpdate and !isRetAndisUpdate}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" select="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1442" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.USER_TXN_TYPES" id="LabelForControl24085043" name="LabelForControl24085043" required="True" text="Accessible Financial Transaction Types:" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1847" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="simple" id="RMUserMaintenanceCRUDFG.USER_TXN_TYPES" key="CODE_TYPE=TTG" multiple="Any" name="RMUserMaintenanceCRUDFG.USER_TXN_TYPES" title="Accessible Financial Transaction Types" visible="${!isBnkUsr and !isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" select="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1848" maxlength="512" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.RET_AUTH_MODE_PREC" id="LabelForControl28099449" name="LabelForControl28099449" required="True" text="Authorization Mode Precedence:" visible="${isRet and !isCorpAndisCopy and !isRetAndisCopy}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1849" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.RET_AUTH_MODE_PREC" key="CODE_TYPE=AMP" name="RMUserMaintenanceCRUDFG.RET_AUTH_MODE_PREC" select="True" title="Authorization Mode Precedence" visible="${isRet and !isCorpAndisCopy and !isRetAndisCopy}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1850" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CUST_ASST_MENU_PRF" id="LabelForControl20590596" name="LabelForControl20590596" text="Customer Assist Menu Profile:" visible="false" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1851" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="horizontal" name="compsite" visible="true">
                <feba2:field disable="true" id="RMUserMaintenanceCRUDFG.CUST_ASST_MENU_PRF" name="RMUserMaintenanceCRUDFG.CUST_ASST_MENU_PRF" style="querytextboxmedium" title="Customer Assist Menu Profile" visible="false" copyAllowed="true" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" titleLC="LC1852" maxlength="8" />
                <feba2:field caption="Look Up" id="CUST_ASST_MENU_PRF_LOOKUP" name="CUST_ASST_MENU_PRF_LOOKUP" visible="false" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC1430" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" id="LabelForControl20590597" name="LabelForControl20590597" required="True" text="Virtual Login Allowed:" visible="false" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1853" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="Horizontal" name="compsite" visible="true">
                <feba2:field checked="False" disable="${isBnkUsrAndisUpdate}" id="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" name="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" preselect="False" title="Yes" value="Y" visible="false" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC53" maxlength="1" />
                <feba2:caption id="Caption8996809" name="Caption20596627" text="Yes" visible="false" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC53" />
                <feba2:field checked="False" disable="${isBnkUsrAndisUpdate}" id="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" name="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" title="No" value="N" visible="false" style="absmiddle" tagHelper="RadioTagHelper" titleLC="LC54" maxlength="1" />
                <feba2:caption id="Caption3830572" name="Caption3830572" text="No" visible="false" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC54" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc14" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" id="LabelForControl2742317" name="LabelForControl2742317" required="True" text="2FA Registration Required" title="2FA Registration Required" displayColonAfterDateFormat="true" style="simpletext" visible="false" textLC="LC1854" titleLC="LC1854" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="Horizontal" name="compsite" visible="true">
                <feba2:field id="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" name="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" title="Yes" value="Y" style="absmiddle" tagHelper="RadioTagHelper" visible="false" titleLC="LC53" maxlength="1" />
                <feba2:caption id="Caption18207567" text="Yes" title="Yes" isDownloadLink="false" isMenu="false" style="simpletext" visible="false" textLC="LC53" titleLC="LC53" />
                <feba2:field id="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" name="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" title="No" value="N" style="absmiddle" tagHelper="RadioTagHelper" visible="false" titleLC="LC54" maxlength="1" />
                <feba2:caption id="Caption32158944" text="No" title="No" isDownloadLink="false" isMenu="false" style="simpletext" visible="false" textLC="LC54" titleLC="LC54" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc15" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.ACC_FMT" id="LabelForControl20262903" name="LabelForControl20262903" required="True" text="Account Format:" title="Account Format" visible="${isCorp}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1362" titleLC="LC1363" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.ACC_FMT" key="CODE_TYPE=ACFT" name="RMUserMaintenanceCRUDFG.ACC_FMT" preSelectValue="2" preselect="True" title="Account Format" visible="${isCorp}" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" select="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC1363" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset identifier="Rowset4" style="width100percent">
          <feba2:row identifier="Rd1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol id="LabelForControl25294209" text="Photo:" visible="${isBnkUsr}" displayColonAfterDateFormat="true" style="simpletext" textLC="LC1855" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.FILE_PATH" name="RMUserMaintenanceCRUDFG.FILE_PATH" title="Choose file" visible="${isBnkUsr}" disable="false" style="txtfield_fl" tagHelper="FileBrowseTagHelper" titleLC="LC1856" maxlength="255" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rd2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:blankfield id="Blankfield19180455" visible="${isBnkUsr}" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="horizontal" name="CaptionBlankOutputTextBox" visible="true">
                <feba2:caption id="Caption24568793" name="Caption24568793" style="queryitalictextwithoutwidth" text="Allowed formats are :" visible="${isBnkUsr}" isDownloadLink="false" isMenu="false" textLC="LC1857" />
                <feba2:blankfield id="Blankfield6873667" visible="true" />
                <feba2:field id="RMUserMaintenanceCRUDFG.SUPPORTED_FORMATS" name="RMUserMaintenanceCRUDFG.SUPPORTED_FORMATS" style="queryitalictextwithoutwidth" visible="${isBnkUsr}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" tagHelper="OutputTextTagHelper" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection collapsible="true" identifier="SubSection2" style="width100percent">
        <feba2:SubSectHeader identifier="Header2" style="header">
          <feba2:caption id="Caption32173387" style="whiteboldlink" text="Personal Details" title="Personal Details" isDownloadLink="false" isMenu="false" visible="true" textLC="LC1294" titleLC="LC1294" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol displayDateFormat="True" forControlId="RMUserMaintenanceCRUDFG.DATE_OF_BIRTH" id="LabelForControl28433699" name="LabelForControl28433699" text="Date Of Birth:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1858" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field fldFormatter="DateFormatter" id="RMUserMaintenanceCRUDFG.DATE_OF_BIRTH" name="RMUserMaintenanceCRUDFG.DATE_OF_BIRTH" title="Date of Birth" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC1859" maxlength="" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.NUM_HOUSEHOLD" id="LabelForControl9007240" name="LabelForControl9007240" text="Number Of Household Members:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1860" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.NUM_HOUSEHOLD" name="RMUserMaintenanceCRUDFG.NUM_HOUSEHOLD" style="querytextboxmedium" title="Number Of Household Members" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1861" maxlength="2" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CUST_STATUS" id="LabelForControl10654680" name="LabelForControl10654680" text="Customer Status:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1862" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CUST_STATUS" key="CODE_TYPE=CST" name="RMUserMaintenanceCRUDFG.CUST_STATUS" select="True" title="Customer Status" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1863" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_GENDER" id="LabelForControl10164452" name="LabelForControl10164452" text="Gender:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1864" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_GENDER" key="CODE_TYPE=GEN" name="RMUserMaintenanceCRUDFG.C_GENDER" select="True" title="Gender" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1865" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol displayDateFormat="True" forControlId="RMUserMaintenanceCRUDFG.ANNIVERSARY_DATE" id="LabelForControl10466159" name="LabelForControl10466159" text="Anniversary Date:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1866" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field fldFormatter="DateFormatter" id="RMUserMaintenanceCRUDFG.ANNIVERSARY_DATE" name="RMUserMaintenanceCRUDFG.ANNIVERSARY_DATE" title="Anniversary Date" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC1867" maxlength="" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.OCCUPATION" id="LabelForControl23122077" name="LabelForControl23122077" text="Occupation:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1868" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.OCCUPATION" key="CODE_TYPE=OCP" name="RMUserMaintenanceCRUDFG.OCCUPATION" select="True" title="Occupation" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1869" maxlength="5" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.MARITAL_STATUS" id="LabelForControl22647062" name="LabelForControl22647062" text="Marital Status:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1870" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.MARITAL_STATUS" key="CODE_TYPE=MST" name="RMUserMaintenanceCRUDFG.MARITAL_STATUS" select="True" title="Marital Status" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1871" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.INC_RNG_CD" id="LabelForControl22462984" name="LabelForControl22462984" text="Income Range:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1872" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.INC_RNG_CD" key="CODE_TYPE=INR" name="RMUserMaintenanceCRUDFG.INC_RNG_CD" select="True" title="Income Range" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1873" maxlength="2" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_RES_STATUS" id="LabelForControl14144045" name="LabelForControl14144045" text="Residential Status:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1874" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_RES_STATUS" key="CODE_TYPE=RSC" name="RMUserMaintenanceCRUDFG.C_RES_STATUS" select="True" title="Residential Status" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1875" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.EDUCATIONAL_LEVEL" id="LabelForControl21388554" name="LabelForControl21388554" text="Educational Level:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1876" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.EDUCATIONAL_LEVEL" key="CODE_TYPE=EDL" name="RMUserMaintenanceCRUDFG.EDUCATIONAL_LEVEL" select="True" title="Educational Level" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1877" maxlength="3" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection collapsible="true" identifier="SubSection3" style="width100percent">
        <feba2:SubSectHeader identifier="Header3" style="header">
          <feba2:caption id="Caption25741589" style="whiteboldlink" text="Contact Details" title="Contact Details" isDownloadLink="false" isMenu="false" visible="true" textLC="LC1303" titleLC="LC1303" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset9" style="width100percent">
          <feba2:row identifier="Ri1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_ADDR1" id="LabelForControl2203340" name="LabelForControl2203340" text="Address (Line 1):" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1878" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ADDR1" name="RMUserMaintenanceCRUDFG.C_ADDR1" style="querytextboxmedium" title="Address (Line1)" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1879" maxlength="80" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_ADDR2" id="LabelForControl17577688" name="LabelForControl17577688" text="Address (Line 2):" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ADDR2" name="RMUserMaintenanceCRUDFG.C_ADDR2" style="querytextboxmedium" title="Address (Line 2)" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1881" maxlength="80" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_ADDR3" id="LabelForControl10217864" name="LabelForControl10217864" text="Address (Line 3):" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1882" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ADDR3" name="RMUserMaintenanceCRUDFG.C_ADDR3" style="querytextboxmedium" title="Address (Line 3)" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1883" maxlength="80" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.CORRESPONDENCE_ADDRESS" id="LabelForControl21235245" name="LabelForControl21235245" text="Address Type:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1884" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CORRESPONDENCE_ADDRESS" key="CODE_TYPE=CRA" name="RMUserMaintenanceCRUDFG.CORRESPONDENCE_ADDRESS" select="True" title="Address Type" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1885" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_CITY" id="LabelForControl12785496" name="LabelForControl12785496" text="City:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC164" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_CITY" name="RMUserMaintenanceCRUDFG.C_CITY" style="querytextboxmedium" title="City" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC165" maxlength="50" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_STATE_DESC" id="LabelForControl4769520" name="LabelForControl4769520" text="State:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC168" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="horizontal" name="compsite" visible="true">
                <feba2:field disable="true" id="RMUserMaintenanceCRUDFG.C_STATE_DESC" name="RMUserMaintenanceCRUDFG.C_STATE_DESC" style="querytextboxmedium" title="State" copyAllowed="true" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC169" maxlength="32" />
                <feba2:field caption="Lookup" id="Button6056791" name="STATE_LOOK_UP" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" visible="true" captionLC="LC50" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_CNTRY" id="LabelForControl3550973" name="LabelForControl3550973" text="Country:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC166" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_CNTRY" key="CODE_TYPE=CNT" name="RMUserMaintenanceCRUDFG.C_CNTRY" select="True" title="Country" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC167" maxlength="5" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_ZIP" id="LabelForControl7272800" name="LabelForControl7272800" text="Zip Code:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC204" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ZIP" name="RMUserMaintenanceCRUDFG.C_ZIP" style="querytextboxmedium" title="Zip Code" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC171" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_PHONE_NO" id="LabelForControl19685429" name="LabelForControl19685429" text="Phone Number:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC174" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_PHONE_NO" name="RMUserMaintenanceCRUDFG.C_PHONE_NO" style="querytextboxmedium" title="Phone Number" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC177" maxlength="20" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_M_PHONE_NO" id="LabelForControl2193527" name="LabelForControl2193527" text="Mobile Phone Number:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1886" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_M_PHONE_NO" name="RMUserMaintenanceCRUDFG.C_M_PHONE_NO" style="querytextboxmedium" title="Mobile Phone Number" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1887" maxlength="20" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_FAX_NO" id="LabelForControl16312118" name="LabelForControl16312118" text="Fax Number:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC178" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_FAX_NO" name="RMUserMaintenanceCRUDFG.C_FAX_NO" style="querytextboxmedium" title="Fax Number" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC179" maxlength="35" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.C_EMAIL_ID" id="LabelForControl12268035" name="LabelForControl12268035" text="Email ID:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC831" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_EMAIL_ID" name="RMUserMaintenanceCRUDFG.C_EMAIL_ID" style="querytextboxmedium" title="Email Id" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1888" maxlength="80" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection collapsible="true" identifier="SubSection4" style="width100percent">
        <feba2:SubSectHeader identifier="Header4" style="header">
          <feba2:caption id="Caption3213471" style="whiteboldlink" text="Other Details" title="Other Details" isDownloadLink="false" isMenu="false" visible="true" textLC="LC1889" titleLC="LC1889" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset13" style="width100percent">
          <feba2:row identifier="Rm1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.PASSPORT_NUMBER" id="LabelForControl20884576" name="LabelForControl20884576" text="Passport Number:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1890" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PASSPORT_NUMBER" name="RMUserMaintenanceCRUDFG.PASSPORT_NUMBER" style="querytextboxmedium" title="Passport Number" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1891" maxlength="20" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol displayDateFormat="True" forControlId="RMUserMaintenanceCRUDFG.PASSPORT_ISSUE_DATE" id="LabelForControl30960217" name="LabelForControl30960217" text="Passport Issue Date:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PASSPORT_ISSUE_DATE" name="RMUserMaintenanceCRUDFG.PASSPORT_ISSUE_DATE" title="Passport Issue Date" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC1893" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.PASSPORT_DETAILS" id="LabelForControl33277320" name="LabelForControl33277320" text="Passport Details:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1894" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PASSPORT_DETAILS" name="RMUserMaintenanceCRUDFG.PASSPORT_DETAILS" style="querytextboxmedium" title="Passport Details" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1895" maxlength="30" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol displayDateFormat="True" forControlId="RMUserMaintenanceCRUDFG.PASSPORT_EXPIRY_DATE" id="LabelForControl12883832" name="LabelForControl12883832" text="Passport Expiry Date:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1896" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field fldFormatter="DateFormatter" id="RMUserMaintenanceCRUDFG.PASSPORT_EXPIRY_DATE" name="RMUserMaintenanceCRUDFG.PASSPORT_EXPIRY_DATE" title="Passport Expiry Date" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC1897" maxlength="" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.PAN_NATIONAL_ID" id="LabelForControl2974691" name="LabelForControl2974691" text="Permanent Account Number/National ID:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1898" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PAN_NATIONAL_ID" name="RMUserMaintenanceCRUDFG.PAN_NATIONAL_ID" size="15" style="querytextboxmedium" title="Permanent Account Number/National ID" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC1899" maxlength="20" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection collapsible="true" identifier="SubSection5" style="width100percent">
        <feba2:SubSectHeader identifier="Header5" style="header">
          <feba2:caption id="Caption19929290" style="whiteboldlink" text="Register For Alerts(Enter the value only if you want to register for alerts)" isDownloadLink="false" isMenu="false" visible="true" textLC="LC1900" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset17" style="width100percent">
          <feba2:row identifier="Rq1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG" id="LabelForControl33390025" name="LabelForControl33390025" text="Register For Alerts:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1901" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="Horizontal" name="compsite" visible="true">
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG" name="RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG" title="Yes" value="Y" style="absmiddle" tagHelper="RadioTagHelper" visible="true" titleLC="LC53" maxlength="1" />
                <feba2:caption id="Caption8996808" text="Yes" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC53" />
                <feba2:field checked="False" id="RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG" name="RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG" title="No" value="N" style="absmiddle" tagHelper="RadioTagHelper" visible="true" titleLC="LC54" maxlength="1" />
                <feba2:caption id="Caption3830571" text="No" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC54" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rq2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.SCHEME_ID" id="LabelForControl30889776" name="LabelForControl30889776" text="Scheme ID:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1902" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="Horizontal" name="compsite" visible="true">
                <feba2:field disable="true" id="RMUserMaintenanceCRUDFG.SCHEME_ID" name="RMUserMaintenanceCRUDFG.SCHEME_ID" title="Scheme ID" copyAllowed="true" pasteAllowed="true" readonly="false" richText="N" style="addressboxfixwidth" tagHelper="TextAreaTagHelper" visible="true" titleLC="LC1364" maxlength="256" />
                <feba2:field caption="Look Up" id="SCHEME_ID_LOOK_UP" name="SCHEME_ID_LOOK_UP" title="Scheme Id Lookup" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" visible="true" captionLC="LC1430" titleLC="LC1903" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rq3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption8404095" name="Caption8404095" text="Do Not Disturb Time(hh:mm):" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1904" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield align="Horizontal" name="compsite" visible="true">
                <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.DDT_FROM" id="LabelForControl24094395" name="LabelForControl24094395" style="searchsimpletext" text="Start Time:" displayColonAfterDateFormat="true" visible="true" textLC="LC994" />
                <feba2:field id="RMUserMaintenanceCRUDFG.DDT_FROM" key="CODE_TYPE=DNDT" name="RMUserMaintenanceCRUDFG.DDT_FROM" select="True" title="StartTime" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1905" maxlength="2" />
                <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.DDT_TO" id="LabelForControl13618606" name="LabelForControl13618606" style="searchsimpletext" text="End Time:" displayColonAfterDateFormat="true" visible="true" textLC="LC997" />
                <feba2:field id="RMUserMaintenanceCRUDFG.DDT_TO" key="CODE_TYPE=DNDT" name="RMUserMaintenanceCRUDFG.DDT_TO" select="True" title="EndTime" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1906" maxlength="2" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C6" style="">
              <feba2:field caption="Continue" function="onclick=&quot;goToLink('usermaintenance/CorporateUserPreviewConfirmation.ppdl')&quot;" id="CONTINUE" name="CONTINUE" style="formbtn_last" title="Continue" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" visible="true" captionLC="LC62" titleLC="LC62" />
            </feba2:Col>
            <feba2:Col identifier="C7" style="">
              <feba2:field caption="Reset" id="ResetButton594870" style="formbtn_last" title="Reset" disable="false" name="Reset" tagHelper="ButtonTagHelper" visible="true" captionLC="LC2" titleLC="LC2" />
            </feba2:Col>
            <feba2:Col identifier="C8" style="">
              <feba2:field caption="Back" id="BACK_TO_RETRIEVE_LIST" name="BACK_TO_RETRIEVE_LIST" style="formbtn_last" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" visible="true" captionLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:hidden id="inparam" name="InquiryFWFG.LOOK_UP_INPARAMS__" value="CALL_BACK=InquiryFWFG.LOOKUP_RETURN__,InquiryFWFG.INQID=INQIDFORBRANCH,InquiryFWFG.BANK_ID=BANK_ID,BRANCH_ID_LOOKUP_FLAG=BRANCH_ID_LOOKUP_FLAG,InquiryFWFG.DEL_MULTIPLE=DELETEMULTIPLE_FORBRANCH" />
  <feba2:hidden id="outparam" name="InquiryFWFG.LOOK_UP_OUTPARAMS__" value="RMUserMaintenanceCRUDFG.BRANCH_ID=BRANCH_ID" />
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="RMUserMaintenanceCRUDFG" />
  <feba2:hidden name="RMUserMaintenanceCRUDFG.AMT_FMT_CD" value="1" />
  <feba2:hidden name="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" value="N" />
 <feba2:hidden name="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" value="N" />
 <feba2:hidden name="RMUserMaintenanceCRUDFG.REPORTTITLE" id="RMUserMaintenanceCRUDFG.REPORTTITLE" value="FBAUserMaintenanceCRUD" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

