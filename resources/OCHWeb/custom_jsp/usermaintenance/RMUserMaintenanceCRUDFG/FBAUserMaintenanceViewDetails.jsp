<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="True" var="DispForm.SubSection2.visible"></feba2:Set>
<feba2:Set value="True" var="DispForm.SubSection4.visible"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.USER_TYPE', 'FormField', pageContext)}" var="userType"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.IS_CHANNEL_LIST_VISIBLE', 'FormField', pageContext)}" var="isChannelListVisible"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'1')}" var="isRet"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'2')}" var="isCorp"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'4')}" var="isBnkUsr"></feba2:Set>
<feba2:Set value="${feba2:equals(isChannelListVisible,'Y')}" var="channelVisible"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.VIEW_MODE', 'FormField', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.CALL_MODE', 'FormField', pageContext)}" var="callMode"></feba2:Set>
<feba2:Set value="${feba2:equals(callMode,'5') or feba2:equals(callMode,'6') or feba2:equals(callMode,'7') or feba2:equals(callMode,'8') or feba2:equals(callMode,'9')}" var="isOption"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'APPROVALS')}" var="isApproval"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'CONF')}" var="isConf"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'VIEW_DETAILS')}" var="viewDetails"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'CONF') or feba2:equalsIgnoreCase(viewMode,'APPROVALS')}" var="isConfOrIsApprovals"></feba2:Set>
<feba2:Set value="${!viewDetails}" var="Authorization.visible"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.C_CNTRY', 'FormField', pageContext)}" var="country"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.SEL_USER_ID','formfield', pageContext)}" var="userIdTemp"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.SEL_CORP_ID','formfield', pageContext)}" var="corpIdTemp"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.USER_TYPE_FLAG','formfield', pageContext)}" var="userTypeTemp"></feba2:Set>
<feba2:Set value="${feba2:getValue('RMUserMaintenanceCRUDFG.IS_CRP_SYSTEM','formfield', pageContext)}" var="crpSystemParam"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(crpSystemParam,'Y')}" var="isCRPSystem"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('isConfOrIsApprovals#BankUserPreviewConf@@isRet#RetailUserMaintenance@@isCorp#CorporateUserMaintenance@@isBnkUsr#BankUserMaintenance',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('isConfOrIsApprovals#Preview Confirmation Details||LC64@@!isConf#View Details||LC255',pageContext)}"/>
<feba2:Page action="Finacle" name="RMUserMaintenanceCRUDFG" forcontrolIDs="Download Details As=RMUserMaintenanceCRUDFG.OUTFORMAT@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (!pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%}%>
<%@include file="/jsp/sidebar.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%} %>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar16082044" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="arrow" id="Image9349517" name="Image9349517" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC1788" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="title26742489" text="${feba2:select('isConfOrIsApprovals#Preview Confirmation Details||LC64@@!isConf#View Details||LC255',pageContext)}" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay21624724" style="" />
  <feba2:Section id="NavMenu" displaymode="N" identifier="NavigationMenuControl" style="greybottomleft">
    <feba2:SubSectionSet identifier="SubSectionSet1" style="greybottomright">
      <feba2:SubSection identifier="SubSection1" style="greytopleft">
        <feba2:rowset identifier="Rowset1" style="greytopright">
          <feba2:row identifier="Ra1" style="greybg_margin">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Option" id="RMUserMaintenanceCRUDFG.OPTION_ID" name="RMUserMaintenanceCRUDFG.OPTION_ID" visible="${isOption}" buttonCaption="OK" tagHelper="AccessControlComboBoxTagHelper" captionLC="LC31" buttonCaptionLC="LC33" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${!isBnkUsr}" var="DispForm.SubSection5.visible" />
  <feba2:Section id="DispForm" identifier="DisplayForm" style="section_blackborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:SubSectHeader identifier="Header1">
          <feba2:caption id="Caption7210546" text="User Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1591" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="Bank Id:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.BANK_ID" name="RMUserMaintenanceCRUDFG.BANK_ID" readonly="readonly" title="Bank Id" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" maxlength="11" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756933" name="Caption23756933" text="Bank User Type:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1478" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.RM_USER_TYPE" key="CODE_TYPE=BUT" name="RMUserMaintenanceCRUDFG.RM_USER_TYPE" title="Bank User Type" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC839" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2224752" name="Caption2224752" text="Corporate Id:" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1800" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.BAY_USER_ID" name="RMUserMaintenanceCRUDFG.BAY_USER_ID" readonly="readonly" title="Corporate Id" visible="${isCorp}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1801" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption24776745" name="Caption24776745" text="Corporate Type:" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1802" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CORP_USER_TYPE" name="RMUserMaintenanceCRUDFG.CORP_USER_TYPE" readonly="readonly" title="Corporate Type" visible="${isCorp}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1803" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption11802215" name="Caption11802215" text="CIF ID:" visible="${isRet}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1804" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CUST_ID" name="RMUserMaintenanceCRUDFG.CUST_ID" readonly="readonly" title="CIF ID" visible="${isRet}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1805" maxlength="64" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4708401" name="Caption4708401" text="Branch Id:" title="Branch Id" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1798" titleLC="LC1799" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.BRANCH_ID" name="RMUserMaintenanceCRUDFG.BRANCH_ID" visible="${isCRPSystem}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption16490224" name="Caption16490224" text="${feba2:select('isCorp#Corporate User Id:||LC1807@@isRet#Retail User Id:||LC1806@@isBnkUsr#Bank User Id:||LC1808',pageContext)}" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CORP_USER" name="RMUserMaintenanceCRUDFG.CORP_USER" readonly="readonly" title="Corporate User" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1941" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption32224164" name="Caption32224164" text="First Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC877" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_F_NAME" name="RMUserMaintenanceCRUDFG.C_F_NAME" readonly="readonly" title="First Name" wrap="65" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC833" maxlength="100" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption9652274" name="Caption9652274" text="Middle Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1816" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_M_NAME" name="RMUserMaintenanceCRUDFG.C_M_NAME" readonly="readonly" title="Middle Name" wrap="65" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1817" maxlength="100" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption10097866" name="Caption10097866" text="Last Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC878" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_L_NAME" name="RMUserMaintenanceCRUDFG.C_L_NAME" readonly="readonly" title="Last Name" wrap="65" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC834" maxlength="100" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption28065794" name="Caption28065794" text="Salutation:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1818" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.SALUTATION" key="CODE_TYPE=DSAL" name="RMUserMaintenanceCRUDFG.SALUTATION" title="Salutation" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1819" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2163768" text="Nick Name:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1942" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="OutputTextbox5727849" name="RMUserMaintenanceCRUDFG.NICKNAME" visible="${!isBnkUsr}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" maxlength="100" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra16" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption14701940" name="Caption14701940" text="Category Code:" visible="${isRet}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1820" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CATEGORY_CODE" key="CODE_TYPE=CAT" name="RMUserMaintenanceCRUDFG.CATEGORY_CODE" title="Category Code" visible="${isRet}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1821" maxlength="8" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra18" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption10905353" name="Caption10905353" text="Location:" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1325" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.LOCATION" name="RMUserMaintenanceCRUDFG.LOCATION" readonly="readonly" title="Location" visible="${isCorp}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1330" maxlength="64" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra19" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption22798505" name="Caption22798505" text="Location Access Indicator:" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1822" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.LOC_ACC_IND" name="RMUserMaintenanceCRUDFG.LOC_ACC_IND" readonly="readonly" title="Location Access Indicator" visible="${isCorp}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1943" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra20" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption5519578" name="Caption5519578" text="Division Access Indicator:" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1825" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.DIV_ACC_IND" name="RMUserMaintenanceCRUDFG.DIV_ACC_IND" readonly="readonly" title="Division Access Indicator" visible="${isCorp}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1944" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra21" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption12267584" name="Caption12267584" text="Amount Format:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1348" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AMT_FMT_CD" key="CODE_TYPE=AFT" name="RMUserMaintenanceCRUDFG.AMT_FMT_CD" title="Amount Format" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1347" maxlength="2" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra22" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption19962650" text="Calendar Type:" visible="true" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1337" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CAL_TYPE" key="CODE_TYPE=CALT" name="RMUserMaintenanceCRUDFG.CAL_TYPE" title="Calendar Type" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1336" maxlength="35" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra23" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption20066692" name="Caption20066692" text="Date Format:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1339" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.DT_FMT" key="CODE_TYPE=DFT" name="RMUserMaintenanceCRUDFG.DT_FMT" title="Date Format" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1338" maxlength="30" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra24" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption18913010" name="Caption18913010" text="Language Id:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1945" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.LANG_ID" key="CODE_TYPE=LANG" name="RMUserMaintenanceCRUDFG.LANG_ID" title="Language Id" visible="True" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1826" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra25" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption14416207" name="Caption14416207" text="Administrator Id:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1827" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.ADM_ID" key="CODE_TYPE=RMRL" name="RMUserMaintenanceCRUDFG.ADM_ID" title="Administrator Id" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1828" maxlength="32" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset identifier="Rowset2" style="width100percent">
          <feba2:row identifier="Rb2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption13822766" name="Caption13822766" text="Primary Division Id:" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1946" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.P_DIV_ID" name="RMUserMaintenanceCRUDFG.P_DIV_ID" readonly="readonly" title="Primary Division Id" visible="${isCorp}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1830" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption16327781" name="Caption16327781" text="Enabled For Transaction:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1947" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.TRANSACTION_ALLOWED" name="RMUserMaintenanceCRUDFG.TRANSACTION_ALLOWED" readonly="readonly" title="Enabled for transaction" visible="${!isBnkUsr}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1948" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption27717998" name="Caption27717998" text="Multi Currency Transaction Allowed:" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1831" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED" name="RMUserMaintenanceCRUDFG.MULTI_CRN_TXN_ALLOWED" readonly="readonly" title="Multi currenct transaction allowed" visible="${isCRPSystem}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1949" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption29679109" name="Caption29679109" text="Workflow Rule Authority:" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1832" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH" name="RMUserMaintenanceCRUDFG.WF_RULE_SELECT_AUTH" readonly="readonly" title="Workflow rule authority" visible="${isCRPSystem}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1950" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption15772901" name="Caption15772901" text="Can Authorize Without Rules:" visible="${isCRPSystem}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1833" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AUTH_USER" name="RMUserMaintenanceCRUDFG.AUTH_USER" readonly="readonly" size="10" title="Can Authorize without rules" visible="${isCRPSystem}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1951" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption15799556" name="Caption15799556" text="Access Scheme:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1081" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.ACCESS_SCHEME" key="CODE_TYPE=CAMS" name="RMUserMaintenanceCRUDFG.ACCESS_SCHEME" title="Access Scheme" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1092" maxlength="56" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption15799538" name="Caption15799538" text="Entry Limit Scheme:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1834" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.RANGE_LIMIT_SCHEME" key="CODE_TYPE=RLS" name="RMUserMaintenanceCRUDFG.RANGE_LIMIT_SCHEME" title="Entry Limit Scheme" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1835" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption17331730" name="Caption17331730" text="Transaction Limit Scheme:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1836" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.LIMIT_SCHEME" key="CODE_TYPE=RLS" name="RMUserMaintenanceCRUDFG.LIMIT_SCHEME" title="Transaction Limit Scheme" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1837" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption31445419" name="Caption31445419" text="Transaction Authorization Scheme:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1838" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.TRAN_AUTH_SCHEME" key="CODE_TYPE=ASC" name="RMUserMaintenanceCRUDFG.TRAN_AUTH_SCHEME" title="Transaction Authorization Scheme" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1839" maxlength="56" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption31445420" name="Caption31445420" text="Authentication Scheme:" visible="${!isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC101" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AUTHENTICATION_SCHEME" key="CODE_TYPE=CAUS" name="RMUserMaintenanceCRUDFG.AUTHENTICATION_SCHEME" title="Authentication Scheme" visible="${!isCorp}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC102" maxlength="56" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb14" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption18997272" name="Caption18997272" text="Page Scheme:" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1840" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PAGE_SCHEME" key="CODE_TYPE=PGS" name="RMUserMaintenanceCRUDFG.PAGE_SCHEME" title="Page Scheme" visible="${isCorp}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1841" maxlength="100" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rb15" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4756369" name="Caption4756369" text="Authentication Mode:" visible="true" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC103" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.BANK_AUTH_MODE" key="CODE_TYPE=ATN" name="RMUserMaintenanceCRUDFG.BANK_AUTH_MODE" title="Authentication Mode" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC104" maxlength="4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset identifier="Rowset3" style="width100percent">
          <feba2:row identifier="Rc1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption25522165" name="Caption25522165" text="Secondary Authentication Mode:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1842" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.SECONDARY_AUTHENTICATION_MODE" key="CODE_TYPE=ATN" name="RMUserMaintenanceCRUDFG.SECONDARY_AUTHENTICATION_MODE" title="Secondary Authentication Mode" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1952" maxlength="4" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption19059543" name="Caption19059543" text="Authorization Mode:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1844" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.AUTHORIZATION_MODE" key="CODE_TYPE=AUT" name="RMUserMaintenanceCRUDFG.AUTHORIZATION_MODE" title="Authorization Mode" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1845" maxlength="4" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption1063683" name="Caption1063683" text="List of Channels associated with the User:" visible="${channelVisible}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1953" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CHANNEL_IDS" name="RMUserMaintenanceCRUDFG.CHANNEL_IDS" visible="${channelVisible}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption1063638" name="Caption1063638" text="Accessible Financial Transaction Types:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1847" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.TXN_TYPES" name="RMUserMaintenanceCRUDFG.TXN_TYPES" visible="${!isBnkUsr}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption5755479" name="Caption5755479" text="Authorization Mode Precedence:" visible="${isRet}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1849" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.RET_AUTH_MODE_PREC" key="CODE_TYPE=AMP" name="RMUserMaintenanceCRUDFG.RET_AUTH_MODE_PREC" title="Authorization Mode Precedence" visible="${isRet}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1850" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption10595277" name="Caption10595277" text="Customer Assist Menu Profile:" visible="false" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1851" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CUST_ASST_MENU_PRF" name="RMUserMaintenanceCRUDFG.CUST_ASST_MENU_PRF" readonly="readonly" visible="false" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" maxlength="8" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption10595278" name="Caption10595278" text="Virtual Login Allowed:" visible="false" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1853" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" key="CODE_TYPE=YNF" name="RMUserMaintenanceCRUDFG.VIRTUAL_LOGIN_ALLOWED" visible="false" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="LabelForControl33551069" name="LabelForControl33551069" text="2FA Registration Required:" title="2FA Registration Required:" isDownloadLink="false" isMenu="false" style="simpletext" visible="false" textLC="LC1954" titleLC="LC1954" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" key="CODE_TYPE=YNF" name="RMUserMaintenanceCRUDFG.TFA_REGISTRATION_REQUIRED" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" visible="false" tagHelper="GenericDescriptionTagHelper" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption16420511" name="Caption16420511" text="Account Format:" title="Account Format" visible="${isCorp}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1362" titleLC="LC1363" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.ACC_FMT" key="CODE_TYPE=ACFT" name="RMUserMaintenanceCRUDFG.ACC_FMT" title="Account Format" visible="${isCorp}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1363" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption3546540" name="Caption3546540" text="Photo:" visible="${(viewDetails or isBnkApproval) and isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1855" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field fileType="image" height="50" id="RM_PHOTO" name="RM_PHOTO" placeHolderId="RM_PHOTO|${corpIdTemp}|${userIdTemp}" title="RM_PHOTO" visible="${(viewDetails or isBnkApproval) and isBnkUsr}" width="50" isMenu="false" tagHelper="ViewCMSContentTagHelper" titleLC="LC1955" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rc11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption30811903" name="Caption30811903" text="Photo:" visible="${isConf and isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1855" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.FILE_PATH" name="RMUserMaintenanceCRUDFG.FILE_PATH" visible="${isConf and isBnkUsr}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" maxlength="255" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:SubSectHeader identifier="Header2">
          <feba2:caption id="Caption21529100" text="Personal Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1294" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption26522547" name="Caption26522547" text="Date Of Birth:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1858" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.DATE_OF_BIRTH" name="RMUserMaintenanceCRUDFG.DATE_OF_BIRTH" readonly="readonly" title="Date of Birth" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1859" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption20751912" name="Caption20751912" text="Number Of Household Members:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1860" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.NUM_HOUSEHOLD" name="RMUserMaintenanceCRUDFG.NUM_HOUSEHOLD" readonly="readonly" title="Number of household members" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1956" maxlength="2" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption7603710" name="Caption7603710" text="Customer Status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1862" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CUST_STATUS" key="CODE_TYPE=CST" name="RMUserMaintenanceCRUDFG.CUST_STATUS" title="Customer Status" visible="True" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1863" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption21659239" name="Caption21659239" text="Gender:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1864" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_GENDER" name="RMUserMaintenanceCRUDFG.C_GENDER" readonly="readonly" title="Gender" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1865" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption7319028" name="Caption7319028" text="Anniversary Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1866" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.ANNIVERSARY_DATE" name="RMUserMaintenanceCRUDFG.ANNIVERSARY_DATE" readonly="readonly" title="Anniversary date" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1957" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption13110503" name="Caption13110503" text="Occupation:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1868" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.OCCUPATION" key="CODE_TYPE=OCP" name="RMUserMaintenanceCRUDFG.OCCUPATION" title="Occupation" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1869" maxlength="5" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption14128594" name="Caption14128594" text="Marital Status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1870" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.MARITAL_STATUS" key="CODE_TYPE=MST" name="RMUserMaintenanceCRUDFG.MARITAL_STATUS" title="Marital Status" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1871" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption12266135" name="Caption12266135" text="Income Range:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1872" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.INC_RNG_CD" key="CODE_TYPE=INR" name="RMUserMaintenanceCRUDFG.INC_RNG_CD" title="Income Range" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1873" maxlength="2" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption17726175" name="Caption17726175" text="Residential Status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1874" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_RES_STATUS" key="CODE_TYPE=RSC" name="RMUserMaintenanceCRUDFG.C_RES_STATUS" title="Residential Status" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1875" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption32456294" name="Caption32456294" text="Educational Level:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1876" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.EDUCATIONAL_LEVEL" key="CODE_TYPE=EDL" name="RMUserMaintenanceCRUDFG.EDUCATIONAL_LEVEL" title="Educational Level" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1877" maxlength="3" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection3" style="width100percent">
        <feba2:SubSectHeader identifier="Header3">
          <feba2:caption id="Caption25108396" text="Contact Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1303" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset9" style="width100percent">
          <feba2:row identifier="Ri1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23284554" name="Caption23284554" text="Address (Line 1):" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1878" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ADDR1" name="RMUserMaintenanceCRUDFG.C_ADDR1" readonly="readonly" title="Address (Line 1)" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1958" maxlength="80" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Address (Line 2):" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ADDR2" name="RMUserMaintenanceCRUDFG.C_ADDR2" readonly="readonly" title="Address (Line 2)" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" maxlength="80" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption929017" name="Caption929017" text="Address (Line 3):" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1882" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ADDR3" name="RMUserMaintenanceCRUDFG.C_ADDR3" readonly="readonly" title="Address (Line 3)" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1883" maxlength="80" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2034817" name="Caption2034817" text="Address Type:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1884" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.CORRESPONDENCE_ADDRESS" key="CODE_TYPE=CRA" name="RMUserMaintenanceCRUDFG.CORRESPONDENCE_ADDRESS" title="Address Type" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1885" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption19410968" name="Caption19410968" text="City:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC164" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_CITY" name="RMUserMaintenanceCRUDFG.C_CITY" readonly="readonly" title="City" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC165" maxlength="50" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption27456842" name="Caption27456842" text="State:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC168" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="OutputTextbox12627485" name="OutputTextbox12627485" readonly="readonly" title="State" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC169" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption29919079" name="Caption29919079" text="Country:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC166" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_CNTRY" key="CODE_TYPE=CNT" name="RMUserMaintenanceCRUDFG.C_CNTRY" title="Country" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC167" maxlength="5" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption24499804" name="Caption24499804" text="Zip Code:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC204" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_ZIP" name="RMUserMaintenanceCRUDFG.C_ZIP" readonly="readonly" title="Zip Code" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC171" maxlength="10" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption9104928" name="Caption9104928" text="Phone Number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC174" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_PHONE_NO" name="RMUserMaintenanceCRUDFG.C_PHONE_NO" readonly="readonly" title="Phone Number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC177" maxlength="20" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri14" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption16583035" name="Caption16583035" text="Mobile Phone Number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1886" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_M_PHONE_NO" name="RMUserMaintenanceCRUDFG.C_M_PHONE_NO" readonly="readonly" title="Mobile Phone Number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1887" maxlength="20" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri15" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption5683070" name="Caption5683070" text="Fax Number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC178" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_FAX_NO" name="RMUserMaintenanceCRUDFG.C_FAX_NO" readonly="readonly" title="Fax Number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC179" maxlength="35" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri16" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption19222596" name="Caption19222596" text="Email Id:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1959" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.C_EMAIL_ID" name="RMUserMaintenanceCRUDFG.C_EMAIL_ID" readonly="readonly" title="Email Id" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1888" maxlength="80" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection4" style="width100percent">
        <feba2:SubSectHeader identifier="Header4">
          <feba2:caption id="Caption7024328" text="Other Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1889" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset13" style="width100percent">
          <feba2:row identifier="Rm1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption28405560" name="Caption28405560" text="Passport Number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1890" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PASSPORT_NUMBER" name="RMUserMaintenanceCRUDFG.PASSPORT_NUMBER" readonly="readonly" size="10" title="Passport Number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1891" maxlength="20" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Passport Issue Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PASSPORT_ISSUE_DATE" name="RMUserMaintenanceCRUDFG.PASSPORT_ISSUE_DATE" readonly="readonly" size="10" title="Passport Issue Date" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1893" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption19140156" name="Caption19140156" text="Passport Details:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1894" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PASSPORT_DETAILS" name="RMUserMaintenanceCRUDFG.PASSPORT_DETAILS" readonly="readonly" size="10" title="Passport Details" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1895" maxlength="30" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption8523803" name="Caption8523803" text="Passport Expiry Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1896" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PASSPORT_EXPIRY_DATE" name="RMUserMaintenanceCRUDFG.PASSPORT_EXPIRY_DATE" readonly="readonly" size="10" title="Passport expiry date" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1960" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption20234404" name="Caption20234404" text="Permanent Account Number/ National Id:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1961" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.PAN_NATIONAL_ID" name="RMUserMaintenanceCRUDFG.PAN_NATIONAL_ID" readonly="readonly" title="Permanent Account Number/ National Id" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1962" maxlength="20" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection5" style="width100percent">
        <feba2:SubSectHeader identifier="Header5">
          <feba2:caption id="Caption31646245" text="Register For Alerts" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1963" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset17" style="width100percent">
          <feba2:row identifier="Rq1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption7737221" name="Caption7737221" text="Register For Alerts:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1901" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG" name="RMUserMaintenanceCRUDFG.USER_ALERT_REGISTRATION_FLAG" readonly="readonly" title="Register For Alerts" visible="${!isBnkUsr}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1963" maxlength="1" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rq2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption1287582" name="Caption1287582" text="Scheme Id:" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1964" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.SCHEME_ID" name="RMUserMaintenanceCRUDFG.SCHEME_ID" readonly="readonly" title="Scheme Id" visible="${!isBnkUsr}" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1965" maxlength="256" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rq3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption16369394" name="Caption16369394" text="Do Not Disturb Start Time(hh:mm):" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1966" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.DDT_FROM" key="CODE_TYPE=DNDT" name="RMUserMaintenanceCRUDFG.DDT_FROM" title="Do Not Disturb Start Time(hh:mm)" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1967" maxlength="2" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rq4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption3102952" name="Caption3102952" text="Do Not Disturb End Time(hh:mm):" visible="${!isBnkUsr}" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1968" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="RMUserMaintenanceCRUDFG.DDT_TO" key="CODE_TYPE=DNDT" name="RMUserMaintenanceCRUDFG.DDT_TO" title="Do Not Disturb End Time(hh:mm)" visible="${!isBnkUsr}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" titleLC="LC1969" maxlength="2" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="Authorization" displaymode="N" identifier="InputForm_Authorization" style="section_grayborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="ApproverDetails18837206" name="ApproverDetails18837206" title="ApproverDetails" visible="${isConfOrIsApprovals}" hideAdditionalDetails="false" isRemarksRequired="true" tagHelper="ApproversDetailsTagHelper">
                <feba2:map>
                  <feba2:param name="isMandatory" value="false" />
                  <feba2:param name="isConfidential" value="N" />
                </feba2:map>
              </feba2:field>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="Authorization26807469" name="Authorization26807469" title="Authorization" visible="${isConfOrIsApprovals}" tagHelper="AuthenticationTagHelper" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C7" style="">
              <feba2:field caption="Submit" function="onclick=&quot;goToLink('usermaintenance/UserMaintenanceViewDetails.ppdl')&quot;" id="USER_SUBMIT" name="USER_SUBMIT" style="formbtn_last" visible="${isConfOrIsApprovals}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC21" />
            </feba2:Col>
            <feba2:Col identifier="C8" style="">
              <feba2:compositefield align="horizontal" name="DownloadComponent" visible="true">
                <feba2:labelforcontrol forControlId="RMUserMaintenanceCRUDFG.OUTFORMAT" id="LabelForControl21895092" name="LabelForControl21895092" visible="${viewDetails}" displayColonAfterDateFormat="true" style="searchsimpletext" text="Download Details As" textLC="LC39" />
                <feba2:field id="RMUserMaintenanceCRUDFG.OUTFORMAT" name="RMUserMaintenanceCRUDFG.OUTFORMAT" visible="${viewDetails}" all="false" combomode="simple" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" key="CODE_TYPE=FLFT" preSelectValue="5" preselect="true" select="false" style="dropdownexpandalbe_download" tagHelper="ComboBoxTagHelper" title="Select Download Format" titleLC="LC40" />
                <feba2:field id="Button32181941" visible="${viewDetails}" caption="OK" disable="false" function="onclick=&quot;sendAlert();formResourceAction();&quot;" isDownloadAction="false" name="GENERATE_REPORT" style="formbtn_drpdwn" tagHelper="ButtonTagHelper" captionLC="LC33" />
              </feba2:compositefield>
            </feba2:Col>
            <feba2:Col identifier="C9" style="">
              <feba2:field caption="Back" id="BACK" name="BACK" style="formbtn_last" visible="${!isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>
            <feba2:Col identifier="C11" style="">
              <feba2:field caption="Back" id="BACK_TO_CREATE" name="BACK_TO_CREATE" style="formbtn_last" visible="${isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="RMUserMaintenanceCRUDFG" />
 <feba2:hidden name="RMUserMaintenanceCRUDFG.REPORTTITLE" id="RMUserMaintenanceCRUDFG.REPORTTITLE" value="FBAUserMaintenanceViewDetails" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

