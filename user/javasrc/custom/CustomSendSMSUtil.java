/**
 * CustomSendSMSUtil.java
 * @since Sep 11, 2018 - 12:46:39 PM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited,
 * Electronic City, Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */

package com.infosys.custom.ebanking.user.custom;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomSmsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.VOFactory;
import com.infosys.feba.framework.cache.AppDataConstants;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.DescriptionMedium;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;
import com.infosys.fentbase.types.primitives.OTP;
import com.infosys.fentbase.types.valueobjects.AuthenticationVO;
import com.infosys.fentbase.types.valueobjects.IAuthenticationVO;
import com.infosys.fentbase.types.valueobjects.ILoginAltFlowVO;
import com.infosys.fentbase.types.valueobjects.LoginAltFlowVO;

/**
 * @author Tarun_Kumar18
 *
 */
public final class CustomSendSMSUtil {

	public void sendSMS(FEBATransactionContext txnContext, Object objInputOutput)
			throws BusinessException, BusinessConfirmation, CriticalException {

		OTP smsOTP = null;
		CustomSmsDetailsVO smsDetailsVO = (CustomSmsDetailsVO) VOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomSmsDetailsVO);

		if (objInputOutput instanceof IAuthenticationVO) {
			AuthenticationVO authenticationVO = (AuthenticationVO) objInputOutput;
			smsOTP = authenticationVO.getCredentials().getSmsOTP();
			smsDetailsVO.setDebitAccount("");
			smsDetailsVO.setMessage(getMessage(txnContext, smsOTP, CustomEBConstants.LOGIN_SMS));
			smsDetailsVO.setMobileNumber(new FEBAUnboundString(authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().getUserPrincipal().toString()));
		} else if (objInputOutput instanceof ILoginAltFlowVO) {
			LoginAltFlowVO loginAltFlowVO = (LoginAltFlowVO) objInputOutput;
			smsOTP = loginAltFlowVO.getLoginAltFlowUserDetailsVO().getOtp();
			smsDetailsVO.setDebitAccount("");
			smsDetailsVO.setMessage(getMessage(txnContext, smsOTP, CustomEBConstants.REGISTRATION_SMS));
			smsDetailsVO.setMobileNumber(loginAltFlowVO.getForgotPasswordOfflineVO().getMobileNumber().getValue());
		}
		EBHostInvoker.processRequest(txnContext, CustomEBRequestConstants.CUSTOM_SEND_SMS, smsDetailsVO);
	}

	private String getMessage(FEBATransactionContext febaTxnContext, OTP smsOTP, String strId) {

		DescriptionMedium strDesc = new DescriptionMedium("");
		try {
			IFEBAType returnedValue = AppDataManager.getValue(febaTxnContext, FBAAppDataConstants.STRINGCONSTANTS_CACHE,
					FBAAppDataConstants.COLUMN_STR_ID + AppDataConstants.EQUALS + strId);
			if (null != returnedValue) {
				strDesc = (DescriptionMedium) returnedValue;
			}
		} catch (CriticalException e) {
			strDesc.setValue("");
		}
		return strDesc.toString().replace("<OTP>", smsOTP.toString());
	}

}
