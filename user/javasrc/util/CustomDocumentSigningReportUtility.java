package com.infosys.custom.ebanking.user.util;

import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.reports.constants.ReportConstants;
import com.infosys.feba.framework.reports.core.IFEBAReportingEngine;
import com.infosys.feba.framework.reports.util.ReportGenerator;
import com.infosys.feba.framework.security.Base64EncoderDecoder;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.NVMapVO;
import com.infosys.feba.framework.types.valueobjects.ReportInputOutputVO;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;


public class CustomDocumentSigningReportUtility {

	private static StringBuilder columnMappingFields;
	private boolean isSaveReport = false;

	/**
	 * 
	 * This constructor instantiates the columnMappingFields, which is used to
	 * hold the fields to be shown in the report.
	 * 
	 * @author arshadiqbal_ansari
	 * @since 20136:56:17 PM
	 */
	public CustomDocumentSigningReportUtility() {

		columnMappingFields = new StringBuilder();
	}

	/**
	 * Used to generate the output stream for the report.
	 * 
	 * @author arshadiqbal_ansari
	 * @param objContext
	 * @param reportName
	 * @param reportData
	 * @param trayId
	 * @param reportGeneratorUtil
	 * @return FEBABinary
	 * @throws BusinessException
	 * @throws CriticalException
	 * @throws BusinessConfirmation
	 */
	public FEBABinary generateReport(FEBATransactionContext objContext,
			String reportName, FEBAArrayList reportData,
			String printDirectlyToPrinter, String printOnly,
			String printerLocation, String trayId)

	throws BusinessException, CriticalException, BusinessConfirmation {

		/*
		 * populate the ReportInputOutputVO, which is the input to the jasper
		 * reporting engine.
		 */
		
		ReportInputOutputVO reportInOutVO = getReportInputOutputVO(reportName,
				reportData, printDirectlyToPrinter, printOnly, printerLocation);
		
		
		
		// invoke method to form the column mapping
		reportInOutVO.setColumnMapping(defineReportFieldsMapping(objContext,
				reportInOutVO));
		
		

		// validate the ReportInputOutputVO
		validateReportInOutVO(objContext, reportInOutVO);

		// call the reporting engine to get the binary report data
		IFEBAReportingEngine reportingEngine = new ReportGenerator()
				.getEngine(objContext);

		// invoke the method, which converts the input list to binary data
		if (isSaveReport()) {
			reportingEngine.saveReport(reportInOutVO, objContext);
		} else {
			reportingEngine.getReport(reportInOutVO, objContext);
		}

		return reportInOutVO.getReportOutputStream();

	}

	/**
	 * Used to generate the output stream for the report.
	 * 
	 * @author arshadiqbal_ansari
	 * @param objContext
	 * @param reportName
	 * @param reportData
	 * @param reportGeneratorUtil
	 * @return FEBABinary
	 * @throws BusinessException
	 * @throws CriticalException
	 * @throws BusinessConfirmation
	 */
	public FEBABinary generateReportForNotification(
			FEBATransactionContext objContext, String reportName,
			FEBAArrayList reportData, String printDirectlyToPrinter,
			String printOnly, String reportSaveLocation,String trayId)

	throws CriticalException, BusinessConfirmation {

		/*
		 * populate the ReportInputOutputVO, which is the input to the jasper
		 * reporting engine.
		 */
		ReportInputOutputVO reportInOutVO = getReportInputOutputVO(reportName,
				reportData, printDirectlyToPrinter, printOnly,
				reportSaveLocation);

		// call the reporting engine to get the binary report data
		IFEBAReportingEngine reportingEngine = new ReportGenerator()
				.getEngine(objContext);


		reportingEngine.saveReport(reportInOutVO, objContext);

		return reportInOutVO.getReportOutputStream();

	}

	/**
	 * Used to get the Report InputOutput Vo.
	 * 
	 * @author arshadiqbal_ansari
	 * @param reportName
	 * @param reportData
	 * @param trayId
	 * @return ReportInputOutputVO
	 * @throws BusinessException
	 * @throws CriticalException
	 * @throws BusinessConfirmation
	 */
	public ReportInputOutputVO getReportInputOutputVO(String reportName,
			FEBAArrayList reportData, String printDirectlyToPrinter,
			String printOnly, String printerLocation)
			 {

		/*
		 * populate the ReportInputOutputVO, which is the input to the jasper
		 * reporting engine.
		 */
		ReportInputOutputVO reportInOutVO = new ReportInputOutputVO();

		// set the report name, which specifies the jasper file name for report
		// generation
		reportInOutVO.setReportName(reportName);

		// set the report name
		reportInOutVO.setReportTitle(reportName);

		// set the report data for downloaded file content
		reportInOutVO.setReportDataList(reportData);

		// set the output file format as PDF
		reportInOutVO.setOutputFormat(ReportConstants.PDF);

		// set the condition for print Directly To Printer
		if (printDirectlyToPrinter != null)
			reportInOutVO.setPrintDirectlyToPrinter(printDirectlyToPrinter);

		// set the condition for print Only
		if (printOnly != null) {
			reportInOutVO.setPrintOnly(printOnly);
		}

		// set the Location for the output Files printed
		if (printerLocation != null) {
			reportInOutVO.setPrinterName(printerLocation);
		}

		// set the data source as Listing Report
		reportInOutVO.setReportDataSource(new FEBAUnboundInt(
				ReportConstants.LISTING_REPORT_DATASOURCE));

		return reportInOutVO;

	}

	/**
	 * This method verifies the field values of ReportInputOutputVO, before
	 * calling the Jasper report engine: It performs the following validations.
	 * if Report name is not set - throw an exception; if Result List is not set
	 * - throw an exception; sets the saveReportAsFile field value as "N"; if
	 * the output format is not set, assign the default value as "CSV"; set the
	 * default report as Listing Report, if the Report Source is not set.
	 * 
	 * 
	 * @param pObjContext
	 * @param pReportInOutVO
	 * @author Chandra_Kumar01
	 * @since Aug 29, 2011 - 10:19:46 AM
	 */
	private void validateReportInOutVO(FEBATransactionContext pObjContext,
			ReportInputOutputVO pReportInOutVO) {

		// check whether the Report name is given
		if (!FEBATypesUtility.isNotNullOrBlank(pReportInOutVO.getReportName())) {
			throw new FatalException(pObjContext,
					FBAIncidenceCodes.REPORT_NAME_NOT_SET,
					"Jasper Name not set for Report Generation",
					FBAErrorCodes.REPORT_NAME_NOT_SET);
		}

		// check whether the Report list is set
		if (!FEBATypesUtility.isSet(pReportInOutVO.getReportDataList())) {
			throw new FatalException(pObjContext,
					FBAIncidenceCodes.REPORT_DATA_NOT_SET,
					"No Data available for Report Generation",
					FBAErrorCodes.REPORT_DATA_NOT_SET);
		}

		pReportInOutVO.setSaveReportAsFile(FBAConstants.NO);

		// if output format is not set, then set default value as 'HTML'
		if (!FEBATypesUtility
				.isNotNullOrBlank(pReportInOutVO.getOutputFormat())) {
			pReportInOutVO.setOutputFormat(ReportConstants.HTML);
		}

		// Set the Report Data Source default value as LISTING_REPORT
		if (!FEBATypesUtility.isNotBlankInt(pReportInOutVO
				.getReportDataSource())) {
			pReportInOutVO.setReportDataSource(new FEBAUnboundInt(
					ReportConstants.LISTING_REPORT_DATASOURCE));
		}

	}

	/**
	 * 
	 * This method populates the columnMapping field in ReportInputOutputVO. The
	 * column names are fragmented by '^' symbol, and each column fields are
	 * added in to the NVMapVO. This VO will be added into an array list.
	 * Finally this resultant list will be set as the columnMapping field.
	 * 
	 * @param pObjContext
	 * @param reportInOutVO
	 * @return
	 * @throws BusinessException
	 * @author Chandra_Kumar01
	 * @since Sep 6, 2011 - 1:05:05 PM
	 */
	private FEBAArrayList defineReportFieldsMapping(
			FEBATransactionContext pObjContext,
			ReportInputOutputVO reportInOutVO) throws BusinessException {

		FEBAArrayList reportColumnList = new FEBAArrayList();
		NVMapVO nameValueVO = new NVMapVO();

		// get the "|" separated fields from an input record
		String pipeSeparatedString = columnMappingFields.toString();
		String[] columnFieldsToProcess = pipeSeparatedString.split(
				FBAConstants.PIPE_SEPERATOR, -1);

		int columnFieldSize = columnFieldsToProcess.length;

		// set the column mapping values to NVMapVO.
		for (int cloumnCount = 0; cloumnCount < columnFieldSize; cloumnCount++) {
			if (null != columnFieldsToProcess[cloumnCount]
					&& !columnFieldsToProcess[cloumnCount]
							.equals(FBAConstants.BLANK)) {
				nameValueVO
						.setSValue(columnFieldsToProcess[cloumnCount].trim());
				reportColumnList.add(nameValueVO);

			}
		}

		// check whether the report fields are set
		if (!FEBATypesUtility.isSet(reportColumnList)) {
			throw new FatalException(pObjContext,
					FBAIncidenceCodes.COLUMN_MAPPING_NOT_SET,
					"Column Mapping Not set for Report Generation",
					FBAErrorCodes.COLUMN_MAPPING_NOT_SET);

		}

		return reportColumnList;

	}

	/**
	 * 
	 * This method gets the report column Name, which will be set as the the
	 * value in NVMapVO. This value will be stored as the Column Mapping in
	 * ReportInputOutputVO. This method can be invoked as many times as the
	 * column mapping is required; this method will keep appending the column
	 * fields separated with '^' symbol.
	 * 
	 * @param columnName
	 * @author Chandra_Kumar01
	 * @since Aug 26, 2011 - 6:21:48 PM
	 */

	public void addFieldName(String columnName) {

		// append the field names with '|' symbol
		if (null != columnMappingFields
				&& !columnMappingFields.toString().equals(FBAConstants.BLANK)) {
			columnMappingFields.append(FBAConstants.SEPERATOR);
		}

		columnMappingFields.append(columnName);

	}

	/**
	 * @return the isSaveReport
	 */
	public boolean isSaveReport() {
		return isSaveReport;
	}

	/**
	 * @param isSaveReport
	 *            the isSaveReport to set
	 */
	public void setSaveReport(boolean isSaveReport) {
		this.isSaveReport = isSaveReport;
	}

	/**
	 * Method to Encode data stream
	 * 
	 * @author shweta_gandhi
	 * @param outputStream
	 * @return
	 * @return FEBAUnboundString
	 */
	public FEBAUnboundString getEncodedDataStream(FEBABinary outputStream) {
		return new FEBAUnboundString(Base64EncoderDecoder.encode(outputStream
				.getValue()));

	}
}
