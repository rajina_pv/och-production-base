package com.infosys.custom.ebanking.user.util;

import com.infosys.custom.ebanking.tao.CLHTTAO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.fentbase.types.primitives.Remarks;

public class CustomLoanHistoryUpdateUtil{
	
	public void updateHistory(ApplicationNumber applicationId,String applicationStatus,String remarks,FEBATransactionContext objTxnContext) throws BusinessException{
		EBTransactionContext ebContext = (EBTransactionContext) objTxnContext;
		CLHTTAO clhtTAO = new CLHTTAO(ebContext);
		try{
		clhtTAO.associateBankId(ebContext.getBankId());
		clhtTAO.associateApplicationId(applicationId);
		clhtTAO.associateApplicationStatus(new CommonCode(applicationStatus));
		clhtTAO.associateRemarks(new Remarks(remarks));
		clhtTAO.insert(ebContext);
		}catch (FEBATableOperatorException e) {
			LogManager.logError(null, e);
		}
		ebContext.getConnection().commit();
	}

	
}
