package com.infosys.custom.ebanking.user.service;







import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanOriginationCriteriaVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;



public class CustomLoanOriginationServiceFetchHistoryListImpl extends AbstractLocalListInquiryTran
{
	

	@Override
	protected void associateQueryParameters(FEBATransactionContext txnContext, IFEBAValueObject objQueryCrit,
			IFEBAValueObject objTxnWM, QueryOperator queryOperator) throws CriticalException {
		final EBTransactionContext eBankingTransactionContext = (EBTransactionContext) txnContext;
		final CustomLoanOriginationCriteriaVO criteriaVO = (CustomLoanOriginationCriteriaVO) objQueryCrit;
			
	
		queryOperator.associate("BANK_ID", eBankingTransactionContext.getBankId());
		queryOperator.associate("APPLICATION_ID", criteriaVO.getApplicationId());	
		// associates the query operator with search criteria fields
	
	}

	/**
	 *  This
	 * method prepares the validations list and pass to val engine for the
	 * validations of the same <BR>
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @return
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran#prepareValidationsList(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since Jan 5, 2011 - 6:56:24 PM
	 */
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		
		
		// instantiating DateRangeVO
		return new FEBAValItem[0];
		
	}

	/**
	 *
	 * sets the query identifier <BR>
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @return
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran#getQueryIdentifier(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since Jan 5, 2014 - 6:56:30 PM
	 */
	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {
		
		return CustomEBQueryIdentifiers.CUSTOM_FETCH_LOAN_ORIGINATION_HISTORY_LIST;
	}

	/**
	 * 
	 * this calls the execute query method.
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran#executeQuery(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since Jan 5, 2014 - 6:56:33 PM
	 */
	@Override
	protected void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		
		try {
			// calls execute query()
			super.executeQuery(objContext, objInputOutput, objTxnWM);
            //call process method to process the result list
     	
		}

		catch (BusinessException be) {
			// throws business exception
			// no record are fetched
				throw new BusinessException(objContext,
	                    EBIncidenceCodes.NO_RECORDS_FOUND,
	                    "No Records Found",
	                    EBankingErrorCodes.RECORDS_NOT_FOUND);
		
		}
	
	}

	
}
