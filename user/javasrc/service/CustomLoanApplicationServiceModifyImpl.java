package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;

public class CustomLoanApplicationServiceModifyImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		CustomLoanApplnMasterDetailsVO loanAppDetlsVO = (CustomLoanApplnMasterDetailsVO) objInputOutput;
		final CLATTAO clatTAO = new CLATTAO(ebContext);
		CLATInfo clatInfo = null;
		
		try {
			
			clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), loanAppDetlsVO.getApplicationId());
			clatTAO.associateBankId(clatInfo.getBankId());
			clatTAO.associateUserId(clatInfo.getUserId());
			clatTAO.associateApplicationId(loanAppDetlsVO.getApplicationId());

			if (!loanAppDetlsVO.getRequestedLoanAmt().toString().equals(EBankingConstants.NULL_AMOUNT)
					&& !loanAppDetlsVO.getRequestedLoanAmt().toString().contains("0.0")) {
				clatTAO.associateRequestedLoanAmt(loanAppDetlsVO.getRequestedLoanAmt());
			}
			if (!loanAppDetlsVO.getRequestedTenor().toString().equals("0")) {
				clatTAO.associateRequestedTenor(loanAppDetlsVO.getRequestedTenor());
			}
			if (!loanAppDetlsVO.getLoanIntRate().toString().equals("0.0")) {
				clatTAO.associateLoanIntRate(loanAppDetlsVO.getLoanIntRate());
			}
			if (!loanAppDetlsVO.getMonthlyInstallment().toString().equals(EBankingConstants.NULL_AMOUNT)
					&& !loanAppDetlsVO.getMonthlyInstallment().toString().contains("0.0")) {
				clatTAO.associateMonhtlyInstallment(loanAppDetlsVO.getMonthlyInstallment());
			}
			if (loanAppDetlsVO.getLegacyCif().toString() != null && loanAppDetlsVO.getLegacyCif().toString() != "") {
				clatTAO.associateLegacyCif(loanAppDetlsVO.getLegacyCif());
			}
			if (loanAppDetlsVO.getPrivyAgreeDate().toString() != null) {
				clatTAO.associatePrivyAgreeDate(loanAppDetlsVO.getPrivyAgreeDate());
			}
			if (loanAppDetlsVO.getProductTnCAgreeDate().toString() != null) {
				clatTAO.associateProductTncAgreeDate(loanAppDetlsVO.getProductTnCAgreeDate());
			}
			if (FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getApplicationStatus())) {
				clatTAO.associateApplicationStatus(loanAppDetlsVO.getApplicationStatus());
			}
			if (loanAppDetlsVO.getProductType().toString() != null
					&& loanAppDetlsVO.getProductType().toString() != "") {
				clatTAO.associateProductType(loanAppDetlsVO.getProductType());
			}
			if (loanAppDetlsVO.getProductCode().toString() != null
					&& loanAppDetlsVO.getProductCode().toString() != "") {
				clatTAO.associateProductCode(loanAppDetlsVO.getProductCode());
			}
			if (loanAppDetlsVO.getProductCategory().toString() != null
					&& loanAppDetlsVO.getProductCategory().toString() != "") {
				clatTAO.associateProductCategory(loanAppDetlsVO.getProductCategory());
			}
			if (loanAppDetlsVO.getProductSubCategory().toString() != null
					&& loanAppDetlsVO.getProductSubCategory().toString() != "") {
				clatTAO.associateProductSubcategory(loanAppDetlsVO.getProductSubCategory());
			}

			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getUid())){
				clatTAO.associateCreditScoreUid(loanAppDetlsVO.getUid());
			}
			
			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getStatusDesc())){
				clatTAO.associateStatusDesc(loanAppDetlsVO.getStatusDesc());
			}
			
			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getFeedback())){
				clatTAO.associateFeedbackStar(loanAppDetlsVO.getFeedback());
			}
			
			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getRemarks())){
				clatTAO.associateRemarks(loanAppDetlsVO.getRemarks());
			}
			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getIsKTPVerified())){
				clatTAO.associateIsKtpVerified(new FEBAUnboundChar(loanAppDetlsVO.getIsKTPVerified().toString()));
			}
			
			if(loanAppDetlsVO.getCrRespCode().toString() !=null && loanAppDetlsVO.getCrRespCode().toString() !=""){
				clatTAO.associateCrRespCode(loanAppDetlsVO.getCrRespCode());
			}
			
			// Added for KTP rejection case start
			if(clatInfo.getKtpRejCnt() !=null && clatInfo.getKtpRejCnt().toString() !=""){
				clatTAO.associateKtpRejCnt(clatInfo.getKtpRejCnt());
			}
			
			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getIsKtpReUploaded())){
				clatTAO.associateIsKtpReuploaded(new FEBAUnboundChar(loanAppDetlsVO.getIsKtpReUploaded().toString()));
			}
			// Added for KTP rejection case end
			
			
			// added for PINANG Pay later start 
			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getSchemeCode())){
				clatTAO.associateSchemeCode(loanAppDetlsVO.getSchemeCode());
			}
			if(FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getPartnerId())){
				clatTAO.associatePartnerId(loanAppDetlsVO.getPartnerId());
			}
			// added for PINANG Pay later end 
			
			clatTAO.associateCookie(clatInfo.getCookie());

			clatTAO.update(ebContext);
			loanAppDetlsVO.getRModTime().setValue(new java.util.Date(System.currentTimeMillis()));
		

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}
	}

}
