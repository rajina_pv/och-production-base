package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomNotificationMaintenanceListServiceFetchImpl extends AbstractLocalListInquiryTran{

	@SuppressWarnings("rawtypes")
	@Override
	protected void associateQueryParameters(FEBATransactionContext txnContext, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {
		EBTransactionContext ebcontext = (EBTransactionContext) txnContext;
		CustomNotificationCriteriaVO criteriaVO = (CustomNotificationCriteriaVO) objQueryCrit;
		queryOperator.associate("bankId", ebcontext.getBankId());
		queryOperator.associate("userId", ebcontext.getUserId());
		queryOperator.associate("notificationHeader", new FEBAUnboundString(criteriaVO.getNotificationHeader().getValue()));
		queryOperator.associate("shortDescription", criteriaVO.getShortDescription());

	}

	@SuppressWarnings("rawtypes")
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String getQueryIdentifier(FEBATransactionContext pObjContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		final String queryIdentifier;
		queryIdentifier = CustomEBQueryIdentifiers.CUSTOM_NOTIFICATION_LIST_DETAILS_FETCH;	
		return queryIdentifier;

	}

	@Override
	@SuppressWarnings("rawtypes")
	protected void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
					throws BusinessException, BusinessConfirmation, CriticalException{

		CustomNotificationEnquiryVO enquiryVO = (CustomNotificationEnquiryVO) objInputOutput;

		try {
			super.executeQuery(objContext, enquiryVO, objTxnWM);

			if (enquiryVO.getResultList().size() == 0) {
				throw new BusinessException(
						objContext,
						EBIncidenceCodes.NO_RECORDS_FETCHED_FROM_TSTM,
						"No Records Found for the Search",
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}

		}

		catch (BusinessException be) {

			throw new BusinessException(
					objContext,
					EBIncidenceCodes.NO_RECORDS_FETCHED_FROM_TSTM,
					"No Records Found in Default Search",
					EBankingErrorCodes.NO_RECORDS_FOUND);
		}

	}

}
