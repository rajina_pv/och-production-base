/**
 * AuthenticationServiceGenerateOTPImpl.java
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2011 Infosys Technologies Limited.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */

package com.infosys.custom.ebanking.user.service;

import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.BusinessWarningVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.types.entityreferences.PrincipalIDER;
import com.infosys.fentbase.types.valueobjects.IAuthenticationVO;
import com.infosys.fentbase.user.UserConstants;
import com.infosys.fentbase.user.service.OTPUtility;
import com.infosys.fentbase.user.service.RMUserDetailsFetchUtility;
import com.infosys.fentbase.user.validators.ApplicationBankIDVal;

/**
 *
 * @author sangeetha_mulaparthi
 * @version 1.0
 * @since FEBA 2.0
 */
public final class CustomRMAuthenticationServiceGenerateOTPImpl extends AbstractHostUpdateTran {


	/**
	 * This method prepares the list of validations to be done.
	 *
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @return
	 */
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		final IAuthenticationVO authenticationVO = (IAuthenticationVO) objInputOutput;

		int arrayListSize = 2;
		final FEBAValItem[] valItem = new FEBAValItem[arrayListSize];

		valItem[0] = new FEBAValItem(authenticationVO.getBankID(), new ApplicationBankIDVal(),
				FEBAValEngineConstants.MANDATORY, FEBAValEngineConstants.INDEPENDENT);
		/**
		 * "bankID", authenticationVO.getBankID(),
		 * FEBAValEngineConstants.MANDATORY, FEBAValEngineConstants.INDEPENDENT,
		 * FBAErrorCodes.CUST_BANKID_MANDATORY);
		 **/
		valItem[1] = new FEBAValItem("userSignOnVO.userProfileVO.principalIDER.userPrincipal",
				authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().getUserPrincipal(),
				FEBAValEngineConstants.MANDATORY, FEBAValEngineConstants.DEPENDENT,
				FBAErrorCodes.INVALID_LOGIN_CREDENTIALS);

		return valItem;

	}

	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		final FBATransactionContext ebContext = (FBATransactionContext) objContext;

		final IAuthenticationVO authenticationVO = (IAuthenticationVO) objInputOutput;
		PrincipalIDER principalIDER = authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER();

		/* Fix fot Ticket - 788891 : START */
		if (ebContext.getIsVirtualLoginSession() != null
				&& "Y".equalsIgnoreCase(ebContext.getIsVirtualLoginSession().toString())) {
			ebContext.setUserId(principalIDER.getUserId());
			ebContext.setCorpId(principalIDER.getCorpId());
		}
		/* Fix fot Ticket - 788891 : END */
		String generatedOTP = OTPUtility.getInstance().generateOTP(ebContext, principalIDER);

		FEBAUnboundString message = new FEBAUnboundString(UserConstants.OTP_GEN_SUCCESS);
		int errorCode = FBAErrorCodes.OTP_GEN_SUCCESS_CODE;
		ebContext.addBusinessInfo(new FEBAUnboundInt(errorCode), message, message);
		authenticationVO.getCredentials().setSmsOTP(generatedOTP);

	}

	/**
	 * Checks user contact details for OTP delivery <BR>
	 *
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran#process(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 */
	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		try {
			RMUserDetailsFetchUtility userUtil = new RMUserDetailsFetchUtility();
			userUtil.checkUserContactDetails(objContext);

			FBATransactionContext ebContext = (FBATransactionContext) objContext;

			FEBAArrayList bwList = ebContext.getWarningList().getWarningList();
			if (bwList != null && bwList.size() > 0) {
				for (int i = 0; i <= bwList.size(); i++) {
					BusinessWarningVO bw = (BusinessWarningVO) bwList.get(i);
					if (bw.getCode().getValue() == FBAErrorCodes.OTP_CONTACT_DETAILS_NOT_AVAILABLE
							|| bw.getCode().getValue() == FBAErrorCodes.ALERTS_EMAIL_DETAILS_NOT_AVAILABLE
							|| bw.getCode().getValue() == FBAErrorCodes.ALERTS_MOBILE_DETAILS_NOT_AVAILABLE) {
						ebContext.getWarningList().initialize();
						bw.setCode(new FEBAUnboundInt(FBAErrorCodes.OTP_CONTACT_DETAILS_NOT_AVAILABLE));
						bw.setDispMessage("The contact details do not exist. Register the details.");
						bw.setLogMessage("The contact details do not exist. Register the details.");
						ebContext.getBussinessInfo().initialize();
						ebContext.addWarning(bw);
						break;
					}
				}
			}
		} catch (Exception e) {
			// Consume the exception if any, since we need to show only warning
			// message the original flow should not get stopped in any case.
			// Log the error
			LogManager.logError(objContext, e);
		}

	}

}
