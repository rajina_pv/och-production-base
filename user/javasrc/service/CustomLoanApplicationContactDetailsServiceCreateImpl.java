package com.infosys.custom.ebanking.user.service;


import com.infosys.custom.ebanking.tao.CLCDTAO;

import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnContactDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;

import com.infosys.ebanking.common.validators.PhoneNumberPatternVal;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;

import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;


public class CustomLoanApplicationContactDetailsServiceCreateImpl extends AbstractLocalUpdateTran{

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanApplnContactDetailsVO contactDetVO = (CustomLoanApplnContactDetailsVO) objInputOutput;
		
		return new FEBAValItem[] {
			
				new FEBAValItem("emergencyContactPhoneNum", contactDetVO.getEmergencyContactPhoneNum(),new PhoneNumberPatternVal(),
						FEBAValEngineConstants.NON_MANDATORY,
						FEBAValEngineConstants.INDEPENDENT),
				
				
			};
		
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		CustomLoanApplnContactDetailsVO contactDetVO = (CustomLoanApplnContactDetailsVO) objInputOutput;

    	
		insertCLCD(txnContext, contactDetVO);
		

	}

	



	private void insertCLCD(FEBATransactionContext objContext,
			CustomLoanApplnContactDetailsVO loanApplicantConDetVO) throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
    	CLCDTAO clcdTAO = new CLCDTAO(objContext);    	
    	
		try {
			clcdTAO.associateBankId(ebContext.getBankId());			
			clcdTAO.associateApplicationId(loanApplicantConDetVO.getApplicationId());
			clcdTAO.associateMaritalStatus(loanApplicantConDetVO.getMaritalStatus());
			clcdTAO.associateSpouseId(loanApplicantConDetVO.getSpouseId());
            clcdTAO.associatePartnernikid(loanApplicantConDetVO.getPartnerNikId());
            clcdTAO.associatePartnername(loanApplicantConDetVO.getPartnerName());
			clcdTAO.associateNoOfDependents(loanApplicantConDetVO.getNoOfDependents());
			clcdTAO.associateMotherName(loanApplicantConDetVO.getMotherName());
			clcdTAO.associateEmergencyContact(loanApplicantConDetVO.getEmergencyContact());
			clcdTAO.associateEmrConRelation(loanApplicantConDetVO.getEmergencyContactRelation());
			clcdTAO.associateEmrConAddress(loanApplicantConDetVO.getEmergencyContactAddress());
			clcdTAO.associateEmrConPhoneNum(loanApplicantConDetVO.getEmergencyContactPhoneNum());
			clcdTAO.associateEmrConPostalCd(loanApplicantConDetVO.getEmergencyContactPostalCode());
			
			clcdTAO.insert(objContext);

		} catch (FEBATableOperatorException e) {
			throw new BusinessException(objContext,
					EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in CLACD",
					EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
	}

   

}
