package com.infosys.custom.ebanking.user.service;

import com.infosys.ci.common.DateUtil;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeCIFInqDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeCIFInqEnquiryVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomSilverlakeCIFCallServiceCIFInquiryImpl extends AbstractHostInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomSilverlakeCIFInqEnquiryVO enquiryVO = (CustomSilverlakeCIFInqEnquiryVO) objInputOutput;
		try {
			EBHostInvoker.processRequest(objContext, CustomEBRequestConstants.SILVERLAKE_CIF_INQUIRY, enquiryVO);
			
			//Tushar: Added to format the date-  Start
			if(FEBATypesUtility.isNotNullOrBlank(enquiryVO.getDetails().getBirthDate()) 
					&& enquiryVO.getDetails().getBirthDate().getLength() == 8){

				String incomingDate = enquiryVO.getDetails().getBirthDate().toString();

				FEBADate formattedDOB = new FEBADate();

				if(incomingDate.startsWith("00")){
					String day = incomingDate.substring(2,4);
					String month = incomingDate.substring(4,6);
					String year = incomingDate.substring(6,8);
					int yearVal = Integer.parseInt(year);
					FEBADate currentDate =  new FEBADate(DateUtil.currentDate());
					currentDate.setDateFormat("dd-MM-yy");
					int currYearVal = Integer.parseInt(currentDate.toString().substring(6,8));
					if( yearVal >= currYearVal && yearVal <= 99){
						year="19"+year;
					}else{
						year="20"+year;
					}
					String newDateFormat = year+"-"+month+"-"+day;

					formattedDOB.setDateFormat("yyyy-MM-dd");
					formattedDOB.set(newDateFormat);
				}else{
					String day = incomingDate.substring(0,2);
					String month = incomingDate.substring(2,4);
					String year = incomingDate.substring(4,8);
					
					FEBADate currentDate =  new FEBADate(DateUtil.currentDate());
					currentDate.setDateFormat("dd-MM-yyyy");
					
					String newDateFormat = year+"-"+month+"-"+day;

					formattedDOB.setDateFormat("yyyy-MM-dd");
					formattedDOB.set(newDateFormat);
				}


				enquiryVO.getDetails().setBirthDate(formattedDOB.toString());
				
			}
			//Tushar: Added to format the date- End
			
		/*
		 *Removal of special characters from white-list name field and replace them by space
		 *Added by : Pratik_shah07 
		 */
		if(FEBATypesUtility.isNotNullOrBlank(enquiryVO.getDetails().getNama())){
			String name = enquiryVO.getDetails().getNama().getValue();
			enquiryVO.getDetails().setNama(name.replaceAll("[^a-zA-Z0-9 ]", " "));
		}
			
			
		} catch (BusinessException be) {
			throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.SILVERLAKE_CIF_HOST_CALL_ERROR,
					"An unexpected exception occurred during silverlake CIF inquiry", null, CustomEBankingErrorCodes.SILVERLAKE_CIF_HOST_CALL_ERROR, null);
		}

	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomSilverlakeCIFInqEnquiryVO enquiryVO = (CustomSilverlakeCIFInqEnquiryVO) objInputOutput;

		CustomSilverlakeCIFInqDetailsVO detailsVO = enquiryVO.getDetails();
		String ktpNumber = detailsVO.getIdNo().toString();

		// expiry date validation
		if (!(ktpNumber.equalsIgnoreCase(enquiryVO.getCriteria().getKtpNumber().toString()))) {

			throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.KTP_NUMBER_MISMATCH,
					"KTP Number mismatch.", null, CustomEBankingErrorCodes.KTP_NUMBER_MISMATCH, null);
		}

	}
}
