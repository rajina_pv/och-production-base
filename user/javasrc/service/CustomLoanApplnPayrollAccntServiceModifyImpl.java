package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CAPWDTAO;
import com.infosys.custom.ebanking.tao.CLEDTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CAPWDInfo;
import com.infosys.custom.ebanking.tao.info.CLEDInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;

public class CustomLoanApplnPayrollAccntServiceModifyImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		CustomLoanApplnPayrollAccntDetailsVO payrollAccntDetVO = (CustomLoanApplnPayrollAccntDetailsVO) objInputOutput;

		// updation into CLAPA using TAO call
		updateCLPA(txnContext, payrollAccntDetVO);
		updateCELD(txnContext, payrollAccntDetVO);
	}

	private void updateCLPA(FEBATransactionContext objContext, CustomLoanApplnPayrollAccntDetailsVO payrollAcctDetVO)
			throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
		CLPATAO clpaTAO = new CLPATAO(objContext);
		CLPAInfo clpaInfo = null;

		try {
			clpaTAO.associateBankId(ebContext.getBankId());
			clpaTAO.associateApplicationId(payrollAcctDetVO.getApplicationId());
			clpaTAO.associatePayrollAccountNum(payrollAcctDetVO.getPayrollAccountId());
			clpaTAO.associatePayrollAccBranch(payrollAcctDetVO.getPayrollAccountBranch());
			//changes for payroll reject/privilege flow -Start
			CAPWDInfo capwdInfo=CAPWDTAO.select(objContext, ebContext.getBankId(), payrollAcctDetVO.getApplicationId());
			clpaTAO.associateAccPayrollDate(capwdInfo.getPayrollDate());
			//changes for payroll reject/privilege flow -End
			
			clpaTAO.associateRefferalCode(payrollAcctDetVO.getRefferalCode());
			clpaTAO.associateSavingAmount(payrollAcctDetVO.getSavingAmount());
			clpaTAO.associateAgentCode(payrollAcctDetVO.getAgentCode());
			clpaTAO.associateCreditFrequency3(payrollAcctDetVO.getCreFre3());
			clpaTAO.associateCreditFrequency6(payrollAcctDetVO.getCreFre6());
			clpaTAO.associateCreditFrequency12(payrollAcctDetVO.getCreFre12());
			clpaTAO.associateDebitFrequency3(payrollAcctDetVO.getDebFre3());
			clpaTAO.associateDebitFrequency6(payrollAcctDetVO.getDebFre3());
			clpaTAO.associateDebitFrequency12(payrollAcctDetVO.getDebFre3());
			clpaTAO.associateCreditTotal3(payrollAcctDetVO.getCreTot3());
			clpaTAO.associateCreditTotal6(payrollAcctDetVO.getCreTot6());
			clpaTAO.associateCreditTotal12(payrollAcctDetVO.getCreTot12());
			clpaTAO.associateDebitTotal3(payrollAcctDetVO.getDebTot3());
			clpaTAO.associateDebitTotal6(payrollAcctDetVO.getDebTot6());
			clpaTAO.associateDebitTotal12(payrollAcctDetVO.getDebTot12());
			clpaTAO.associateCreditAvg3(payrollAcctDetVO.getCreAvg3());
			clpaTAO.associateCreditAvg6(payrollAcctDetVO.getCreAvg6());
			clpaTAO.associateCreditAvg12(payrollAcctDetVO.getCreAvg12());
			clpaTAO.associateDebitAvg3(payrollAcctDetVO.getDebAvg3());
			clpaTAO.associateDebitAvg6(payrollAcctDetVO.getDebAvg6());
			clpaTAO.associateDebitAvg12(payrollAcctDetVO.getDebAvg12());
			clpaTAO.associateInetBankingStatus(payrollAcctDetVO.getNetBanking());
			clpaTAO.associateMobileBankingStatus(payrollAcctDetVO.getMobileBanking());
			clpaTAO.associatePrimaryPhoneStatus(payrollAcctDetVO.getPrimaryPhone());
			clpaTAO.associateSecondaryPhoneStatus(payrollAcctDetVO.getSecondaryPhone());
			clpaTAO.associateSavAccountOwnershipStatus(payrollAcctDetVO.getSavingAccountOwnershipStatus());
			clpaTAO.associateSavAccountOwnershipDate(payrollAcctDetVO.getSavingAccountOwnershipDate());
			clpaTAO.associateCompanyName(payrollAcctDetVO.getCompanyName());
			clpaTAO.associatePersonelName(payrollAcctDetVO.getPersonelName());
			clpaTAO.associatePersonelNumber(payrollAcctDetVO.getPersonelNumber());
			/*
			 * Fields added in CLPA for saving WhiteList inquiry fields
			 * 
			 */
			clpaTAO.associateNameWl(payrollAcctDetVO.getNameWL());
			clpaTAO.associateBirthDateWl(payrollAcctDetVO.getBirthDateWL());
			clpaTAO.associateEmpEndDateWl(payrollAcctDetVO.getEmpEndDateWL());
			clpaTAO.associateEmpStartDateWl(payrollAcctDetVO.getEmpStartDateWL());
			clpaTAO.associateNetIncomeWl(payrollAcctDetVO.getNetIncomeWL());
			clpaTAO.associateGenderWl(payrollAcctDetVO.getGenderWL());
			clpaTAO.associateAddressWl(payrollAcctDetVO.getAddressWL());
			clpaTAO.associatePlaceOfBirthWl(payrollAcctDetVO.getPlaceOfBirthWL());
			//non payroll NPWP addition--Start
			clpaTAO.associateNpwp(payrollAcctDetVO.getNPWP());
			//non payroll NPWP addition--End
			//non payroll company address addition--Start
			clpaTAO.associateCompanyAddress(payrollAcctDetVO.getCompanyAddress());
			//non payroll company address addition--End
			// Added for BRI AGRO Payroll - start
			clpaTAO.associateEmployeeStatus(payrollAcctDetVO.getEmployeeStatus());
			clpaTAO.associateExpenditureAmt(payrollAcctDetVO.getExpenditureAmount());
			// Added for BRI AGRO Payroll - end
			// Added for PINANG web view start
			clpaTAO.associateWorkAddress(payrollAcctDetVO.getWorkAddress());
			// Added for PINANG web view end
			clpaInfo = CLPATAO.select(ebContext, ebContext.getBankId(), payrollAcctDetVO.getApplicationId());			
			clpaTAO.associateCookie(clpaInfo.getCookie());

			clpaTAO.update(ebContext);

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
			LogManager.logError(null, e);
			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}
	}
	
	
	protected void updateCELD(FEBATransactionContext objContext, CustomLoanApplnPayrollAccntDetailsVO payrollAcctDetVO)
			throws BusinessException {
		
		// Declare a TAO object
		EBTransactionContext ebContext = (EBTransactionContext) objContext;
		final CLEDTAO cledTAO = new CLEDTAO(ebContext);
		CLEDInfo cledInfo = null;
		
		try {
			
			cledInfo = CLEDTAO.select(ebContext, ebContext.getBankId(), payrollAcctDetVO.getApplicationId());
			
			cledTAO.associateBankId(ebContext.getBankId());
			cledTAO.associateApplicationId(cledInfo.getApplicationId());
			cledTAO.associateTaxId(cledInfo.getTaxId());
			cledTAO.associateFinancialSource(cledInfo.getFinancialSource());
			cledTAO.associateMonthlyIncome(payrollAcctDetVO.getNetIncomeWL());
			cledTAO.associateMonthlyExpense(payrollAcctDetVO.getExpenditureAmount());
			cledTAO.associateWorkType(cledInfo.getWorkType());
			cledTAO.associateEmployerName(cledInfo.getEmployerName());
			cledTAO.associateEmpStatus(cledInfo.getEmpStatus());
			cledTAO.associateEmpStartDate(cledInfo.getEmpStartDate());
			cledTAO.associateEmpEndDate(cledInfo.getEmpEndDate());

			cledTAO.associateCookie(cledInfo.getCookie());

			cledTAO.update(ebContext);

		} catch (FEBATableOperatorException e) {
			LogManager.logError(null, e);
			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}
	}

}
