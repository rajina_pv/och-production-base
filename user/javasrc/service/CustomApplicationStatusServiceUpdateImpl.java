package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CBETTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CNSDTAO;
import com.infosys.custom.ebanking.tao.info.CBETInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CNSDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationUpdateStatusDetailsVO;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomApplicationStatusServiceUpdateImpl extends AbstractLocalUpdateTran {

	private static final CommonCode APP_ID_SHAHABAT = new CommonCode(CustomEBConstants.SAHABAT);
	private static final FEBAUnboundString MSG_VERIFY_EMAIL = new FEBAUnboundString("VERIFY_EMAIL");
	private static final FEBAUnboundString STATUS = new FEBAUnboundString("EMAIL_VERIFIED");
	
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomApplicationUpdateStatusDetailsVO detailsVO = (CustomApplicationUpdateStatusDetailsVO)objInputOutput;
		 
		 try { 
			updateCNSD(txnContext, detailsVO);
			updateCLAT(txnContext, detailsVO);
			updateCBET(txnContext, detailsVO);
		} catch (FEBATableOperatorException e1) {
			LogManager.logError(txnContext, e1);
		}
	}
	private void updateCNSD(FEBATransactionContext txnContext,CustomApplicationUpdateStatusDetailsVO detailsVO) throws FEBATableOperatorException, BusinessException {
		CNSDTAO tao = new CNSDTAO(txnContext);
		tao.associateAppId(APP_ID_SHAHABAT);
		tao.associateBankId(detailsVO.getBankId());
		tao.associateApplicationId(detailsVO.getApplicationId());
		tao.associateMessageEvent(MSG_VERIFY_EMAIL);
		tao.associateMessageStatus(STATUS);
		try {
			CNSDInfo cnsdInfo = CNSDTAO.select(txnContext, detailsVO.getBankId(), detailsVO.getApplicationId(),
					MSG_VERIFY_EMAIL);
			tao.associateCookie(cnsdInfo.getCookie());
			tao.update(txnContext);
			txnContext.getConnection().commit();
		} catch (FEBATableOperatorException e) {
			
			if(e.getErrorCode() != EBankingErrorCodes.NO_RECORDS_FOUND)
			{
				throw new BusinessException(txnContext, CustomEBankingIncidenceCodes.CNSD_UPD_FAILED, 
						"", CustomEBankingErrorCodes.CNSD_UPD_FAILED); 
			}
		}
		
	}
	private void updateCLAT(FEBATransactionContext txnContext,CustomApplicationUpdateStatusDetailsVO detailsVO) throws FEBATableOperatorException, BusinessException {
		CLATTAO clatTao = new CLATTAO(txnContext);
		clatTao.associateBankId(detailsVO.getBankId());
		clatTao.associateApplicationId(detailsVO.getApplicationId());
		clatTao.associateEmailVerified(new FEBAUnboundChar(EBankingConstants.CHAR_Y));
		clatTao.associateApplicationStatus(new CommonCode(CustomEBConstants.ACT_LINK_CONF));
		try {
			CLATInfo clatInfo = CLATTAO.select(txnContext, txnContext.getBankId(), detailsVO.getApplicationId());
			
			clatTao.associateCookie(clatInfo.getCookie());
			clatTao.update(txnContext);
			txnContext.getConnection().commit();
		} catch (FEBATableOperatorException e) {
			if(e.getErrorCode() != EBankingErrorCodes.NO_RECORDS_FOUND)
			{
				throw new BusinessException(txnContext, CustomEBankingIncidenceCodes.CBET_UPD_FAILED, 
						"", CustomEBankingErrorCodes.CLAT_UPD_FAILED); 
			}
		}
	}
private void updateCBET(FEBATransactionContext txnContext,CustomApplicationUpdateStatusDetailsVO detailsVO) throws FEBATableOperatorException, BusinessException {
		
		CommonCode notificationEvent = new CommonCode(CustomEBConstants.APP_EML_PEN);
		UserId userId = new UserId(detailsVO.getUserId().toString());
		CBETTAO cbetTao = new CBETTAO(txnContext);
		
		try {
			CBETInfo cbetInfo = CBETTAO.select(txnContext, txnContext.getBankId(), notificationEvent, detailsVO.getApplicationId(), userId);
			
			cbetTao.associateCookie(cbetInfo.getCookie());
			cbetTao.associateBankId(detailsVO.getBankId());
			cbetTao.associateNotifEvent(notificationEvent);
			cbetTao.associateAppId(detailsVO.getApplicationId());
			cbetTao.associateUserId(userId);
			cbetTao.logicalDelete(txnContext);
			txnContext.getConnection().commit();
		} catch (FEBATableOperatorException e) {
			if(e.getErrorCode() != EBankingErrorCodes.NO_RECORDS_FOUND)
			{
				throw new BusinessException(txnContext, CustomEBankingIncidenceCodes.CBET_UPD_FAILED, 
						"", CustomEBankingErrorCodes.CBET_UPD_FAILED); 
			}
		}
	}
}