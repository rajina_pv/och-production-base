
package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CUPRTAO;
import com.infosys.custom.ebanking.tao.info.CUPRInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomPhotoValidationDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.utils.insulate.ArrayList;

/**
 * This service is used for photo validation ..
 * 
 */

public class CustomPhotoValidateServiceValidateImpl extends AbstractLocalUpdateTran {

		
	private void validateMandatory(FEBATransactionContext ebContext, CustomPhotoValidationDetailsVO refVO)
			throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();
		if (!FEBATypesUtility.isNotNullOrBlank(refVO.getApplicationId())) {
			valError.add(getBusinessExceptionVO(ebContext, "application id is mandatory"));
		}
		if (!FEBATypesUtility.isNotNull(refVO.getSelfie1())) {
			valError.add(getBusinessExceptionVO(ebContext, "Photo is mandatory"));
		}
		if (!FEBATypesUtility.isNotNull(refVO.getSelfie2())) {
			valError.add(getBusinessExceptionVO(ebContext, "Photo is mandatory"));
		}
		if (!FEBATypesUtility.isNotNullOrBlank(refVO.getToken())) {
			valError.add(getBusinessExceptionVO(ebContext, "Token is mandatory"));
		}
		if (!valError.isEmpty()) {
			throw new BusinessException(ebContext, valError);
		}
	}

	
	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}


	@Override
	public void process(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
	final EBTransactionContext ebContext = (EBTransactionContext) arg0;
		// Added for validating photo confidence score :: START
		String photoConfidenceScorePRPM = PropertyUtil.getProperty("PHOTO_CONFIDENCE_SCORE", ebContext);
		double photoConfidenceScore = 0.0;
		// Added for validating photo confidence score :: END
		CustomPhotoValidationDetailsVO detVO = (CustomPhotoValidationDetailsVO) arg1;
		validateMandatory(ebContext, detVO);		
		detVO.setMerchantKey(PropertyUtil.getProperty("PRIVY_MERCHANT_KEY", ebContext));
		
		try {
			
			CUPRInfo cuprinfo=CUPRTAO.select(arg0, ebContext.getBankId(), new FEBAUnboundString(ebContext.getUserId().getValue()));
			detVO.setSelfie2(cuprinfo.getPhotoObj1());
			EBHostInvoker.processRequest(ebContext, "CustomPhotoValidateRequest",
					detVO);	
			// Added for validating photo confidence score :: START
			if (FEBATypesUtility.isNotNullOrBlank(detVO.getConfidenceScore()) && detVO.getCode().toString().equalsIgnoreCase("200")) {
		    	  
				photoConfidenceScore = Double.valueOf(detVO.getConfidenceScore().getValue());
				double photoConfidenceScoreConfig = Double.valueOf(photoConfidenceScorePRPM);
	
				boolean isConfScoreFlag = (photoConfidenceScore > photoConfidenceScoreConfig) ? true : false;
				
				if(!isConfScoreFlag)
					throw new BusinessException(true, ebContext, "PRIVAL02","Face does not match", null, 211057, null);
											
			  } 
			// Added for validating photo confidence score :: END
			  else if(detVO.getCode().toString().equals("400")) {
					   throw new BusinessException(true, ebContext, "PRIVAL01",			  
					"Face not found", null, 211056, null);
		      }
			else if(detVO.getCode().toString().equals("407"))
			  {
				throw new BusinessException(true, ebContext, "PRIVAL02",			  
					"Face does not match", null, 211057, null);
		      }
			else if(detVO.getCode().toString().equals("402"))
			  {
				throw new BusinessException(true, ebContext, "PRIVAL03",			  
					"Invalid Privy Id", null, 211058, null);
		      }
			else if(detVO.getCode().toString().equals("405"))
			  {
				throw new BusinessException(true, ebContext, "PRIVAL04",			  
					"Invalid OTP", null, 211059, null);
		      }
			// Default error block :: START
			else {
				throw new BusinessException(ebContext,
						EBIncidenceCodes.UNABLE_TO_PROCESS_REQUEST_PLEASE_TRY_AFTER_SOME_TIME,
						EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
			}
			// Default error block :: END
		} catch (CriticalException | FEBATypeSystemException | FEBATableOperatorException ce) {
			throw new BusinessException(
					ebContext,
					EBIncidenceCodes.UNABLE_TO_PROCESS_REQUEST_PLEASE_TRY_AFTER_SOME_TIME,
					EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
		
	}


	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {		
		return new FEBAValItem[] {};
	}		
	}

