package com.infosys.custom.ebanking.user.service;

import java.text.SimpleDateFormat;

import com.infosys.custom.ebanking.tao.CAPWDTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.CPITTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollWLEnquiryVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomCaptureApplnPayrollAccntServiceCreateImpl extends AbstractHostUpdateTran {
	public static final String PAYROLL_REJ = "PAYROLL_REJ";
	public static final String PAYROLL_APP = "PAYROLL_APP";
	public static final String HOME_CUR_CODE = "HOME_CUR_CODE";
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	protected void processLocalData(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanApplnPayrollAccntDetailsVO payrollAccntDetVO = (CustomLoanApplnPayrollAccntDetailsVO) objInputOutput;

		// insertion into CLAPA using TAO call
	}

	@Override
	protected void processHostData(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplnPayrollAccntDetailsVO payrollAccntDetVO = (CustomLoanApplnPayrollAccntDetailsVO) objInputOutput;

		CustomPayrollWLEnquiryVO customPayrollWLEnquiryVO = (CustomPayrollWLEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomPayrollWLEnquiryVO);

		customPayrollWLEnquiryVO.getCriteria().setAccountNumber(payrollAccntDetVO.getPayrollAccountId().getValue());
		customPayrollWLEnquiryVO.getCriteria().setBankCode(payrollAccntDetVO.getBankCode());
		EBHostInvoker.processRequest(txnContext, "CustomPayrollWhiteListRequest", customPayrollWLEnquiryVO);
		
		int respCode=Integer.valueOf(customPayrollWLEnquiryVO.getDetails().getRespCode().getValue());
		if (respCode>=100 && respCode<=199) {
			throw new BusinessException(txnContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"WhiteList API Connection Error", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
		insertCAPWD(txnContext, payrollAccntDetVO, customPayrollWLEnquiryVO);
		updateCLPA(txnContext, payrollAccntDetVO, customPayrollWLEnquiryVO);
		insertCPIT(txnContext, payrollAccntDetVO);
		try {
			if (customPayrollWLEnquiryVO.getDetails().getRespCode().getValue().equals("000")) {

				CLATInfo clatInfo = CLATTAO.select(txnContext, txnContext.getBankId(),
						payrollAccntDetVO.getApplicationId());
				CLATTAO clatTAO = new CLATTAO(txnContext);
				clatTAO.associateBankId(clatInfo.getBankId());
				clatTAO.associateUserId(clatInfo.getUserId());
				clatTAO.associateApplicationId(payrollAccntDetVO.getApplicationId());
				clatTAO.associateApplicationStatus(new CommonCode(PAYROLL_APP));
				clatTAO.associateCookie(clatInfo.getCookie());
				clatTAO.update(txnContext);
				(new CustomLoanHistoryUpdateUtil()).updateHistory(payrollAccntDetVO.getApplicationId(), PAYROLL_APP,
						customPayrollWLEnquiryVO.getDetails().getRespDesc().getValue(), txnContext);
				payrollAccntDetVO.setApplicationStatus(PAYROLL_APP);
			} else {
				CLATInfo clatInfo = CLATTAO.select(txnContext, txnContext.getBankId(), payrollAccntDetVO.getApplicationId());

				CLATTAO clatTAO = new CLATTAO(txnContext);
				clatTAO.associateBankId(clatInfo.getBankId());
				clatTAO.associateUserId(clatInfo.getUserId());
				clatTAO.associateApplicationId(payrollAccntDetVO.getApplicationId());
				clatTAO.associateApplicationStatus(new CommonCode(PAYROLL_REJ));
				clatTAO.associateCookie(clatInfo.getCookie());
				clatTAO.update(txnContext);
				(new CustomLoanHistoryUpdateUtil()).updateHistory(payrollAccntDetVO.getApplicationId(), PAYROLL_REJ,
						customPayrollWLEnquiryVO.getDetails().getRespDesc().getValue(), txnContext);
				payrollAccntDetVO.setApplicationStatus(PAYROLL_REJ);
			}
		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
			LogManager.logError(null, e);
			throw new BusinessException(txnContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in CLAT", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		// Do nothing .

	}

	private void insertCAPWD(FEBATransactionContext objContext, CustomLoanApplnPayrollAccntDetailsVO payrollAccntDetVO,
			CustomPayrollWLEnquiryVO customPayrollWLEnquiryVO) throws BusinessException {
		try {
			CAPWDTAO capwdtao = new CAPWDTAO(objContext);
			capwdtao.associateApplicationId(payrollAccntDetVO.getApplicationId());
			capwdtao.associateBankId(objContext.getBankId());

			capwdtao.associateRespcode(customPayrollWLEnquiryVO.getDetails().getRespCode());
			capwdtao.associateRespdesc(customPayrollWLEnquiryVO.getDetails().getRespDesc());

			if (customPayrollWLEnquiryVO.getDetails().getRespCode().getValue().equals("000")) {

				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00'Z'");
				capwdtao.associateAccountNumber(customPayrollWLEnquiryVO.getDetails().getAccountNumber());
				capwdtao.associateNik(customPayrollWLEnquiryVO.getDetails().getNik());
				capwdtao.associateName(customPayrollWLEnquiryVO.getDetails().getName());
				capwdtao.associatePayrollDate(new FEBADate(
						dateFormat.parse(customPayrollWLEnquiryVO.getDetails().getPayrollDate().getValue())));
				capwdtao.associateIncome(customPayrollWLEnquiryVO.getDetails().getIncome());
				capwdtao.associateJobStatus(customPayrollWLEnquiryVO.getDetails().getJobStatus());
				capwdtao.associateWorkingStartDate(new FEBADate(
						dateFormat.parse(customPayrollWLEnquiryVO.getDetails().getWorkingStartDate().getValue())));
				capwdtao.associateRemainingWorkingPeriod(
						customPayrollWLEnquiryVO.getDetails().getRemainingWorkingPeriod());
				capwdtao.associateWorkingPeriod(customPayrollWLEnquiryVO.getDetails().getWorkingPeriod());
				capwdtao.associateMedianCredit(customPayrollWLEnquiryVO.getDetails().getMedianCredit());
				capwdtao.associateMedianBalance(customPayrollWLEnquiryVO.getDetails().getMedianBalance());
				capwdtao.associateCompanyName(customPayrollWLEnquiryVO.getDetails().getCompanyName());
				capwdtao.associateBankCode(customPayrollWLEnquiryVO.getDetails().getBankCode());
				capwdtao.associateBankName(customPayrollWLEnquiryVO.getDetails().getBankName());
				capwdtao.associateUserMaker(customPayrollWLEnquiryVO.getDetails().getUserMaker());
				capwdtao.associateUserSigner(customPayrollWLEnquiryVO.getDetails().getUserSigner());
				capwdtao.associateRgdesc(customPayrollWLEnquiryVO.getDetails().getRgdesc());
				capwdtao.associateMbdesc(customPayrollWLEnquiryVO.getDetails().getMbdesc());
				capwdtao.associateBrdesc(customPayrollWLEnquiryVO.getDetails().getBrdesc());
				capwdtao.associateSuccess(customPayrollWLEnquiryVO.getDetails().getSuccess());
				capwdtao.associateIsprivilege(customPayrollWLEnquiryVO.getDetails().getIsPrivilegeWithAgreement());
				capwdtao.associatePreapprovedplafond(customPayrollWLEnquiryVO.getDetails().getPreapprovedPlafondWl());
				capwdtao.associatePreapprovedtenor(customPayrollWLEnquiryVO.getDetails().getPreapprovedTenorWl());
				capwdtao.associateAccttype(customPayrollWLEnquiryVO.getDetails().getAcctType());
			}

			capwdtao.insert(objContext);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logError(null, e);
			throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in CAPWD", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}

	}

	private void insertCPIT(FEBATransactionContext objContext, CustomLoanApplnPayrollAccntDetailsVO payrollAcctDetVO)
			throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
		CPITTAO cpitTAO = new CPITTAO(objContext);

		try {
			cpitTAO.associateBankId(ebContext.getBankId());
			cpitTAO.associateApplicationId(payrollAcctDetVO.getApplicationId());
			cpitTAO.associatePayrollAccountNum(payrollAcctDetVO.getPayrollAccountId());
			cpitTAO.associatePayrollAccBranch(payrollAcctDetVO.getPayrollAccountBranch());
			cpitTAO.associateSavingAmount(payrollAcctDetVO.getSavingAmount());
			cpitTAO.associateNameWl(payrollAcctDetVO.getNameWL());
			cpitTAO.associateBirthDateWl(payrollAcctDetVO.getBirthDateWL());
			cpitTAO.associateGenderWl(payrollAcctDetVO.getGenderWL());
			cpitTAO.associateAddressWl(payrollAcctDetVO.getAddressWL());
			cpitTAO.associatePlaceOfBirthWl(payrollAcctDetVO.getPlaceOfBirthWL());

			cpitTAO.insert(objContext);

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
			LogManager.logError(null, e);
			throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in CPIT", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
	}
	
    private void updateCLPA( FEBATransactionContext txnContext,CustomLoanApplnPayrollAccntDetailsVO payrollAccntDetVO,
			CustomPayrollWLEnquiryVO customPayrollWLEnquiryVO)
    {
        CLPATAO clpaTAO = new CLPATAO(txnContext);
        CLPAInfo clpaInfo = null;
        try
        {
        	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00'Z'");
            clpaTAO.associateBankId(txnContext.getBankId());
            clpaTAO.associateApplicationId(payrollAccntDetVO.getApplicationId());
            clpaTAO.associateAccPayrollDate(new FEBADate(
					dateFormat.parse(customPayrollWLEnquiryVO.getDetails().getPayrollDate().getValue())));
//            clpaTAO.associateEmpEndDateWl(criteriaVO.getValidEndDate());
            clpaTAO.associateEmpStartDateWl(new FEBADate(
					dateFormat.parse(customPayrollWLEnquiryVO.getDetails().getWorkingStartDate().getValue())));
            FEBAAmount income=new FEBAAmount(PropertyUtil.getProperty(HOME_CUR_CODE, txnContext), Double.valueOf(customPayrollWLEnquiryVO.getDetails().getIncome().getValue()));
            clpaTAO.associateNetIncomeWlResp(income);
            clpaInfo = CLPATAO.select(txnContext, txnContext.getBankId(), payrollAccntDetVO.getApplicationId());
            clpaTAO.associateCookie(clpaInfo.getCookie());
            clpaTAO.update(txnContext);
        }
        catch(Exception e)
        {
            LogManager.logError(null, e);
        }
    }
}
