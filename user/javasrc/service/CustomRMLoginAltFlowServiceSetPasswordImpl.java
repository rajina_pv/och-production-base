package com.infosys.custom.ebanking.user.service;

import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.entityreferences.UserIDER;
import com.infosys.fentbase.types.primitives.LAFRequestType;
import com.infosys.fentbase.types.primitives.UserSignOnStatusFlag;
import com.infosys.fentbase.types.valueobjects.ILoginAltFlowVO;
import com.infosys.fentbase.types.valueobjects.PasswordVO;
import com.infosys.fentbase.user.CUSRTableUtility;
import com.infosys.fentbase.user.IUSRTableUtility;
import com.infosys.fentbase.user.LoginAltFlowConstants;
import com.infosys.fentbase.user.service.LoginAltFlowNavigationUtility;
import com.infosys.fentbase.user.service.PasswordChangeUtility;
import com.infosys.fentbase.user.service.SetPasswordHostInvokerUtility;
import com.infosys.fentbase.user.service.UserIDExpireAuthUtility;

public class CustomRMLoginAltFlowServiceSetPasswordImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(

			FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
					throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};

	}

	public char getUserFlag(FEBATransactionContext objContext) {
		if (!objContext.getUserType().getValue().equals(FBAConstants.BANK_USER)) {
			return FBAConstants.LOGIN_STATUS_COMPLETELY_AUTHENTICATED_CHAR;
		} else {
			return FBAConstants.FORGOT_PWD_STATUS_USER_VALIDATED_CHAR;
		}
	}

	@Override
	public void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		final FBATransactionContext ebContext = (FBATransactionContext) objContext;
		LAFRequestType requestType = (LAFRequestType) ebContext
				.getFromContextData(new FEBAUnboundString("REQUEST_TYPE"));

		boolean isPasswordSet = false;
		boolean isUserEnabled = false;
		final ILoginAltFlowVO loginAltFlowVO = (ILoginAltFlowVO) objInputOutput;
		CUSRInfo cusrInfo = null;

		// FIX for Forgot Password functionality for retail,corporate and RM
		// user
		char userFlag = getUserFlag(objContext);

		LoginAltFlowNavigationUtility.getInstance().vaidateLGINSessionEntry(ebContext,
				new UserSignOnStatusFlag(userFlag));

		final PasswordVO passwordVO = loginAltFlowVO.getChangePwdVO();
		UserIDExpireAuthUtility userIDExpireAuthUtility = new UserIDExpireAuthUtility();
		if (userIDExpireAuthUtility.isPWDChangeNotRequiredForSelfEnableFlow(objContext, passwordVO)) {
			// do nothing as this flow does not require pwd to be modified.
		} else {
			if (!requestType.toString().equals("FPONU")) {
				PasswordChangeUtility.getInstance().update((FBATransactionContext) objContext, passwordVO);
				PasswordChangeUtility.getInstance().setBankName(objContext, passwordVO);
				isPasswordSet = true;
			}

			else {

				SetPasswordHostInvokerUtility.getInstance().resetPasswordHostInvoker(objContext, passwordVO);

				passwordVO.setTxnFlg(new YNFlag(EBankingConstants.YES));
				passwordVO.setSignOnFlg(new YNFlag(EBankingConstants.NO));
				passwordVO.setTxnNewPwd(passwordVO.getSignOnNewPwd());
				passwordVO.setTxnDupNewPwd(passwordVO.getSignOnDupNewPwd());
				passwordVO.setSignOnNewPwd("");
				passwordVO.setSignOnDupNewPwd("");

				SetPasswordHostInvokerUtility.getInstance().resetPasswordHostInvoker(objContext, passwordVO);
				isPasswordSet = true;
			}

		}

		cusrInfo = CUSRTableUtility.getInstance().getCUSRInfo(ebContext);

		UserIDER userIDER = (UserIDER) FEBAAVOFactory
				.createInstance("com.infosys.fentbase.types.entityreferences.UserIDER");

		userIDER.setOrgId(cusrInfo.getOrgId());
		userIDER.setUserId(cusrInfo.getUserId());
		userIDER.setUserType(cusrInfo.getUserType());
		userIDER.setCFName(cusrInfo.getCFName());
		userIDER.setCLName(cusrInfo.getCLName());

		if (userIDExpireAuthUtility.checkIfSelfFlowEnableIsApplicableForOnline(ebContext)
				&& userIDExpireAuthUtility.isUserallowedToEnableID(ebContext, cusrInfo, userIDER)) {

			CUSRTAO cusrTAO = new CUSRTAO(ebContext);
			cusrTAO.associateLoginAllowed(new YNFlag("Y"));
			cusrTAO.associateLoginDate(new FEBADate(DateUtil.getCurrentDate(ebContext)));
			cusrTAO.associateBankId(ebContext.getBankId());
			cusrTAO.associateOrgId(cusrInfo.getOrgId());
			cusrTAO.associateUserId(cusrInfo.getUserId());
			cusrTAO.associateCookie(cusrInfo.getCookie());
			try {
				cusrTAO.update(ebContext);
			} catch (FEBATableOperatorException e1) {
				Logger.logError("Exception e" + e1);
			}
			cusrInfo = CUSRTableUtility.getInstance().getCUSRInfo(ebContext);
			isUserEnabled = true;
		}

		/* If user wants to change only the sign on password */
		if (passwordVO.getSignOnFlg().equals(new YNFlag(FBAConstants.CHAR_Y))) {
			if (cusrInfo.getLoginAllowed().equals(new YNFlag(FBAConstants.CHAR_N))) {
				objContext.getBussinessInfo().initialize();
				objContext.addBusinessInfo(new FEBAUnboundInt(FBAErrorCodes.PLS_CONTACT_BANK_ADMIN_TO_COMPLETE_PROCESS),
						new FEBAUnboundString("Successful But"),
						new FEBAUnboundString("Please Contact Bank Administrator To Complete The Process"));
			} else if (userIDExpireAuthUtility.checkIfSelfFlowEnableIsApplicableForOnline(ebContext) && isUserEnabled) {
				if (isPasswordSet) {
					objContext.getBussinessInfo().initialize();
					objContext.addBusinessInfo(new FEBAUnboundInt(FBAErrorCodes.USR_ENABLED_WITH_PWD_CHANGE),
							new FEBAUnboundString("User ID enabled successfully.Password(s) reset successfully."),
							new FEBAUnboundString("User ID enabled successfully.Password(s) reset successfully."));
				} else {
					objContext.getBussinessInfo().initialize();
					objContext.addBusinessInfo(new FEBAUnboundInt(FBAErrorCodes.USR_ENABLED_WO_PWD_CHANGE),
							new FEBAUnboundString("User ID enabled successfully.Please login with existing password."),
							new FEBAUnboundString("User ID enabled successfully.Please login with existing password."));
				}
			}
		}
		// Added for ticket id 787181
		if (ebContext.getRequestType().getValue()
				.equalsIgnoreCase(LoginAltFlowConstants.FIRST_TIME_USER_PASSWORD_SET_REQUEST)
				|| ebContext.getRequestType().getValue().equalsIgnoreCase(LoginAltFlowConstants.RM_SET_PASSWORD)
				|| ebContext.getRequestType().getValue()
						.equalsIgnoreCase(LoginAltFlowConstants.FORGOT_PWD_UX3_ONLINE_REQUEST)) {

			CUSRTAO cusrtao = null;
			if (ebContext.isUnifiedLoginSession()) {
				UserIDER userIder = IUSRTableUtility.getInstance().getDefaultRelationship(ebContext);
				cusrtao = CUSRTableUtility.getInstance().getCUSRTAO(ebContext, userIder);
				cusrInfo = CUSRTableUtility.getInstance().getUserRecord(ebContext, userIder.getUserId(),
						userIder.getOrgId(), ebContext.getBankId());
			} else {
				cusrtao = CUSRTableUtility.getInstance().getCUSRTAO(ebContext);
				cusrInfo = CUSRTableUtility.getInstance().getCUSRInfo(ebContext);
			}
			cusrtao.associateCookie(cusrInfo.getCookie());
			try {
				cusrtao.update(objContext);
			} catch (FEBATableOperatorException e) {
				throw new CriticalException(objContext, FBAIncidenceCodes.COULD_NOT_UPDATE_INTO_CUSR,
						FBAErrorCodes.LIMITED_FUNCTIONALITY_ERROR);
			}
		}
	}

}
