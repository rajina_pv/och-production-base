package com.infosys.custom.ebanking.user.service;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureCriteriaVO;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;

public class CustomLoanProductMaintenanceServiceFetchImpl extends AbstractLocalListInquiryTran{

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {
		CustomLoanTenureCriteriaVO customLoanTenureCritVO = (CustomLoanTenureCriteriaVO) objQueryCrit;
		queryOperator.associate("configType",customLoanTenureCritVO.getConfigType());
		queryOperator.associate("bankId", context.getBankId());
		queryOperator.associate("selectedAmount", customLoanTenureCritVO.getSelectedAmount());
		queryOperator.associate("productCode", customLoanTenureCritVO.getProductCode());
		queryOperator.associate("prodCategory", customLoanTenureCritVO.getProdCategory());
		queryOperator.associate("merchantId", customLoanTenureCritVO.getMerchantId());
		queryOperator.associate("tenor", customLoanTenureCritVO.getTenor());
		queryOperator.associate("productSubCategory", customLoanTenureCritVO.getProductSubCategory());
	}
	@Override
	protected final void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
					throws BusinessException, BusinessConfirmation, CriticalException {
		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			LogManager.logDebug(be.getDispMessage());

		}
	}

	@Override
	public void postProcess(
			FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM)
					throws BusinessException,
					BusinessConfirmation,
					CriticalException {
			/*
			 * 
			 */

	}
	@Override	
	/**
	 * 
	 * Associating DAL CustomLoanProductDetailsListDAL.xml
	 * @see CustomLoanProductDetailsListDAL.xml
	 */
	public String getQueryIdentifier(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_LOAN_TENURE_DETAILS_MAINTENANCE;

	}

	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
	BusinessConfirmation, CriticalException {
		CustomLoanTenureCriteriaVO criteriaVO = (CustomLoanTenureCriteriaVO)objInputOutput;

		FEBAValItem[] vls=null;
		String configType = ""; 
		configType = criteriaVO.getConfigType().getValue();

		if(!(null==configType || configType.length()==0)){
			
			FEBAHashList resultList = (FEBAHashList) AppDataManager.getList(
					objContext, FBAConstants.COMMONCODE_CACHE, 
					FBAConstants.codeType+FBAConstants.EQUAL_TO+CustomResourceConstants.LOAN_CONFIG_TYPE);
			CommonCodeVO commonCodeVO=null;
			Boolean isValidConfigType = false;

			for (Object obj : resultList) {
				commonCodeVO = (CommonCodeVO) obj;
				if (commonCodeVO.getCommonCode().toString().trim()
						.equalsIgnoreCase(configType)){
					isValidConfigType=true;
					break;
				}
			}
			if(!isValidConfigType){
				throw new BusinessException(
						true,
						objContext,
						CustomEBankingIncidenceCodes.CLNSIM_INVALID_LOAN_CONFIG_TYPE,
						"Config Type is invalid",
						null,
						CustomEBankingErrorCodes.INVALID_LOAN_CONFIG_TYPE,
						null);
			}
		}

		return new FEBAValItem[] {};
	}}

