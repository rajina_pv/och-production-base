package com.infosys.custom.ebanking.user.service;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollAccountFetchCriteriaVO;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;

public class CustomPayrollAccountDetailsServiceFetchImpl extends AbstractLocalListInquiryTran{

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {
		CustomPayrollAccountFetchCriteriaVO customPayrollAccountFetchCriteriaVO = (CustomPayrollAccountFetchCriteriaVO) objQueryCrit;
		queryOperator.associate("applicationStatus",customPayrollAccountFetchCriteriaVO.getApplicationStatus());
		queryOperator.associate("userId", customPayrollAccountFetchCriteriaVO.getUserId());
		queryOperator.associate("bankId", context.getBankId());
		
	}
	@Override
	protected final void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
					throws BusinessException, BusinessConfirmation, CriticalException {
		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			LogManager.logDebug(be.getDispMessage());

		}
	}

	@Override
	public void postProcess(
			FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM)
					throws BusinessException,
					BusinessConfirmation,
					CriticalException {
			/*
			 * 
			 */

	}
	@Override	
	/**
	 * 
	 * Associating DAL CustomLoanProductDetailsListDAL.xml
	 * @see CustomLoanProductDetailsListDAL.xml
	 */
	public String getQueryIdentifier(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_PAYROLL_ACCOUNT_DETAILS_FETCH;

	}

	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
	BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}}

