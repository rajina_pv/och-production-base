
package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLCDTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.CUPRTAO;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLCDInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.tao.info.CUPRInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.ChannelId;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.fentbase.tao.CSIPTAO;
import com.infosys.fentbase.tao.info.CSIPInfo;

/**
 * This service is used for privy registration..
 * 
 */

public class CustomPrivyServiceRegisterImpl extends AbstractHostUpdateTran {

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	/**
	 *
	 * This method is used to process local data
	 *
	 */

	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}

	/**
	 * This method is used to make the HOST calls
	 *
	 */

	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		boolean isEKYCEnabled = true;
		
		String isEKYCEnabledProperty = PropertyUtil.getProperty("IS_EKYC_ENABLED", objContext);
		String asliApprovalRatePRPM = PropertyUtil.getProperty("ASLIRI_APPROVAL_RATE", objContext);
		String isASLIEnabledProperty = PropertyUtil.getProperty("IS_ASLIRI_ENABLED", objContext);
				
		double selfieApprovalRate = 0.0;
		
		if(null!=isEKYCEnabledProperty && isEKYCEnabledProperty.equalsIgnoreCase("N")){
			isEKYCEnabled=false;
		}
		final EBTransactionContext ebContext = (EBTransactionContext) objContext;
		CustomPrivyRegistrationVO privyRegVO = (CustomPrivyRegistrationVO) objInputOutput;
		ApplicationNumber applicationId = privyRegVO.getApplicationId();
		String applicationIdStr = applicationId.toString();

		String regStage = validateAlreadyRegistered(objContext);

		if(null==regStage || regStage.equalsIgnoreCase("")){
			if (applicationIdStr != "" && !(applicationIdStr.isEmpty())) {
				try {
					populateEKYCRegVO(ebContext, privyRegVO);
				} catch (FEBATableOperatorException e) {
					throw new FatalException(ebContext, "Unable to select LAAD or  LACD record",
							CustomEBankingIncidenceCodes.UNABLE_TO_SELECT_LAAD_LACD_RECORD, e);
				}
			}
			validateMandatory(objContext, privyRegVO);

			if(null !=isEKYCEnabledProperty && isEKYCEnabledProperty.equalsIgnoreCase("Y")){
				
				EBHostInvoker.processRequest(objContext,CustomEBRequestConstants.CUSTOM_EKYC_REGISTER_REQUEST,
				privyRegVO);
		
				if (!privyRegVO.getStatus().getValue().equalsIgnoreCase("SUCCEED")
						|| privyRegVO.getMessage().getValue().equalsIgnoreCase("NOT_VERIFIED")) {
					if(isEKYCEnabled){
				throw getBusinessException(objContext, null, "EKYC registration failed.");
					}
				}
			}
			
			if(null != isASLIEnabledProperty && isASLIEnabledProperty.equalsIgnoreCase("Y")){
				
				// Added for ASLI RI - start
				EBHostInvoker.processRequest(objContext,CustomEBRequestConstants.CUSTOM_ASLIRI_REQUEST,
						privyRegVO);
								
				if(null !=privyRegVO){
					
					if(FEBATypesUtility.isNotNullOrBlank(privyRegVO.getStatus()) && !privyRegVO.getStatus().getValue().equalsIgnoreCase("200"))
					  {
						
						throw new BusinessException(true, ebContext, "ASLIRI03",			  
							"Internal service error, try in 15 minutes.", null, 211103, null); 
						
				      } else if (FEBATypesUtility.isNotNullOrBlank(privyRegVO.getStatus()) && privyRegVO.getStatus().getValue().equalsIgnoreCase("200")) {
										    	  
							   if(FEBATypesUtility.isNotNullOrBlank(privyRegVO.getSelfiePercentage())){
									
									selfieApprovalRate = Double.valueOf(privyRegVO.getSelfiePercentage().getValue());
									double asliApprovalRateConfig = Double.valueOf(asliApprovalRatePRPM);

									boolean isApprovalRateFlag = (selfieApprovalRate > asliApprovalRateConfig) ? true : false;
									
									if(!isApprovalRateFlag){ 
										
										throw new BusinessException(true, ebContext, "ASLIRI02",			  
												"Gagal verifikasi foto.Selfie_photo do not belong to the same individual.", null, 211102, null);	
									}
									
								} else if(FEBATypesUtility.isNotNullOrBlank(privyRegVO.getNikErr()) && FEBATypesUtility.isNotNullOrBlank(privyRegVO.getSelfiePhotoErr())
									   && privyRegVO.getNikErr().getValue().equalsIgnoreCase("invalid") 
									   && privyRegVO.getSelfiePhotoErr().getValue().equalsIgnoreCase("invalid")){
						  		
								   throw new BusinessException(true, ebContext, "ASLIRI04",			  
										"NIK and selfie_photo do not belong to the same individual.", null, 211104, null);
								   
						  	  	} else if(FEBATypesUtility.isNotNullOrBlank(privyRegVO.getSelfiePhotoErr()) 
						  	  			&& privyRegVO.getSelfiePhotoErr().getValue().equalsIgnoreCase("no face detected")){
								   
								   throw new BusinessException(true, ebContext, "ASLIRI05",			  
											"NIK registered. Face in the photo entry can not be detected.", null, 211105, null);
								   
							   }else if(FEBATypesUtility.isNotNullOrBlank(privyRegVO.getErrMessage()) 
						  	  			&& privyRegVO.getErrMessage().getValue().equalsIgnoreCase("Data not found")){
								   
								   throw new BusinessException(true, ebContext, "ASLIRI06",			  
											"NIK not registered. Selfie_photo is null because NIK not registered so the data can not be verified.", null, 211106, null);
							   }
					 }else{

							throw new BusinessException(true, ebContext, "ASLIRI03",			  
								"Internal service error, try in 15 minutes.", null, 211103, null); 
					}					
					
				}else{

					throw new BusinessException(true, ebContext, "ASLIRI03",			  
						"Internal service error, try in 15 minutes.", null, 211103, null); 
				}
				// Added for ASLI RI - end
				
			}
			
		}else if(regStage.equalsIgnoreCase(CustomEBConstants.EKYC_COM)){
			privyRegVO.setStage(regStage);
			if (applicationIdStr != "" && !(applicationIdStr.isEmpty())) {
				try {
					populatePRIVYRegVO(ebContext, privyRegVO);
				} catch (FEBATableOperatorException e) {
					throw new FatalException(ebContext, "Unable to select LAAD or  LACD record",
							CustomEBankingIncidenceCodes.UNABLE_TO_SELECT_LAAD_LACD_RECORD, e);
				}
			}

			EBHostInvoker.processRequest(objContext, CustomEBRequestConstants.CUSTOM_PRIVY_REGISTER_REQUEST,
					privyRegVO);

			if (!FEBATypesUtility.isNotNullOrBlank(privyRegVO.getPrivyId())) {
				throw getBusinessException(objContext, null, "privy registration failed.");
			}
		}else if(regStage.equalsIgnoreCase(CustomEBConstants.PRIVY_COM)){
			privyRegVO.setStage(regStage);
			throw getBusinessException(objContext, null, "record already exits");
		}
	}

	private String validateAlreadyRegistered(FEBATransactionContext objContext)
			throws BusinessException {
		CUPRInfo uprtInfo = null;
		try {
			uprtInfo = CUPRTAO.select(objContext, objContext.getBankId(),
					new FEBAUnboundString(objContext.getUserId().getValue()));
		} catch (FEBATypeSystemException e) {
			throw getBusinessException(objContext, null, e.getMessage());
		} catch (FEBATableOperatorException e) {
			if (EBankingErrorCodes.RECORD_DOES_NOT_EXIST != e.getErrorCode()) {
				throw getBusinessException(objContext, null, e.getMessage());
			}
		}
		if(uprtInfo != null){
			if(uprtInfo.getStage().toString().equalsIgnoreCase(CustomEBConstants.EKYC_COM)){
				return CustomEBConstants.EKYC_COM;
			}else if(uprtInfo.getStage().toString().equalsIgnoreCase(CustomEBConstants.PRIVY_COM)){
				return CustomEBConstants.PRIVY_COM;
			}
		}
		return null;
	}

	private void validateMandatory(FEBATransactionContext ebContext, CustomPrivyRegistrationVO privyRegVO)
			throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();
		if (!FEBATypesUtility.isNotBlankLong(privyRegVO.getApplicationId())) {
			valError.add(getBusinessExceptionVO(ebContext, null, "application id is mandatory"));
		}
		if (!FEBATypesUtility.isNotNullOrBlank(privyRegVO.getKTP())) {
			valError.add(getBusinessExceptionVO(ebContext, null, "KTP is mandatory"));
		}
		if (!FEBATypesUtility.isNotNullOrBlank(new FEBAUnboundString(privyRegVO.getSelfie().toString()))) {
			valError.add(getBusinessExceptionVO(ebContext, null, "File1 is mandatory"));
		}
		if (!valError.isEmpty()) {
			throw new BusinessException(ebContext, valError);
		}
	}

	private void populateEKYCRegVO(EBTransactionContext ebContext, CustomPrivyRegistrationVO privyRegVO)
			throws FEBATableOperatorException {
		CLADInfo laadInfo = CLADTAO.select(ebContext, ebContext.getBankId(), privyRegVO.getApplicationId());
		CLCDInfo lacdInfo = CLCDTAO.select(ebContext, ebContext.getBankId(), privyRegVO.getApplicationId());
		String bloodGroup = "-";
		String citizenship = "Indonesia";
		privyRegVO.setEmail(laadInfo.getEmailId());
		privyRegVO.setKTP(laadInfo.getNationalId());
		privyRegVO.setName(laadInfo.getName());
		privyRegVO.setPlaceOfBirth(laadInfo.getPlaceOfBirth());
		privyRegVO.setRtrw(laadInfo.getAddrLine2().toString() + "/" + laadInfo.getAddrLine3().toString());
		privyRegVO.setGender(laadInfo.getGender());
		privyRegVO.setReligion(laadInfo.getReligion());
		privyRegVO.setBloodGroup(bloodGroup);
		privyRegVO.setCitizenship(citizenship);
		privyRegVO.setMaritalStatus(lacdInfo.getMaritalStatus());
		privyRegVO.setDateOfBirth(laadInfo.getDateOfBirth());
		privyRegVO.setKtpAddress(laadInfo.getAddrLine1());
		privyRegVO.setProvince(laadInfo.getState());
		privyRegVO.setCity(laadInfo.getCity());
		privyRegVO.setDistrict(laadInfo.getAddrLine4());
		privyRegVO.setKelurahan(laadInfo.getAddrLine5());
		privyRegVO.setMerchantKey(PropertyUtil.getProperty("EKYC_API_KEY", ebContext));
		privyRegVO.setUserId(ebContext.getUserId().getValue());
	}
	private void populatePRIVYRegVO(EBTransactionContext ebContext, CustomPrivyRegistrationVO privyRegVO)
			throws FEBATableOperatorException {
		CLADInfo laadInfo = CLADTAO.select(ebContext, ebContext.getBankId(), privyRegVO.getApplicationId());
		CLCDInfo lacdInfo = CLCDTAO.select(ebContext, ebContext.getBankId(), privyRegVO.getApplicationId());
		CUPRInfo cuprInfo = CUPRTAO.select(ebContext, ebContext.getBankId(), new FEBAUnboundString(ebContext.getUserId().getValue()));
		CLPAInfo clpaInfo = CLPATAO.select(ebContext, ebContext.getBankId(), privyRegVO.getApplicationId());

		CSIPInfo csipInfo = CSIPTAO.select(ebContext, ebContext.getBankId(), new ChannelId("I"), ebContext.getIndividualId());
		String bloodGroup = "-";
		String citizenship = "Indonesia";
		privyRegVO.setEmail(laadInfo.getEmailId());
		privyRegVO.setKTP(laadInfo.getNationalId());
		privyRegVO.setName(clpaInfo.getNameWl());
		privyRegVO.setPlaceOfBirth(clpaInfo.getPlaceOfBirthWl());
		privyRegVO.setRtrw(laadInfo.getAddrLine2().toString() + "/" + laadInfo.getAddrLine3().toString());
		privyRegVO.setGender(new CommonCode(clpaInfo.getGenderWl().getValue()));
		privyRegVO.setReligion(laadInfo.getReligion());
		privyRegVO.setBloodGroup(bloodGroup);
		privyRegVO.setCitizenship(citizenship);
		privyRegVO.setMobileNum(csipInfo.getPrincipalId().getValue());
		privyRegVO.setMaritalStatus(lacdInfo.getMaritalStatus());
		privyRegVO.setDateOfBirth(clpaInfo.getBirthDateWl());
		privyRegVO.setKtpAddress(laadInfo.getAddrLine1());
		privyRegVO.setProvince(laadInfo.getState());
		privyRegVO.setCity(laadInfo.getCity());
		privyRegVO.setDistrict(laadInfo.getAddrLine4());
		privyRegVO.setKelurahan(laadInfo.getAddrLine5());
		privyRegVO.setMerchantKey(PropertyUtil.getProperty("PRIVY_MERCHANT_KEY", ebContext));
		privyRegVO.setUserId(ebContext.getUserId().getValue());
		privyRegVO.setSelfie(cuprInfo.getPhotoObj1());
		privyRegVO.setFile2(cuprInfo.getPhotoObj2());
	}

	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam[] { new AdditionalParam("message", msg) });
	}

	private BusinessException getBusinessException(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessException(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg, errorField,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam[] { new AdditionalParam("message", msg) });
	}

	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		final EBTransactionContext ebContext = (EBTransactionContext) objContext;
		CustomPrivyRegistrationVO privyRegistrationVO = (CustomPrivyRegistrationVO) objInputOutput;
		String regStage = privyRegistrationVO.getStage().getValue();
		if(null==regStage || regStage.equalsIgnoreCase("")){
			insertEKYC(ebContext,privyRegistrationVO);
			new CustomLoanHistoryUpdateUtil().updateHistory(privyRegistrationVO.getApplicationId(), CustomEBConstants.EKYC_COM, "EKYC Registration Completed",objContext);
		}else if(regStage.equalsIgnoreCase(CustomEBConstants.EKYC_COM)){
			updatePRIVY(ebContext,privyRegistrationVO);
			new CustomLoanHistoryUpdateUtil().updateHistory(privyRegistrationVO.getApplicationId(), CustomEBConstants.PRIVY_COM, "PRIVY Registration Completed",objContext);
		}

	}
	private void insertEKYC(FEBATransactionContext ebContext, CustomPrivyRegistrationVO privyRegistrationVO)
			throws BusinessException {
		CUPRTAO uprtTAO = new CUPRTAO(ebContext);
		try {

			uprtTAO.associateBankId(ebContext.getBankId());
			uprtTAO.associateUserId(new FEBAUnboundString(ebContext.getUserId().getValue()));
			uprtTAO.associateKtpNum(privyRegistrationVO.getKTP());
			uprtTAO.associatePrivyId(new FEBAUnboundString("PRIVY"));
			if(null==privyRegistrationVO.getStage()|| privyRegistrationVO.getStage().getValue().equalsIgnoreCase("")){
				uprtTAO.associateStage(new FEBAUnboundString(CustomEBConstants.EKYC_COM));
			}else if(privyRegistrationVO.getStage().toString().equalsIgnoreCase(CustomEBConstants.EKYC_COM)){
				uprtTAO.associateStage(new FEBAUnboundString("DIG_SIGN_COM"));
			}
			if(null!=privyRegistrationVO.getSelfie()){
				uprtTAO.associatePhotoObj1(privyRegistrationVO.getSelfie());
			}
			if(null!=privyRegistrationVO.getFile2()){
				uprtTAO.associatePhotoObj2(privyRegistrationVO.getFile2());
			}
			uprtTAO.insert(ebContext);
			updateCLAT(ebContext, privyRegistrationVO, CustomEBConstants.EKYC_COM);
		} catch (FEBATableOperatorException e) {
			throw new BusinessException(ebContext, CustomEBankingIncidenceCodes.UNABLE_TO_CREATE_UPRT_RECORD, "Unable to create UPRT record",
					CustomEBankingErrorCodes.UNABLE_TO_CREATE_UPRT_RECORD, e);

		}
	}
	private void updatePRIVY(FEBATransactionContext ebContext, CustomPrivyRegistrationVO privyRegistrationVO)
			throws BusinessException {
		CUPRInfo uprtInfo = null;
		try {
			uprtInfo = CUPRTAO.select(ebContext, ebContext.getBankId(),
					new FEBAUnboundString(ebContext.getUserId().getValue()));
		} catch (FEBATypeSystemException e) {
			throw getBusinessException(ebContext, null, e.getMessage());
		} catch (FEBATableOperatorException e) {
			if (EBankingErrorCodes.RECORD_DOES_NOT_EXIST != e.getErrorCode()) {
				throw getBusinessException(ebContext, null, e.getMessage());
			}
		}
		CUPRTAO uprtTAO = new CUPRTAO(ebContext);
		try {
			uprtTAO.associateBankId(ebContext.getBankId());
			uprtTAO.associateUserId(new FEBAUnboundString(ebContext.getUserId().getValue()));
			uprtTAO.associatePrivyId(privyRegistrationVO.getPrivyId());
			uprtTAO.associatePrivyUserName(privyRegistrationVO.getName());
			if(null==privyRegistrationVO.getStage()){
				uprtTAO.associateStage(new FEBAUnboundString(CustomEBConstants.EKYC_COM));
			}else if(privyRegistrationVO.getStage().toString().equalsIgnoreCase(CustomEBConstants.EKYC_COM)){
				uprtTAO.associateStage(new FEBAUnboundString(CustomEBConstants.PRIVY_COM));
			}
			if(null!=uprtInfo){
				uprtTAO.associateCookie(uprtInfo.getCookie());
			}
			uprtTAO.update(ebContext);
			updateCLAT(ebContext, privyRegistrationVO, CustomEBConstants.PRIVY_COM);
			privyRegistrationVO.setStage(CustomEBConstants.PRIVY_COM);
		} catch (FEBATableOperatorException e) {
			throw new BusinessException(ebContext, CustomEBankingIncidenceCodes.UNABLE_TO_CREATE_UPRT_RECORD, "Unable to create UPRT record",
					CustomEBankingErrorCodes.UNABLE_TO_CREATE_UPRT_RECORD, e);

		}
	}
	private void updateCLAT(FEBATransactionContext ebContext, CustomPrivyRegistrationVO privyRegistrationVO, String regStatus)
			throws BusinessException {
		CLATInfo clatInfo = null;
		try {
			clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), privyRegistrationVO.getApplicationId());
		} catch (FEBATableOperatorException e) {
			throw new FatalException(ebContext, "Unable to select LAAD or  LACD record",
					CustomEBankingIncidenceCodes.UNABLE_TO_SELECT_LAAD_LACD_RECORD, e);
		}
		CLATTAO clatTAO = new CLATTAO(ebContext);
		try {
			clatTAO.associateBankId(ebContext.getBankId());
			clatTAO.associateUserId(ebContext.getUserId());
			clatTAO.associateApplicationId(privyRegistrationVO.getApplicationId());
			clatTAO.associateApplicationStatus(new CommonCode(regStatus));
			clatTAO.associateCookie(clatInfo.getCookie());
			clatTAO.update(ebContext);
		} catch (FEBATableOperatorException e) {
			throw new BusinessException(ebContext, CustomEBankingIncidenceCodes.UNABLE_TO_CREATE_UPRT_RECORD, "Unable to create UPRT record",
					CustomEBankingErrorCodes.UNABLE_TO_CREATE_UPRT_RECORD, e);

		}
	}
}
