package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqEnquiryVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomSilverlakeAcctCallServiceAcctInquiryImpl extends AbstractHostInquiryTran{

	String accountStatusOpen = "Open";
	
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext context, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomSilverlakeAccountInqEnquiryVO enquiryVO = (CustomSilverlakeAccountInqEnquiryVO) objInputOutput;
		try {
			EBHostInvoker.processRequest(context, CustomEBRequestConstants.SILVERLAKE_ACCT_INQUIRY, enquiryVO);
		} catch (BusinessException be) {
			throw new BusinessException(true, context, CustomEBankingIncidenceCodes.SILVERLAKE_ACCT_HOST_CALL_ERROR,
					"An unexpected exception occurred during silverlake account inquiry",null,
					CustomEBankingErrorCodes.SILVERLAKE_ACCT_HOST_CALL_ERROR, null);
		}
		
		
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomSilverlakeAccountInqEnquiryVO enquiryVO = (CustomSilverlakeAccountInqEnquiryVO) objInputOutput;
		
		CustomSilverlakeAccountInqDetailsVO detailsVO = enquiryVO.getDetails();
		String acctStatus = detailsVO.getStatusRekening().toString();
		// expiry date validation
		if (!(acctStatus.equalsIgnoreCase("Open"))) {
			
				throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.ACCOUNT_NOT_OPEN,
						"Account is not open.", null, CustomEBankingErrorCodes.ACCOUNT_NOT_OPEN, null);
			}
		

	}
}
