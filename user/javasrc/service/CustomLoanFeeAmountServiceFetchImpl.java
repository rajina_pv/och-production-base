package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanFeeAmtEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanFeeAmountServiceFetchImpl extends AbstractHostInquiryTran {
		
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {					
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext febaContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) febaContext;
		CustomLoanFeeAmtEnquiryVO enquiryVO = (CustomLoanFeeAmtEnquiryVO) objInputOutput;
		System.out.println("CustomLoanFeeAmountServiceFetchImpl::enquiryVO::"+enquiryVO);
		
		try {
		
			EBHostInvoker.processRequest(context, "CustomFetchLoanFeeAmtRequest", enquiryVO);
			
		} catch (BusinessException be) {
			be.printStackTrace();
		throw new BusinessException(context, "LNINQ003",
				"An unexpected exception occurred during loan list retrieval",
				211082, be);
	    }catch (Exception e) {
	    	e.printStackTrace();
			throw new BusinessException(context, EBIncidenceCodes.HIF_HOST_NOT_AVAILABLE, "The host does not exist.",
					EBankingErrorCodes.HOST_NOT_AVAILABLE);
		}
		if(enquiryVO.getDetails() == null)
		{
			throw new BusinessException(context, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
					"No Fee Amounts mapped for the Plafond amounts", EBankingErrorCodes.NO_RECORDS_FOUND);		
		}
	}
	

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// To Do Code
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {	
		// To Do Code
	}

}
