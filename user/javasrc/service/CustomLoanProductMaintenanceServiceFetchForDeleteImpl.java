/*
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.tao.CLPMTAO;
import com.infosys.custom.ebanking.tao.info.CLPMInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;

/**
 * This class will fetch the user details to be edited
 * 
 * @author
 * @version 1.0
 * @since FEBA 2.0
 */

public class CustomLoanProductMaintenanceServiceFetchForDeleteImpl extends AbstractLocalInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	/**
	 * This method is used to fetch the details of the User for modification.
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTransactionWM
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @author
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran#process(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since
	 */
	@Override
	protected void process(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;
		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		criteria.setDependencyFlag(FBAConstants.NO);
		try {
			CLPMInfo clpmInfo = CLPMTAO.select(pObjContext, pObjContext.getBankId(),
					customLoanProductDetailsVO.getLnRecId());

			updateVOWithValues(customLoanProductDetailsVO, clpmInfo);

			CustomLoanMaintenanceTableOperationUtil.processCLPM(pObjContext, customLoanProductDetailsVO,
					FEBATMOConstants.TMO_OPERATION_SELECT_FOR_DELETE);

			criteria.setDependencyFlag(FBAConstants.YES);

		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_SELECT_FOR_DELETE + tmoExp.getErrorCode());
			throw new BusinessException(pObjContext, FBAIncidenceCodes.USER_SELECT_FOR_DELETE_TMO_EXCEPTION, errorCode,
					tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {
			throw new CriticalException(pObjContext, FBAIncidenceCodes.TAO_EXP_USER_FETCH_DELETE,
					tblOperExp.getMessage(), tblOperExp.getErrorCode());
		}
	}

	private void updateVOWithValues(CustomLoanProductDetailsVO customLoanProductDetailsVO, CLPMInfo clpmInfo) {
		customLoanProductDetailsVO.setMerchantID(clpmInfo.getMerchantId());
		customLoanProductDetailsVO.setProductCode(clpmInfo.getProductCode());
		customLoanProductDetailsVO.setProductCategory(clpmInfo.getProductCategory());
		customLoanProductDetailsVO.setProductSubCategory(clpmInfo.getProductSubcategory());
		customLoanProductDetailsVO.setConfigType(clpmInfo.getConfigType());
		customLoanProductDetailsVO.setMinAmount(clpmInfo.getMinAmount());
		customLoanProductDetailsVO.setMaxAmount(clpmInfo.getMaxAmount());
		customLoanProductDetailsVO.setLnRecId(clpmInfo.getLnRecId());
		customLoanProductDetailsVO.setLnPurpose(clpmInfo.getLnPurpose());

	}
}
