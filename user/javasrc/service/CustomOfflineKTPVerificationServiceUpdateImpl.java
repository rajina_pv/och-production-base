package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomOfflineKTPVerificationCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomOfflineKTPVerificationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomOfflineKTPVerificationEnquiryVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomOfflineKTPVerificationServiceUpdateImpl extends AbstractLocalUpdateTran {


	@SuppressWarnings("rawtypes")
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomOfflineKTPVerificationEnquiryVO enquiryVO = (CustomOfflineKTPVerificationEnquiryVO) objInputOutput;
		CustomOfflineKTPVerificationCriteriaVO criteriaVO = enquiryVO.getCriteria();
		String isKTPVerified = "N";
		isKTPVerified = criteriaVO.getIsKTPVerified().toString();
		CustomOfflineKTPVerificationDetailsVO detailsVO = (CustomOfflineKTPVerificationDetailsVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomOfflineKTPVerificationDetailsVO");
		CLATInfo clatInfo = new CLATInfo();
		CLATTAO clatTAO = new CLATTAO(txnContext);
		
		try {
			clatInfo = CLATTAO.select(txnContext, txnContext.getBankId(), criteriaVO.getApplicationId());
			if(clatInfo.getIsKtpVerified().toString().equalsIgnoreCase("Y")){
				throw new BusinessException(true, txnContext, CustomEBankingIncidenceCodes.OFFLINE_KTP_VERIFICATION_FAILED,
						"KTP Already Verified", null, CustomEBankingErrorCodes.OFFLINE_KTP_VERIFICATION_FAILED, null);
			}
			if( !clatInfo.getApplicationStatus().toString().equalsIgnoreCase("CR_SCORE_APR")){
				throw new BusinessException(true, txnContext, CustomEBankingIncidenceCodes.OFFLINE_KTP_VERIFICATION_FAILED,
						"Loan application not in Credit Scoring Approval Stage", null, CustomEBankingErrorCodes.OFFLINE_KTP_VERIFICATION_FAILED, null);
			}
			clatTAO.associateApplicationId(criteriaVO.getApplicationId());
			clatTAO.associateBankId(clatInfo.getBankId());
			clatTAO.associateIsKtpVerified(new FEBAUnboundChar(isKTPVerified));
			clatTAO.associateCookie(clatInfo.getCookie());
			if(isKTPVerified.equals("N")){
				
				//changes for PINANG DASHBOARD KTP rejection case - start			
				int ktpRejCntClat = 0;
				int rejCnt = 0; 
				
				if(null != clatInfo.getKtpRejCnt() && clatInfo.getKtpRejCnt().getValue() != 0){
					
					// Get already existing counter and check and increase if not zero
					ktpRejCntClat = clatInfo.getKtpRejCnt().getValue();
				
					clatTAO.associateKtpRejCnt(new FEBAUnboundInt(++ktpRejCntClat));
					clatTAO.associateApplicationStatus(clatInfo.getApplicationStatus());
					clatTAO.associateIsKtpReuploaded(new FEBAUnboundChar("N"));
					
					if(null != clatInfo.getKtpRejCnt() && ktpRejCntClat == 3){
						clatTAO.associateApplicationStatus(new CommonCode("KTP_REJECT"));
						detailsVO.setResponseStatus("KTP verification Rejected by Pinang Dashboard Admin");
						new CustomLoanHistoryUpdateUtil().updateHistory(criteriaVO.getApplicationId(), "KTP_REJECT", "KTP verification Rejected by Pinang Dashboard Admin", txnContext);
					}
				
				}else{
					// If already not present then increase the counter zero to 1.
					clatTAO.associateKtpRejCnt(new FEBAUnboundInt(++rejCnt));
					clatTAO.associateIsKtpReuploaded(new FEBAUnboundChar("N"));
				}
				//changes for PINANG DASHBOARD KTP rejection case - end
				
				detailsVO.setResponseCode("200");
				
			}else if(isKTPVerified.equals("Y")){
				detailsVO.setResponseStatus("KTP Verification Complete");
				detailsVO.setResponseCode("200");
				new CustomLoanHistoryUpdateUtil().updateHistory(criteriaVO.getApplicationId(), "KTP_VERIFIED", "KTP verified by Pinang Dashboard Admin", txnContext);

			}else{
				throw new BusinessException(true, txnContext, CustomEBankingIncidenceCodes.OFFLINE_KTP_VERIFICATION_FAILED,
						"Invalid Value passed for KTP Flag", null, CustomEBankingErrorCodes.OFFLINE_KTP_VERIFICATION_FAILED, null);
			}

			clatTAO.update(txnContext);
			
			enquiryVO.setDetails(detailsVO);
		} catch (FEBATableOperatorException e) {
			throw new BusinessException(true, txnContext, CustomEBankingIncidenceCodes.OFFLINE_KTP_VERIFICATION_FAILED,
					"Update in table CLAT Failed.", null, CustomEBankingErrorCodes.OFFLINE_KTP_VERIFICATION_FAILED, null);
		} 
	}

}
