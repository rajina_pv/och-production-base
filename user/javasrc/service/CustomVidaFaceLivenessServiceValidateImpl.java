package com.infosys.custom.ebanking.user.service;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomVidaLivenessDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomVidaLivenessEnquiryVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.feba.utils.insulate.ArrayList;

public class CustomVidaFaceLivenessServiceValidateImpl extends AbstractHostUpdateTran {
	/**
	 * This service is used for Vida Liveness face validation..
	 * 
	 */
	
	public static final int SCORE_VALID = 1043;
	public static final int INVALID_PHOTO = 1041;
	public static final int LARGE_PHOTO = 1000;
	public static final int  CORRUPTED_PHOTO = 21004;
	
	
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}
	
	/**
	 *
	 * This method is used to process local data
	 *
	 */
	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}
	
	/**
	 * This method is used to make the HOST calls
	 *
	 */
	@Override
	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		

		final EBTransactionContext ebContext = (EBTransactionContext) objContext;
		CustomVidaLivenessEnquiryVO enqVO = (CustomVidaLivenessEnquiryVO) objInputOutput;
		validateMandatory(objContext, enqVO);
		
		String apigeeUsrName =  PropertyUtil.getProperty("APIGEE_NEW_USERNAME", objContext);
		String apigeePassWord =  PropertyUtil.getProperty("APIGEE_NEW_PASSWORD", objContext);	
		
		String keys = apigeeUsrName + ":" + apigeePassWord;
		Base64.Encoder enc= Base64.getEncoder();
		
		try {
			String encAccessToken =enc.encodeToString(keys.getBytes("UTF-8"));
			if(null != encAccessToken){
				enqVO.getCriteria().setAccessToken(encAccessToken);
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		try {
				EBHostInvoker.processRequest(objContext,
						CustomEBRequestConstants.CUSTOM_VIDA_LIVENESS_FACE_VALIDATION_REQUEST, enqVO);
	
		} catch (CriticalException ce) {
			ce.printStackTrace();
			Logger.logError("Exception e" + ce);
		}

	}


	private void validateMandatory(FEBATransactionContext ebContext, CustomVidaLivenessEnquiryVO enqVO)
			throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();

		/*if (!FEBATypesUtility.isNotNullOrBlank(enqVO.getCriteria().getUserId())) {
			valError.add(getBusinessExceptionVO(ebContext, "User Id is mandatory"));
		}*/
		if (!FEBATypesUtility.isNotNullOrBlank(enqVO.getCriteria().getImage())) {
			valError.add(getBusinessExceptionVO(ebContext, "Image is mandatory"));
		}

		if (!valError.isEmpty()) {
			throw new BusinessException(ebContext,
					(com.infosys.feba.utils.insulate.ArrayList<BusinessExceptionVO>) valError);
		}
	}
	
	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null, new AdditionalParam("message", msg));
	}

	private BusinessException getBusinessException(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessException(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg, errorField,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null, new AdditionalParam("message", msg));
	}
	
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		final EBTransactionContext ebContext = (EBTransactionContext) objContext;
		CustomVidaLivenessEnquiryVO enqVO = (CustomVidaLivenessEnquiryVO) objInputOutput;
		
		String prpmScoreVal =  PropertyUtil.getProperty("BRI_VIDA_LIVENESS_SCORE", objContext);
		int scoreValue = Integer.valueOf(prpmScoreVal);
		
		int score = 0;
		
		CustomVidaLivenessDetailsVO detailsVO = enqVO.getDetails();
		
		System.out.println("CustomVidaFaceLivenessServiceValidateImpl::after::HIF::Call::enqVO::"+enqVO);

		System.out.println("CustomVidaFaceLivenessServiceValidateImpl::detailsVO::"+detailsVO);

		if(FEBATypesUtility.isNotNullOrBlank(detailsVO.getScore()) && detailsVO.getCode().getValue() == SCORE_VALID){
			
			float respScoreVal=Float.parseFloat(detailsVO.getScore().getValue());
			score = (int) (respScoreVal * 100);
			
			    if(score <= scoreValue) {
					detailsVO.setMessage("Selfie photo is a live photo");
					detailsVO.setCode("1043");
				}else{
					throw new BusinessException(true, ebContext, "VIDARI53",			  
							"Photo liveness score is below the threshold", null, 211153, null);
				}

		}else if(FEBATypesUtility.isNotNullOrBlank(detailsVO.getScore()) && detailsVO.getCode().getValue() == INVALID_PHOTO)
		{
				detailsVO.setMessage("Photo liveness score is below the threshold");
				detailsVO.setCode("1041");
				
				float respScoreVal=Float.parseFloat(detailsVO.getScore().getValue());
				score = (int) (respScoreVal * 100);
				 if(score != scoreValue) {
					 throw new BusinessException(true, ebContext, "VIDARI53",			  
								"Photo liveness score is below the threshold", null, 211153, null);
				 }
			   
	  	}else if(detailsVO.getCode().getValue() == LARGE_PHOTO)
		{
	  		detailsVO.setMessage("Large Photo");
			detailsVO.setCode("1000");
			   throw new BusinessException(true, ebContext, "VIDARI54",			  
					"An error occurred while processing request.Its a Large Photo.", null, 211154, null);
			   
	  	}else if(detailsVO.getCode().getValue() == CORRUPTED_PHOTO)
		{
	  		detailsVO.setMessage("Corrupted photo");
			detailsVO.setCode("21004");
		    throw new BusinessException(true, ebContext, "VIDARI55",			  
				"Unprocessable Entity.Its a Corrupted photo.", null, 211155, null);
		   
  	  	}

	}
	
}
