package com.infosys.custom.ebanking.user.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CAKDTAO;
import com.infosys.custom.ebanking.tao.CAPWDTAO;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLCDTAO;
import com.infosys.custom.ebanking.tao.CLEDTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CAKDInfo;
import com.infosys.custom.ebanking.tao.info.CAPWDInfo;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLCDInfo;
import com.infosys.custom.ebanking.tao.info.CLEDInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnApplicantDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnContactDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnEmploymentDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.cache.AppDataConstants;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.lists.FEBAListIterator;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.CommonCodeType;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.DescriptionMedium;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBAUnboundDouble;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;
import com.infosys.fentbase.tao.CSIPTAO;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CSIPInfo;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.primitives.AddressLine;
import com.infosys.fentbase.types.primitives.CustomerId;
import com.infosys.fentbase.types.primitives.Name;
import com.infosys.fentbase.types.primitives.PanOrNationalID;
import com.infosys.fentbase.types.valueobjects.CommonCodeCacheVO;


public class CustomCIFAndLoanAcctServiceCreateImpl extends AbstractHostUpdateTran {

	public static final String DISB_IN_PROCESS = "DISB_IN_PROCESS";
	public static final String DOCUMENT_SIGNED = "DOCUMENT_SIGNED";
	public static final String LOAN_CREATED = "LOAN_CREATED";

	

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext ebContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {
		};


	}

	protected void processLocalData(FEBATransactionContext ebContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) objInputOutput;
		CustomLoanApplicationCriteriaVO criteriaVO = enquiryVO.getCriteria();
		CustomLoanApplicationDetailsVO detailsVO = (CustomLoanApplicationDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO");
		FEBAArrayList<CustomLoanApplicationDetailsVO> resultlist = new FEBAArrayList<>();
		try {
			CUSRTAO cusrTAO = new CUSRTAO();
			CorporateId corpId = new CorporateId(criteriaVO.getUserId().toString());
			CUSRInfo cusrInfo = cusrTAO.select(ebContext, ebContext.getBankId(), corpId, criteriaVO.getUserId());
			CustomerId custId = cusrInfo.getCustId();

			CSIPInfo info = CSIPTAO.select(ebContext, ebContext.getBankId(), ebContext.getAccessChannelId(), cusrInfo.getIndividualId());    					

			detailsVO = populateLocalData(ebContext, enquiryVO);
			detailsVO.getLoanApplicantDetails().setMobileNum(new MobileNumber(info.getPrincipalId().toString()));			
			detailsVO.setCustId(custId);
			
			/*
			 * Updating
			 */
			DescriptionMedium codeDescription4 = null;
			DescriptionMedium codeDescription5 = null;
			DescriptionMedium codeDescriptionRLT = null;
			
			try{
				codeDescription4 = (DescriptionMedium) AppDataManager.getValue(ebContext, FBAAppDataConstants.COMMONCODE_CACHE,
						FBAAppDataConstants.COLUMN_CODE_TYPE + FBAConstants.EQUAL_TO + detailsVO.getLoanApplicantDetails().getCity().getValue()
								+ FBAAppDataConstants.SEPERATOR + FBAAppDataConstants.COLUMN_CM_CODE + FBAConstants.EQUAL_TO
								+ detailsVO.getLoanApplicantDetails().getAddressLine4());
				
				codeDescription5 = (DescriptionMedium) AppDataManager.getValue(ebContext, FBAAppDataConstants.COMMONCODE_CACHE,
						FBAAppDataConstants.COLUMN_CODE_TYPE + FBAConstants.EQUAL_TO + detailsVO.getLoanApplicantDetails().getAddressLine4().getValue()
								+ FBAAppDataConstants.SEPERATOR + FBAAppDataConstants.COLUMN_CM_CODE + FBAConstants.EQUAL_TO
								+ detailsVO.getLoanApplicantDetails().getAddressLine5());
				codeDescriptionRLT = (DescriptionMedium) AppDataManager.getValue(ebContext, FBAAppDataConstants.COMMONCODE_CACHE,
						FBAAppDataConstants.COLUMN_CODE_TYPE + FBAConstants.EQUAL_TO + "RLT"
								+ FBAAppDataConstants.SEPERATOR + FBAAppDataConstants.COLUMN_CM_CODE + FBAConstants.EQUAL_TO
								+ detailsVO.getLoanApplicantContactDetails().getEmergencyContactRelation());
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(null!=codeDescription4 && FEBATypesUtility.isNotNullOrBlank(codeDescription4)){
				detailsVO.getLoanApplicantDetails().setAddressLine4(codeDescription4.getValue());
			}
			if(null!=codeDescription5 && FEBATypesUtility.isNotNullOrBlank(codeDescription5)){
				detailsVO.getLoanApplicantDetails().setAddressLine5(codeDescription5.getValue());
			}
			if(null!=codeDescriptionRLT && FEBATypesUtility.isNotNullOrBlank(codeDescriptionRLT)){
				detailsVO.getLoanApplicantContactDetails().setEmergencyContactRelation(codeDescriptionRLT.getValue());
			}
			resultlist.add(detailsVO);
			enquiryVO.setResultList(resultlist);
		} catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		}
	}

	/**
	 * This method is used to make the HOST calls
	 *
	 */

	protected void processHostData(FEBATransactionContext ebContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {


		CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) objInputOutput;
		FEBAArrayList<CustomLoanApplicationDetailsVO> resultlist = enquiryVO.getResultList();
		CustomLoanApplicationDetailsVO detailsVO = null;
		if(resultlist != null) {
			detailsVO = resultlist.get(0);
		}

		if(detailsVO != null) {
			boolean createLoan = true;
			boolean updateCUSRFlag = true;
			
			if(!FEBATypesUtility.isNotNullOrBlank(detailsVO.getCustId())) {
				try
				{
					EBHostInvoker.processRequest(ebContext, CustomEBRequestConstants.CUSTOM_RETAIL_CUST_ADD_REQUEST,
							detailsVO);
				}
				catch(Exception e)
				{ throw new BusinessException(ebContext,
						EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
						"CIF creation failed", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);	
				}
				if(FEBATypesUtility.isNotNullOrBlank(detailsVO.getStatus()) 
						&& detailsVO.getStatus().toString().equalsIgnoreCase("SUCCESS"))
				{
					updateCUSR(ebContext,detailsVO);
					updateCUSRFlag=false;
				}
				else
				{
					throw new BusinessException(ebContext,
							EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
							"CIF creation failed", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);				
				}
			}
			String pinangOrSahabat=PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", ebContext);
			if(pinangOrSahabat!=null && pinangOrSahabat!="")
			{
				if(pinangOrSahabat.equalsIgnoreCase("PINANG")){
					String appliationStatus = detailsVO.getLoanApplnMasterDetails().getApplicationStatus().getValue();
					// Allow user to proceed loan creation request If previous application status is in DOCUMENT_SIGNED stage.
					if(null != appliationStatus && appliationStatus.equals(DOCUMENT_SIGNED)){
						try{
							updateCLATDisbStatus(ebContext,detailsVO);
							EBHostInvoker.processRequest(ebContext, CustomEBRequestConstants.CUSTOM_CREATE_LOAN_ACCNT_REQUEST,
									detailsVO);
						}
						catch(Exception e)
						{
							System.out.println("In Exception ");
							LogManager.logError(ebContext, e, "ERROR");
							//update application status back to DOCUMENT_SIGNED in case any failure from Loan creation system.
							updateCLATDocSignStatus(ebContext,detailsVO);
							throw new BusinessException(ebContext,
									EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
									"Loan creation failed", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
						}
						if(!FEBATypesUtility.isNotNullOrBlank(detailsVO.getStatus()) || !detailsVO.getStatus().toString().equalsIgnoreCase("S"))
						{
							System.out.println("The loan creation is failed. ");
							//update application status back to DOCUMENT_SIGNED in case failure happens from loan creation system.
							updateCLATDocSignStatus(ebContext,detailsVO);
							throw new BusinessException(ebContext,
									EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
									"Loan creation failed", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);

						}
						if(detailsVO.getAcctId() != null) 
						{
							updateCLAT(ebContext,detailsVO);
							detailsVO.getLoanApplnMasterDetails().setApplicationStatus(LOAN_CREATED);
							updateCAKD(ebContext,detailsVO);
						}
						if(updateCUSRFlag){
							updateCUSR(ebContext,detailsVO);
						}
					}else{
						detailsVO.getLoanApplnMasterDetails().setApplicationStatus(DISB_IN_PROCESS);
						System.out.println("Duplicate loan creation request from app.");
						LogManager.logDebug(ebContext,"Duplicate loan creation request from app.");
					}
				}
				if(pinangOrSahabat.equalsIgnoreCase("SAHABAT")){
					try{
						EBHostInvoker.processRequest(ebContext, CustomEBRequestConstants.CUSTOM_CREATE_CREDIT_LIMIT_REQUEST,
								detailsVO);
					}
					catch(Exception e)
					{
						throw new BusinessException(ebContext,
								EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
								"Loan creation failed", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
					}
					if(detailsVO.getAcctId() != null) 
					{
						updateCLAT(ebContext,detailsVO);
					}
					else
					{
						throw new BusinessException(ebContext,
								EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
								"Loan creation failed", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
					}

				}
			}

		}

	}

	private CustomLoanApplicationDetailsVO populateLocalData(FEBATransactionContext ebContext,
			CustomLoanApplicationEnquiryVO enquiryVO) {

		CustomLoanApplicationCriteriaVO criteriaVO = enquiryVO.getCriteria();
		ApplicationNumber applicationId=criteriaVO.getApplicationId();
		CustomLoanApplnContactDetailsVO contactDetailsVO = (CustomLoanApplnContactDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnContactDetailsVO");
		CustomLoanApplnApplicantDetailsVO applicantDetailsVO = (CustomLoanApplnApplicantDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnApplicantDetailsVO");
		CustomLoanApplnDocumentsDetailsVO customDocDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO");
		CustomLoanApplnDocumentsDetailsVO tempDocDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO");
		CustomLoanApplnDocumentsDetailsVO documentDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO");
		CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO");
		CustomLoanApplnDocumentsDetailsVO cusTempDocDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO");
		CustomLoanApplnPayrollAccntDetailsVO payrollAcctDetailsVO = (CustomLoanApplnPayrollAccntDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO");
		CustomLoanApplnEmploymentDetailsVO emplymentDetails = (CustomLoanApplnEmploymentDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnEmploymentDetailsVO");
		CustomLoanApplnMasterDetailsVO masterDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO");
		CustomLoanApplicationDetailsVO detailsVO = (CustomLoanApplicationDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO");

		FEBAUnboundString documentName1= null;
		FEBAUnboundString documentName2 =null;


		try {
			CLADInfo cladInfo=CLADTAO.select(ebContext,ebContext.getBankId(), applicationId);
			CLCDInfo clcdInfo=CLCDTAO.select(ebContext,ebContext.getBankId(), applicationId);
			CLEDInfo cledInfo=CLEDTAO.select(ebContext,ebContext.getBankId(), applicationId);
			CLATInfo clatInfo=CLATTAO.select(ebContext,ebContext.getBankId(), applicationId);
			CLPAInfo clpaInfo=CLPATAO.select(ebContext,ebContext.getBankId(), applicationId);
			CAPWDInfo capwdInfo=CAPWDTAO.select(ebContext, ebContext.getBankId(), applicationId);
			


			FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> resultList = fetchLoanDocList(ebContext,applicationId);
			FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> tempResultList = new FEBAArrayList<>();

			for (int i = 0; i < resultList.size(); i++) {
				cusTempDocDetailsVO = resultList.get(i);
				if(cusTempDocDetailsVO.getDocType().toString().equalsIgnoreCase("KTP")) {
					docDetailsVO.setDocType(cusTempDocDetailsVO.getDocType());
					docDetailsVO.setDocCode(cusTempDocDetailsVO.getDocCode());
					docDetailsVO.setFileSeqNo(cusTempDocDetailsVO.getFileSeqNo());
					docDetailsVO.setDocStorePath(cusTempDocDetailsVO.getDocStorePath());
				} else {
					tempResultList.add(cusTempDocDetailsVO);
				}
			}
			int size = tempResultList.size();
			if(size != 0) {
				for (int i = 0; i < size; i++) {

					if(i<=2) 
					{
						cusTempDocDetailsVO = tempResultList.get(i);    
						if(i+1>size)
						{
							break;
						}
						tempDocDetailsVO = tempResultList.get(i+1);
						documentDetailsVO = tempResultList.get(i+2);
					}

					String saveDirectory = PropertyUtil.getProperty("FEBA_SHARED_SYS_PATH", null);    		
					String docpath = cusTempDocDetailsVO.getDocStorePath().toString();
					String[] strArray=docpath.replaceAll(Pattern.quote(saveDirectory), " ").split(" ");
					int lenght = strArray.length;
					String docname = strArray[lenght-1];
					FEBAUnboundString documentName = new FEBAUnboundString(docname);
					if(tempDocDetailsVO!=null)
					{
						String docpath1 = tempDocDetailsVO.getDocStorePath().toString();
						String[] strArray1=docpath1.replaceAll(Pattern.quote(saveDirectory), " ").split(" ");
						int lenght1 = strArray1.length;
						String docname1 = strArray1[lenght1-1];
						documentName1 = new FEBAUnboundString(docname1);	            	
					}
					if(documentDetailsVO!=null)
					{
						String docpath2 = documentDetailsVO.getDocStorePath().toString();
						String[] strArray2=docpath2.replaceAll(Pattern.quote(saveDirectory), " ").split(" ");
						int lenght2 = strArray2.length;
						String docname2 = strArray2[lenght2-1];
						documentName2 = new FEBAUnboundString(docname2);

					}
					if(size==1)
					{
						customDocDetailsVO.setDocName1(documentName);
					}
					else if(size==2)
					{
						customDocDetailsVO.setDocName1(documentName);
						customDocDetailsVO.setDocName2(documentName1);
					}
					else if(size==3)
					{
						customDocDetailsVO.setDocName1(documentName);
						customDocDetailsVO.setDocName2(documentName1);
						customDocDetailsVO.setDocName3(documentName2);
					}

				}
				docDetailsVO.setDocName1(customDocDetailsVO.getDocName1());
				docDetailsVO.setDocName2(customDocDetailsVO.getDocName2());
				docDetailsVO.setDocName3(customDocDetailsVO.getDocName3());
			}

			masterDetailsVO.setLegacyCif(clatInfo.getLegacyCif());
			masterDetailsVO.setRequestedTenor(clatInfo.getRequestedTenor());
			//Added for non payroll changes :: START
			masterDetailsVO.setMonthlyInstallment(clatInfo.getMonhtlyInstallment());
			//Added for non payroll changes :: END
			masterDetailsVO.setApprovedAmount(clatInfo.getApprovedAmount());
			masterDetailsVO.setUserId(clatInfo.getUserId());
			// Added for BRI AGRO payroll - update to get Bank Code and set here
			masterDetailsVO.setBankCode(clatInfo.getBankCode());
			// added for double loan credit.
			masterDetailsVO.setApplicationStatus(clatInfo.getApplicationStatus());
			
			// added for PINANG Pay later - start
				masterDetailsVO.setSchemeCode(clatInfo.getSchemeCode());
				masterDetailsVO.setPartnerId(clatInfo.getPartnerId());
			// added for PINANG Pay later - start	

			applicantDetailsVO.setApplicationId(applicationId);
			applicantDetailsVO.setAddressLine4(cladInfo.getAddrLine4());
			applicantDetailsVO.setAddressLine5(cladInfo.getAddrLine5());


			applicantDetailsVO.setCity(cladInfo.getCity());
			applicantDetailsVO.setState(cladInfo.getState());

			/*
			 * WhiteListChange fields Start
			 *
			 * 		ADD1|STATE|CITY|RT|RW|ZIPCODE
			 * 
			 * 
			 */
//			if(FEBATypesUtility.isNotNullOrBlank(cladInfo.getIsAddrSameAsKtp())
//					&& cladInfo.getIsAddrSameAsKtp().getValue().equalsIgnoreCase(FEBAConstants.YES)){
//				
//				String addressWhiteList = clpaInfo.getAddressWl().getValue();
//				System.out.println("addressWhiteList - "+addressWhiteList);
//				String [] addressList = addressWhiteList.split("\\|");
//				System.out.println("addressList - "+addressList);
//				applicantDetailsVO.setAddressLine1(addressList[0]);
//				applicantDetailsVO.setAddressLine2(addressList[3]);
//				applicantDetailsVO.setAddressLine3(addressList[4]);
//				applicantDetailsVO.setPostalCode(addressList[5]);
//				applicantDetailsVO.setCity(addressList[2]);
//				applicantDetailsVO.setState(addressList[1]);
//				System.out.println("applicantDetailsVO - "+applicantDetailsVO);
//			}else{

				applicantDetailsVO.setAddressLine1(cladInfo.getAddrLine1());
				applicantDetailsVO.setAddressLine2(cladInfo.getAddrLine2());
				applicantDetailsVO.setAddressLine3(cladInfo.getAddrLine3());
				applicantDetailsVO.setPostalCode(cladInfo.getPostalCode());
				applicantDetailsVO.setCity(cladInfo.getCity());
				applicantDetailsVO.setState(cladInfo.getState());
//				
//			}
			applicantDetailsVO.setPlaceOfBirth(clpaInfo.getPlaceOfBirthWl());
			applicantDetailsVO.setGender(new CommonCode(clpaInfo.getGenderWl().getValue()));
			applicantDetailsVO.setName(clpaInfo.getNameWl());
			applicantDetailsVO.setDateOfBirth(clpaInfo.getBirthDateWl());

			/*
			 * WhiteListChange fields Ends
			 */

			applicantDetailsVO.setHomePhoneNum(cladInfo.getHomePhoneNum());
			applicantDetailsVO.setEmailId(cladInfo.getEmailId());
			applicantDetailsVO.setReligion(cladInfo.getReligion());
			applicantDetailsVO.setLastEducation(cladInfo.getLastEducation());
			applicantDetailsVO.setNationalId(cladInfo.getNationalId());
			applicantDetailsVO.setLastEducation(cladInfo.getLastEducation());
			applicantDetailsVO.setIsAddressSameAsKTP(cladInfo.getIsAddrSameAsKtp());

			if(FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getName())) {
				String name = applicantDetailsVO.getName().toString();
				applicantDetailsVO.setShortName(new FEBAUnboundString(name));
				if(name.length() > 10){
					String shortName = name.substring(0, 10);
					applicantDetailsVO.setShortName(new FEBAUnboundString(shortName));
				}    		
			}

			contactDetailsVO.setMaritalStatus(clcdInfo.getMaritalStatus());
			contactDetailsVO.setMotherName(clcdInfo.getMotherName());
			contactDetailsVO.setEmergencyContact(clcdInfo.getEmergencyContact());
			contactDetailsVO.setEmergencyContactPhoneNum(clcdInfo.getEmrConPhoneNum());
			contactDetailsVO.setEmergencyContactRelation(clcdInfo.getEmrConRelation());
			contactDetailsVO.setEmergencyContactAddress(clcdInfo.getEmrConAddress());
			contactDetailsVO.setEmergencyContactPostalCode(clcdInfo.getEmrConPostalCd());
			DateFormat dateFormatPayrollDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");    	

			/*String aDt=clpaInfo.getAccPayrollDate().getValue().toString();

			Calendar cal = Calendar.getInstance();      	    	
			String date=dateFormatPayrollDate.format(cal.getTime()); 

			String today=date.substring(0,2);
			String accDate=aDt.substring(8,10);
			String newAccDate =accDate+date.substring(2);   
			if(Integer.parseInt(accDate)>=Integer.parseInt(today))
			{    		
				payrollAcctDetailsVO.getAccountPayrollDate().set(newAccDate);
			}
			else
			{
				int month=Integer.parseInt(date.substring(3,5))+1;    		
				String newDate2=newAccDate.substring(0,3)+month+newAccDate.substring(5);    	
				payrollAcctDetailsVO.getAccountPayrollDate().set(newDate2);
			}*/
			
			payrollAcctDetailsVO.getAccountPayrollDate().set(dateFormatPayrollDate.format(clpaInfo.getAccPayrollDate().getValue()));

			String payrollAccountNum = StringUtils.leftPad(clpaInfo.getPayrollAccountNum().getValue(), 15, '0');
			payrollAcctDetailsVO.setPayrollAccountId(payrollAccountNum);
			payrollAcctDetailsVO.setCompanyName(clpaInfo.getCompanyName());
			payrollAcctDetailsVO.setPersonelName(clpaInfo.getPersonelName());
			payrollAcctDetailsVO.setPersonelNumber(clpaInfo.getPersonelNumber());
			payrollAcctDetailsVO.setRefferalCode(clpaInfo.getRefferalCode());
			payrollAcctDetailsVO.setNameWL(clpaInfo.getNameWl());
			payrollAcctDetailsVO.setBirthDateWL(clpaInfo.getBirthDateWl());

			emplymentDetails.setEmploymentStatus(cledInfo.getEmpStatus());
			emplymentDetails.setWorkType(cledInfo.getWorkType());
			emplymentDetails.setEmployerName(cledInfo.getEmployerName());
			emplymentDetails.setTaxId(cledInfo.getTaxId());
			emplymentDetails.setMonthlyIncome(cledInfo.getMonthlyIncome());
			emplymentDetails.setMonthlyExpense(cledInfo.getMonthlyExpense());
			FEBAAmount monthlyIncome = cledInfo.getMonthlyIncome();
			FEBAUnboundDouble monIncome = monthlyIncome.getAmount();

			CommonCodeCacheVO cocdCacheVO = (CommonCodeCacheVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.CommonCodeCacheVO);
			CommonCodeVO commonCodeVO = (CommonCodeVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.CommonCodeVO);
			FEBAHashList<CommonCodeVO> cocdList = null;
			EBTransactionContext ebcontext = (EBTransactionContext) ebContext;
			commonCodeVO.setCodeType(new CommonCodeType("SGMC"));
			commonCodeVO.setBankID(ebcontext.getBankId());
			commonCodeVO.setLanguageID(ebcontext.getLangId());
			if (commonCodeVO.getCodeType() != null) {
				cocdCacheVO.setCriteria(commonCodeVO);
				try {
					cocdList = (FEBAHashList<CommonCodeVO>) AppDataManager.getList(ebContext,
							AppDataConstants.COMMONCODE_CACHE, cocdCacheVO);
					FEBAListIterator<FEBAHashList<CommonCodeVO>, CommonCodeVO> itr = cocdList.iterator();
					while (itr.hasNext()) {
						CommonCodeVO cmCode = itr.next();
						String amountlimit = cmCode.getCommonCode().toString();
						String[] amountLimitList = amountlimit.split("\\|");
						Double minAmt = null;
						Double maxAmt = null;
						if(amountLimitList != null) {
							if(amountLimitList.length == 2) {	
								minAmt = Double.parseDouble(amountLimitList[0]);
								maxAmt = Double.parseDouble(amountLimitList[1]);
							} else if(amountLimitList.length == 1) {
								minAmt = Double.parseDouble(amountLimitList[0]);
							}
							if(maxAmt != null) {
								if(monIncome.getValue() >= minAmt && monIncome.getValue() <= maxAmt) {
									emplymentDetails.setSegmentationClass(cmCode.getCodeDescription().toString());
								}
							} else {
								if(monIncome.getValue() >= minAmt) {
									emplymentDetails.setSegmentationClass(cmCode.getCodeDescription().toString());
								}
							}
						}
					}
				} catch (CriticalException e) {
					LogManager.logError(ebContext, e, "ERROR");
				}
			}

			String pinangOrSahabat=PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", ebContext);
			if(pinangOrSahabat!=null && pinangOrSahabat!="")
			{
				if(pinangOrSahabat.equalsIgnoreCase("PINANG")){
					applicantDetailsVO.setPrimaryServiceCenter("9176");
				}
				if(pinangOrSahabat.equalsIgnoreCase("SAHABAT")){
					applicantDetailsVO.setPrimaryServiceCenter("0330");
				}
			}
			applicantDetailsVO.setApplicationId(applicationId);

			String expiryDateInYears = PropertyUtil.getProperty("CREDIT_LIMIT_EXPIRY_DATE_IN_YEARS", ebContext);
			int year = Integer.parseInt(expiryDateInYears);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Calendar c = Calendar.getInstance();    
			c.add(Calendar.YEAR, year);
			Calendar calender = Calendar.getInstance();    
			calender.add(Calendar.DATE, 30);
			String acctStmtNxtPrintDt = dateFormat.format(calender.getTime());
			String expiryDate = dateFormat.format(c.getTime());
			String[] acctStmtNxtPrint = acctStmtNxtPrintDt.split(" ");
			String acctStmtDt = acctStmtNxtPrint[0].concat("T").concat(acctStmtNxtPrint[1]);
			String[] expDateStr = expiryDate.split(" ");
			String expDate = expDateStr[0].concat("T").concat(expDateStr[1]);

			detailsVO.setAcctStmtNxtPrintDt(new FEBAUnboundString(acctStmtDt));
			detailsVO.setExpDt(new FEBAUnboundString(expDate));
			detailsVO.setLoanApplicantContactDetails(contactDetailsVO);
			detailsVO.setLoanApplicantDetails(applicantDetailsVO);
			detailsVO.setLoanDocumentDetails(docDetailsVO);
			detailsVO.setPayrollAccntDetails(payrollAcctDetailsVO);
			detailsVO.setLoanApplicantEmploymentDetails(emplymentDetails);
			detailsVO.setLoanApplnMasterDetails(masterDetailsVO);	
			
			// Added for Non payroll - sending account type in loan creation API 
			if(capwdInfo!=null && capwdInfo.getAccttype() !=null){
				detailsVO.setAccountType(capwdInfo.getAccttype());
			}
			

			System.out.println("detailsVO-->"+detailsVO);
		}catch(FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		}
		return detailsVO;
	}

	private void updateCUSR (FEBATransactionContext ebContext,CustomLoanApplicationDetailsVO detailsVO) {
		
		System.out.println("CIF AND LOAN detailsVO - "+detailsVO);
		CustomLoanApplnApplicantDetailsVO applicantDetailsVO = detailsVO.getLoanApplicantDetails();
		CustomLoanApplnMasterDetailsVO masterDetailsVO = detailsVO.getLoanApplnMasterDetails();
		CustomLoanApplnPayrollAccntDetailsVO payrollAccntDetailsVO = detailsVO.getPayrollAccntDetails();
		try {

			CUSRInfo cusrInfo = null;
			CUSRTAO cusrTao = new CUSRTAO(ebContext);
			CorporateId corpId = new CorporateId(masterDetailsVO.getUserId().toString());
			Name lastName = new Name(payrollAccntDetailsVO.getNameWL().getValue());
			AddressLine addressLine1 = new AddressLine(applicantDetailsVO.getAddressLine1().toString());
			cusrInfo = CUSRTAO.select(ebContext, ebContext.getBankId(), corpId, masterDetailsVO.getUserId());
			cusrTao.associateBankId(ebContext.getBankId());
			cusrTao.associateUserId(masterDetailsVO.getUserId());
			cusrTao.associateOrgId(corpId);
			cusrTao.associateCEmailId(applicantDetailsVO.getEmailId());
			cusrTao.associateCLName(lastName);
			cusrTao.associateCFName(lastName);
			cusrTao.associateCZip(applicantDetailsVO.getPostalCode());
			cusrTao.associateDateOfBirth(payrollAccntDetailsVO.getBirthDateWL());
			cusrTao.associateCustId(detailsVO.getCustId());
			cusrTao.associateCAddr1(addressLine1);
			cusrTao.associateCMPhoneNo(applicantDetailsVO.getMobileNum());
			cusrTao.associatePanNationalId(new PanOrNationalID(applicantDetailsVO.getNationalId().getValue()));
			cusrTao.associateCookie(cusrInfo.getCookie());
			cusrTao.update(ebContext);
			FEBATableOperator.commit(ebContext);
		}   catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		} 
	}

	public FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> fetchLoanDocList(FEBATransactionContext objContext,
			ApplicationNumber applicationId) {

		CustomLoanApplnDocumentsEnquiryVO docEnquiryVO = (CustomLoanApplnDocumentsEnquiryVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsEnquiryVO");
		CustomLoanApplnDocumentsCriteriaVO docCriteriaVO = (CustomLoanApplnDocumentsCriteriaVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsCriteriaVO");

		FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> resultList = null;
		docCriteriaVO.setApplicationId(applicationId);
		docEnquiryVO.setCriteria(docCriteriaVO);

		try {
			EBTransactionContext ebcontext = (EBTransactionContext) objContext;
			QueryOperator dateQueryHandleOperator = QueryOperator
					.openHandle(objContext,CustomEBQueryIdentifiers.CUSTOM_FETCH_LOAN_DOCUMENT_LIST);
			dateQueryHandleOperator.associate("applicationId",docCriteriaVO.getApplicationId());
			dateQueryHandleOperator.associate("bankId",objContext.getBankId());

			resultList = dateQueryHandleOperator.fetchList(ebcontext);
		} catch (DALException e) {
			if (e.getErrorCode() == ErrorCodes.NO_RECORD_FOUND_IN_DAL) {
				LogManager.logError(objContext, e, "ERROR");
			}
		}
		return resultList;
	}

	private void updateCLAT (FEBATransactionContext ebContext,CustomLoanApplicationDetailsVO detailsVO) {
		ApplicationNumber applicationId = detailsVO.getLoanApplicantDetails().getApplicationId();
		CLATTAO clatTao = new CLATTAO(ebContext);
		CLATInfo clatInfo = null;
		try {
			clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), applicationId);
			clatTao.associateBankId(clatInfo.getBankId());
			clatTao.associateUserId(clatInfo.getUserId());
			clatTao.associateApplicationId(clatInfo.getApplicationId());
			clatTao.associateLoanAccountId(detailsVO.getAcctId());
			clatTao.associateApplicationStatus(new CommonCode("LOAN_CREATED"));
			clatTao.associateCookie(clatInfo.getCookie());
			clatTao.update(ebContext);					
			FEBATableOperator.commit(ebContext);
		} catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		}
	}
	/*
	 * Created for production issue fix to update loan application status to DISB_IN_PROCESS.
	 * 
	 */
	private void updateCLATDisbStatus (FEBATransactionContext ebContext,CustomLoanApplicationDetailsVO detailsVO) {
		ApplicationNumber applicationId = detailsVO.getLoanApplicantDetails().getApplicationId();
		CLATTAO clatTao = new CLATTAO(ebContext);
		CLATInfo clatInfo = null;
		try {
			clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), applicationId);
			clatTao.associateBankId(clatInfo.getBankId());
			clatTao.associateUserId(clatInfo.getUserId());
			clatTao.associateApplicationId(clatInfo.getApplicationId());
			clatTao.associateLoanAccountId(detailsVO.getAcctId());
			clatTao.associateApplicationStatus(new CommonCode(DISB_IN_PROCESS));
			clatTao.associateCookie(clatInfo.getCookie());
			clatTao.update(ebContext);					
			FEBATableOperator.commit(ebContext);			
		} catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		}
	}
	
	/*
	 * Created for production issue fix to update loan application status to DOCUMENT_SIGNED.
	 * 
	 */
	private void updateCLATDocSignStatus (FEBATransactionContext ebContext,CustomLoanApplicationDetailsVO detailsVO) {
		ApplicationNumber applicationId = detailsVO.getLoanApplicantDetails().getApplicationId();
		CLATTAO clatTao = new CLATTAO(ebContext);
		CLATInfo clatInfo = null;
		try {
			clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), applicationId);
			clatTao.associateBankId(clatInfo.getBankId());
			clatTao.associateUserId(clatInfo.getUserId());
			clatTao.associateApplicationId(clatInfo.getApplicationId());
			clatTao.associateLoanAccountId(detailsVO.getAcctId());
			clatTao.associateApplicationStatus(new CommonCode(DOCUMENT_SIGNED));
			clatTao.associateCookie(clatInfo.getCookie());
			clatTao.update(ebContext);					
			FEBATableOperator.commit(ebContext);
		} catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		}
	}
	
	private void updateCAKD (FEBATransactionContext ebContext,CustomLoanApplicationDetailsVO detailsVO) {
		ApplicationNumber applicationId = detailsVO.getLoanApplicantDetails().getApplicationId();
		CAKDTAO cakdTao = new CAKDTAO(ebContext);
		CAKDInfo cakdInfo = null;
		try {
			cakdInfo = CAKDTAO.select(ebContext, ebContext.getBankId(), applicationId);
			cakdTao.associateBankId(cakdInfo.getBankId());
			cakdTao.associateApplicationId(cakdInfo.getApplicationId());
			cakdTao.associateLoanAccountId(detailsVO.getAcctId());
			cakdTao.associateFinacleCif(new FEBAUnboundString(detailsVO.getCustId().getValue()));
			cakdTao.associateCookie(cakdInfo.getCookie());
			cakdTao.update(ebContext);					
			FEBATableOperator.commit(ebContext);
		} catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		}
	}

}
