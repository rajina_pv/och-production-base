package com.infosys.custom.ebanking.user.service;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.CNETTAO;
import com.infosys.custom.ebanking.tao.CNSDTAO;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.tao.info.CNETInfo;
import com.infosys.custom.ebanking.tao.info.CNSDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomEmailDetailsVO;
import com.infosys.custom.ebanking.user.custom.CustomSendEmailUtil;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.security.CryptManager;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBACookie;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomCreditScoringCallbackServiceRequestPayloadImpl extends AbstractLocalUpdateTran {

	private static final CommonCode APP_ID_SHAHABAT = new CommonCode(CustomEBConstants.SAHABAT);
	private static final FEBAUnboundString MSG_VERIFY_EMAIL = new FEBAUnboundString("VERIFY_EMAIL");
	private static final String EMAIL_VO = "com.infosys.custom.ebanking.types.valueobjects.CustomEmailDetailsVO";
	private static final String REJ_STATUS = "-1";
	private static final String APPROVE_STATUS = "APPROVE";
	private static final String APPROVE_REJECT = "REJECT";
	private static final List<int[]> EMAIL_ERR_CODES = Arrays.asList(new int[] { 200601, 200602, 200603, 200607 });
	private static final FEBAUnboundString EMAIL = new FEBAUnboundString("EMAIL");

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		boolean isNotificationRequired = false;
		CustomCreditScoringCallbackEnquiryVO enquiryVO = (CustomCreditScoringCallbackEnquiryVO) objInputOutput;
		CustomCreditScoringCallbackCriteriaVO criteriaVO = enquiryVO.getCriteria();
		CustomCreditScoringCallbackDetailsVO detailsVO = (CustomCreditScoringCallbackDetailsVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackDetailsVO");
		CLATInfo clatData = new CLATInfo();
		CLATTAO clatTAO = new CLATTAO(txnContext);

		try {
			clatData = CLATTAO.select(txnContext, txnContext.getBankId(), criteriaVO.getApplicationId());

			clatTAO.associateApplicationId(criteriaVO.getApplicationId());
			clatTAO.associateBankId(clatData.getBankId());
			clatTAO.associateStatusDesc(criteriaVO.getDesc());
			clatTAO.associateCreditScoreUid(criteriaVO.getUid());
			clatTAO.associateCreditScore(criteriaVO.getScore());
			clatTAO.associateGrade(criteriaVO.getGrade());

			clatTAO.associateApprovedAmount(criteriaVO.getPlafond());
			// Added for Missing Simpanan case.
			clatTAO.associateCrRespCode(criteriaVO.getCrRespCode());
			clatTAO.associateCookie(clatData.getCookie());
			FEBAUnboundString status = criteriaVO.getStatus();
			FEBAUnboundString plafond = new FEBAUnboundString(criteriaVO.getPlafond().getAmount().toString());
			String success = PropertyUtil.getProperty("CREDIT_SCORE_SUCCESS", txnContext);
			String failed = PropertyUtil.getProperty("CREDIT_SCORE_FAILED", txnContext);
			String applicationStatus = "";
			if (FEBATypesUtility.isNotNullOrBlank(status) && FEBATypesUtility.isNotNullOrBlank(plafond)) {
				if (status.toString().equalsIgnoreCase(success)) {
					boolean isRejected = REJ_STATUS.equals(plafond.toString());
					boolean statusRejected = APPROVE_REJECT.equalsIgnoreCase(criteriaVO.getDesc().toString());
					if (statusRejected) {
						// reject case
						clatTAO.associateApplicationStatus(new CommonCode("CR_SCORE_REJ"));
						detailsVO.setStatus("200");
						applicationStatus = "CR_SCORE_REJ";
						detailsVO.setApplicationStatus(applicationStatus);
					} else {
						// success case
						// if sahabat send Email
						if (CustomEBConstants.SAHABAT.equals(
								PropertyUtil.getProperty(CustomEBConstants.INSTALLATION_PRODUCT_ID, txnContext))) {
							sendEmail(txnContext, clatData);
						}

						/* 
						 * Added to calculate the Loan Installment based on the approved amount
						 */
						/*
						 * Calculation of Loan Installment based on approved user selected amount and tenor is moved to LoanUpdateDetais API
						
					
						double intPerMonth = (criteriaVO.getPlafond().getAmountValue()
								* clatData.getLoanIntRate().getValue()) / 1200;
						double prinAmountPerMonth = criteriaVO.getPlafond().getAmountValue()
								/ clatData.getRequestedTenor().getValue();
						double emi = intPerMonth + prinAmountPerMonth;

						String homeCurrencyCode = PropertyUtil.getProperty(EBankingConstants.HOME_CURRENCY, txnContext);

						clatTAO.associateMonhtlyInstallment(new FEBAAmount(homeCurrencyCode,
								new BigDecimal(emi).setScale(0, RoundingMode.HALF_UP).doubleValue()));
						 */
						
						isNotificationRequired = true;
						
						/*
						 * Associate the plafond and tenor received from CreditSCoring to clatTAO
						 */
						
						clatTAO.associatePlafond1(criteriaVO.getPlafond1());
						clatTAO.associatePlafond2(criteriaVO.getPlafond2());
						clatTAO.associateTenor1(criteriaVO.getTenor1());
						clatTAO.associateTenor2(criteriaVO.getTenor2());
						clatTAO.associateTenorMax(criteriaVO.getTenormax());
						
						clatTAO.associateApplicationStatus(new CommonCode("CR_SCORE_APR"));
						applicationStatus = "CR_SCORE_APR";
						detailsVO.setStatus("200");
						detailsVO.setApplicationStatus(applicationStatus);
						
					}
				}
				if (status.toString().equalsIgnoreCase("failed")) {
					// && plafond.toString().equalsIgnoreCase(REJ_STATUS)) {
					// failed case
					/*
					 * clatTAO.associateApplicationStatus(new
					 * CommonCode("CR_SCORE_REJ")); applicationStatus =
					 * "CR_SCORE_REJ"; detailsVO.setStatus("200");
					 */
					// throw new BusinessException(true, txnContext,
					// "CRSCTBEX03",
					// "Call Back Failure", null, 211305, null);

				}
			}
			enquiryVO.setDetails(detailsVO);
			clatTAO.update(txnContext);

			// updating in loan history table
			new CustomLoanHistoryUpdateUtil().updateHistory(criteriaVO.getApplicationId(), applicationStatus,
					criteriaVO.getDesc().toString(), txnContext);

			/*
			 * Notification Event creation in CBET for LON_ACT_REM
			 */
		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
			throw new BusinessException(true, txnContext, CustomEBankingIncidenceCodes.CREDIT_SCORE_CALLBACK_TABLE_EXC,
					"Table update failed", null, CustomEBankingErrorCodes.CREDIT_SCORE_CALLBACK_TABLE_EXC, null);
		} finally {
			detailsVO.setStatus("500");
		}
	}

	public void sendEmail(FEBATransactionContext txnContext, CLATInfo clatData)
			throws FEBATableOperatorException, BusinessConfirmation, CriticalException, BusinessException {
		String key = CryptManager
				.encrypt(clatData.getBankId() + "#" + clatData.getApplicationId() + "#" + clatData.getUserId());
		CustomEmailDetailsVO emailVO = getCustomEmailDetailsVO(clatData, txnContext, key);
		try {
			CustomSendEmailUtil.sendEmail(txnContext, emailVO);
			insertCNSD(txnContext, clatData, new FEBAUnboundString("SEND_SUCCESS"), new FEBAUnboundString(key));
		} catch (Exception ex) {
			// Initialize data",200601, //Incorrect HP No.",200602,
			// Non-technical errors",200603, // Error while delivering
			// Email",200607
			insertCNSD(txnContext, clatData, new FEBAUnboundString("SEND_FAILED"), new FEBAUnboundString(key));
		}
	}

	private void insertCNSD(FEBATransactionContext txnContext, CLATInfo clatData, FEBAUnboundString status,
			FEBAUnboundString messageKey) throws FEBATableOperatorException {
		CNSDTAO tao = new CNSDTAO(txnContext);
		tao.associateAppId(APP_ID_SHAHABAT);
		tao.associateBankId(clatData.getBankId());
		tao.associateUserId(clatData.getUserId());
		tao.associateApplicationId(clatData.getApplicationId());
		tao.associateMessageEvent(MSG_VERIFY_EMAIL);
		tao.associateMessageType(EMAIL);
		tao.associateChannelType(EMAIL);
		tao.associateMessageKey(messageKey);
		tao.associateMessageStatus(status);
		try {
			CNSDInfo cnsdInfo = CNSDTAO.select(txnContext, clatData.getBankId(), clatData.getApplicationId(),
					MSG_VERIFY_EMAIL);
			tao.associateCookie(cnsdInfo.getCookie());
			tao.update(txnContext);
			txnContext.getConnection().commit();
		} catch (FEBATypeSystemException e) {
			throw e;
		} catch (FEBATableOperatorException e) {
			if (e.getErrorCode() == EBankingErrorCodes.RECORD_DOES_NOT_EXIST) {
				tao.associateCookie(new FEBACookie());
				tao.insert(txnContext);
				txnContext.getConnection().commit();
			}
		}
	}

	private CustomEmailDetailsVO getCustomEmailDetailsVO(CLATInfo clatData, FEBATransactionContext txnContext,
			String key) throws FEBATableOperatorException {
		CLADInfo cladInfo = CLADTAO.select(txnContext, clatData.getBankId(), clatData.getApplicationId());
		String url = PropertyUtil.getProperty("EML_VRFY_URL", txnContext);
		CNETInfo cnetInfo = CNETTAO.select(txnContext, clatData.getBankId(), new CommonCode("VRFEML_MSG"),
				APP_ID_SHAHABAT);
		CustomEmailDetailsVO emailVO = (CustomEmailDetailsVO) FEBAAVOFactory.createInstance(EMAIL_VO);
		emailVO.setSubject(cnetInfo.getMessageHeader());
		emailVO.setToAddress(cladInfo.getEmailId().getValue());
		// String verifyHref = "<a href=\"" + url + "?uid=" + key +
		// "\">Verifikasi</a>";
		String verifyHref = "<a href=\"" + url + key + "\">Verifikasi</a>";// included
																			// uid
																			// in
																			// PRPM
		// STCTInfo msgText = STCTTAO.select(txnContext, clatData.getBankId(),
		// new StringConstantID("VRFEML_MSG"), txnContext.getLangId());
		emailVO.setMessageContent(
				MessageFormat.format(cnetInfo.getDetailMessage().getValue(), new String[] { verifyHref }));
		emailVO.setFeature("EMAIL_VERIFY");
		emailVO.setProductName("SHAHABAT");
		return emailVO;
	}
	

}
