package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollDateChangeVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomPayrollDateServiceValidateImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return null;
	}

	@Override
	protected void processHostData(FEBATransactionContext objTrnCtx, IFEBAValueObject objInOut, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomPayrollDateChangeVO inOutVO = (CustomPayrollDateChangeVO) objInOut;

		try {
			CLATInfo clatInfo = CLATTAO.select(objTrnCtx, objTrnCtx.getBankId(), inOutVO.getApplicationId());
			inOutVO.setLoanAcctId(clatInfo.getLoanAccountId());
			CLPAInfo clpaInfo = CLPATAO.select(objTrnCtx, objTrnCtx.getBankId(), inOutVO.getApplicationId());
			inOutVO.setOldPayrollDate(clpaInfo.getAccPayrollDate());
			EBHostInvoker.processRequest(objTrnCtx, "CustomPayrollDateChangeRequest", inOutVO);
		} catch (FEBATableOperatorException e) {
			inOutVO.setStatus("FAILURE");
		}
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub

	}

}
