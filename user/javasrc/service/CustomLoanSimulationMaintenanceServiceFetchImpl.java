package com.infosys.custom.ebanking.user.service;

import java.text.DecimalFormat;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSimulationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSimulationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundDouble;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.types.primitives.CmCode;

public class CustomLoanSimulationMaintenanceServiceFetchImpl extends AbstractLocalInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		FEBAValItem[] vls = null;

		CustomLoanSimulationEnquiryVO enqVO = (CustomLoanSimulationEnquiryVO) objInputOutput;
		String merchandId = enqVO.getCriteria().getMerchantId().getValue();

		if (null == enqVO.getCriteria().getMerchantId().getValue()
				|| enqVO.getCriteria().getMerchantId().getValue().length() == 0) {

			throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.CLNSIM_MERCHANT_ID_MANDATORY,
					"Merchant Id is mandatory", null, CustomEBankingErrorCodes.CLNSIM_MERCHANT_ID_MANDATORY, null);
		} else {
			FEBAHashList resultList1 = (FEBAHashList) AppDataManager.getList(objContext, FBAConstants.COMMONCODE_CACHE,
					FBAConstants.codeType + FBAConstants.EQUAL_TO + CustomResourceConstants.LOAN_MERCH_ID_CODE_TYPE);
			CommonCodeVO commonCodeVO = null;
			Boolean isValidMerchId = false;

			for (Object obj : resultList1) {
				commonCodeVO = (CommonCodeVO) obj;
				if (commonCodeVO.getCommonCode().toString().trim().equalsIgnoreCase(merchandId)) {
					isValidMerchId = true;
					break;
				}
			}
			if (!isValidMerchId) {
				throw new BusinessException(true, objContext,
						CustomEBankingIncidenceCodes.CLNSIM_INVALID_LOAN_MERCHANT_ID, "Merchant Id is invalid", null,
						CustomEBankingErrorCodes.INVALID_LOAN_MERCHANT_ID, null);
			}
		}
		if (null == enqVO.getCriteria().getPaymentAmount().toString()
				|| enqVO.getCriteria().getPaymentAmount().toString().length() == 0) {

			throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.CLNSIM_PAYMENT_AMOUNT_MANDATORY,
					"Payment Amount is mandatory", null, CustomEBankingErrorCodes.CLNSIM_PAYMENT_AMOUNT_MANDATORY,
					null);
		}
		CmCode tenor = new CmCode(enqVO.getCriteria().getTenor().getValue());
		if (!(null == enqVO.getCriteria().getTenor().getValue()
				|| enqVO.getCriteria().getTenor().getValue().length() == 0)) {
			FEBAHashList resultList = (FEBAHashList) AppDataManager.getList(objContext, FBAConstants.COMMONCODE_CACHE,
					FBAConstants.codeType + FBAConstants.EQUAL_TO + CustomResourceConstants.LOAN_TENURE_CODE_TYPE);
			CommonCodeVO commonCodeVO = null;
			Boolean isValidTenure = false;

			for (Object obj : resultList) {
				commonCodeVO = (CommonCodeVO) obj;
				if (commonCodeVO.getCommonCode().toString().trim().equalsIgnoreCase(tenor.getValue())) {
					isValidTenure = true;
					break;
				}
			}
			if (!isValidTenure) {
				throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.CLNSIM_INVALID_LOAN_TENURE,
						"Tenure is invalid", null, CustomEBankingErrorCodes.INVALID_LOAN_TENURE, null);
			}
		}

		return new FEBAValItem[] {};
	}

	@Override
	protected void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanSimulationEnquiryVO enqVO = (CustomLoanSimulationEnquiryVO) objInputOutput;
		FEBAArrayList<CustomLoanTenureInterestDetailsVO> tenureInterestListVO = new FEBAArrayList<>();
		CustomLoanTenureInterestDetailsVO tenIntDetVO = (CustomLoanTenureInterestDetailsVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanTenureInterestDetailsVO);

		CommonCode merchantId = enqVO.getCriteria().getMerchantId();
		FEBAUnboundString paymentAmount = enqVO.getCriteria().getPaymentAmount();
		CommonCode tenure = enqVO.getCriteria().getTenor();
		CommonCode configType = enqVO.getCriteria().getConfigType();

		QueryOperator queryHandleOperator = QueryOperator.openHandle(objContext,
				CustomEBQueryIdentifiers.CUSTOM_LOAN_TENURE_DETAILS_MAINTENANCE);
		queryHandleOperator.associate("bankId", objContext.getBankId());
		queryHandleOperator.associate("merchantId", merchantId);
		queryHandleOperator.associate("selectedAmount", paymentAmount);
		queryHandleOperator.associate("tenor", tenure);
		queryHandleOperator.associate("configType", configType);
		FEBAArrayList<CustomLoanTenureDetailsVO> customLnTenDetListVO = new FEBAArrayList<>();

		try {
			customLnTenDetListVO.addAllObjects(queryHandleOperator.fetchList(objContext));
		} catch (Exception e) {

			throw new BusinessException(true, objContext, FBAIncidenceCodes.NO_RECORDS_FOUND, "No records Found", null,
					FBAErrorCodes.NO_RECORDS_RETURNED_BY_DAL, null);
		}

		for (int i = 0; i < customLnTenDetListVO.size(); i++) {
			tenIntDetVO.setInterest(customLnTenDetListVO.get(i).getInterest());
			tenIntDetVO.setTenor(customLnTenDetListVO.get(i).getTenor());
			tenureInterestListVO.add(tenIntDetVO);
		}

		enqVO.setResultList(calculateLoanSimDetails(tenureInterestListVO, paymentAmount));

	}

	protected FEBAArrayList<CustomLoanSimulationDetailsVO> calculateLoanSimDetails(
			FEBAArrayList<CustomLoanTenureInterestDetailsVO> tenIntListVO, FEBAUnboundString paymentAmount) {
		CustomLoanSimulationEnquiryVO enqVO = (CustomLoanSimulationEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanSimulationEnquiryVO);
		@SuppressWarnings("unchecked")
		FEBAArrayList<CustomLoanSimulationDetailsVO> loanSimDetailsListVO = enqVO.getResultList();

		FEBAUnboundDouble installmentAmount = new FEBAUnboundDouble();
		FEBAUnboundDouble totalInterest = new FEBAUnboundDouble();
		FEBAUnboundDouble repaymentAmount = new FEBAUnboundDouble();

		for (int i = 0; i < tenIntListVO.size(); i++) {
			CustomLoanSimulationDetailsVO loanSimDetailsVO = (CustomLoanSimulationDetailsVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomLoanSimulationDetailsVO);
			CustomLoanTenureInterestDetailsVO tenIntDetVo = tenIntListVO.get(i);

			Double tenure = Double.valueOf(tenIntDetVo.getTenor().getValue());
			Double interest = Double.valueOf(tenIntDetVo.getInterest().getValue());
			Double principal = Double.valueOf(paymentAmount.toString());

			Double monthlyPrincipal = principal / tenure;
			Double monthlyInterest = (principal * interest) / 100;
			Double monthlyInstAmt = monthlyPrincipal + monthlyInterest;

			repaymentAmount.setValue(monthlyInstAmt * tenure);
			totalInterest.setValue(monthlyInterest * tenure);
			installmentAmount.setValue(monthlyInstAmt);
			DecimalFormat numberFormat = new DecimalFormat("#.00");

			loanSimDetailsVO.setPaymentAmount(new FEBAUnboundDouble(numberFormat.format(principal)));
			loanSimDetailsVO.setTenor(new CommonCode(tenure.toString()));
			loanSimDetailsVO.setRepaymentAmount(numberFormat.format(repaymentAmount.getValue()));
			loanSimDetailsVO.setTotalInterest(numberFormat.format(totalInterest.getValue()));
			loanSimDetailsVO.setInstallmentAmount(numberFormat.format(installmentAmount.getValue()));
			loanSimDetailsVO.setYearlyInterestRate(new FEBAUnboundDouble(interest));

			loanSimDetailsListVO.add(loanSimDetailsVO);

		}

		return loanSimDetailsListVO;

	}

}
