package com.infosys.custom.ebanking.user.service;

import com.infosys.feba.framework.common.ApplicationConfig;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.valueobjects.BusinessWarningVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.primitives.EmailId;
import com.infosys.fentbase.types.valueobjects.ILoginAltFlowVO;
import com.infosys.fentbase.user.service.IHostCIFValidateUserDetailsUtility;

public class CustomForgotPasswordServiceGenerateAuthAccessCodeImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		final ILoginAltFlowVO loginAltFlowVO = (ILoginAltFlowVO) objInputOutput;

		boolean haveConatctDetails = true;
		if (loginAltFlowVO.getForgotPasswordOfflineVO().getFpFieldDetailsVO().getSectionId().toString()
				.equalsIgnoreCase("CLGOTP")) {

			FBATransactionContext ebContext = (FBATransactionContext) objContext;

			FEBAArrayList bwList = ebContext.getWarningList().getWarningList();

			if (bwList.size() > 0) {

				for (int i = 0; i <= bwList.size(); i++) {
					BusinessWarningVO bw = (BusinessWarningVO) bwList.get(i);
					if (bw.getCode().getValue() == FBAErrorCodes.OTP_CONTACT_DETAILS_NOT_AVAILABLE
							|| bw.getCode().getValue() == FBAErrorCodes.ALERTS_EMAIL_DETAILS_NOT_AVAILABLE
							|| bw.getCode().getValue() == FBAErrorCodes.ALERTS_MOBILE_DETAILS_NOT_AVAILABLE) {

						haveConatctDetails = false;
						ebContext.getWarningList().initialize();
						bw.setCode(new FEBAUnboundInt(FBAErrorCodes.OTP_CONTACT_DETAILS_NOT_AVAILABLE));
						bw.setDispMessage("The contact details do not exist. Register the details.");
						bw.setLogMessage("The contact details do not exist. Register the details.");
						ebContext.getBussinessInfo().initialize();
						ebContext.addWarning(bw);
						break;
					}
				}
			}
			if (haveConatctDetails) {

				try {
					checkCusrInfo(loginAltFlowVO, ebContext);
					CustomRMLoginAltFlowUtility.getInstance().generateOTP(loginAltFlowVO, ebContext);

				} catch (CriticalException e) {
					if (ebContext.getFromContextData(FEBAConstants.XSERVICEREQUEST) != null && "Y"
							.equalsIgnoreCase(ebContext.getFromContextData(FEBAConstants.XSERVICEREQUEST).toString())) {
						CustomRMLoginAltFlowUtility.getInstance().generateOTP(loginAltFlowVO, ebContext);
					}
				}
			}

			if (ebContext.getContextMap().containsKey("CUSTOMER_ID")) {

				IHostCIFValidateUserDetailsUtility hostCIFValidateUserDetails = (IHostCIFValidateUserDetailsUtility) ApplicationConfig
						.getImplementationInstance("HOST_CIF_VALIDATE_USER_DETAILS_UTILITY");
				hostCIFValidateUserDetails.validateHostCIF(ebContext, loginAltFlowVO);
			}
		}
	}

	public void checkCusrInfo(ILoginAltFlowVO loginAltFlowVO, FBATransactionContext ebContext) {

		CUSRInfo cusrInfo = null;
		try {
			cusrInfo = CUSRTAO.select(ebContext, ebContext.getBankId(), ebContext.getCorpId(), ebContext.getUserId());
		} catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e);
		}
		if (null != cusrInfo) {
			loginAltFlowVO.getLoginAltFlowUserDetailsVO().setMobileNumber(cusrInfo.getCMPhoneNo());
			loginAltFlowVO.getLoginAltFlowUserDetailsVO().setEmailID(new EmailId(cusrInfo.getCEmailId().getValue()));
		}

	}
}