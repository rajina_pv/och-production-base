package com.infosys.custom.ebanking.user.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;

public class CustomLoanApplicationServiceModifyLoanDetailsImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		CustomLoanApplnMasterDetailsVO loanAppDetlsVO = (CustomLoanApplnMasterDetailsVO) objInputOutput;
		final CLATTAO clatTAO = new CLATTAO(ebContext);
		CLATInfo clatInfo = null;
		
		try {
			
			clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), loanAppDetlsVO.getApplicationId());
			clatTAO.associateBankId(clatInfo.getBankId());
			clatTAO.associateUserId(clatInfo.getUserId());
			clatTAO.associateApplicationId(loanAppDetlsVO.getApplicationId());

		
			if(loanAppDetlsVO.getApplicationStatus().getValue().equalsIgnoreCase("CR_SCORE_REJ")){
				clatTAO.associateApplicationStatus(loanAppDetlsVO.getApplicationStatus());
			}else{
				clatTAO.associateApprovedAmount(loanAppDetlsVO.getApprovedAmount());
				clatTAO.associateRequestedTenor(loanAppDetlsVO.getRequestedTenor());
				clatTAO.associateApplicationStatus(loanAppDetlsVO.getApplicationStatus());

				double totalIntAmt = (double) (loanAppDetlsVO.getApprovedAmount().getAmountValue()
						* loanAppDetlsVO.getRequestedTenor().getValue() * clatInfo.getLoanIntRate().getValue()) / 1200;
								
				double intPerMonth = (double) totalIntAmt/loanAppDetlsVO.getRequestedTenor().getValue();
								
				double prinAmountPerMonth = (double) loanAppDetlsVO.getApprovedAmount().getAmountValue()
						/ loanAppDetlsVO.getRequestedTenor().getValue();
								
				double emi = intPerMonth + prinAmountPerMonth;
				
				System.out.println("emi:::"+emi);

				String homeCurrencyCode = PropertyUtil.getProperty(EBankingConstants.HOME_CURRENCY, txnContext);

				clatTAO.associateMonhtlyInstallment(new FEBAAmount(homeCurrencyCode,
						new BigDecimal(emi).setScale(0, RoundingMode.CEILING).doubleValue()));
				// Added for PINANG Pay later start
					clatTAO.associatePayLaterProcessFee(loanAppDetlsVO.getProcessingFee());
				// Added for PINANG Pay later end
			}
			
			if (FEBATypesUtility.isNotNullOrBlank(loanAppDetlsVO.getApplicationStatus())) {
			}
			clatTAO.associateCookie(clatInfo.getCookie());

			clatTAO.update(ebContext);
			loanAppDetlsVO.getRModTime().setValue(new java.util.Date(System.currentTimeMillis()));
		

		} catch (FEBATableOperatorException e) {
			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}
	}

}
