/*

 * CustomOTPValidator.java

 * @since feb 17, 2012 - 3:10:40 PM

 *

 * COPYRIGHT NOTICE:

 * Copyright (c) 2007 Infosys Technologies Limited, Electronic City,

 * Hosur Road, Bangalore - 560 100, India.

 * All Rights Reserved.

 * This software is the confidential and proprietary information of

 * Infosys Technologies Ltd. ("Confidential Information"). You shall

 * not disclose such Confidential Information and shall use it only

 * in accordance with the terms of the license agreement you entered

 * into with Infosys.

 */

package com.infosys.custom.ebanking.user.validator;

import com.infosys.feba.framework.authentication.AuthConstants;
import com.infosys.feba.framework.authentication.AuthEngineFactory;
import com.infosys.feba.framework.authentication.IAuthenticationEngine;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.TranCommitBusinessException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.feba.utils.insulate.HashMap;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.entityreferences.IUserIDER;
import com.infosys.fentbase.types.primitives.OTP;
import com.infosys.fentbase.types.valueobjects.ICredentialVO;

/**
 * 
 * This class validates the user principal is valid or not
 * 
 * @author Renita_Pinto
 * 
 * @version 1.0
 * 
 * @since FEBA 2.0
 * 
 */

public class CustomOTPValidator implements IFEBAStandardValidator {
	/**
	 * This method validates the OTP
	 * @author Shilpa_S09
	 * @param tc
	 * @param objectToBeValidated
	 * @throws BusinessException,CriticalException,FatalException
	 * R
	 */
	public void validate(FEBATransactionContext tc,
			IFEBAType objectToBeValidated) throws BusinessException,
			CriticalException {


		 OTP otp = new OTP(objectToBeValidated.toString());

		final FBATransactionContext ebContext = (FBATransactionContext) tc;

		
			
			final IUserIDER userIDER=(IUserIDER) FEBAAVOFactory
			.createInstance(TypesCatalogueConstants.UserIDER);
			
			 userIDER.setUserId(ebContext.getUserId());
		        userIDER.setOrgId(ebContext.getCorpId());

			// user type is required to check no. of attempts from AMPM
			userIDER.setUserType(ebContext.getUserType());

			ICredentialVO credentialVO = (ICredentialVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.CredentialVO);
			credentialVO.setOtp(otp);
			// authenticate otp

			boolean result = false;
			HashMap customMap = null;
			IAuthenticationEngine authEngine = AuthEngineFactory.getAuthEngine(
					ebContext, AuthConstants.OTP_AUTHMODE);
			result = authEngine.authenticate(ebContext, ebContext.getCorpId().toString() + "." + ebContext.getUserId().toString(), otp.toString().toUpperCase(),"", customMap);
			if (!result) {
					// throw a special class of exception which commits the
				// transaction
				throw new TranCommitBusinessException(ebContext,
						FBAIncidenceCodes.AUTHENTICATION_NOT_SUCCESSFUL,
						"Authentication not successful for the user",
						FBAErrorCodes.AUTHENTICATION_FAILED);
			}
	}
}

