package com.infosys.custom.ebanking.user.validator;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.fentbase.common.FBATransactionContext;

public class CustomLoanTenureInterestValidator implements IFEBAStandardValidator  {
	private static CustomLoanTenureInterestValidator instance = null;

	public void validate(FEBATransactionContext tc, IFEBAType objectToBeValidated)
			throws BusinessException, CriticalException {
		CustomLoanTenureInterestDetailsVO customLoanTenureDetailsVO = (CustomLoanTenureInterestDetailsVO) objectToBeValidated;
	FBATransactionContext ctx = (FBATransactionContext) tc;
	
	ArrayList<BusinessExceptionVO> pExceptionListList = new ArrayList<>();
	BusinessException completeErrors = new BusinessException(tc,(ArrayList<BusinessExceptionVO>) pExceptionListList);
	
		
	if(customLoanTenureDetailsVO.getTenor().toString().equals(""))
	{
		
		completeErrors.add(	new BusinessException(true, ctx,
						"CUSER00010", null,
						"tenor", CustomEBankingErrorCodes.TENOR_MANDATORY, null,
						(AdditionalParam)null));
	}
	if(FEBATypesUtility.isDefaultDouble(customLoanTenureDetailsVO.getInterest()))
	{
		completeErrors.add(
				new BusinessException(true, ctx,
						"CUSER00011", null,
						"interest", CustomEBankingErrorCodes.INTEREST_MANDATORY, null,
						(AdditionalParam)null));
	}	
	
	if (completeErrors.count() > 0) 
	{
		throw new BusinessException(ctx, completeErrors.getExceptionList());
	}	
	}

	/**
	 * Returns the Singleton instance of this validator.
	 */
	public static CustomLoanTenureInterestValidator getInstance() {

		if (instance == null) {
			instance = new CustomLoanTenureInterestValidator();
		}
		return instance;
	}
}
