/*
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.applicationmaintenance.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.tao.S_CLTITAO;
import com.infosys.custom.ebanking.tao.info.S_CLTIInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;

/**
 * This class will fetch the user details to be edited
 * 
 * @author
 * @version 1.0
 * @since FEBA 2.0
 */

public class CustomLoanInterestMaintenanceServiceFetchForRecallImpl extends AbstractLocalInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	/**
	 * This method is used to fetch the details of the User for modification.
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTransactionWM
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @author
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran#process(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since
	 */
	@Override
	protected void process(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO = (CustomLoanTenureInterestDetailsVO) objInputOutput;
		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		criteria.setDependencyFlag(FBAConstants.NO);
		try {
			S_CLTIInfo cltiInfo = S_CLTITAO.select(pObjContext, pObjContext.getBankId(),
					customLoanTenureInterestDetailsVO.getLnRecId(), customLoanTenureInterestDetailsVO.getTenor());

			updateVOWithValues(customLoanTenureInterestDetailsVO, cltiInfo);
			CustomLoanMaintenanceTableOperationUtil.processCLTI(pObjContext, customLoanTenureInterestDetailsVO,
					FEBATMOConstants.TMO_OPERATION_SELECT_FOR_CANCEL);

			criteria.setDependencyFlag(FBAConstants.YES);

		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_SELECT_FOR_CANCEL + tmoExp.getErrorCode());
			throw new BusinessException(pObjContext, FBAIncidenceCodes.USER_SELECT_FOR_RECALL_TMO_EXCEPTION, errorCode,
					tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {
			throw new CriticalException(pObjContext, FBAIncidenceCodes.TAO_EXP_USER_FETCH_RECALL,
					tblOperExp.getMessage(), tblOperExp.getErrorCode());
		}
	}

	private void updateVOWithValues(CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO,
			S_CLTIInfo cltiInfo) {
		customLoanTenureInterestDetailsVO.setLnRecId(cltiInfo.getLnRecId());
		customLoanTenureInterestDetailsVO.setTenor(cltiInfo.getTenor());
		customLoanTenureInterestDetailsVO.setInterest(cltiInfo.getInterest());

	}
}
