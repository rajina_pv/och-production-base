package com.infosys.custom.ebanking.applicationmaintenance.validators;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;

public class CustomLoanMinAmtVal implements IFEBAStandardValidator {

	public void validate(FEBATransactionContext tc,
			IFEBAType objectToBeValidated) throws BusinessException,
			CriticalException{

		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objectToBeValidated;
		FEBAAmount minAmt = customLoanProductDetailsVO.getMinAmount();
		FEBAAmount maxAmt = customLoanProductDetailsVO.getMaxAmount();
		
		if(minAmt.getAmountValue() > maxAmt.getAmountValue()) {
			throw new BusinessException(
                    tc,
                    CustomEBankingIncidenceCodes.ERROR_MIN_AMOUNT,
                    "Minimum Amount Should be less than Maximum Amount",
                    CustomEBankingErrorCodes.ERROR_MIN_AMOUNT);
		}

		
	}
}
