package com.infosys.custom.ebanking.applicationmaintenance.util;

import com.infosys.custom.ebanking.contentmanagement.util.CustomCMMaintenanceReqdUtility;
import com.infosys.custom.ebanking.tmo.CLPMTMO;
import com.infosys.custom.ebanking.tmo.CLTITMO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.ebanking.applicationmaintenance.common.TxnTypeConstants;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.types.primitives.FEBADeleteFlag;
import com.infosys.feba.framework.types.primitives.FEBAMaintenanceRequired;
import com.infosys.feba.framework.types.primitives.FEBATMOOperation;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBACookie;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.common.util.OperationModeUtil;
import com.infosys.fentbase.usermaintenance.common.UserMaintenanceConstants;

public class CustomLoanMaintenanceTableOperationUtil {

	CustomLoanMaintenanceTableOperationUtil() {
	}

	public static void processCLPM(FEBATransactionContext pObjContext,
			CustomLoanProductDetailsVO customLoanProductDetailsVO, int tmoMode)
					throws FEBATMOException, FEBATableOperatorException {

		FEBATMOCriteria criteria = new FEBATMOCriteria();
		FEBAUnboundString recID = null;
		if ((tmoMode == FEBATMOConstants.TMO_OPERATION_INSERT)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_MODIFY)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_MODIFY)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_DELETE)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_DELETE)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_LOGICAL_DELETE)) {
			criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		} else {
			if ((tmoMode == FEBATMOConstants.TMO_OPERATION_APPROVE)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_APPROVE)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_REJECT)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_REJECT)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_CANCEL)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_CANCEL)) {
				criteria.setDependencyFlag(FBAConstants.YES);
			}
			criteria.setMaintenanceRequired(new FEBAMaintenanceRequired("Y"));
		}

		if (OperationModeUtil.isWorkflowOperation(tmoMode)) {
			criteria.setMaintenanceRequired(EBankingConstants.YES);
		} else {
			criteria.setMaintenanceRequired(CustomCMMaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		}

		if (customLoanProductDetailsVO.getLnRecId().toString() == null
				|| customLoanProductDetailsVO.getLnRecId().toString().isEmpty()) {
			long lnRecId = SequenceGeneratorUtil.getNextSequenceNumber("NEXT_LN_REC_ID", pObjContext);
			recID = new FEBAUnboundString(String.valueOf(lnRecId));
			customLoanProductDetailsVO.setLnRecId(recID);
		}

		CLPMTMO clpmTMO = new CLPMTMO(pObjContext, criteria);
		clpmTMO.associateBankId(pObjContext.getBankId());
		clpmTMO.associateMerchantId(customLoanProductDetailsVO.getMerchantID());
		clpmTMO.associateProductCode(customLoanProductDetailsVO.getProductCode());
		clpmTMO.associateProductCategory(customLoanProductDetailsVO.getProductCategory());
		clpmTMO.associateProductSubcategory(customLoanProductDetailsVO.getProductSubCategory());
		clpmTMO.associateConfigType(customLoanProductDetailsVO.getConfigType());
		clpmTMO.associateMinAmount(customLoanProductDetailsVO.getMinAmount());
		clpmTMO.associateMaxAmount(customLoanProductDetailsVO.getMaxAmount());
		clpmTMO.associateLnPurpose(customLoanProductDetailsVO.getLnPurpose());

		if (customLoanProductDetailsVO.getLnRecId() == null
				|| customLoanProductDetailsVO.getLnRecId().toString().isEmpty()) {
			clpmTMO.associateLnRecId(recID);

		} else {
			clpmTMO.associateLnRecId(customLoanProductDetailsVO.getLnRecId());
		}

		final FEBADeleteFlag delFlag = new FEBADeleteFlag(UserMaintenanceConstants.CHAR_N);
		clpmTMO.associateDelFlg(delFlag);

		if (customLoanProductDetailsVO.getClpmCookie() == null) {
			customLoanProductDetailsVO.getClpmCookie().set(new FEBACookie());
		}
		clpmTMO.associateCookie(customLoanProductDetailsVO.getClpmCookie());
		try {
			clpmTMO.process(pObjContext, new FEBATMOOperation(tmoMode));
		} catch (Exception e) {
			LogManager.logError(pObjContext, e);
		}
	}

	public static void processCLTI(FEBATransactionContext pObjContext,
			CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO, int tmoMode)
					throws FEBATMOException, FEBATableOperatorException {

		FEBATMOCriteria criteria = new FEBATMOCriteria();

		if ((tmoMode == FEBATMOConstants.TMO_OPERATION_INSERT)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_MODIFY)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_MODIFY)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_DELETE)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_DELETE)
				|| (tmoMode == FEBATMOConstants.TMO_OPERATION_LOGICAL_DELETE)) {
			criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		} else {
			if ((tmoMode == FEBATMOConstants.TMO_OPERATION_APPROVE)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_APPROVE)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_REJECT)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_REJECT)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_CANCEL)
					|| (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_CANCEL)) {
				criteria.setDependencyFlag(FBAConstants.YES);
			}
			criteria.setMaintenanceRequired(new FEBAMaintenanceRequired("Y"));
		}

		if (OperationModeUtil.isWorkflowOperation(tmoMode)) {
			criteria.setMaintenanceRequired(EBankingConstants.YES);
		} else {
			criteria.setMaintenanceRequired(CustomCMMaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		}

		CLTITMO cltiTMO = new CLTITMO(pObjContext, criteria);
		cltiTMO.associateBankId(pObjContext.getBankId());
		cltiTMO.associateTenor(customLoanTenureInterestDetailsVO.getTenor());
		cltiTMO.associateInterest(customLoanTenureInterestDetailsVO.getInterest());
		cltiTMO.associateLnRecId(customLoanTenureInterestDetailsVO.getLnRecId());

		final FEBADeleteFlag delFlag = new FEBADeleteFlag(UserMaintenanceConstants.CHAR_N);
		cltiTMO.associateDelFlg(delFlag);

		if (customLoanTenureInterestDetailsVO.getCltiCookie() == null) {
			customLoanTenureInterestDetailsVO.getCltiCookie().set(new FEBACookie());
		}
		cltiTMO.associateCookie(customLoanTenureInterestDetailsVO.getCltiCookie());
		try {
			cltiTMO.process(pObjContext, new FEBATMOOperation(tmoMode));
		} catch (Exception e) {
			LogManager.logError(pObjContext, e);
		}
	}

	public static BusinessInfoVO getSuccessMessage(EBTransactionContext objContext, int errorCodes) {
		FEBAUnboundInt errorCode = null;
		// fetching the approval id
		TransactionKey transactionKey = objContext.getApprovalTxnKey();
		// Set the error code
		final BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.BusinessInfoVO);
		if (MaintenanceReqdUtility.getMaintenanceReqd(objContext).equals(EBankingConstants.YES)
				&& (errorCodes == EBankingErrorCodes.CAFW_INSERT_SUCCESS)
				|| (errorCodes == EBankingErrorCodes.RECORD_UPDATED_SUCCESSFULLY)
				|| (errorCodes == EBankingErrorCodes.RECORD_DELETED_SUCCESSFULLY)) {
			errorCode = new FEBAUnboundInt(EBankingErrorCodes.ADMIN_RECORD_SENT_FOR_APPROVAL);
			infoDetails.setParam(new AdditionalParam(TxnTypeConstants.REF_ID,
					new FEBAUnboundString(String.valueOf(transactionKey.getValue()))));
		} else {
			errorCode = new FEBAUnboundInt(errorCodes);
		}
		// create an instance of businessinfo object and set the error code
		infoDetails.setCode(errorCode);

		// return business info object
		return infoDetails;
	}

}
