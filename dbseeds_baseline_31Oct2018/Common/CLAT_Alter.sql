ALTER TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE ADD EMAIL_VERIFIED Char(1);
ALTER TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE ADD CREDIT_SCORE_UID nvarchar2(100);
ALTER TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE ADD CREDIT_SCORE number(10,2);
ALTER TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE ADD GRADE number(2);
ALTER TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE ADD APPROVED_AMOUNT NUMBER(18,3);
ALTER TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE ADD STATUS_DESC nvarchar2(100);
ALTER TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE ADD LOAN_ACCOUNT_ID nvarchar2(16);

Alter TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE add FEEDBACK_STAR NVARCHAR2(50);
Alter TABLE CUSTOM_LOAN_APPLN_MASTER_TABLE add REMARKS NVARCHAR2(100);