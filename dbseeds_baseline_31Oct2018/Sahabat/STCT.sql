SET DEFINE OFF;
INSERT INTO STCT (BANK_ID,STR_ID,LANG_ID,STR_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES ('01','SMS_REG','001','Kode Otorisasi untuk registrasi akun Ceria Anda adalah <OTP>. Kode Anda berlaku 30 menit.','N','system',SYSDATE,'system',SYSDATE);

--added by sahana for email otp 15/10/18-start
Insert into STCT ("bank_id","str_id","lang_id","str_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values ('01','EMAIL_SUB','001','Forgot PIN/Reset PIN','N','system',sysdate,'system',sysdate);
Insert into STCT ("bank_id","str_id","lang_id","str_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values ('01','EMAIL_MSG','001','Kode Otorisasi utk mengubah PIN aplikasi Ceria Anda adalah <OTP>. Kode Anda berlaku 30 menit.','N','system',sysdate,'system',sysdate);
--added by sahana for email otp 15/10/18-end

COMMIT;

Insert into STCT ("bank_id","str_id","lang_id","str_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values ('01','MPA_MSG','001','Kode Otorisasi utk transaksi adalah <OTP>. Kode Anda berlaku 15 menit.','N','system',sysdate,'system',sysdate);
COMMIT;