SET DEFINE OFF;
Insert into AUTHUSER.AMPM ("db_ts","bank_id","authentication_scheme","authentication_mode","number_of_attempts","expiry_period","multi_auth_factor","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SME','TPWD',100,999,'N','N','setup',sysdate,'setup',sysdate);

Insert into AUTHUSER.AMPM ("db_ts","bank_id","authentication_scheme","authentication_mode","number_of_attempts","expiry_period","multi_auth_factor","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RES','TPWD',100,999,'N','N','setup',sysdate,'setup',sysdate);
Insert into AUTHUSER.AMPM ("db_ts","bank_id","authentication_scheme","authentication_mode","number_of_attempts","expiry_period","multi_auth_factor","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','REG','TPWD',100,999,'N','N','setup',sysdate,'setup',sysdate);
Insert into AUTHUSER.AMPM ("db_ts","bank_id","authentication_scheme","authentication_mode","number_of_attempts","expiry_period","multi_auth_factor","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BUR','TPWD',100,999,'N','N','setup',sysdate,'setup',sysdate);

update AUTHUSER.AMPM set number_of_attempts=100, expiry_period=999 where bank_id='01' and authentication_mode in ('SPWD','TPWD');

COMMIT;