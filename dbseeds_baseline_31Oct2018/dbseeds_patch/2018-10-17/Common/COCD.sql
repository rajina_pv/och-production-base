Insert into COCD values (1,'01','STXT','DTS','001','Privy Terms & Conditions','N','setup',sysdate,'setup',sysdate);
commit;

-- Religion common code
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','BUD','001','Buddha','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','HIN','001','Hindu','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','ISL','001','Islam','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','KAT','001','Katolik','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','KRI','001','Kristen','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RELI','ZZZ','001','Lainnya','N','setup',sysdate,'setup',sysdate);
