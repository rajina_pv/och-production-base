package com.infosys.custom.ebanking.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.google.common.io.Files;
import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomCUSRInfoVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationInOutVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.batch.FEBABatchContext;
import com.infosys.feba.framework.batch.IGenericInputData;
import com.infosys.feba.framework.batch.impl.GenericInputData;
import com.infosys.feba.framework.batch.impl.ReadOnlyUserParams;
import com.infosys.feba.framework.batch.pattern.AbstractFileProcessBatch;
import com.infosys.feba.framework.batch.util.EBBatchUtility;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.FEBADateFormat;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.formsgroup.ContentUploadDetails;
import com.infosys.feba.framework.reports.constants.ReportConstants;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.DataLength;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.UserType;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FileUploadDetailsVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.types.valueobjects.IReportMessageVO;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.fentbase.common.FBAErrorCodes;

public class CustomNotificationCoreBatch extends AbstractFileProcessBatch {


	public static final String ID = "CUSTOMNOTCORE";
	BufferedReader br = null;
	FileReader fr = null;
	private EBBatchUtility eBBatchUtil;
	private static int recordNumber = 1;
	StringBuffer bufferSuccess = new StringBuffer();
	StringBuffer bufferFailure;
	CustomNotificationInOutVO customNotificationInOutVO;
	boolean check = false;
	private String strStatus = "";


	public CustomNotificationCoreBatch()
	{
		super();
		this.setId(ID);
		this.setRecordLevelCommit(true);
		this.setSingleThread(true);
		eBBatchUtil = new EBBatchUtility();

	}

	@Override
	protected IGenericInputData getListOfRecordsToProcess(FEBABatchContext context, ReadOnlyUserParams userInputs)throws BusinessConfirmation, CriticalException, BusinessException{
		String currentLine = "";
		IGenericInputData inputData = new GenericInputData();
		System.out.println("<<<<<<<CUSTOMNOTCORE>>>>>>> <<<<<getListOfRecords>>>>");
		List<CustomNotificationInOutVO> list = new ArrayList<>();
		try {
			String propertyVal = PropertyUtil.getProperty("CUSTOM_NOTIFICATION_CORE_INPUTPATH", context);
			String processedFilePath = PropertyUtil.getProperty("CUSTOM_NOTIFICATION_CORE_PROCESSEDPATH", context);
			//processedFilePath="D://BRI_onsite//ModulesWise_onsite//CoreNotification_Final_19122018//PROCESSED_FILES//";

			//propertyVal="D://BRI_onsite//ModulesWise_onsite//CoreNotification_Final_19122018//FLAT_FILES//";
			System.out.println("propertyVal - "+propertyVal);
			File directory = new File(propertyVal);
			//get all the files from a directory
			File[] fList = directory.listFiles();
			for (File file : fList){
				if (file.isFile() && file.getName().endsWith(".txt")){
					System.out.println("file.getName() - "+file.getName());
					fr = new FileReader(propertyVal+file.getName());
					br = new BufferedReader(fr);
					br = new BufferedReader(new FileReader(propertyVal+file.getName()));
					currentLine=br.readLine();
					while (currentLine != null && !currentLine.isEmpty())
					{
						
						CustomNotificationInOutVO customNotiInOutVO= (CustomNotificationInOutVO)FEBAAVOFactory.createInstance(CustomTypesCatalogueConstants.CustomNotificationInOutVO);
						String delim="|";
						String[] strValue = splitTheText(currentLine.trim(), delim);
						System.out.println("strValue - "+strValue);
						
						try
						{
							customNotiInOutVO=(CustomNotificationInOutVO) populateVO(context, strValue,currentLine,customNotiInOutVO);
							list.add(customNotiInOutVO);
							System.out.println("customNotiInOutVO - "+customNotiInOutVO);
						}catch(Exception e)
						{
							LogManager.logError(context, e);
							bufferFailure = new StringBuffer();
							bufferFailure.append(currentLine);
							bufferFailure.append("~");
							bufferFailure.append(e.getMessage());
							bufferFailure.append("\n");
							writeToFile(context, bufferFailure.toString(),"FAILURE");

						}
						
						currentLine=br.readLine();
					}///while ends
					
					inputData.addItems(list);
					
					File proccessedFilesfolder = new File(processedFilePath);
					File sourceFileName = new File(directory,file.getName());
					System.out.println("Renaming Source File - ");
					String newSourceFileName = sourceFileName+"_"+new Date().getTime();
					File  newSourceFile = new File(newSourceFileName);
					Files.move(sourceFileName, newSourceFile);
					System.out.println("Renamed Source File to - "+newSourceFile);
					
					System.out.println("list - "+list);
				}
			}
		}
		catch (Exception e) {
			throw new BusinessException(context,
					"Record does not exist.",
					e.getMessage(),
					FBAErrorCodes.FILE_NOT_FOUND); 
		} 
		return inputData;
	}



	@Override
	protected IFEBAValueObject getRecord(FEBABatchContext arg0, String arg1, ReadOnlyUserParams arg2)
			throws BusinessException, CriticalException {
		return null;
	}

	@Override
	protected void processBatch(FEBABatchContext context, IFEBAValueObject vo, Object arg2, ReadOnlyUserParams arg3)throws BusinessException, CriticalException, BusinessConfirmation, FatalException {
		System.out.println("<<<<<<<CUSTOMNOTCORE>>>>>>> <<<<<processBatch>>>>");

		FEBAUnboundString batchService = new FEBAUnboundString("CustomNotificationBatchMaintenanceService");
		String batchMethod = "create";
		FEBATransactionContext objContext = eBBatchUtil.populateTransactionContext(context);
		objContext.setServiceName(batchService);
		objContext.setMethodName(new FEBAUnboundString(batchMethod));
		CustomNotificationInOutVO customNotInOutVO = (CustomNotificationInOutVO) vo;
	System.out.println("customNotInOutVO - "+customNotInOutVO);
		try {
			try{
				CustomLoanApplnMasterDetailsVO loanAppDetails = null;

				if(customNotInOutVO.getNotificationEvent().getValue().equalsIgnoreCase(CustomEBConstants.LOAN_CLO)){
					QueryOperator operator = QueryOperator.openHandle(context, CustomEBQueryIdentifiers.CUSTOM_LOAN_APPLICATION_INQUIRY_FOR_LOAN_PAID);
					operator.associate("bankId", context.getBankId());
					operator.associate("userId", customNotInOutVO.getUserId());
					//operator.associate("applicationStatus", new CommonCode(CustomEBConstants.LOAN_CREATED));
					operator.associate("loanAccountId", customNotInOutVO.getLoanAccount());
					
					FEBAArrayList<CustomLoanApplnMasterDetailsVO> loanAppDetailsResultList = operator.fetchList(objContext);
					System.out.println("loanAppDetailsResultList - "+loanAppDetailsResultList);
					if(loanAppDetailsResultList.size()==1){
						loanAppDetails= loanAppDetailsResultList.get(0);
						
					}
					System.out.println("loanAppDetails-  "+loanAppDetails);
					if(null!=loanAppDetails){
						if (!loanAppDetails.getApplicationStatus().getValue().equalsIgnoreCase("LOAN_PAID")) {
							try {
								CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
										.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
								loanApplnDetailsVO.setApplicationId(loanAppDetails.getApplicationId());
								loanApplnDetailsVO.setApplicationStatus(new CommonCode("LOAN_PAID"));
								
								System.out.println("Update Application Status for - "+loanAppDetails.getApplicationId());
								LocalServiceUtil.invokeService(objContext,"CustomLoanApplicationService", new FEBAUnboundString("modify"), loanApplnDetailsVO);
								new CustomLoanHistoryUpdateUtil().updateHistory(loanApplnDetailsVO.getApplicationId(),
										"LOAN_PAID", "COREBATCHUPDATE", objContext);
								System.out.println("Updated Application Status to - LOAN_PAID");

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

//						Added for PayoffDocument -Start
						UploadPayOffDocument(objContext, loanAppDetails.getApplicationId(), customNotInOutVO);
						loanAppDetails=null;
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("Calling Notification Serivce");

			LocalServiceUtil.invokeService(objContext,batchService.getValue(), new FEBAUnboundString(batchMethod), customNotInOutVO);
			System.out.println("Notification service exited");
		} catch(Exception e){
			LogManager.logError(objContext, e, "ERROR");
		} finally {
			if (objContext != null) {
				try {
					objContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}

	}
	protected static void updateApplicationStatus(IOpContext opContext, String applicationId, String applicationStatus)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
		loanApplnDetailsVO.setApplicationId(new ApplicationNumber(applicationId));
		final IClientStub customLoanApplicationService = ServiceUtil
				.getService(new FEBAUnboundString("CustomLoanApplicationService"));
		loanApplnDetailsVO.setApplicationStatus(new CommonCode(applicationStatus));
		customLoanApplicationService.callService(opContext, loanApplnDetailsVO, new FEBAUnboundString("modify"));
		// mapping main application details:: end

	}

	@Override
	protected void validateHeader(FEBABatchContext arg0, IFEBAValueObject arg1, Object arg2)
			throws BusinessException, CriticalException {

	}

	@Override
	protected void validateInputFile(FEBABatchContext arg0, IGenericInputData arg1, ReadOnlyUserParams arg2)
			throws BusinessException, CriticalException {

	}

	@Override
	public void checkUsage(ReadOnlyUserParams arg0) throws CriticalException {

	}

	@Override
	protected IReportMessageVO getRecordErrorMessage(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {
		return null;
	}

	@Override
	protected List getRecordErrorMessagesList(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {
		return null;
	}

	@Override
	protected IReportMessageVO getRecordSuccessMessage(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {
		return null;
	}

	@Override
	protected List getRecordSuccessMessagesList(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {
		return null;
	}

	@Override
	protected String getSuccessLogMessage(FEBABatchContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {
		return null;
	}


	private void writeToFile(FEBATransactionContext objTxnContext, String data, String sts) {
		
		String propertyVal = PropertyUtil.getProperty("CUSTOM_NOTIFICATION_CORE_OUTPUTPATH", objTxnContext);   
		
		File outPutFile = new File(propertyVal);
		if (!outPutFile.exists()) {
			outPutFile.mkdirs();
		}
		final FEBADateFormat dateFormat = new FEBADateFormat(ReportConstants.REPORT_DEFAULT_DATE_FORMAT);
		final String dateString = dateFormat.format(DateUtil.getCurrentServerDate());
		outPutFile = new File(propertyVal + File.separator+"CNED"+sts+"_"+ dateString+ ".txt");
		if (!outPutFile.exists()) {
			try {
				outPutFile.createNewFile();
			} catch (IOException e) {
			}
		}

		BufferedWriter bfrWrtr = null;
		try {
			bfrWrtr = new BufferedWriter(new FileWriter(outPutFile, true));
			bfrWrtr.write(data);
			bfrWrtr.flush();
		} catch (IOException e) {			
		}finally{
			try {
				if(bfrWrtr != null){
					bfrWrtr.close();
				}
			} catch (IOException e) {				
			}
		}

	}

	
	public static String[] splitTheText(String pInputText, String pSeparator) {
		String[] splittedText=new String[0];
		//Checking if the input text and delimiter are null
		if (null != pInputText && null != pSeparator) {
			int indexOfSepr = 0;
			if (-1 != pInputText.indexOf(pSeparator)) {
				pInputText = pInputText.concat(pSeparator);
			}
			List<String> stringText = new ArrayList<>();
			while (true) {
				indexOfSepr = pInputText.indexOf(pSeparator);
				if (-1 == indexOfSepr) {
					break;
				}
				// text before separator
				stringText.add(pInputText.substring(0, indexOfSepr));

				if ((indexOfSepr + pSeparator.length()) <= pInputText.length()) {
					pInputText = pInputText.substring(indexOfSepr+ pSeparator.length());
				}

			} 
			splittedText = stringText.toArray(splittedText); 
		}

		return splittedText;
	}
	
	@Override
	protected FEBAValItem[] prepareValidationsList(FEBABatchContext objContext,
			IFEBAValueObject objInputOutput, Object objTxnWM)
					throws BusinessException, CriticalException{

		return new FEBAValItem[]{};

	}

	private IFEBAValueObject populateVO(FEBABatchContext context, String[] strValue, String currentLine,CustomNotificationInOutVO customNotificationInOutVO) throws Exception{
    //null check for strvalue n vo
		
			String strVal[] = currentLine.split("\\|");
			customNotificationInOutVO.setNotificationEvent(strVal[0]);
			customNotificationInOutVO.setCifID(strVal[1]);
			customNotificationInOutVO.setLoanAccount(strVal[2]);
			UserId userId  = null;
			
			QueryOperator operator = QueryOperator.openHandle(context, CustomEBQueryIdentifiers.CUSTOM_CUSR_INFO_DAL);
			operator.associate("bankId", context.getBankId());
			operator.associate("userType", new UserType("1"));
			operator.associate("cif", customNotificationInOutVO.getCifID());
			FEBAArrayList<CustomCUSRInfoVO> resultList = null;
			try {
				resultList = operator.fetchList(context);
				userId=resultList.get(0).getUserId();
				customNotificationInOutVO.setUserId(userId);
				if(!FEBATypesUtility.isNotNullOrBlank(userId)){
					throw new BusinessException(true, context, "User ID not found in CUSR", "", null, 211053,
							null);
				}
			} catch (DALException e) {
				System.out.println("DAL Exception from CUSTOM_CUSR_INFO_DAL");
				LogManager.logError(context, e, "ERROR");
				throw e;
			} catch (Exception e){
				System.out.println("Other Exception from CUSTOM_CUSR_INFO_DAL");
				throw e;
			}
			
			
			
			String propertyValCur = PropertyUtil.getProperty("HOME_CUR_CODE", context);
			String amtFromInput=strValue[3].trim();
			if(!amtFromInput.isEmpty()) {
			String amtFinal=propertyValCur+"|"+amtFromInput;
			customNotificationInOutVO.setAmount1(new FEBAAmount(amtFinal));
			}
			String amtFromInput2=strValue[4].trim();
			if(!amtFromInput2.isEmpty()) {
			String amtFinal2=propertyValCur+"|"+amtFromInput2;
			customNotificationInOutVO.setAmount2(new FEBAAmount(amtFinal2));
			}
			String amtFromInput3=strValue[5].trim();
			if(!amtFromInput3.isEmpty()) {
			String amtFinal3=propertyValCur+"|"+amtFromInput3;
			customNotificationInOutVO.setAmount3(new FEBAAmount(amtFinal3));
			}
			//date formatting
			String oldstring = strValue[6].trim();
			if(!oldstring.isEmpty()) {
			String append=oldstring +" 00:00:00";
			customNotificationInOutVO.setDate1(new FEBADate(append));
			}
			String oldstring1 = strValue[7].trim();
			if(!oldstring1.isEmpty()) {
			String append1=oldstring1 +" 00:00:00";
			customNotificationInOutVO.setDate2(new FEBADate(append1));
			}
			
			String oldstring2 = strValue[8].trim();
			if(!oldstring2.isEmpty()) {
			String append2=oldstring2 +" 00:00:00";
			customNotificationInOutVO.setDate3(new FEBADate(append2));
			}
			String otherstring = strValue[9].trim();
			if(!otherstring.isEmpty()) {
			customNotificationInOutVO.setOther1(otherstring);
			}
			String otherstring1 = strValue[10].trim();
			if(!otherstring1.isEmpty()) {
			customNotificationInOutVO.setOther2(otherstring1);
			}
			String otherstring2 = strValue[11].trim();
			if(!otherstring2.isEmpty()) {
			customNotificationInOutVO.setOther3(otherstring2);
			}
		return customNotificationInOutVO;

	}

	public void UploadPayOffDocument(FEBATransactionContext objContext, ApplicationNumber applicationId,CustomNotificationInOutVO customNotInOutVO) throws BusinessException, CriticalException, BusinessConfirmation, FEBATypeSystemException, IOException {


		String saveDirectory = PropertyUtil.getProperty("FEBA_SHARED_SYS_PATH", null);

		String coreDocDirectory = PropertyUtil.getProperty("FEBA_SHARED_CORE_PATH_PAYOFF", null);
		File dir = new File((new StringBuilder()).append(saveDirectory).append("/").append(applicationId).toString());
//		hardcoded-start
		File file = new File(coreDocDirectory+"/"+customNotInOutVO.getOther1());
		String originalFileName = file.getName();
		String mimeType = "Application/pdf";
//		hardcoded-end
        if(!dir.exists())
            dir.mkdirs();
        if(!dir.isDirectory())
            throw new CriticalException(null, "FEBAFU0006", 101912);
        if(!dir.canWrite())
            throw new CriticalException(null, "FEBAFU0007", 101913);
		ContentUploadDetails fileContentDetails = null;
		fileContentDetails = new ContentUploadDetails(file, originalFileName, mimeType);
		fileContentDetails.setClientFilePath(originalFileName);
		fileContentDetails.setFileStoragePath(dir.toString());

//		FileUploadedFileInfo fileDetails = (new FEBAFileUploadUtility()).uploadFileFromFileSystem(objContext,
//				fileContentDetails);
//		if (fileDetails != null) {
//			documentDetails.setFileSeqNo(Long.toString(fileDetails.getSequenceNumber()));
//			documentDetails.setFileUploadPath((new StringBuilder()).append(fileContentDetails.getFileStoragePath())
//					.append("/").append(documentDetails.getFileName()).toString());
//		}
		
		FileUploadDetailsVO inputOutputVO = (FileUploadDetailsVO) FEBAAVOFactory
				.createInstance("com.infosys.feba.framework.types.valueobjects.FileUploadDetailsVO");
		// For the first time it will be the default value -1, on subsequent
		// call what ever returned from the outputVO is resend back
		long fileSequenceNumber = -1L;
		inputOutputVO.setFileseqno(new FEBAUnboundLong(fileSequenceNumber));
		FEBABinary fileContent = new FEBABinary();
		fileContent.setValue(java.nio.file.Files.readAllBytes(file.toPath()));
		inputOutputVO.setFiledata(fileContent);
		inputOutputVO.setFileserno(new FEBAUnboundInt(1));
		inputOutputVO.setFileName(fileContentDetails.getFileName());
		inputOutputVO.setFileMime(fileContentDetails.getContentType());
		inputOutputVO
				.setFileStoragePath(fileContentDetails.getClientFilePath());
		inputOutputVO.setFileDataLength(new DataLength(fileContentDetails
				.getContentLength()));

			inputOutputVO.setEndoffile(new FEBAUnboundString(
					FEBAConstants.FLAG_Y));
			inputOutputVO.setCrpCorporateId(customNotInOutVO.getUserId().getValue());
System.out.println("Before upload service call-->"+inputOutputVO.toDebugString());
		LocalServiceUtil.invokeService(objContext, FEBAConstants.FILE_UPLOAD_SERVICE_NAME, new FEBAUnboundString(FEBAConstants.EVENT_NAME), inputOutputVO);
System.out.println("After upload service call-->"+inputOutputVO.toDebugString());

		CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplnDocumentsDetailsVO);
		docDetailsVO.setApplicationId(applicationId);
		docDetailsVO.setDocType(new DocumentType("PAYOFF"));

		docDetailsVO.setDocStorePath(dir+"/"+file.getName());
		docDetailsVO.setFileSeqNo(String.valueOf(inputOutputVO.getFileseqno().getValue()));

		LocalServiceUtil.invokeService(objContext, "CustomLoanApplicationDocDetailsService",  new FEBAUnboundString("createOrUpdate"), docDetailsVO);
		System.out.println("After cldd update service call-->"+inputOutputVO.toDebugString());

	}
}
