package com.infosys.custom.ebanking.batch;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationInOutVO;
import com.infosys.feba.framework.batch.FEBABatchContext;
import com.infosys.feba.framework.batch.IGenericInputData;
import com.infosys.feba.framework.batch.impl.GenericInputData;
import com.infosys.feba.framework.batch.impl.ReadOnlyUserParams;
import com.infosys.feba.framework.batch.pattern.AbstractBusinessProcessBatch;
import com.infosys.feba.framework.batch.util.EBBatchUtility;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.types.valueobjects.IReportMessageVO;
import com.infosys.feba.framework.types.valueobjects.ReportMessageVO;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;

public class CustomNotificationEventGenerationBatch extends AbstractBusinessProcessBatch{

	/*
	 * Batch Program Identifier
	 */
	public static final String ID = "CUSTNOTGEN";
	private EBBatchUtility eBBatchUtil = new EBBatchUtility();

	private static final String BATCH_SERVICE = "CustomNotificationBatchMaintenanceService";
	private static final String BATCH_METHOD = "create";

	/**
	 * Default Constructor instantiation
	 *
	 * @author Pratik Shah
	 * 
	 */
	public CustomNotificationEventGenerationBatch(){
		super();
		this.setId(ID);
		this.setSingleThread(true);
	}

	/**
	 * This method is used to process batch based on VO details.
	 * @return 
	 * @author Pratik Shah
	 * @throws CriticalException 
	 * 
	 * 
	 */
	@Override
	protected void processBatch(FEBABatchContext batchContext, IFEBAValueObject VO,
			Object arg2, ReadOnlyUserParams userInputs) throws CriticalException {
		System.out.println("In Process batch");

		FEBATransactionContext objContext = eBBatchUtil.populateTransactionContext(batchContext);

		CustomNotificationInOutVO inOutVO = (CustomNotificationInOutVO)VO;

		CommonCodeVO commonCodeVO =null;

		FEBADate rModTime = inOutVO.getRModTime();

		try {
			FEBAHashList resultList = (FEBAHashList) AppDataManager.getList(
					objContext, FBAConstants.COMMONCODE_CACHE, 
					FBAConstants.codeType+FBAConstants.EQUAL_TO+"CNED");
			System.out.println("In COCD List - "+resultList);

			long dateDifference = DateUtil.dateDiff(new Timestamp(System.currentTimeMillis()), rModTime.getTimestampValue());
			System.out.println("dateDifference" +dateDifference);
			Boolean isTodayAValidDate = false;

			for (Object obj : resultList) {
				commonCodeVO = (CommonCodeVO) obj;
				if (Long.valueOf(commonCodeVO.getCommonCode().toString()).equals(dateDifference)){
					isTodayAValidDate=true;
					break;
				}
			}
			System.out.println("In isTodayAValidDate "+isTodayAValidDate);

			if(isTodayAValidDate){
				LocalServiceUtil.invokeService(objContext,(BATCH_SERVICE),new FEBAUnboundString(BATCH_METHOD), inOutVO);
			}
		} catch (Exception e) {
			LogManager.logError(objContext, e, "ERROR");
		}
	}


	@Override
	public void checkUsage(ReadOnlyUserParams arg0) {
		/*
		 * 
		 */
	}

	@Override
	/**
	 * This method is used to get list of records to processed using the Service
	 * @author Pratik Shah
	 * @throws CriticalException
	 * @throws BusinessException  
	 *  * @throws BusinessException
	 * 
	 */
	protected IGenericInputData getListOfRecordsToProcess(
			FEBABatchContext batchContext, ReadOnlyUserParams userInputs) 
					throws CriticalException, BusinessException
	{

		IGenericInputData batchInputData = new GenericInputData();

		QueryOperator queryOperator = QueryOperator.openHandle(batchContext, CustomEBQueryIdentifiers.CUSTOM_NOTIFICATION_EVENT_BATCH_DAL);
		queryOperator.associate("bankId", batchContext.getBankId());
		FEBAArrayList<CustomNotificationInOutVO> notifVOList = new FEBAArrayList<>();
		try {
			notifVOList = queryOperator.fetchList(batchContext);
			System.out.println("CBETVO Fetch notification details - "+notifVOList);
		} catch (DALException de) {

			throw new BusinessException(batchContext,
					"Record does not exist.",
					de.getMessage(),
					de.getErrorCode());
		}

		batchInputData.addItems(notifVOList);
		return batchInputData;
	}


	@Override
	protected IReportMessageVO getRecordErrorMessage(
			FEBATransactionContext arg0, IFEBAValueObject arg1){

		return null;
	}

	@Override
	protected List getRecordErrorMessagesList(FEBATransactionContext arg0,
			IFEBAValueObject arg1) {

		return new ArrayList<>();
	}

	@Override
	protected IReportMessageVO getRecordSuccessMessage(
			FEBATransactionContext arg0, IFEBAValueObject arg1)
	{
		return new ReportMessageVO();
	}

	@Override
	protected List getRecordSuccessMessagesList(FEBATransactionContext arg0,
			IFEBAValueObject arg1) {
		return new ArrayList<>();
	}

	@Override
	protected String getSuccessLogMessage(FEBABatchContext arg0,
			IFEBAValueObject arg1)  {
		return null;
	}

	@Override
	protected FEBAValItem[] prepareValidationsList(FEBABatchContext arg0,
			IFEBAValueObject arg1, Object arg2){
		return null;
	}

}
