delete from PRPM where property_name in('CAMS_INQ_CHANNEL_ID','CAMS_INQ_PASSWORD','PRIVY_MERCHANT_KEY','PRIVY_AUTHORIZATION_KEY');

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CAMS_INQ_CHANNEL_ID','PINANGCERIA','Cams Inquiry Channel Id','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CAMS_INQ_PASSWORD','123456','Cams Inquiry Password','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','PRIVY_MERCHANT_KEY','hSmmyXqkgKGCw2TM7TUW','Privy API Merchant Key','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','PRIVY_AUTHORIZATION_KEY','QlJJOkYhJVtqXk0ybDI9RTBvQ3xkeTkzUjwwNURwSHJE','Privy Merchant Authorization key','N','setup',sysdate,'setup',sysdate);



COMMIT;

--Loan Status Update Batch 
delete from PRPM where property_name in('APP_STATUS_FORM_EXPIRY_PERIOD','CR_SCORE_STATUS_EXPIRY_PERIOD','DISBURSEMENT_INCOMPLETE_STATUS_PERIOD');

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg", "r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','APP_STATUS_FORM_EXPIRY_PERIOD','3', 'Application Status form expiry period', 'N','setup',sysdate, 'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg", "r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CR_SCORE_STATUS_EXPIRY_PERIOD',21, 'Application Status form expiry period', 'N','setup',sysdate, 'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg", "r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','DISBURSEMENT_INCOMPLETE_STATUS_PERIOD',21, 'Application Status form expiry period', 'N','setup',sysdate, 'setup',sysdate);
COMMIT;
