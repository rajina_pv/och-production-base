DROP TABLE S_CUSTOM_LOAN_PROD_MASTER_TABLE CASCADE CONSTRAINTS;
create table S_CUSTOM_LOAN_PROD_MASTER_TABLE
(
    DB_TS number(5,0),
	BANK_ID nvarchar2(11) NOT NULL, 
	MERCHANT_ID  nvarchar2(4),
    PRODUCT_CODE  nvarchar2(4),
    PRODUCT_CATEGORY  nvarchar2(4),
    PRODUCT_SUBCATEGORY  nvarchar2(56),
    CONFIG_TYPE  nvarchar2(4),
    MIN_AMOUNT  number(18,3),
    MAX_AMOUNT  number(18,3),
    LN_REC_ID  nvarchar2(32),
    LN_PURPOSE  nvarchar2(512),
	DEL_FLG CHAR(1), 
	R_MOD_ID NVARCHAR2(65), 
	R_MOD_TIME DATE, 
	R_CRE_ID NVARCHAR2(65), 
	R_CRE_TIME DATE,
	FUNC_CODE CHAR(1),
    MKCT_SRL_NO NUMBER(10)
)
TABLESPACE MASTER
/

create synonym S_CLPM for S_CUSTOM_LOAN_PROD_MASTER_TABLE;
/

create unique index IDX_S_CLPM
on S_CUSTOM_LOAN_PROD_MASTER_TABLE(LN_REC_ID)
TABLESPACE IDX_MASTER
/
