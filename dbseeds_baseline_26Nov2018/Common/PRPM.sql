
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','OCR_DATA_FETCH_NO_RETRIES','6',
'Number of retries for data fetch from OCR',
'N','setup',sysdate,
'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','OCR_DATA_FETCH_IN_PROGRESS_ERROR','in progress',
'In progress error message from OCR',
'N','setup',sysdate,
'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','OCR_DATA_FETCH_TIMEOUT_BEFORE_RETRY','5000',
'Time out before retrying OCH data fetch',
'N','setup',sysdate,
'setup',sysdate);

update PRPM set PROPERTY_VAL='90' where PROPERTY_NAME='COOLDOWN_PERIOD_FOR_CREDIT_REJECTION' and bank_id='01';

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values 
(1,'01','BWY','CREDT_SCORE_ROOT_URL','http://172.18.102.44:9001','Credit Score Root URL','N','setup',
sysdate,'setup',sysdate);

delete from PRPM where property_name in('ALLOWED_TRIES_FOR_API_CALL','COOLDOWN_PERIOD_FOR_API_CALL');

COMMIT;

--Loan Status Update Batch 
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','APP_STATUS_FORM_EXPIRY_PERIOD','5',
'Application Status form expiry period',
'N','setup',sysdate,
'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','CR_SCORE_STATUS_EXPIRY_PERIOD',3,
'Application Status form expiry period',
'N','setup',sysdate,
'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','DISBURSEMENT_INCOMPLETE_STATUS_PERIOD',4,
'Application Status form expiry period',
'N','setup',sysdate,
'setup',sysdate);

delete from PRPM where property_name in('CAMS_INQ_CHANNEL_ID','CAMS_INQ_PASSWORD','PRIVY_MERCHANT_KEY','PRIVY_AUTHORIZATION_KEY');

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CAMS_INQ_CHANNEL_ID','PINANGCERIA','Cams Inquiry Channel Id','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CAMS_INQ_PASSWORD','123456','Cams Inquiry Password','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','PRIVY_MERCHANT_KEY','hSmmyXqkgKGCw2TM7TUW','Privy API Merchant Key','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','PRIVY_AUTHORIZATION_KEY','QlJJOkYhJVtqXk0ybDI9RTBvQ3xkeTkzUjwwNURwSHJE','Privy Merchant Authorization key','N','setup',sysdate,'setup',sysdate);

COMMIT;
