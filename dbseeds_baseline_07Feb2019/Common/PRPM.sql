Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BLOCK_DAYS_BEFORE_PAYMENT_DUE_DATE','2','Days to block Advance payment before N due days','N','setup',sysdate,'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BLOCK_DAYS_AFTER_LOAN_START_DATE','1','Days to block advance payment after N days of loan opening','N','setup',sysdate,'setup',sysdate);
commit;

update PRPM set property_val=0 where property_name='RET_MAX_PWD_IN_HISTORY' and bank_id='01';
update PRPM set property_val='Y' where property_name='SIGNON_TXN_PWD_SAME_FLAG' and bank_id='01';

update PRPM set property_val='N' where property_name='CRM_ENABLED' and bank_id='01';
commit;



Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id",
"r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','DAYS_TO_DUE_DATE_INST_ONE','0',
'Days to due days for installment number one','N','setup',
sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id",
"r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','DAYS_TO_DUE_DATE_INST_TWO_OR_MORE','2',
'Days to due days for installment number two or more','N','setup',
sysdate,'setup',sysdate);
commit;

DELETE FROM PRPM WHERE PROPERTY_NAME IN ('CUSTOM_NOTIFICATION_CORE_PROCESSEDPATH','CUSTOM_NOTIFICATION_CORE_INPUTPATH','CUSTOM_NOTIFICATION_CORE_OUTPUTPATH');
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_PROCESSEDPATH','/opt/OCH/FebaBatch/output/ProcessedNotification/','Notification Core Batch Output file path','N','setup',sysdate,'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_INPUTPATH','/share/pinang/notifications/','Notification Core Batch Input file path','N','setup',sysdate,'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_OUTPUTPATH','/opt/OCH/FebaBatch/output/FailureNotification/','Notification Core Batch Output file path','N','setup',sysdate,'setup',sysdate);
