Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BLOCK_DAYS_BEFORE_PAYMENT_DUE_DATE','2','Days to block Advance payment before N due days','N','setup',sysdate,'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BLOCK_DAYS_AFTER_LOAN_START_DATE','1','Days to block advance payment after N days of loan opening','N','setup',sysdate,'setup',sysdate);
commit;

update PRPM set property_val=0 where property_name='RET_MAX_PWD_IN_HISTORY' and bank_id='01';
update PRPM set property_val='Y' where property_name='SIGNON_TXN_PWD_SAME_FLAG' and bank_id='01';
