package com.infosys.custom.hif.channel.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.model.IHostMessage;
import com.infosys.feba.framework.types.FEBAStringBuilder;
import com.infosys.fentbase.common.FBAConstants;

/**
 * Class added as part of HIF Rest Service Integration Invokes the REST API and
 * gets the response from the REST service and passes the response back.
 * 
 * @author Suresh
 */
public class CustomRestClient {

	private static final String CONNECT_TIMEOUT = "CONNECT_TIMEOUT";
	private static final String READ_TIMEOUT = "READ_TIMEOUT";
	private static final String METHOD_POST = "POST";
	private static final String METHOD_GET = "GET";
	private static final String METHOD_PUT = "PUT";
	private static final String METHOD_DELETE = "DELETE";
	private static final String METHOD_HEAD = "HEAD";
	private static final String METHOD_OPTIONS = "OPTIONS";
	private static final String METHOD_TRACE = "TRACE";

	private static final String XML_HTTP = "XML/HTTP";
	private static final String JSON_HTTP = "JSON/HTTP";
	private static final String ATOM_XML_HTTP = "ATOM_XML/HTTP";
	private static final String MULITPART_FORM_DATA_HTTP = "MULITPART_FORM_DATA/HTTP";

	// creation of Client instance is expensive, Methods to create instances of
	// WebTarget are thread-safe
	private static Client jersyClient = null;
	// =Client.create();

	public String sSSLCertName = null;

	/**
	 * Properties
	 */
	public static Properties properties;

	/**
	 * to set the properties
	 * 
	 * @param properties
	 */
	public static synchronized void setProperties(Properties pProperties) {
		// System.out.println("jersyClient outer--------->"+ jersyClient);
		if (jersyClient == null) {
			// System.out.println("jersyClient inner --------->"+jersyClient);
			// setting the timeout parameters
			CustomRestClient.properties = pProperties;

			ClientConfig config = new ClientConfig();
			config.register(MultiPartFeature.class);
			config.property(ClientProperties.CONNECT_TIMEOUT, properties.getProperty(CONNECT_TIMEOUT));
			config.property(ClientProperties.READ_TIMEOUT, properties.getProperty(READ_TIMEOUT));

			// creating the REST client
			jersyClient = ClientBuilder.newClient(config);

			SslConfigurator sslConfig = getSSlConfig();
			SSLContext sc = sslConfig.createSSLContext();
			jersyClient = ClientBuilder.newBuilder().build();
			// System.out.println("jersyClient After creating
			// --------->"+jersyClient);
		}

	}

	/***
	 *
	 * Method used for POST REQUEST
	 * 
	 * @param pTransactionContext
	 ** @param pHostMessage
	 * @return
	 * @throws CriticalException
	 */

	public static String post(FEBATransactionContext pTransactionContext, IHostMessage pHostMessage)
			throws CriticalException {
		String responseString = null;
		// Client jersyClient = Client.create();

		// getting the request headers set in tranformer
		Map<String, String> reqHeaders = pHostMessage.getRequestHeaders();
		// getting the request json message created in transformer
		String requestMessage = (String) pHostMessage.getMessage();
		// getting the log message created in transformer.
		// String logMessage=
		// pHostMessage.getHostMessageConfig().getLogMessage();

		String URL = "";


		LogManager.logDebug(pTransactionContext, "HttpPostClient : In POST method ");

		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			URL = prepareURL(pHostMessage);
			HttpPost httpPost = new HttpPost(URL);
			MultipartEntityBuilder preparedEntity = prepareEntity(requestMessage,
					pHostMessage.getHostMessageConfig().getRouteName());

			String timestamp = getCurrentTimeStamp();
			httpPost.setHeader("Content-type", "multipart/form-data; boundary=X-BRI-Digital-Boundary");
			httpPost.setHeader("X-BRI-Timestamp", timestamp);

			if(null != pHostMessage.getUrlPath() && pHostMessage.getUrlPath().contains("privy")){
				String merchantKey = PropertyUtil.getProperty("PRIVY_MERCHANT_KEY", pTransactionContext);
				String privyAuthorizationKey = PropertyUtil.getProperty("PRIVY_AUTHORIZATION_KEY", pTransactionContext);
				String authorization = "Basic "+privyAuthorizationKey;
				httpPost.setHeader("Authorization", authorization);
				httpPost.setHeader("merchantKey", merchantKey);
			}

			httpPost.setEntity(preparedEntity.build());

			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			preparedEntity.build().writeTo(bytes);
			String content = bytes.toString();

			String signature = generateHMACHeader(pTransactionContext, pHostMessage.getUrlPath(), "POST", timestamp,
					content);
			httpPost.setHeader("X-BRI-Signature", signature);
			Map<String, String> reqHeader = pHostMessage.getRequestHeaders();


			if(null == reqHeader){
				reqHeader = new HashMap<String, String>();
			}
			reqHeader.put(CustomEBConstants.BRI_SIGNATURE_TOKEN_TAG, signature);
			reqHeader.put(CustomEBConstants.BRI_SIGNATURE_TIMESTAMP_TAG, timestamp);
			reqHeader.put("Content-type", "multipart/form-data");
			
				LogManager.log(pTransactionContext,
						"JSONRestClient : POST Request details: " + getHTTPRequestDetails(METHOD_POST, content, URL),
						LogManager.HOST_MESSAGE);
	

			// getting the response string
			HttpResponse response = httpclient.execute(httpPost);
			System.out.println(response);

			HttpEntity resEntity = response.getEntity();
			responseString = EntityUtils.toString(resEntity);

			LogManager.log(pTransactionContext, "HttpPostClient : POST Response details: "
					+ getHTTPResponseDetails(METHOD_POST, responseString, ""), LogManager.HOST_MESSAGE);

			EntityUtils.consume(resEntity);
			httpclient.getConnectionManager().shutdown();

			// handleResponseHeader(pTransactionContext,response);

			// System.out.println(responseString);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logDebug(pTransactionContext, "JSONRestClient : Error in HTTP transmission : " + e.getMessage());
			throw new CriticalException(pTransactionContext, FEBAIncidenceCodes.HTTP_POST_TRANSMISSION_FAILED,
					"JSON on HTTP transmission to[" + URL + "] failed.", ErrorCodes.HTTP_POST_TRANSMISSION_FAILED, e);
		}

		return responseString;
	}

	/**
	 * Method used for creating the final REQUEST log message
	 * 
	 * @param httpMethod
	 * @param webTarget
	 * @param pContent
	 * @param requestHeaders
	 * @return
	 */

	private static String getHTTPRequestDetails(String httpMethod, String pContent, String URL) {

		FEBAStringBuilder returnString = new FEBAStringBuilder("");
		returnString.append("\n--------------------------------------------\n");
		returnString.append("Request URL: " + URL);
		returnString.append("\n");
		returnString.append("Request method: " + httpMethod);
		returnString.append("\n");
		returnString.append("Request content: " + maskingOTP(pContent, URL));
		returnString.append("\n--------------------------------------------\n");

		return returnString.toString();
	}

	/**
	 * Method used for creating the final RESPONSE log message
	 * 
	 * @param httpMethod
	 * @param response
	 * @param pContent
	 * @return
	 */

	private static String getHTTPResponseDetails(String httpMethod, String response,

			String pContent) {
		FEBAStringBuilder returnString = new FEBAStringBuilder("");
		returnString.append("\n--------------------------------------------\n");
		returnString.append("Response method: " + httpMethod);
		returnString.append("\n");
		returnString.append("Response content: " + response);
		returnString.append("\n--------------------------------------------\n");

		return returnString.toString();
	}

	/**
	 *
	 * To handle different response headers
	 *
	 *
	 *
	 */
	private static void handleResponseHeader(FEBATransactionContext pTransactionContext, Response response)
			throws CriticalException {
		/*
		 * if(!(response.getStatus()==200 || response.getStatus()==201
		 * ||response.getStatus()==204)) { throw new
		 * CriticalException(pTransactionContext,
		 * FEBAIncidenceCodes.RESPONSE_NOT_200, ErrorCodes.RESPONSE_NOT_200); }
		 */

		/**
		 * TO-DO
		 */
	}

	/**
	 * Prepare the REST URL endpoint
	 *
	 * @param pHostMessage
	 * @return
	 */

	private static String prepareURL(IHostMessage pHostMessage) throws MalformedURLException, URISyntaxException

	{

		// getting the REST url configured in HIF_Message.xml
		String sURL = pHostMessage.getHostMessageConfig().getRootURL();
		// getting the url path set in the tranformer
		String urlPath = pHostMessage.getUrlPath();
		// getting the request url params set in tranformer
		Map<String, String> requestUrlParams = pHostMessage.getRequestUrlParams();

		if (urlPath != null) {
			sURL = sURL + urlPath;
		}

		String splChar = "?";// initially before setting the first param

		if (requestUrlParams != null) {

			for (Object key : requestUrlParams.keySet()) {
				sURL = sURL + splChar + key.toString() + "=" + requestUrlParams.get(key.toString());
				splChar = "&";// changed so that ampersand is used from second
				// param onwards
			}
		}

		validateURI(sURL);
		return sURL;

	}

	/**
	 * Prepare the Invocation Builder
	 *
	 * @param pHostMessage
	 * @param webTarget
	 * @return
	 */

	private static Invocation.Builder prepareBuilder(IHostMessage pHostMessage, Map reqHeaders, WebTarget webTarget) {
		Invocation.Builder builder = null;
		if (pHostMessage.getHostMessageConfig().getRouteName().equalsIgnoreCase(MULITPART_FORM_DATA_HTTP)) {
			builder = webTarget.request(MediaType.MULTIPART_FORM_DATA);
			builder.accept(MediaType.MULTIPART_FORM_DATA);
		} else {
			builder = webTarget.request(MediaType.TEXT_PLAIN);
			builder.accept(MediaType.TEXT_PLAIN);
		}
		// setting the request headers
		if (reqHeaders != null) {
			for (Object key : reqHeaders.keySet()) {
				builder.header(key.toString(), reqHeaders.get(key.toString()));
			}
		}

		return builder;

	}

	/**
	 * prepare the entity
	 * 
	 * @param requestMessage
	 * @param routeName
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private static MultipartEntityBuilder prepareEntity(String requestMessage, String routeName)
			throws UnsupportedEncodingException {

		MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
		String[] bodyParts = requestMessage.split("&");
		for (int iCount = 0; iCount < bodyParts.length; iCount++) {
			String[] bodyPart = bodyParts[iCount].split("=#@");
			if (bodyPart.length > 1) {
				if (bodyPart[0].contains("selfie")) {
					reqEntity.addTextBody(bodyPart[0], bodyPart[1], ContentType.APPLICATION_OCTET_STREAM);
				}else if(bodyPart[0].contains("file")){
					reqEntity.addPart("file", new FileBody(new File(bodyPart[1])));
				}
				else {
					reqEntity.addPart(bodyPart[0], new StringBody(bodyPart[1]));
				}
			} else {
				reqEntity.addPart(bodyPart[0], new StringBody(""));
			}

		}
		reqEntity.setBoundary("X-BRI-Digital-Boundary");
		return reqEntity;

	}

	/**
	 * URL validation
	 * 
	 * @param url
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */

	private static void validateURI(String url) throws MalformedURLException, URISyntaxException {
		URL u = null;

		try {
			u = new URL(url);
		} catch (MalformedURLException e) {
			throw e;
		}

		try {
			u.toURI();
		} catch (URISyntaxException e) {
			throw e;
		}

	}

	/**
	 * getting SSL Congiguration
	 * 
	 * @param url
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */
	private static SslConfigurator getSSlConfig() {
		// reading the property from WAS Custom properties

		String keyStoreProvider = PropertyUtil.getProperty(FBAConstants.ENCRYPTION_PROVIDER, null);
		String keyManagFactAlgo = PropertyUtil.getProperty(FBAConstants.KEY_MANAGER_FACT_ALGO, null);
		String clientProtocol = PropertyUtil.getProperty(FBAConstants.CLIENT_PROTOCOL, null);
		String trustStorePassword = PropertyUtil.getProperty(FBAConstants.STOREPASSWORD, null);
		String trustStoreType = PropertyUtil.getProperty(FBAConstants.TRUST_STORE_TYPE, null);
		String trustStoreFile = PropertyUtil.getProperty(FBAConstants.TRUST_STORE_PATH, null);
		String trustStoreMangFactAlgo = PropertyUtil.getProperty(FBAConstants.TRUST_MANAGER_FACT_ALGO, null);

		// configuring the sslconfig
		SslConfigurator sslConfig = SslConfigurator.newInstance().trustStoreFile(trustStoreFile)
				.trustStorePassword(trustStorePassword).trustStoreType(trustStoreType)
				.trustManagerFactoryAlgorithm(trustStoreMangFactAlgo)

				.keyStoreFile(trustStoreFile).keyPassword(trustStorePassword).keyStoreType(trustStoreType)
				.keyManagerFactoryAlgorithm(keyManagFactAlgo).keyStoreProvider(keyStoreProvider)
				.securityProtocol(clientProtocol);
		return sslConfig;
	}

	private static String generateHMACHeader(FEBATransactionContext pTransactionContext, String url, String reqMethod,
			String timestamp, String body) {

		String apigeeToken = PropertyUtil.getProperty(CustomEBConstants.APIGEE_TOKEN, pTransactionContext);
		String apigeeSecretKey = PropertyUtil.getProperty(CustomEBConstants.APIGEE_SECRET_KEY, pTransactionContext);

		StringBuilder inputText = new StringBuilder();
		String generatedToken = null;
		try {

			inputText.append("path=" + url + "&");
			inputText.append("verb=" + reqMethod.toUpperCase() + "&");
			inputText.append("token=" + apigeeToken + "&");
			inputText.append("timestamp=" + timestamp + "&");
			inputText.append("body=" + body);
			Base64.Encoder encoder = Base64.getEncoder();

			Mac sha256HMAC = Mac.getInstance(CustomEBConstants.HMAC_ALGO);
			SecretKeySpec apigeeSecretKeySpec = new SecretKeySpec(apigeeSecretKey.getBytes(),
					CustomEBConstants.HMAC_ALGO);
			sha256HMAC.init(apigeeSecretKeySpec);

			generatedToken = encoder.encodeToString(sha256HMAC.doFinal(inputText.toString().getBytes()));
		} catch (Exception e) {
		}
		return generatedToken;
	}
	private static String maskingOTP(String content, String URL) {
		String maskedContent = "";

		if(URL.contains("agro") || URL.contains("otpcheck")){
			String re1=".*?";	 
			String re2="(\\d{5,6})";	
			Pattern p = Pattern.compile(re1+re2,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			Matcher m = p.matcher(content);
			if (m.find()){
				String appearanceOne=m.group(1);
		        maskedContent = content.replace(appearanceOne, "xxxxxx");
			}

		}else{
			maskedContent=content;
		}

		return maskedContent;
	}

	private static String getCurrentTimeStamp() {
		return ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT);
	}



}
