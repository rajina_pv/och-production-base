Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211102,'CUST','001','0','Gagal verifikasi foto.Selfie_photo do not belong to the same individual.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211103,'CUST','001','0','Internal service error, try in 15 minutes.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211104,'CUST','001','0','NIK and selfie_photo do not belong to the same individual.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211105,'CUST','001','0','NIK registered. Face in the photo entry can not be detected.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211106,'CUST','001','0','NIK not registered. Selfie_photo is null because NIK not registered so the data can not be verified.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

