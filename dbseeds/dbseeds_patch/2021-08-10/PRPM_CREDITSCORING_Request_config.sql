Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CREDITSCORE_NP','CustomPinangCreditScoreNonPayrollRequest','CreditScoreRequest for non-payroll customers','N','setup',sysdate,'setup',sysdate);

update PRPM set property_val = 'CustomPinangCreditScoreNonPayrollRequest' where property_name = 'CREDITSCORE_NP';


-- CS API request entry for credit score privilege customer
SET DEFINE OFF;
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CREDITSCORE_PP','CustomCreditScorePrivilegeRequest','CreditScoreRequest for payroll privilege customers','N','setup',sysdate,'setup',sysdate);

-- CS API request entry for credit score pinang customer
SET DEFINE OFF;
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CREDITSCORE_P','CustomPinangCreditScoreRequest','CreditScoreRequest for payroll customers','N','setup',sysdate,'setup',sysdate);
