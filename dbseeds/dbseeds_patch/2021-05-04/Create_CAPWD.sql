

drop table ececuser.CUSTOM_APPLICANT_PAYROLL_WS_DLTS;
create table ececuser.CUSTOM_APPLICANT_PAYROLL_WS_DLTS
(
	DB_TS NUMBER(5,0),
	BANK_ID NVARCHAR2(11) NOT NULL,
	APPLICATION_ID NUMBER(10,0) NOT NULL,
	account_Number               NVARCHAR2(40),
	nik				             NVARCHAR2(40),
	name			             NVARCHAR2(40),
	payroll_Date	             Date         ,
	income                       NVARCHAR2(40),
	job_Status		             NVARCHAR2(40),
	working_Start_Date			 Date         ,
	remaining_Working_Period     NVARCHAR2(40),
	working_Period				 NVARCHAR2(40),
	median_Credit                NVARCHAR2(40),
	median_Balance               NVARCHAR2(40),
	company_Name                 NVARCHAR2(80),
	bank_Code	                 NVARCHAR2(40),
	bank_Name	                 NVARCHAR2(80),
	user_Maker	                 NVARCHAR2(40),
	user_Signer	                 NVARCHAR2(40),
	rgdesc	                	 NVARCHAR2(40),
	mbdesc	                	 NVARCHAR2(40),
	brdesc	                	 NVARCHAR2(40),
	success	                     NVARCHAR2(15),
	respCode                     NVARCHAR2(10),
	respDesc                     NVARCHAR2(80),
	DEL_FLG CHAR(1),
	R_MOD_ID NVARCHAR2(65),
	R_MOD_TIME DATE,
	R_CRE_ID NVARCHAR2(65),
	R_CRE_TIME DATE 
) 
TABLESPACE MASTER
/
drop synonym CAPWD;
create synonym CAPWD for CUSTOM_APPLICANT_PAYROLL_WS_DLTS
/
drop index IDX_CAPWD;
create unique index IDX_CAPWD
on CUSTOM_APPLICANT_PAYROLL_WS_DLTS( BANK_ID, APPLICATION_ID )
TABLESPACE IDX_MASTER
/

