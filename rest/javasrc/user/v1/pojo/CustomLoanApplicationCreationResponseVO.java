package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanApplicationCreationResponseVO extends RestHeaderFooterHandler {
	private CustomLoanApplicationCreationOutputVO data = null;
	
	public void setData(CustomLoanApplicationCreationOutputVO data) {
		this.data = data;
	}

	public CustomLoanApplicationCreationOutputVO getData() {
		return data;
	}

}
