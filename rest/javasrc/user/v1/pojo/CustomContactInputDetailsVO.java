package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Contact details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomContactInputDetailsVO {
	
	@ApiModelProperty(value = "Application id")
	private Long applicationId;
	
	@ApiModelProperty(value = "Mother Name")
	private String motherName;
	
	@ApiModelProperty(value = "No of dependents")
	private int noOfDependent;
	
	@ApiModelProperty(value = "marital status")
	private String maritalStatus;
	
	@ApiModelProperty(value = "Spouse Id")
	private String spouseId;
	
	@ApiModelProperty(value = "Partner Nik Id")
	private String partnerNikId;
	
	@ApiModelProperty(value = "Partner Name")
	private String partnerName;
	
	@ApiModelProperty(value = "Contact Name")
	private String contactName;

	@ApiModelProperty(value = "relationship")
	private String relationShip;

	@ApiModelProperty(value = "Contact Number")
	private Long contactNumber;
	
	@ApiModelProperty(value = "Contact Address")
	private String contactAddress;
	
	@ApiModelProperty(value = "Contact Postal code")
	private Long contactPostalCode;
	
	@ApiModelProperty(value = "Marital Reference VO")
	private CodeReferencesVO maritalStatusReference;
	
	@ApiModelProperty(value = "Relationship Reference VO")
	private CodeReferencesVO relationshipReference;
	
	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public int getNoOfDependent() {
		return noOfDependent;
	}

	public void setNoOfDependent(int noOfDependent) {
		this.noOfDependent = noOfDependent;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getSpouseId() {
		return spouseId;
	}

	public void setSpouseId(String spouseId) {
		this.spouseId = spouseId;
	}

	public String getPartnerNikId() {
		return partnerNikId;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public void setPartnerNikId(String partnerNikId) {
		this.partnerNikId = partnerNikId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getRelationShip() {
		return relationShip;
	}

	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactAddress() {
		return contactAddress;
	}

	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}

	public Long getContactPostalCode() {
		return contactPostalCode;
	}

	public void setContactPostalCode(Long contactPostalCode) {
		this.contactPostalCode = contactPostalCode;
	}
	
	public CodeReferencesVO getMaritalStatusReference() {
		return maritalStatusReference;
	}

	public void setMaritalStatusReference(CodeReferencesVO maritalStatusReference) {
		this.maritalStatusReference = maritalStatusReference;
	}

	public CodeReferencesVO getRelationshipReference() {
		return relationshipReference;
	}

	public void setRelationshipReference(CodeReferencesVO relationshipReference) {
		this.relationshipReference = relationshipReference;
	}
}
