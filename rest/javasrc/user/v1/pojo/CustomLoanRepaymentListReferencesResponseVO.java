package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanRepaymentListReferencesResponseVO extends RestHeaderFooterHandler {

	private List<CustomLoanRepaymentListReferencesVO> list;

	public List<CustomLoanRepaymentListReferencesVO> getList() {
		return list;
	}

	public void setList(List<CustomLoanRepaymentListReferencesVO> list) {
		this.list = list;
	}
	
}
