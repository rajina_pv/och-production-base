package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Notification List")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomAppStatusNotificationListReferencesVO {

	@ApiModelProperty(value = "Application Notification Type")
	private String appNotificationType;

	@ApiModelProperty(value = "Notification Message Short Description")
	private String shortDescription;

	@ApiModelProperty(value = "Notification Message Date")
	private String notificationDate;

	@ApiModelProperty(value = "Notification Header")
	private String notificationHeader;

	@ApiModelProperty(value = "Is New")
	private String isNew;

	@ApiModelProperty(value = "notification id")
	private String notificationId;

	@ApiModelProperty(value = "Application Notification Status")
	private String appNotificationStatus;

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public CustomAppStatusNotificationListReferencesVO() {
		super();
	}

	public String getIsNew() {
		return isNew;
	}

	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}

	public String getNotificationHeader() {
		return notificationHeader;
	}

	public void setNotificationHeader(String notificationHeader) {
		this.notificationHeader = notificationHeader;
	}

	
	public String getAppNotificationType() {
		return appNotificationType;
	}

	public void setAppNotificationType(String appNotificationType) {
		this.appNotificationType = appNotificationType;
	}

	public String getAppNotificationStatus() {
		return appNotificationStatus;
	}

	public void setAppNotificationStatus(String appNotificationStatus) {
		this.appNotificationStatus = appNotificationStatus;
	}

	public CustomAppStatusNotificationListReferencesVO(String appNotificationType, String shortDescription,
			String notificationDate, String notificationHeader, String isNew, String notificationId) {
		super();
		this.appNotificationType = appNotificationType;
		this.shortDescription = shortDescription;
		this.notificationDate = notificationDate;
		this.notificationHeader = notificationHeader;
		this.isNew = isNew;
		this.notificationId = notificationId;
	}

}
