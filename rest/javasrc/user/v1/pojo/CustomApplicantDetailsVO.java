package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Loan applicant details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomApplicantDetailsVO {
	@ApiModelProperty(value = "Application id", required = true)
	private Long applicationId;
	
	@ApiModelProperty(value = "email Id", required = true)
	private String emailId;
	
	@ApiModelProperty(value = "mobile number", required = true)
	private String mobileNo;
	
	@ApiModelProperty(value = "name", required = true)
	private String name;
	
	@ApiModelProperty(value = "gender")
	private CustomCommonCodeDetailsVO gender;
	
	@ApiModelProperty(value = "place of birth", required = true)
	private String placeOfBirth;
	
	@ApiModelProperty(value = "date of birth", required = true)
	private Date dateOfBirth;
	
	@ApiModelProperty(value = "national id", required = true)
	private String nationalId;
	
	@ApiModelProperty(value = "last education")
	private CustomCommonCodeDetailsVO lastEducation;
	
	@ApiModelProperty(value = "credit card issuer")
	private CustomCommonCodeDetailsVO creditCardIssuer;

	@ApiModelProperty(value = "Address line 1", required = true)
	private String addressLine1;
	
	@ApiModelProperty(value = "Address line 2")
	private String addressLine2;
	
	@ApiModelProperty(value = "Address line 3")
	private String addressLine3;
	
	@ApiModelProperty(value = "Address line 4")
	private CustomCommonCodeDetailsVO addressLine4;
	
	@ApiModelProperty(value = "Address line 5")
	private CustomCommonCodeDetailsVO addressLine5;
	
	@ApiModelProperty(value = "Province")
	private CustomCommonCodeDetailsVO provinceDetails;
	
	@ApiModelProperty(value = "city")
	private CustomCommonCodeDetailsVO city;
	
	@ApiModelProperty(value = "Postal code", required = true)
	private Long postalCode;
	
	@ApiModelProperty(value = "homeOwnershipStatus")
	private CustomCommonCodeDetailsVO homeOwnershipStatus;
	
	@ApiModelProperty(value = "home ownership duration")
	private CustomCommonCodeDetailsVO homeOwnershipDuration;
	
	@ApiModelProperty(value = "home phone number")
	private String homePhoneNumber;
	
	@ApiModelProperty(value = "religion")
	private CustomCommonCodeDetailsVO religion;

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CustomCommonCodeDetailsVO getGender() {
		return gender;
	}

	public void setGender(CustomCommonCodeDetailsVO gender) {
		this.gender = gender;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public CustomCommonCodeDetailsVO getLastEducation() {
		return lastEducation;
	}

	public void setLastEducation(CustomCommonCodeDetailsVO lastEducation) {
		this.lastEducation = lastEducation;
	}

	public CustomCommonCodeDetailsVO getCreditCardIssuer() {
		return creditCardIssuer;
	}

	public void setCreditCardIssuer(CustomCommonCodeDetailsVO creditCardIssuer) {
		this.creditCardIssuer = creditCardIssuer;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public CustomCommonCodeDetailsVO getAddressLine4() {
		return addressLine4;
	}

	public void setAddressLine4(CustomCommonCodeDetailsVO addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public CustomCommonCodeDetailsVO getAddressLine5() {
		return addressLine5;
	}

	public void setAddressLine5(CustomCommonCodeDetailsVO addressLine5) {
		this.addressLine5 = addressLine5;
	}

	public CustomCommonCodeDetailsVO getProvinceDetails() {
		return provinceDetails;
	}

	public void setProvinceDetails(CustomCommonCodeDetailsVO provinceDetails) {
		this.provinceDetails = provinceDetails;
	}

	public CustomCommonCodeDetailsVO getCity() {
		return city;
	}

	public void setCity(CustomCommonCodeDetailsVO city) {
		this.city = city;
	}

	public Long getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(Long postalCode) {
		this.postalCode = postalCode;
	}

	public CustomCommonCodeDetailsVO getHomeOwnershipStatus() {
		return homeOwnershipStatus;
	}

	public void setHomeOwnershipStatus(CustomCommonCodeDetailsVO homeOwnershipStatus) {
		this.homeOwnershipStatus = homeOwnershipStatus;
	}

	public CustomCommonCodeDetailsVO getHomeOwnershipDuration() {
		return homeOwnershipDuration;
	}

	public void setHomeOwnershipDuration(CustomCommonCodeDetailsVO homeOwnershipDuration) {
		this.homeOwnershipDuration = homeOwnershipDuration;
	}

	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public CustomCommonCodeDetailsVO getReligion() {
		return religion;
	}

	public void setReligion(CustomCommonCodeDetailsVO religion) {
		this.religion = religion;
	}


}
