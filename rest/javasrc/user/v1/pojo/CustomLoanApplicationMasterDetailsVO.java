package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Loan application master details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomLoanApplicationMasterDetailsVO {
	@ApiModelProperty(value = "The master application details")
	private CustomLoanApplicationInputDetailsVO applicationDetails;
	
	@ApiModelProperty(value = "The payroll details")
	private CustomPayrollInputDetailsVO payrollDetails;

	@ApiModelProperty(value = "The document details")
	private CustomDocumentInputDetailsVO documentDetails;
	
	@ApiModelProperty(value = "The applicant details")
	private CustomApplicantInputDetailsVO applicantInformation;
	
	@ApiModelProperty(value = "The applicant details")
	private CustomApplicantDetailsVO applicantDetails;
	
	@ApiModelProperty(value = "The contact details")
	private CustomContactInputDetailsVO contactInformation;
	
	@ApiModelProperty(value = "The employment details")
	private CustomEmploymentInputDetailsVO employmentInformation;
	
	@ApiModelProperty(value = "The applicant details")
	private CustomEmploymentDetailsVO employmentDetails;
	
	@ApiModelProperty(value = "The payroll details")
	private CustomResponsePayrollDetailsVO respPayrollDetails;
	
	@ApiModelProperty(value = "The payroll details")
	private CustomPayrollSessionDetailsVO payrollSessionDetails;
	
	@ApiModelProperty(value = "KTP address details")
	private CustomApplicantKtpAddressDetailsVO ktpAddressDetails;
	
	
	public CustomLoanApplicationInputDetailsVO getApplicationDetails() {
		return applicationDetails;
	}

	public void setApplicationDetails(CustomLoanApplicationInputDetailsVO applicationDetails) {
		this.applicationDetails = applicationDetails;
	}

	public CustomPayrollInputDetailsVO getPayrollDetails() {
		return payrollDetails;
	}

	public void setPayrollDetails(CustomPayrollInputDetailsVO payrollDetails) {
		this.payrollDetails = payrollDetails;
	}

	public CustomDocumentInputDetailsVO getDocumentDetails() {
		return documentDetails;
	}

	public void setDocumentDetails(CustomDocumentInputDetailsVO documentDetails) {
		this.documentDetails = documentDetails;
	}

	public CustomApplicantInputDetailsVO getApplicantInformation() {
		return applicantInformation;
	}

	public void setApplicantInformation(CustomApplicantInputDetailsVO applicantInformation) {
		this.applicantInformation = applicantInformation;
	}

	public CustomApplicantDetailsVO getApplicantDetails() {
		return applicantDetails;
	}

	public void setApplicantDetails(CustomApplicantDetailsVO applicantDetails) {
		this.applicantDetails = applicantDetails;
	}

	public CustomContactInputDetailsVO getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(CustomContactInputDetailsVO contactInformation) {
		this.contactInformation = contactInformation;
	}

	public CustomEmploymentInputDetailsVO getEmploymentInformation() {
		return employmentInformation;
	}

	public void setEmploymentInformation(CustomEmploymentInputDetailsVO employmentInformation) {
		this.employmentInformation = employmentInformation;
	}

	public CustomEmploymentDetailsVO getEmploymentDetails() {
		return employmentDetails;
	}

	public void setEmploymentDetails(CustomEmploymentDetailsVO employmentDetails) {
		this.employmentDetails = employmentDetails;
	}

	public CustomResponsePayrollDetailsVO getRespPayrollDetails() {
		return respPayrollDetails;
	}

	public void setRespPayrollDetails(CustomResponsePayrollDetailsVO respPayrollDetails) {
		this.respPayrollDetails = respPayrollDetails;
	}

	public CustomPayrollSessionDetailsVO getPayrollSessionDetails() {
		return payrollSessionDetails;
	}

	public void setPayrollSessionDetails(CustomPayrollSessionDetailsVO payrollSessionDetails) {
		this.payrollSessionDetails = payrollSessionDetails;
	}

	public CustomApplicantKtpAddressDetailsVO getKtpAddressDetails() {
		return ktpAddressDetails;
	}

	public void setKtpAddressDetails(CustomApplicantKtpAddressDetailsVO ktpAddressDetails) {
		this.ktpAddressDetails = ktpAddressDetails;
	}
	

}
