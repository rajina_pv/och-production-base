package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBADate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Loan Repayment Field List")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomLoanRepaymentListReferencesVO {

	@ApiModelProperty(value = "Loan Account Id")
	private String accountId;
	
	@ApiModelProperty(value = "Installment Number")
	private String installmentNo;
	
	@ApiModelProperty(value = "Installment Amount")
	private String installmentAmount;
	
	public String getInstallmentId() {
		return installmentId;
	}

	public void setInstallmentId(String installmentId) {
		this.installmentId = installmentId;
	}

	public String getSrlNo() {
		return srlNo;
	}

	public void setSrlNo(String srlNo) {
		this.srlNo = srlNo;
	}

	@ApiModelProperty(value = "Installment Id")
	private String installmentId;
	
	@ApiModelProperty(value = "Srl No")
	private String srlNo;
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@ApiModelProperty(value = "Currency")
	private String currency;
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getInstallmentNo() {
		return installmentNo;
	}

	public void setInstallmentNo(String installmentNo) {
		this.installmentNo = installmentNo;
	}

	public String getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public String getInstallmentDate() {
		return installmentDate;
	}

	public void setInstallmentDate(String installmentDate) {
		this.installmentDate = installmentDate;
	}

	@ApiModelProperty(value = "Installment Date")
	private String installmentDate;

	
}
