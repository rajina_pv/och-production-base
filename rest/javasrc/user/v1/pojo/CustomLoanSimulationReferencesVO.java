package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "Loan Product Simulation Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomLoanSimulationReferencesVO {

	@ApiModelProperty(value = "Product Amount")
	private Double paymentAmount;
	
	@ApiModelProperty(value = "tenor")
	private Double tenor;
	
	@ApiModelProperty(value = "yearlyInterestRate")
	private Double yearlyInterestRate;
	
	@ApiModelProperty(value = "installment Amount")
	private Double installmentAmount;
	
	@ApiModelProperty(value = "Total Interest")
	private Double totalInterest;
	
	@ApiModelProperty(value = "Repayment Amount")
	private Double repaymentAmount;

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Double getTenor() {
		return tenor;
	}

	public void setTenor(Double tenor) {
		this.tenor = tenor;
	}

	public Double getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(Double installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public Double getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(Double totalInterest) {
		this.totalInterest = totalInterest;
	}

	public Double getRepaymentAmount() {
		return repaymentAmount;
	}

	public void setRepaymentAmount(Double repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}
	
	public CustomLoanSimulationReferencesVO() {
		super();
		
	}

	public Double getYearlyInterestRate() {
		return yearlyInterestRate;
	}

	public void setYearlyInterestRate(Double yearlyInterestRate) {
		this.yearlyInterestRate = yearlyInterestRate;
	}

	public CustomLoanSimulationReferencesVO(Double paymentAmount, Double tenor, Double yearlyInterestRate,
			Double installmentAmount, Double totalInterest, Double repaymentAmount) {
		super();
		this.paymentAmount = paymentAmount;
		this.tenor = tenor;
		this.yearlyInterestRate = yearlyInterestRate;
		this.installmentAmount = installmentAmount;
		this.totalInterest = totalInterest;
		this.repaymentAmount = repaymentAmount;
	}



}
