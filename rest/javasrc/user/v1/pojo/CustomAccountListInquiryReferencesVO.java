package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Account List Inquiry")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomAccountListInquiryReferencesVO {

	@JsonIgnore
	@ApiModelProperty(value = "User Id")
	private String userid;
	
	@ApiModelProperty(value = "Purchase Date")
	private String accountOpenDate;	
	
	@ApiModelProperty(value = "Sanction Amount")
	private String purchaseAmount;
	
	@ApiModelProperty(value = "Installment Amount")
	private String installmentAmount;
	
	@ApiModelProperty(value = "Outstanding balance")
	private String totalOutstandingBalance;
	
	@ApiModelProperty(value = "Number Of Installment")
	private String numberOfInstallments;
	
	@ApiModelProperty(value = "Id of the merchant")
	private String merchantId;
	
	@ApiModelProperty(value = "Criteria field Account Opening Date From")
	private String accountOpeningDateFrom;
	
	@ApiModelProperty(value = "Criteria field Account Opening Date to")
	private String accountOpeningDateTo;
	
	@ApiModelProperty(value = "Account Status")
	private String accountStatus;
	
	@ApiModelProperty(value = "Purchase Remarks")
	private String purchaseRemarks;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(String accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public String getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public String getTotalOutstandingBalance() {
		return totalOutstandingBalance;
	}

	public void setTotalOutstandingBalance(String totalOutstandingBalance) {
		this.totalOutstandingBalance = totalOutstandingBalance;
	}

	public String getNumberOfInstallments() {
		return numberOfInstallments;
	}

	public void setNumberOfInstallments(String numberOfInstallments) {
		this.numberOfInstallments = numberOfInstallments;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getAccountOpeningDateFrom() {
		return accountOpeningDateFrom;
	}

	public void setAccountOpeningDateFrom(String accountOpeningDateFrom) {
		this.accountOpeningDateFrom = accountOpeningDateFrom;
	}

	public String getAccountOpeningDateTo() {
		return accountOpeningDateTo;
	}

	public void setAccountOpeningDateTo(String accountOpeningDateTo) {
		this.accountOpeningDateTo = accountOpeningDateTo;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getPurchaseRemarks() {
		return purchaseRemarks;
	}

	public void setPurchaseRemarks(String purchaseRemarks) {
		this.purchaseRemarks = purchaseRemarks;
	}
	
	
	
}
