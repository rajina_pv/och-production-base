package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationRetailInputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.RetailUserInputDetailsVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "Device registration details")
public class CustomDeviceRegistrationRetailInputVO {
	
	@ApiModelProperty(value="Specify the device registration retail input details.", required=true)
	private RetailUserInputDetailsVO userDetails;
	
	@ApiModelProperty(value="Specify the device details.", required=true)
	private DeviceRegistrationRetailInputVO deviceDetails;
	
	@ApiModelProperty(value="Specify the authorization details.", required=true)
	private CustomDeviceRegistrationAuthDetails authorizationDetails;
	
	@ApiModelProperty(value="Flag to state if HP Number has changed", required=true)
	private String isHPNumberChanged;
	
	public RetailUserInputDetailsVO getUserDetails() {
		return userDetails;
	}

	public String getIsHPNumberChanged() {
		return isHPNumberChanged;
	}

	public void setIsHPNumberChanged(String isHPNumberChanged) {
		this.isHPNumberChanged = isHPNumberChanged;
	}

	public void setUserDetails(RetailUserInputDetailsVO userDetails) {
		this.userDetails = userDetails;
	}

	public DeviceRegistrationRetailInputVO getDeviceDetails() {
		return deviceDetails;
	}

	public void setDeviceDetails(DeviceRegistrationRetailInputVO deviceDetails) {
		this.deviceDetails = deviceDetails;
	}

	public CustomDeviceRegistrationAuthDetails getAuthorizationDetails() {
		return authorizationDetails;
	}

	public void setAuthorizationDetails(CustomDeviceRegistrationAuthDetails authorizationDetails) {
		this.authorizationDetails = authorizationDetails;
	}


}
