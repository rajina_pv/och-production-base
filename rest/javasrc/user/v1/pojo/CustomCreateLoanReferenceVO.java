package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.BaseFileDetailsVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Create Loan")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomCreateLoanReferenceVO {

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
				
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ApiModelProperty(value = "applicationId")
	private String applicationId;
	
	
	@ApiModelProperty(value = "statusCode")
	private String statusCode;
	
	@ApiModelProperty(value = "status")
	private String status;

	@ApiModelProperty(value = "userid")
	private String userid;
	
	@ApiModelProperty(value = "payrollAccount")
	private String payrollAccount;
	
	@ApiModelProperty(value = "loanAccountId")
	private String loanAccountId;
	
	//Added for non payroll requirement :: START
	@ApiModelProperty(value = "selectedTenor")
	private String selectedTenor;
	
	public String getSelectedTenor() {
		return selectedTenor;
	}

	public void setSelectedTenor(String selectedTenor) {
		this.selectedTenor = selectedTenor;
	}
	
	@ApiModelProperty(value = "Monthly installment")
	private double monthlyInstallment;
	
	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}
	
	@ApiModelProperty(value = "Whitelist Name")
	private String nameWL;
	

	public String getNameWL() {
		return nameWL;
	}

	public void setNameWL(String nameWL) {
		this.nameWL = nameWL;
	}
	//Added for non payroll requirement :: END
	
	
	public String getLoanAccountId() {
		return loanAccountId;
	}

	public void setLoanAccountId(String loanAccountId) {
		this.loanAccountId = loanAccountId;
	}

	public String getPayrollAccount() {
		return payrollAccount;
	}

	public void setPayrollAccount(String payrollAccount) {
		this.payrollAccount = payrollAccount;
	}

	public String getAccountLimit() {
		return accountLimit;
	}

	public void setAccountLimit(String accountLimit) {
		this.accountLimit = accountLimit;
	}

	@ApiModelProperty(value = "loanAmount")
	private String accountLimit;

		public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
			
}
