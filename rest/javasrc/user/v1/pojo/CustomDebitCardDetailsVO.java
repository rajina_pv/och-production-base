package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Anju_AL
 * 
 * This is the VO which holds the fields for debit card authentication details.
 *
 */
@ApiModel(value="The debit card details of the user required for authentication.")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomDebitCardDetailsVO {

	@ApiModelProperty(value="The debit card number of the user.", required=false)
	@Size(min=0,max=19)
	private String debitCardNumber; 

	@ApiModelProperty(value="The card expiry date of the user.", required=false)
	private String debitCardExpiryDate;
	
	@ApiModelProperty(value="The name on the card.", required=false)
	private String debitKtpNo;
	
	@ApiModelProperty(value = "Bank Code", required  = true)
	private String bankCode;
	
	@ApiModelProperty(value = "product Code", required  = true)
	private String productCode;
	

	public String getDebitCardNumber() {
		return debitCardNumber;
	}

	public void setDebitCardNumber(String debitCardNumber) {
		this.debitCardNumber = debitCardNumber;
	}

	public String getDebitCardExpiryDate() {
		return debitCardExpiryDate;
	}

	public void setDebitCardExpiryDate(String debitCardExpiryDate) {
		this.debitCardExpiryDate = debitCardExpiryDate;
	}

	public String getDebitKtpNo() {
		return debitKtpNo;
	}

	public void setDebitKtpNo(String debitKtpNo) {
		this.debitKtpNo = debitKtpNo;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	
}
