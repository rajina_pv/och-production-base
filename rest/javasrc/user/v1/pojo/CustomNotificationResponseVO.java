package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "Notification Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomNotificationResponseVO extends RestHeaderFooterHandler{

	private CustomNotificationOutputVO data;

	public CustomNotificationOutputVO getData() {
		return data;
	}

	public CustomNotificationResponseVO() {
		super();
	}

	public CustomNotificationResponseVO(CustomNotificationOutputVO data) {
		super();
		this.data = data;
	}

	public void setData(CustomNotificationOutputVO data) {
		this.data = data;
	}

	
}
