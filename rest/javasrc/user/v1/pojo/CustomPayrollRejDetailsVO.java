package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomPayrollRejDetailsVO {
	
@ApiModelProperty(value = "Next eligibility date")
private Date nextEligibilityDate;

public Date getNextEligibilityDate() {
	return nextEligibilityDate;
}

public void setNextEligibilityDate(Date nextEligibilityDate) {
	this.nextEligibilityDate = nextEligibilityDate;
}
}
