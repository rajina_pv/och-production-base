package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class CustomApprovalDetailsVO {
	@ApiModelProperty(value = "applied limit")
	private double appliedAmount;
	
	@ApiModelProperty(value = "approval amount")
	private double approvalAmount;
	
	@ApiModelProperty(value = "plafond1")
	private double plafond1;
	
	@ApiModelProperty(value = "plafond2")
	private double plafond2;
	
	@ApiModelProperty(value = "tenor1")
	private String tenor1;
	
	@ApiModelProperty(value = "tenor2")
	private String tenor2;
	
	@ApiModelProperty(value = "disbursement account")
	private String disbursementAccount;
	
	@ApiModelProperty(value = "disbursement account branch")
	private String accountBranch;
	
	@ApiModelProperty(value = "disbursement account name")
	private String accountName;
	
	@ApiModelProperty(value = "validity date")
	private Date validityDate;
	
	@ApiModelProperty(value = "Is KTP Verified")
	private String isKTPVerified;
	
	@ApiModelProperty(value = "Reupload KTP flag")
	private String isKTPReUploaded;
	
	@ApiModelProperty(value = "KTP rejection count")
	private int ktpRejectCnt;
	
	@ApiModelProperty(value = "Customer name")
	private String nama;
	
	@ApiModelProperty(value = "Time left for BRIVA payment")
	private long timeLeftForBrivaPayment;
	
	@ApiModelProperty(value = "payment Status Flag")
	private String paymentStatusFlg;
	
	// Added for PINANG Pay later
	@ApiModelProperty(value = "feeAmount")
	private double feeAmountForPlanfond1;	
	
	// Added for PINANG Pay later
	@ApiModelProperty(value = "feeAmount")
	private double feeAmountForPlanfond2;	
	
	@ApiModelProperty(value = "Loan tenor and monthly payment details")
	private List<CustomMonthlyPayDetails> monthlyPayDetails;
	
	@ApiModelProperty(value = "Loan tenor and monthly payment details")
	CustomOneMonthlyPaymentDetails oneMonthPayDetails;

	public String getIsKTPVerified() {
		return isKTPVerified;
	}

	public void setIsKTPVerified(String isKTPVerified) {
		this.isKTPVerified = isKTPVerified;
	}

	public double getAppliedAmount() {
		return appliedAmount;
	}

	public void setAppliedAmount(double appliedAmount) {
		this.appliedAmount = appliedAmount;
	}

	public double getApprovalAmount() {
		return approvalAmount;
	}

	public void setApprovalAmount(double approvalAmount) {
		this.approvalAmount = approvalAmount;
	}

	public String getDisbursementAccount() {
		return disbursementAccount;
	}

	public void setDisbursementAccount(String disbursementAccount) {
		this.disbursementAccount = disbursementAccount;
	}

	public String getAccountBranch() {
		return accountBranch;
	}

	public void setAccountBranch(String accountBranch) {
		this.accountBranch = accountBranch;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}

	public double getPlafond1() {
		return plafond1;
	}

	public void setPlafond1(double plafond1) {
		this.plafond1 = plafond1;
	}

	public double getPlafond2() {
		return plafond2;
	}

	public void setPlafond2(double plafond2) {
		this.plafond2 = plafond2;
	}

	public String getTenor1() {
		return tenor1;
	}

	public void setTenor1(String tenor1) {
		this.tenor1 = tenor1;
	}

	public String getTenor2() {
		return tenor2;
	}

	public void setTenor2(String tenor2) {
		this.tenor2 = tenor2;
	}

	
	public List<CustomMonthlyPayDetails> getMonthlyPayDetails() {
		return monthlyPayDetails;
	}

	public void setMonthlyPayDetails(List<CustomMonthlyPayDetails> monthlyPayDetails) {
		this.monthlyPayDetails = monthlyPayDetails;
	}
	
	public CustomOneMonthlyPaymentDetails getOneMonthPayDetails() {
		return oneMonthPayDetails;
	}

	public void setOneMonthPayDetails(CustomOneMonthlyPaymentDetails oneMonthPayDetails) {
		this.oneMonthPayDetails = oneMonthPayDetails;
	}
	
	public String getIsKTPReUploaded() {
		return isKTPReUploaded;
	}

	public void setIsKTPReUploaded(String isKTPReUploaded) {
		this.isKTPReUploaded = isKTPReUploaded;
	}
	
	public int getKtpRejectCnt() {
		return ktpRejectCnt;
	}

	public void setKtpRejectCnt(int ktpRejectCnt) {
		this.ktpRejectCnt = ktpRejectCnt;
	}
	
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	
	public long getTimeLeftForBrivaPayment() {
		return timeLeftForBrivaPayment;
	}

	public void setTimeLeftForBrivaPayment(long timeLeftForBrivaPayment) {
		this.timeLeftForBrivaPayment = timeLeftForBrivaPayment;
	}

	public String getPaymentStatusFlg() {
		return paymentStatusFlg;
	}

	public void setPaymentStatusFlg(String paymentStatusFlg) {
		this.paymentStatusFlg = paymentStatusFlg;
	}
	

	public double getFeeAmountForPlanfond1() {
		return feeAmountForPlanfond1;
	}

	public void setFeeAmountForPlanfond1(double feeAmountForPlanfond1) {
		this.feeAmountForPlanfond1 = feeAmountForPlanfond1;
	}

	public double getFeeAmountForPlanfond2() {
		return feeAmountForPlanfond2;
	}

	public void setFeeAmountForPlanfond2(double feeAmountForPlanfond2) {
		this.feeAmountForPlanfond2 = feeAmountForPlanfond2;
	}

	public CustomApprovalDetailsVO(double appliedAmount, double approvalAmount, double plafond1, double plafond2,
			String tenor1, String tenor2, String disbursementAccount, String accountBranch, String accountName,
			Date validityDate, String isKTPVerified, String isKTPReUploaded, int ktpRejectCnt,  List<CustomMonthlyPayDetails> monthlyPayDetails,
			CustomOneMonthlyPaymentDetails oneMonthPayDetails, String nama) {
		super();
		this.appliedAmount = appliedAmount;
		this.approvalAmount = approvalAmount;
		this.plafond1 = plafond1;
		this.plafond2 = plafond2;
		this.tenor1 = tenor1;
		this.tenor2 = tenor2;
		this.disbursementAccount = disbursementAccount;
		this.accountBranch = accountBranch;
		this.accountName = accountName;
		this.validityDate = validityDate;
		this.isKTPVerified = isKTPVerified;
		this.isKTPReUploaded = isKTPReUploaded;
		this.ktpRejectCnt = ktpRejectCnt;
		this.monthlyPayDetails = monthlyPayDetails;
		this.oneMonthPayDetails = oneMonthPayDetails;
		this.nama = nama;
	}

	public CustomApprovalDetailsVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
