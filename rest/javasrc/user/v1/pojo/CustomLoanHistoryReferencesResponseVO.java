package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanHistoryReferencesResponseVO extends RestHeaderFooterHandler {
	private List<CustomLoanHistoryReferencesVO> data;

	public List<CustomLoanHistoryReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomLoanHistoryReferencesVO> data) {
		this.data = data;
	}
}
