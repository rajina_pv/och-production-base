package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Loan application input details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomResponsePayrollDetailsVO {
	
	@ApiModelProperty(value = "Payroll account number", required = true)
	private String payrollAccount;
	
	@ApiModelProperty(value = "Payroll account branch", required = true)
	private String payrollAccountBranch;
	
	@ApiModelProperty(value = "Saving amount", required = true)
	private double savingAmount;
	
	@ApiModelProperty(value = "Whitelist Name")
	private String nameWL;
	
	
	@ApiModelProperty(value = "Whitelist Birth Date")
	private Date birthDateWL;
	

	@ApiModelProperty(value = "Whitelist Gender")
	private String genderWL;
	
	@ApiModelProperty(value = "Whitelist Address")
	private String addressWL;
	
	@ApiModelProperty(value = "Whitelist Place of Birth")
	private String placeOfBirthWL;

	public String getPayrollAccount() {
		return payrollAccount;
	}

	public void setPayrollAccount(String payrollAccount) {
		this.payrollAccount = payrollAccount;
	}

	public String getPayrollAccountBranch() {
		return payrollAccountBranch;
	}

	public void setPayrollAccountBranch(String payrollAccountBranch) {
		this.payrollAccountBranch = payrollAccountBranch;
	}

	public double getSavingAmount() {
		return savingAmount;
	}

	public void setSavingAmount(double savingAmount) {
		this.savingAmount = savingAmount;
	}

	public String getNameWL() {
		return nameWL;
	}

	public void setNameWL(String nameWL) {
		this.nameWL = nameWL;
	}

	public Date getBirthDateWL() {
		return birthDateWL;
	}

	public void setBirthDateWL(Date birthDateWL) {
		this.birthDateWL = birthDateWL;
	}

	public String getGenderWL() {
		return genderWL;
	}

	public void setGenderWL(String genderWL) {
		this.genderWL = genderWL;
	}

	public String getAddressWL() {
		return addressWL;
	}

	public void setAddressWL(String addressWL) {
		this.addressWL = addressWL;
	}

	public String getPlaceOfBirthWL() {
		return placeOfBirthWL;
	}

	public void setPlaceOfBirthWL(String placeOfBirthWL) {
		this.placeOfBirthWL = placeOfBirthWL;
	}
	
	

		
	
}
