package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomLoanLimitDetailsVO {
	@ApiModelProperty(value = "sanctioned limit")
	private double sanctionedLimit;
	
	@ApiModelProperty(value = "available limit")
	private double availableLimit;

	public double getSanctionedLimit() {
		return sanctionedLimit;
	}

	public void setSanctionedLimit(double sanctionedLimit) {
		this.sanctionedLimit = sanctionedLimit;
	}

	public double getAvailableLimit() {
		return availableLimit;
	}

	public void setAvailableLimit(double availableLimit) {
		this.availableLimit = availableLimit;
	}

	
}
