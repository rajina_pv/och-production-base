package com.infosys.custom.ebanking.rest.user.v1.pojo;

import io.swagger.annotations.ApiModelProperty;

public class CustomCommonCodeDetailsVO {
	@ApiModelProperty(value = "application status code type")
	private String codeType;
	
	@ApiModelProperty(value = "application status cm code")
	private String cmCode;
	
	@ApiModelProperty(value = "application status code desc")
	private String codeDescription;

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getCmCode() {
		return cmCode;
	}

	public void setCmCode(String cmCode) {
		this.cmCode = cmCode;
	}

	public String getCodeDescription() {
		return codeDescription;
	}

	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}
}
