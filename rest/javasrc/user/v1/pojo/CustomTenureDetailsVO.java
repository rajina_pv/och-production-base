package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "Loan Product Maintenance Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomTenureDetailsVO {

	@ApiModelProperty(value = "Tenure")
	private String tenure;
	
	@ApiModelProperty(value ="Interest")
	private String interest;
	
	public String getTenure() {
		return tenure;
	}
	public void setTenure(String tenure) {
		this.tenure = tenure;
	}
	public String getInterest() {
		return interest;
	}
	public void setInterest(String interest) {
		this.interest = interest;
	}

	public CustomTenureDetailsVO(String tenure, String interest) {
		super();
		this.tenure = tenure;
		this.interest = interest;
	}
	public CustomTenureDetailsVO(){
		super();
	}
	
}
