package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(value = "Device Authorization details")
public class CustomDeviceRegistrationAuthDetails {
	
	@ApiModelProperty(value="Specified by the bank .In navigaton.xml oneTimePassword is required .", required=false)
	@Size(min=0,max=20)
	private String oneTimePassword;	

	@ApiModelProperty(value="Specify the Channel. SMS or EMAIL", required=true)
	private String channel;
	
	
	@ApiModelProperty(value="Specify the authorization details.", required=true)
	private CustomCAMSCallReferencesVO cardDetails;
	
	public String getOneTimePassword() {
		return oneTimePassword;
	}


	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}


	public String getChannel() {
		return channel;
	}


	public void setChannel(String channel) {
		this.channel = channel;
	}


	public CustomCAMSCallReferencesVO getCardDetails() {
		return cardDetails;
	}


	public void setCardDetails(CustomCAMSCallReferencesVO cardDetails) {
		this.cardDetails = cardDetails;
	}


}
