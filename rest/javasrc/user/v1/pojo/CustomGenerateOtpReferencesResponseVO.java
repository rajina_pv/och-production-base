package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomGenerateOtpReferencesResponseVO extends RestHeaderFooterHandler {

	//private List<CustomGenerateOtpReferencesVO> data;

	/**
	 * @return the data
	 */
	private CustomGenerateOtpReferencesVO data;

	public CustomGenerateOtpReferencesVO getData() {
		return data;
	}

	public void setData(CustomGenerateOtpReferencesVO data) {
		this.data = data;
	}
}
	