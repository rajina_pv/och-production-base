package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Document Text Table Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomDocTextTablePOJO {

	private List<CustomDocTextTableRowPOJO> rows;

	public List<CustomDocTextTableRowPOJO> getRows() {
		return rows;
	}

	public void setRows(List<CustomDocTextTableRowPOJO> rows) {
		this.rows = rows;
	}
}
