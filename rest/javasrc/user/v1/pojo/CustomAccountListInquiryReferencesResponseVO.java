package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomAccountListInquiryReferencesResponseVO extends RestHeaderFooterHandler{
	
	private List<CustomAccountListInquiryReferencesVO> list;

	public List<CustomAccountListInquiryReferencesVO> getList() {
		return list;
	}

	public void setList(List<CustomAccountListInquiryReferencesVO> list) {
		this.list = list;
	}
    
	

}
