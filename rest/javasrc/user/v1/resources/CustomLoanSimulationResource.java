package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanSimulationReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanSimulationReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSimulationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSimulationEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Product Simulation")

@ApiResponses(value = {
		@ApiResponse(code=400, message= ResourcePaths.HTTP_ERROR_400 , response = Header.class),
		@ApiResponse(code=401, message= ResourcePaths.HTTP_ERROR_401 , response = Header.class),
		@ApiResponse(code=403, message= ResourcePaths.HTTP_ERROR_403 , response = Header.class),
		@ApiResponse(code=405, message= ResourcePaths.HTTP_ERROR_405 , response = Header.class),
		@ApiResponse(code=415, message= ResourcePaths.HTTP_ERROR_415 , response = Header.class),
		@ApiResponse(code=422, message= ResourcePaths.HTTP_ERROR_422 , response = Header.class),
		@ApiResponse(code=500, message= ResourcePaths.HTTP_ERROR_500 , response = Header.class),
})
public class CustomLoanSimulationResource extends  ResourceAuthenticator{
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	/**
	 * Authenticates if the userid and bankid who is trying to access the resource is same as the corpid and bankid which is associated with the context.
	 *
	 * @return boolean if the userid and bankid is authenticated it returns false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {


		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankId = pathParams.getFirst("bankid");
		String userId = pathParams.getFirst("userid");

		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);
		UserId userIdFromContext = (UserId) opcontext.getFromContextData(EBankingConstants.USER_ID);


		boolean isInValid;

		if (bank.getValue().equals(bankId) && userIdFromContext.getValue().equals(userId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.",
					FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}



	@GET
	@MethodInfo(uri = CustomResourcePaths.LOAN_PRODUCT_SIMULATION_URL, auditFields = {"merchantId : merchant ID input",
			"paymentAmount: payment amount input", "tenor"
	})
	@ApiOperation(value="Fetch Loan Product Simulation details based on merchantId, paymentAmount and tenor" , response=CustomLoanSimulationReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code=200, message= "Successfull Retrieval of loan simulation  Details" , response = CustomLoanSimulationReferencesResponseVO.class),
			@ApiResponse(code=404, message= "Loan simulation Details do not exist"),
			@ApiResponse(code=400, message= "Merchant id is mandatory"),
			@ApiResponse(code=500, message= "Internal Server Error")
	})
	public Response fetchRequest(
			@ApiParam(value = "Loan simulation merchantId input" ,required=true) @QueryParam("merchantId") String merchantId,
			@ApiParam(value = "Loan simulation paymentAmount input", required=true) @QueryParam("paymentAmount") String paymentAmount,
			@ApiParam(value = "Loan simulation tenor input") @QueryParam("tenor") String tenor
			){
		CustomLoanSimulationReferencesResponseVO responseVO = new CustomLoanSimulationReferencesResponseVO();
		try {
			isUnAuthenticatedRequest();
			IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
			List<CustomLoanSimulationReferencesVO> outputVO = fetchLoanSimulationDetails(merchantId,paymentAmount, tenor, context);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO	, request, 0);

		} catch (Exception e) {
			
			RestResourceManager.handleFatalErrorOutput(request,response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	public List<CustomLoanSimulationReferencesVO> fetchLoanSimulationDetails(String merchantId, String paymentAmount, String tenure, IOpContext context)
			throws FEBAException {
		List<CustomLoanSimulationReferencesVO> outputVO = null;
		

		CustomLoanSimulationEnquiryVO enquiryVO = (CustomLoanSimulationEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanSimulationEnquiryVO);
		if(null!=merchantId){
			enquiryVO.getCriteria().setMerchantId(merchantId);
		}
		if(null!=paymentAmount){
		
			enquiryVO.getCriteria().setPaymentAmount(paymentAmount);
			
		}
		if(null!=tenure){
			enquiryVO.getCriteria().setTenor(tenure);
		}

		enquiryVO.getCriteria().setConfigType(CustomResourceConstants.LOAN_CONFIG_CM_CODE_PRIN);
		
		outputVO = new ArrayList<>();
		final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanSimulationMaintenanceService"));
		enquiryVO = (CustomLoanSimulationEnquiryVO)serviceCStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		FEBAArrayList<CustomLoanSimulationDetailsVO> detailsListVO = enquiryVO.getResultList();

		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomLoanSimulationDetailsVO detailsVO = detailsListVO.get(i);
			CustomLoanSimulationReferencesVO loanSimDetails = new CustomLoanSimulationReferencesVO();

			loanSimDetails.setPaymentAmount(detailsVO.getPaymentAmount().getValue());
			loanSimDetails.setRepaymentAmount(detailsVO.getRepaymentAmount().getValue());
			loanSimDetails.setTotalInterest(detailsVO.getTotalInterest().getValue());
			loanSimDetails.setInstallmentAmount(detailsVO.getInstallmentAmount().getValue());
			loanSimDetails.setTenor(Double.valueOf(detailsVO.getTenor().getValue()));
			loanSimDetails.setYearlyInterestRate(detailsVO.getYearlyInterestRate().getValue());
			
			outputVO.add(loanSimDetails);
		}
		return outputVO;
	}
	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();

		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211030, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211031, "BE", 400));
		
		return restResponceErrorCode;
	}

}

