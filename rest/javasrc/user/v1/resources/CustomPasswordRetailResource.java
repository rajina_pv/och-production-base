package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.pojo.ChangePasswordVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.ForcePasswordInputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.ForcePasswordOutputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.ForcePasswordResponseVO;
import com.infosys.feba.framework.cache.AppDataConstants;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.valueobjects.AuthenticationVO;
import com.infosys.fentbase.types.valueobjects.PasswordVO;
import com.infosys.fentbase.types.valueobjects.PersonalProfileVO;
import com.infosys.fentbase.types.valueobjects.UserProfileVO;
import com.infosys.fentbase.types.valueobjects.UserSignonVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Pratik_shah07
 *
 * This is the Resource through which Password requests handling is done.
 *
 */
@ServiceInfo(moduleName = "USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Password Maintenance")
public class CustomPasswordRetailResource extends ResourceAuthenticator {

	@Context  protected HttpServletRequest request;

	/**
	 * This method is used when the user force password change is enabled for the user.
	 *
	 * @return Response
	 */
	@PUT
	@ApiOperation(value = "Force/Change Password for Retail User", response = ForcePasswordOutputVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_CHANGE_PASS_RETAIL_URL, auditFields = { "" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Password has been changed successfully.", response = ForcePasswordOutputVO.class) })
	public Response forcePasswordChange(@ApiParam(value = "The ID of the user", required = true) @Size(min=0,max=32) @PathParam("userid") String userid, ForcePasswordInputVO forcePwdInputVO) {


		ForcePasswordResponseVO responseVO = new ForcePasswordResponseVO();
		ForcePasswordOutputVO output = null;
		try {

			IOpContext opcontext = (OpContext) request
					.getAttribute("OP_CONTEXT");

			isUnAuthenticatedRequest();

			AuthenticationVO authVO=(AuthenticationVO) opcontext.getFromContextData("authVO");
			UserSignonVO userSignOnVO= authVO.getUserSignOnVO();
			UserProfileVO userProfileVO = userSignOnVO.getUserProfileVO();

			PersonalProfileVO personalProfileVO = (PersonalProfileVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.PersonalProfileVO); 

			Character forceSignOnPwdFlag = userSignOnVO.getNavigationVO().getForceSignonPasswordChangeFlag().getValue();
			Character forceTxnPwdFlag = userSignOnVO.getNavigationVO().getForceTxnPasswordChangeFlag().getValue();
			boolean isForcePassword = (forceSignOnPwdFlag=='Y'||forceTxnPwdFlag=='Y');

			String chgPasswdOption = forcePwdInputVO.getChangePasswordOption();
			boolean isChgPasswd = RestCommonUtils.isNotNullorEmpty(chgPasswdOption);

			List<String> passwords= new ArrayList<>();
			List<String> passwordsNotRequired= new ArrayList<>();

			output = new ForcePasswordOutputVO();

			ChangePasswordVO signOnPwd = null;
			ChangePasswordVO txnPwd = null;

			PasswordVO passwordVO=userSignOnVO.getPasswordVO();

			if(forcePwdInputVO!=null && forcePwdInputVO.getSignOnPasswordDetails()!=null) {
				signOnPwd = forcePwdInputVO.getSignOnPasswordDetails();
			}
			/*
			 * Change#
			 */
			if(forcePwdInputVO!=null && forcePwdInputVO.getSignOnPasswordDetails()!=null) {
				txnPwd = forcePwdInputVO.getSignOnPasswordDetails();
			}

			Character signOnStatusFlag = userSignOnVO.getNavigationVO().getSignOnStatusFlag().getValue();

			if(!isForcePassword && !isChgPasswd) {
				//Throw Error saying "You are not authorized to change the password".

				throw new BusinessException(opcontext,
						EBIncidenceCodes.UNATHORIZED_USER,
						"The user is not authorized to perform the action",
						EBankingErrorCodes.UNATHORIZED_USER);
			}
			if(!isForcePassword){

				checkValidTransactrionType(opcontext,"CPES",forcePwdInputVO.getChangePasswordOption());

			}
			if(FBAConstants.LOGIN_STATUS_PARTIALLY_AUTHENTICATED_CHAR==signOnStatusFlag) {
				throw new BusinessException(opcontext,
						EBIncidenceCodes.AUTHENTICATION_NOT_SUCCESSFUL,
						"Please complete the authentication to proceed.",
						109788);
			}

			if (isForcePassword) {
				if(FBAConstants.LOGIN_STATUS_SHOULD_ACCEPT_TERMS_CHAR==signOnStatusFlag) {
					throw new BusinessException(opcontext,
							EBIncidenceCodes.LOGIN_STATUS_SHOULD_ACCEPT_TERMS,
							"Please accept terms and conditions to proceed.",
							110978);
				} else if(FBAConstants.LOGIN_STATUS_SHOULD_REGISTER_QNA_CHAR==signOnStatusFlag) {
					throw new BusinessException(opcontext,
							EBIncidenceCodes.QUESTION_RESPONSE_NOT_PROVIDED,
							"Please complete Q&A to proceed.",
							110979);
				} else if (FBAConstants.LOGIN_STATUS_SHOULD_FORCE_PAM_CHANGE_CHAR==signOnStatusFlag) {
					throw new BusinessException(opcontext,
							EBIncidenceCodes.IMAGE_PHRASE_MANDATORY_VALUE,
							"Please provide Image & Phrase to proceed.",
							110980);
				}
			}

			if((forceSignOnPwdFlag=='Y' || "BOTH".equalsIgnoreCase(chgPasswdOption) || "SPWD".equalsIgnoreCase(chgPasswdOption)) && !userProfileVO.getPrimaryAuthenticationModeER().getCode().getValue().equals("SPWD")) { // Can check for 'Y' or 'N' of flag if required
				throw new BusinessException(opcontext,
						EBIncidenceCodes.INVALID_MODE_OF_AUTHENTICATION,
						"SignOn Password Change is valid only for Password based Authentication",
						EBankingErrorCodes.INVALID_AUTH_MODE);
			}

			if((forceTxnPwdFlag=='Y' || "BOTH".equalsIgnoreCase(chgPasswdOption) || "TPWD".equalsIgnoreCase(chgPasswdOption)) && !userProfileVO.getAuthorizationMode().getCode().getValue().equals("TPWD")) { // Can check for 'Y' or 'N' of flag if required
				throw new BusinessException(opcontext,
						EBIncidenceCodes.INVALID_MODE_OF_AUTHENTICATION,
						"Transaction Password Change is valid only for Password based Authentication",
						EBankingErrorCodes.INVALID_AUTH_MODE);
			}

			if(forceSignOnPwdFlag=='Y'|| "SPWD".equalsIgnoreCase(chgPasswdOption)||"BOTH".equalsIgnoreCase(chgPasswdOption)) {

				if(signOnPwd==null) {
					passwords.add(CustomResourceConstants.SIGN_ON_OLD_PASS);
					passwords.add(CustomResourceConstants.SIGN_ON_NEW_PASS);
					passwords.add(CustomResourceConstants.SIGN_ON_CONFIRM_NEW_PASS);
				}
				else {
					if(!RestCommonUtils.isNotNullorEmpty(signOnPwd.getOldPassword())){
						passwords.add(CustomResourceConstants.SIGN_ON_OLD_PASS);
					}
					if(!RestCommonUtils.isNotNullorEmpty(signOnPwd.getNewPassword())){
						passwords.add(CustomResourceConstants.SIGN_ON_NEW_PASS);
					}

					if(!RestCommonUtils.isNotNullorEmpty(signOnPwd.getConfirmPassword())){
						passwords.add(CustomResourceConstants.SIGN_ON_CONFIRM_NEW_PASS);
					}
				}
				passwordVO.setSignOnFlg(FBAConstants.YNFLAG_Y);
			} else {
				if(signOnPwd!=null) {
					if(signOnPwd.getOldPassword()!=null){
						passwordsNotRequired.add(CustomResourceConstants.SIGN_ON_OLD_PASS);
					}
					if(signOnPwd.getNewPassword()!=null){
						passwordsNotRequired.add(CustomResourceConstants.SIGN_ON_NEW_PASS);
					}

					if(signOnPwd.getConfirmPassword()!=null){
						passwordsNotRequired.add(CustomResourceConstants.SIGN_ON_CONFIRM_NEW_PASS);
					}
				}
			}

			if(forceTxnPwdFlag=='Y' || "TPWD".equalsIgnoreCase(chgPasswdOption)||"BOTH".equalsIgnoreCase(chgPasswdOption)) {

				if(txnPwd==null) {
					passwords.add(CustomResourceConstants.TXN_ON_OLD_PASS);
					passwords.add(CustomResourceConstants.TXN_ON_NEW_PASS);
					passwords.add(CustomResourceConstants.TXN_ON_CONFIRM_NEW_PASS);
				}
				else {
					if(!RestCommonUtils.isNotNullorEmpty(txnPwd.getOldPassword())){
						passwords.add(CustomResourceConstants.TXN_ON_OLD_PASS);
					}
					if(!RestCommonUtils.isNotNullorEmpty(txnPwd.getNewPassword())){
						passwords.add(CustomResourceConstants.TXN_ON_NEW_PASS);
					}

					if(!RestCommonUtils.isNotNullorEmpty(txnPwd.getConfirmPassword())){
						passwords.add(CustomResourceConstants.TXN_ON_CONFIRM_NEW_PASS);
					}
				}
				passwordVO.setTxnFlg(FBAConstants.YNFLAG_Y);
			} else {
				if(txnPwd!=null) {
					if(txnPwd.getOldPassword()!=null){
						passwordsNotRequired.add(CustomResourceConstants.TXN_ON_OLD_PASS);
					}
					if(txnPwd.getNewPassword()!=null){
						passwordsNotRequired.add(CustomResourceConstants.TXN_ON_NEW_PASS);
					}

					if(txnPwd.getConfirmPassword()!=null){
						passwordsNotRequired.add(CustomResourceConstants.TXN_ON_CONFIRM_NEW_PASS);
					}
				}
			}
			/*
			 * Change#
			 */

			if(("BOTH".equalsIgnoreCase(chgPasswdOption)) && !passwordsNotRequired.isEmpty() 
					|| (!("BOTH".equalsIgnoreCase(chgPasswdOption))) && passwordsNotRequired.isEmpty()) {
				throw new BusinessException(opcontext,
						EBIncidenceCodes.OPTION_NOT_APPLICABLE,
						passwordsNotRequired+" should not be entered.",
						110985);
			}

			if(!passwords.isEmpty()) {
				throw new BusinessException(opcontext,
						EBIncidenceCodes.PASSWORD_IS_BLANK,
						passwords+" cannot be Blank.",
						110983);
			}



			if(signOnPwd!=null) {
				passwordVO.setSignOnPwd(signOnPwd.getOldPassword());
				passwordVO.setSignOnNewPwd(signOnPwd.getNewPassword());
				passwordVO.setSignOnDupNewPwd(signOnPwd.getConfirmPassword());
//				custom pin validation check--start
				if (RestCommonUtils.isNotNullorEmpty(signOnPwd.getNewPassword())) {
					customCheckPinforPattern(signOnPwd.getNewPassword(), opcontext);
				}
//				custom pin validation check--send
			}

			if(txnPwd!=null) {
				passwordVO.setTxnPwd(txnPwd.getOldPassword());
				passwordVO.setTxnNewPwd(txnPwd.getNewPassword());
				passwordVO.setTxnDupNewPwd(txnPwd.getConfirmPassword());
//				custom pin validation check--start
				if (RestCommonUtils.isNotNullorEmpty(txnPwd.getNewPassword())) {
					customCheckPinforPattern(txnPwd.getNewPassword(), opcontext);
				}
//				custom pin validation check--send
			}


			userSignOnVO.setPasswordVO(passwordVO);
			personalProfileVO.setChangePwdVO(passwordVO);
			if(isForcePassword && FBAConstants.LOGIN_STATUS_PASSWORD_CHANGE_REQUIRED_CHAR==signOnStatusFlag) {
				IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("UserSignOnService"));
				userSignOnVO = (UserSignonVO) serviceCStub.callService(opcontext, userSignOnVO, new FEBAUnboundString("changePasswords"));
				signOnStatusFlag = userSignOnVO.getNavigationVO().getSignOnStatusFlag().getValue();

				if(FBAConstants.LOGIN_STATUS_SIGNED_ON_CHAR==signOnStatusFlag) {
					output = new ForcePasswordOutputVO();
					output.setMessage("Password Changed Successfully.");
					output.setStatus(true);
				}
			} else if (isChgPasswd) {				
				IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("PersonalProfileService"));
				personalProfileVO = (PersonalProfileVO) serviceCStub.callService(opcontext, personalProfileVO, new FEBAUnboundString("changePassword"));
				passwordVO = personalProfileVO.getChangePwdVO();

				output.setMessage("Password Changed Successfully.");
				output.setStatus(true);  
			}

			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, response, 0);

		}catch (Exception e) {
			
			RestResourceManager.handleFatalErrorOutput(request, response, e, null);
		}

		return Response.ok().entity(output).build();
	}


	public void checkValidTransactrionType(IOpContext opcontext, String code_type,String cmcode) throws BusinessException{


		IFEBAType codeDescription = null;
		if(RestCommonUtils.isNotNullorEmpty(cmcode)){
			try {

				codeDescription = AppDataManager.getValue(opcontext, AppDataConstants.COMMONCODE_CACHE,
						AppDataConstants.COLUMN_CODE_TYPE + EBankingConstants.EQUAL_TO + code_type + AppDataConstants.SEPERATOR + AppDataConstants.COLUMN_CM_CODE + EBankingConstants.EQUAL_TO + cmcode);

			} catch (CriticalException e) {
				throw new FatalException(opcontext,FEBAIncidenceCodes.UNEXPECTED_EXCEPTION_IN_ERVALIDATOR, e.getCause());

			}

			if(codeDescription ==null ){
				throw new BusinessException(opcontext,
						EBIncidenceCodes.INVALID_COCD_VALUE,
						"Record does not exist in COCD",
						EBankingErrorCodes.INVALID_CODE_TYPE,new AdditionalParam("CODE_TYPE", cmcode));
			}
		}

	}

//	added for custom pin validation -start
	private void customCheckPinforPattern(String password,IOpContext opcontext)
			throws BusinessException {
		if (password.matches("\\d+")) {
			int first = Integer.valueOf(String.valueOf(password.charAt(0)));
			String patern;
			if (first>0) {
				patern = String.valueOf(first * 111111);
			}else{
				patern = "000000";
			}
			if (password.equalsIgnoreCase(patern)) {
				System.out.println("same pin");
				throw new BusinessException(opcontext, FBAIncidenceCodes.EXCLUDED_NUMBERS_IN_PWD,
						"Password Should not have these letters:"+password, FBAErrorCodes.EXCLUDED_NUMBERS_IN_PWD);
			}

			for (int i = 1; i < password.length(); i++) {
				if (Integer.valueOf(String.valueOf(password.charAt(i))) == (first + 1)) {
					first++;
				} else {
					return;
				}
			}			
			throw new BusinessException(opcontext, FBAIncidenceCodes.EXCLUDED_NUMBERS_IN_PWD,
					"Password Should not have these letters:"+password, FBAErrorCodes.EXCLUDED_NUMBERS_IN_PWD);
		}
	}
//	added for custom pin validation -end
}
