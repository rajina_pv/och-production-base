package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomBankDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomBankListInquiryReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomBankListInquiryReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomBankListDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomBankListEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Bank List Details")
public class CustomBanksListInquiryResource extends ResourceAuthenticator {


	@Context  
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;
	
	@Context
	UriInfo uriInfo;
	
	/**
	 * Authenticates if the corpid and bankid who is trying to access the resource is same as the corpid and bankid which is associated with the context.
	 *
	 * @return boolean if the corpid and bankid is authenticated it returns false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {


		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankId = pathParams.getFirst("bankid");
		String userId = pathParams.getFirst("userid");

		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);
		UserId userIdFromContext = (UserId) opcontext.getFromContextData(EBankingConstants.USER_ID);


		boolean isInValid;

		if (bank.getValue().equals(bankId) && userIdFromContext.getValue().equals(userId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.",
					FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}

    @GET
    @ApiOperation(value = "Fetch Bank List", response = CustomBankListInquiryReferencesResponseVO.class)
    @MethodInfo(uri = CustomResourcePaths.BANK_CODES_MAINTENANCE_URL)
    @ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Bank List", response = CustomBankListInquiryReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Bank List do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
    
    
    public Response fetchAccountList (
    	@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
		@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
		@PathParam("productCode") String productCode){
    	IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
    	CustomBankListInquiryReferencesResponseVO responseVO= new CustomBankListInquiryReferencesResponseVO();
    	
    	
    	try {
    		isUnAuthenticatedRequest();	
    		List <CustomBankListInquiryReferencesVO> outputVO = fetchBankList (userId,productCode,opcontext);
    		responseVO.setData(outputVO); 	
    		RestResourceManager.updateHeaderFooterResponseData(responseVO	, request, 0);
    	}
    	
    	catch (Exception e) {
    		e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
    		
    	}finally {
			if (opcontext != null) {
				opcontext.cleanup();
			}
		}
    	
    	return Response.ok().entity(responseVO).build();
    }
    
    public List <CustomBankListInquiryReferencesVO> fetchBankList (String userid,String productCode,IOpContext context) throws FEBAException {
    	
    	List <CustomBankListInquiryReferencesVO> outputVO = new ArrayList<CustomBankListInquiryReferencesVO>();
    	
    	CustomBankListEnquiryVO enquiryVO = (CustomBankListEnquiryVO) FEBAAVOFactory.createInstance(CustomTypesCatalogueConstants.CustomBankListEnquiryVO);
    	
    	CustomBankListInquiryReferencesVO   outVO  = new  CustomBankListInquiryReferencesVO(); 
    	
    	List<CustomBankDetailsVO> bankDetailsList = new ArrayList<CustomBankDetailsVO>();
    	
    	enquiryVO.setUserId(userid);
    	enquiryVO.getCriteria().setProductCode(productCode);
    	
    	final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomBankListInquiryService"));
		enquiryVO = (CustomBankListEnquiryVO) serviceStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		
		FEBAArrayList<CustomBankListDetailsVO> responseList = enquiryVO.getResultList();
		
		if (responseList.size() == 0)  {
			
			throw new BusinessException(true, context, CustomEBankingIncidenceCodes.BANK_DATA_NOT_FOUND,
					"Bank data is not found for the product code.", null, CustomEBankingErrorCodes.BANK_DATA_NOT_FOUND, null);
		}
		
		
		if(null != responseList){
			
			for (int i = 0; i < responseList.size(); i++) {
				
				CustomBankDetailsVO bankDetails = new CustomBankDetailsVO();  
				
				bankDetails.setBankCode(responseList.get(i).getBankCode().getValue());
				bankDetails.setBankName(responseList.get(i).getBankName().getValue());
				
				bankDetailsList.add(bankDetails);
				outVO.setBankDetails(bankDetailsList);
				
				
			}
			
			outputVO.add(outVO);
		}
		
    	return outputVO;
    }
    
    protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211100, "BE", 400));
		return restResponceErrorCode;
	}
    
    
}
