/**
 * 
 */
package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomVerifyPayrollAcctReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomVerifyPayrollAcctReferencesVO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqEnquiryVO;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value ="Verify Payroll Account Inquiry Call")
@Path("/v1/banks/{bankid}/users/{userid}/custom/verifyPayroll")
@ApiResponses(value = { @ApiResponse(code=400, message=ResourcePaths.HTTP_ERROR_400, response=Header.class),
						@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
						@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
						@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
						@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
						@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
						@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })
public class CustomVerifyPayrollResource extends ResourceAuthenticator {

	final IClientStub slakeAcctServiceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomSilverlakeAcctCallService"));
	
	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";
	public static final String MESSAGE = "No records fetched";
	
	@Context
	HttpServletRequest request1;
	
	@Context
	HttpServletResponse httpResponse1;
	
	@Context
	UriInfo uriInfo1;
	
	@GET
	@MethodInfo(uri = CustomResourcePaths.VERIFY_PAYROLL_ACCT_URL, auditFields = {
			"applicationId: Application Id"})
	@ApiOperation(value = "Fetchs savings account balance based on payroll account", response = CustomVerifyPayrollAcctReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of payroll account details", response = CustomVerifyPayrollAcctReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Payroll details do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response fetchBalance(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid){

		CustomVerifyPayrollAcctReferencesResponseVO responseVO = new CustomVerifyPayrollAcctReferencesResponseVO();
		
		FEBATransactionContext objTxnContext = null;
		
		try {
			isUnAuthenticatedRequest();
			IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
			
			String productCode = PropertyUtil.getProperty("BRI_BANK_PRODUCT_CODE", context);
			
			// calling inquiry service
			CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
					.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
			
			enquiryVO.getCriteria().setApplicationStatus("LOAN_PAID");

			final IClientStub customLoanInquiryService = ServiceUtil
					.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
			customLoanInquiryService.callService(context, enquiryVO, new FEBAUnboundString("fetchForInquiry"));
			if (enquiryVO.getResultList().size() == 0) {
				throw new BusinessException(context, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}
			
			CustomLoanApplnMasterDetailsVO detailsVO = (CustomLoanApplnMasterDetailsVO) enquiryVO.getResultList()
					.get(0);
			
			String status = detailsVO.getApplicationStatus().toString();
			
			objTxnContext = RestCommonUtils.getTransactionContext(context);

			CLPAInfo clpainfo = CLPATAO.select(objTxnContext,
					new BankId(context.getFromContextData("BANK_ID").toString()),
					detailsVO.getApplicationId());
			
			FEBAUnboundString accountNumber = new FEBAUnboundString(clpainfo.getPayrollAccountNum().getValue());
			
			if (null != status && status.equals("LOAN_PAID")) {
				
				String branchCode = "";
				CustomVerifyPayrollAcctReferencesVO outputVO = null;
				
				// Silverlake Account Inquiry Host Call
				CustomSilverlakeAccountInqCriteriaVO accountInqCriteriaVO = (CustomSilverlakeAccountInqCriteriaVO) FEBAAVOFactory
						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqCriteriaVO");
				CustomSilverlakeAccountInqEnquiryVO accountInqEnquiryVO = (CustomSilverlakeAccountInqEnquiryVO) FEBAAVOFactory
						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqEnquiryVO");
				
				if (FEBATypesUtility.isNotNullOrBlank(accountNumber)) {
					accountInqCriteriaVO.setAccountNumber(accountNumber);
					branchCode = clpainfo.getPayrollAccountNum().subString(0, 4);
				}
				accountInqCriteriaVO.setBranch(branchCode);
				String appProductId = PropertyUtil.getProperty(CustomEBConstants.INSTALLATION_PRODUCT_ID, context);
				String branchCodeSuffix = PropertyUtil.getProperty(CustomEBConstants.BRANCH_CODE_SUFFIX, context);
				accountInqCriteriaVO.setRequestBy(appProductId);
				accountInqCriteriaVO.setUserId(branchCode + branchCodeSuffix);
				
				accountInqCriteriaVO.setBankCode(detailsVO.getBankCode());
				accountInqCriteriaVO.setProductCode(productCode);
				
				accountInqEnquiryVO.setCriteria(accountInqCriteriaVO);
				
				outputVO = silverLakeAcctInquiry(context, accountInqEnquiryVO);
				responseVO.setData(outputVO);
			}
						
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request1, 0);

		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		}finally{
			if(objTxnContext != null){
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		} 
		return Response.ok().entity(responseVO).build();
	}
	
	
	public CustomVerifyPayrollAcctReferencesVO silverLakeAcctInquiry(IOpContext context,
			CustomSilverlakeAccountInqEnquiryVO accountInqEnquiryVO)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomVerifyPayrollAcctReferencesVO acctInquiryReferencesVO = new CustomVerifyPayrollAcctReferencesVO();

		accountInqEnquiryVO = (CustomSilverlakeAccountInqEnquiryVO) slakeAcctServiceCStub.callService(context,
				accountInqEnquiryVO, new FEBAUnboundString("acctInquiry"));
		CustomSilverlakeAccountInqDetailsVO accountInqDetailsVO = accountInqEnquiryVO.getDetails();
		if (FEBATypesUtility.isNotNullOrBlank(accountInqDetailsVO.getCifNo())) {
			acctInquiryReferencesVO.setCifNo(accountInqDetailsVO.getCifNo().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(accountInqDetailsVO.getStatusRekening())) {
			acctInquiryReferencesVO.setStatusRekening(accountInqDetailsVO.getStatusRekening().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(accountInqDetailsVO.getSavingsAmount())) {
			acctInquiryReferencesVO.setSavingsAmount(accountInqDetailsVO.getSavingsAmount().toString());
		}
		
		return acctInquiryReferencesVO;
	}
	
	
	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211041, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211042, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211043, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211049, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211050, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211051, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211099, "BE", 400));
		return restResponceErrorCode;
	}
	
	
}
