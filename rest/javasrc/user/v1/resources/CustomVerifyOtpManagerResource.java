package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomGenerateOtpReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomVerifyOtpReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomVerifyOtpReferencesVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Generate OTP")
@Path("/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/verifyOTP")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })
public class CustomVerifyOtpManagerResource extends ResourceAuthenticator {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;
	
	@Context
	UriInfo uriInfo;
	
	@POST
	@MethodInfo(uri = CustomResourcePaths.OTP_VERIFICATION_URL, auditFields = {"privyId : Privyd Id"})
	@ApiOperation(value="Privy Token Creation" , response=CustomVerifyOtpReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code=200, message= "OTP Sent successfully" , response = CustomVerifyOtpReferencesResponseVO.class),
			@ApiResponse(code=403, message= "Failed"),
			@ApiResponse(code=500, message= "Internal Server Error")
	})
	
	public Response createVerifyOtpResquest(
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "Application Id input", required = true) @PathParam("applicationId") String applicationId,
			CustomVerifyOtpReferencesVO pojoVO) {
		
		CustomVerifyOtpReferencesResponseVO responseVO = new CustomVerifyOtpReferencesResponseVO();
		//CustomVerifyOtpReferencesVO pojoVO= new CustomVerifyOtpReferencesVO();
		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		
		try {
			isUnAuthenticatedRequest();
			
			CustomPrivyRegistrationVO enquiryVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPrivyRegistrationVO);			
			
			enquiryVO.setToken(pojoVO.getToken());
			enquiryVO.setOtp(pojoVO.getOtp());
			
			
			final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));
			enquiryVO= (CustomPrivyRegistrationVO) serviceStub.callService(opcontext, enquiryVO, new FEBAUnboundString("OTPVerification"));
		    
			CustomVerifyOtpReferencesVO outputVO= fetchTokenDetails(enquiryVO,opcontext);
			List<CustomVerifyOtpReferencesVO> data =new ArrayList<CustomVerifyOtpReferencesVO>();
			data.add(outputVO);
	        responseVO.setData(data);
	        RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request, 0);
		}
		
		catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}
	
    public CustomVerifyOtpReferencesVO fetchTokenDetails(CustomPrivyRegistrationVO enquiryVO ,IOpContext context) throws FEBAException{
		
	CustomVerifyOtpReferencesVO outputVO=new CustomVerifyOtpReferencesVO();
		 
		
		if(enquiryVO.getCode()!=null){
			outputVO.setCode(enquiryVO.getCode().toString());;
		}
		if(enquiryVO.getStatus()!=null){
			outputVO.setStatus(enquiryVO.getStatus().toString());
		}
		
		if(enquiryVO.getMessage()!=null){
			outputVO.setMessage(enquiryVO.getMessage().toString());
		}
	    
		if(enquiryVO.getRequestId()!=null){
			outputVO.setRequestId(enquiryVO.getRequestId().toString());
		}
	    return outputVO;
	}
    
    protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();



		return restResponceErrorCode;
	}
}
