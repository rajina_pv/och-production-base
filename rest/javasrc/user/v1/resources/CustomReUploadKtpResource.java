package com.infosys.custom.ebanking.rest.user.v1.resources;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocumentInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomReUploadKtpOutputVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomReUploadKtpResponseVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.user.util.CustomDocumentUploadUtil;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 *
 *  This is the Resource through which re upload document is done.
 *
 */
@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Reupload document maintenance")
@Path("/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/reuploadktp")
public class CustomReUploadKtpResource extends ResourceAuthenticator {
	
	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";
	private static final String MODIFY = "modify";

	@Context
	protected HttpServletRequest request;


	/**
	 * This method is used for re uploading KTP.
	 * 
	 * @return Response
	 */
	@PUT
	@ApiOperation(value = "Custom Reupload document maintenance", response = CustomReUploadKtpResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_REUPLOAD_KTP_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Record reuploaded successfully.", response = CustomReUploadKtpResponseVO.class) })
	public Response updateRequest(
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "The Application ID", required = true) @Size(min = 0, max = 32) @PathParam("applicationId") String applicationId,
			CustomDocumentInputDetailsVO documentDetails) {

		CustomReUploadKtpResponseVO responseVO = new CustomReUploadKtpResponseVO();
		
		CustomReUploadKtpOutputVO outputVO  = null;

		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");

		try {
			isUnAuthenticatedRequest();
			
			CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory
					.createInstance(CustomLoanApplnDocumentsDetailsVO.class.getName());
			
			if(null != documentDetails){
				
				documentDetails.setApplicationId(Long.valueOf(applicationId));
				outputVO = reuploadKtpDetails(request, documentDetails, docDetailsVO, opcontext);
				responseVO.setData(outputVO);
			} 
			
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, 0);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, null);
		}finally {
			if (opcontext != null) {
				opcontext.cleanup();
			}
		}
		return Response.ok().entity(responseVO).build();
	}
	
	private CustomReUploadKtpOutputVO reuploadKtpDetails(HttpServletRequest request, CustomDocumentInputDetailsVO documentDetails,
			CustomLoanApplnDocumentsDetailsVO docDetailsVO, IOpContext opContext)
			throws CriticalException, BusinessException, BusinessConfirmation, FEBATypeSystemException{
		
		CustomReUploadKtpOutputVO  outputVO = new CustomReUploadKtpOutputVO();
		
		try {

				FEBAUnboundString file = new FEBAUnboundString(documentDetails.getFile());
				String inputKTP = documentDetails.getDocumentCode();
				System.out.println("file.getLength() - "+file.getLength());
				System.out.println("inputKTP:: - "+inputKTP);
				
				documentDetails = new CustomDocumentUploadUtil()
						.createFileInSharedDirAndUploadEncryptedFile(documentDetails, opContext, request);
								
				docDetailsVO.setApplicationId(new ApplicationNumber(documentDetails.getApplicationId()));
				docDetailsVO.setDocType(documentDetails.getDocumentType());
				docDetailsVO.setDocCode(documentDetails.getDocumentCode());
				docDetailsVO.setDocKey(documentDetails.getDocKey());
				docDetailsVO.setDocStorePath(documentDetails.getFileUploadPath());
				docDetailsVO.setFileSeqNo(documentDetails.getFileSeqNo());
				final IClientStub customDocDetailsService = ServiceUtil
						.getService(new FEBAUnboundString("CustomLoanApplicationDocDetailsService"));
				
				customDocDetailsService.callService(opContext, docDetailsVO, new FEBAUnboundString("createOrUpdate"));
				
				outputVO.setApplicationId(documentDetails.getApplicationId());
				outputVO.setStatus("Document has reuploaded successfully");
				
				if(docDetailsVO !=null){
					updateKTPReuploadFlag (opContext, docDetailsVO.getApplicationId().toString());
				}
							
		} catch (Exception ee) {

			LogManager.logError(opContext, ee);
			
		}finally{
			if(opContext != null){
				opContext.cleanup();
			}
		}
		
		return outputVO;
	}
	
	protected static void updateKTPReuploadFlag(IOpContext opContext, String applicationId)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
		loanApplnDetailsVO.setApplicationId(new ApplicationNumber(applicationId));
		final IClientStub customLoanApplicationService = ServiceUtil
				.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
		loanApplnDetailsVO.setIsKtpReUploaded(new FEBAUnboundString("Y"));
		customLoanApplicationService.callService(opContext, loanApplnDetailsVO, new FEBAUnboundString(MODIFY));

	}

}
