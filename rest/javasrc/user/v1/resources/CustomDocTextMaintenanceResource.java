package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocTextResponsePOJO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocTextResponseReferencesResponsePOJO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocTextTablePOJO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocTextTableRowPOJO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextMaintVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextResultVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextTableRowVO;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAListIterator;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.utils.insulate.ArrayList;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Document Text Maintenance")
@Path(CustomResourcePaths.DOC_MAINTENANCE_URL)
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })
public class CustomDocTextMaintenanceResource extends ResourceAuthenticator {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	@GET
	@MethodInfo(uri = CustomResourcePaths.DOC_MAINTENANCE_URL, auditFields = {
			"application Id : applicatio id input", "document Id : document id input" })
	@ApiOperation(value = "Fetch Document Text details based on application id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfull Retrieval of Document text", response = CustomDocTextResponseReferencesResponsePOJO.class),
			@ApiResponse(code = 404, message = "document do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response fetchByApplicationId(
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "User Id input", required = true) @PathParam("applicationId") String applicationId,
			@ApiParam(value = "User Id input", required = true) @PathParam("documentId") String documentId) {
		System.out.println("**inside get method");
		CustomDocTextResponseReferencesResponsePOJO responseVO = new CustomDocTextResponseReferencesResponsePOJO();
		try {
			isUnAuthenticatedRequest();
			CustomDocTextMaintVO enquiryVO = (CustomDocTextMaintVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomDocTextMaintVO);
			enquiryVO.getCriteria().setDocumentName(documentId);
			enquiryVO.getCriteria().setApplicationId(applicationId);
			responseVO.setData(callService(enquiryVO, "fetch"));
			// Added for PINANG pay later start
			responseVO.setSchemeCode(enquiryVO.getSchemeCode().getValue());
			// Added for PINANG pay later end
			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request, 0);
		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	private ArrayList<CustomDocTextResponsePOJO> callService(CustomDocTextMaintVO enquiryVO, String method)
			throws BusinessException, BusinessConfirmation, FEBAException {
		IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
		final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomDocTextMaintService"));
		serviceCStub.callService(context, enquiryVO, new FEBAUnboundString(method));
		ArrayList<CustomDocTextResponsePOJO> results = new ArrayList<CustomDocTextResponsePOJO>();
		FEBAListIterator itr = enquiryVO.getResults().iterator();
		while (itr.hasNext()) {
			results.add(getResponsePOJO((CustomDocTextResultVO) itr.next()));
		}
		return results;
	}

	private CustomDocTextResponsePOJO getResponsePOJO(CustomDocTextResultVO docTxtVO) {
		CustomDocTextResponsePOJO pojo = new CustomDocTextResponsePOJO();
		pojo.setRowNumber(docTxtVO.getRowNumber().getValue());
		pojo.setTextName(docTxtVO.getTextName().getValue());
		pojo.setSrlNumber(docTxtVO.getSrlNumber().getValue());
		pojo.setText(docTxtVO.getText().getValue());
		pojo.setTextType(docTxtVO.getTextType().getValue());
		pojo.setParentTextName(docTxtVO.getParentTextName().getValue());
		pojo.setIndentLevel(docTxtVO.getTextIndentation().getValue());
		FEBAArrayList rows = docTxtVO.getTable().getRows();
		if(rows.size() > 0){
			CustomDocTextTablePOJO table = new CustomDocTextTablePOJO();
			List<CustomDocTextTableRowPOJO> pojoRows = new ArrayList<CustomDocTextTableRowPOJO>();
			FEBAListIterator itr = rows.iterator();
			while(itr.hasNext()){
				CustomDocTextTableRowVO row = (CustomDocTextTableRowVO) itr.next();
				CustomDocTextTableRowPOJO tableRow = new CustomDocTextTableRowPOJO();
				tableRow.setLabel(row.getLabel().getValue());
				tableRow.setValue(row.getValue().getValue());
				pojoRows.add(tableRow);
			}
			table.setRows(pojoRows);
			pojo.setTable(table);
		}
		return pojo;
	}

	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));
		return restResponceErrorCode;
	}

}
