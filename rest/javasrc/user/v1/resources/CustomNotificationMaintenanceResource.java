package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomNotificationListDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomNotificationListReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomNotificationListReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomNotificationOutputVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomNotificationResponseVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationInOutVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.retail.user.v1.pojo.ForcePasswordOutputVO;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Pratik_shah07
 *
 * This is the Resource through which notification maintenance is done.
 *
 */
@ServiceInfo(moduleName = "USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Notification Maintenance")
public class CustomNotificationMaintenanceResource extends ResourceAuthenticator {

	@Context  
	protected HttpServletRequest request;
	

	@GET
	@ApiOperation(value = "Fetch Notification List", response = CustomNotificationListReferencesResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_NOTIFICATION_LIST_RESOURCE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Notification List", response = CustomNotificationListReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Notification List do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response fetchNotificationList (
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "notificationHeader", required = true)	@QueryParam("notificationHeader") String notificationHeader,
			@ApiParam(value = "shortDescription", required = true)	@QueryParam("shortDescription") String shortDescription) {

		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		
		CustomNotificationListReferencesResponseVO responseVO = new CustomNotificationListReferencesResponseVO();
		
		try {		
			isUnAuthenticatedRequest();

			List <CustomNotificationListReferencesVO> outputVO = fetchNotificationListOutput (notificationHeader,shortDescription,opcontext);
			responseVO.setList(outputVO);
		}
		catch (Exception e) {
			LogManager.logError(opcontext, e);
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}
		
		return Response.ok().entity(responseVO).build();
	}
	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		return new ArrayList<>();
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List <CustomNotificationListReferencesVO> fetchNotificationListOutput (String notificationHeader, String shortDescription ,IOpContext context) throws FEBAException {
	
		List <CustomNotificationListReferencesVO> outputVO= new ArrayList();
		
		CustomNotificationEnquiryVO enquiryVO = (CustomNotificationEnquiryVO) FEBAAVOFactory.createInstance(CustomTypesCatalogueConstants.CustomNotificationEnquiryVO);
		if(RestCommonUtils.isNotNull(notificationHeader)){
			enquiryVO.getCriteria().getNotificationHeader().set(notificationHeader);
		}
		
		if(RestCommonUtils.isNotNull(shortDescription)){
			enquiryVO.getCriteria().getShortDescription().set(shortDescription);
		}
		
		
		final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomNotificationMaintenanceListService"));
		enquiryVO = (CustomNotificationEnquiryVO) serviceStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		FEBAArrayList<CustomNotificationDetailsVO> detailsListVO = enquiryVO.getResultList();
		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomNotificationDetailsVO detailsVO = detailsListVO.get(i);
			CustomNotificationListReferencesVO pojoVO = new CustomNotificationListReferencesVO ();
			
			pojoVO.setNotificationEvent(detailsVO.getNotificationEvent().toString());
			if(FEBATypesUtility.isNotBlankDate(detailsVO.getNotificationDate())){
				pojoVO.setNotificationDate(formatDateBahasa(detailsVO.getNotificationDate().toString().substring(0,10)));
			}
			pojoVO.setNotificationHeader(detailsVO.getNotificationHeader().toString());
			pojoVO.setShortDescription(detailsVO.getShortDescription().getValue());
			pojoVO.setIsNew(detailsVO.getIsNew().toString());
			pojoVO.setNotificationId(detailsVO.getNotificationId().toString());
			
			CustomNotificationListDetailsVO childVO = new CustomNotificationListDetailsVO ();
			childVO.setMessage(detailsVO.getDetailMessage().getValue());
			if(FEBATypesUtility.isNotBlankAmount(detailsVO.getAmount1())){
				childVO.setAmount1(formatAmount(String.valueOf(detailsVO.getAmount1().getAmount().getNumeralValue().longValue())));
			}
			
			if(FEBATypesUtility.isNotBlankAmount(detailsVO.getAmount2())){
				childVO.setAmount2(formatAmount(String.valueOf(detailsVO.getAmount2().getAmount().getNumeralValue().longValue())));
			}
			
			if(FEBATypesUtility.isNotBlankAmount(detailsVO.getAmount3())){
				childVO.setAmount3(formatAmount(String.valueOf(detailsVO.getAmount3().getAmount().getNumeralValue().longValue())));
			}
			childVO.setDate1( formatDateBahasa((FEBATypesUtility.isNotBlankDate(detailsVO.getDate1())) ? detailsVO.getDate1().toString().substring(0, 10) : "" ));
			childVO.setDate2( formatDateBahasa((FEBATypesUtility.isNotBlankDate(detailsVO.getDate2())) ? detailsVO.getDate2().toString().substring(0, 10) : "" ));
			childVO.setDate3( formatDateBahasa((FEBATypesUtility.isNotBlankDate(detailsVO.getDate3())) ? detailsVO.getDate3().toString().substring(0, 10) : "" ));
			childVO.setOtherInfo1(detailsVO.getOtherInfo1().getValue());
			childVO.setOtherInfo2(detailsVO.getOtherInfo2().getValue());
			childVO.setOtherInfo3(detailsVO.getOtherInfo3().getValue());
			pojoVO.setDetails(childVO);
			outputVO.add(pojoVO);
		}
		return outputVO;
	}
	
	
	/**
	 * This method is used for updating notification status
	 * @author Pratik_Shah07
	 * @return Response
	 */
	@PUT
	@ApiOperation(value = "Update notification Table based on notification id and user id", response = ForcePasswordOutputVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_NOTIFICATION_UPDATE_RESOURCE, auditFields = {"notificationId : Notification Id input","userId : User Id input" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Record Updated successfully.", response = ForcePasswordOutputVO.class) })
	public Response updateRequest(@ApiParam(value = "The ID of the user", required = true) @Size(min=0,max=32) @PathParam("userid") String userid, 
			@ApiParam(value = "The notification ID", required = true) @Size(min=0,max=32) @PathParam("notificationId") String notificationId) {

		CustomNotificationResponseVO responseVO = new CustomNotificationResponseVO();

		CustomNotificationOutputVO output = null;
		

		IOpContext opcontext = (OpContext) request
				.getAttribute("OP_CONTEXT");
		
		try {
			isUnAuthenticatedRequest();
			
			CustomNotificationInOutVO inOutVO = (CustomNotificationInOutVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomNotificationInOutVO);
			inOutVO.setNotificationId(notificationId);
			updateNotificationStatus(inOutVO, opcontext);
			
			output = new CustomNotificationOutputVO();
			output.setMessage("Notification Status updated Successfully");
			output.setStatus(true);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, response, 0);
		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, null);
		}

		return Response.ok().entity(output).build();
	}
	private CustomNotificationInOutVO updateNotificationStatus(CustomNotificationInOutVO inOutVO, IOpContext context) throws CriticalException, BusinessException, BusinessConfirmation{
		
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomNotificationMaintenanceService"));
		inOutVO = (CustomNotificationInOutVO) serviceCStub.callService(context, inOutVO, new FEBAUnboundString("update"));
		
		return inOutVO;
		
	}
	public static String formatAmount(String amount){
		String formattedAmount = "";

		try{
			//amount = amount.substring(0, amount.indexOf("."));
			Long longVal = Long.parseLong(amount);
			Locale localIDR = new Locale("in","ID");//100.0
			DecimalFormat formatIDR = (DecimalFormat)NumberFormat.getInstance(localIDR);
			formattedAmount = formatIDR.format(longVal);

		}catch(Exception e){
			e.printStackTrace();
		}
		return formattedAmount;
	}
	public static String formatDateBahasa(String date){
		String formattedDate="";
		Date tempDate = new Date();
		Locale locale = new Locale("id", "ID");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy",locale);

		try{
			tempDate=df.parse(date);
		}catch(Exception e){
			return date;	
		}
		df.applyPattern("dd MMM yyyy");

		formattedDate=df.format(tempDate);

		return formattedDate;
	}
}
