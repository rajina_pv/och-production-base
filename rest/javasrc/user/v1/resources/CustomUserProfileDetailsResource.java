package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanProductReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomTenureDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomUserProfileDetailsVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollAccountFetchDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollAccountFetchEnquiryVO;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.RestResourceConstants;
import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.types.valueobjects.CUSRInoutVO;
import com.infosys.fentbase.types.valueobjects.CUSROutputVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "USER")
@Api(value = "Custom User Profile")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApiResponses(value = { @ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401),
		@ApiResponse(code = 404, message = ResourcePaths.HTTP_ERROR_404),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500) })
public class CustomUserProfileDetailsResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request;

	/**
	 * Below method will fetch the user profile details from CUSR table by
	 * taking input as access token
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(value = "Fetch the profile details of a user", response = CustomUserProfileDetailsVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_USER_PROFILE_DETAILS)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The user profile details are fetched successfully.", response = CustomUserProfileDetailsVO.class) })
	public Response fetchUserProfileDetails() {

		CustomUserProfileDetailsVO userProfileDetailsVO = new CustomUserProfileDetailsVO();
		RestCommonSuccessMessageHandler requestVO = new RestCommonSuccessMessageHandler();
		CUSROutputVO cusrInfo = null;
		
		
		try {
			IOpContext opContext = (OpContext) request.getAttribute("OP_CONTEXT");
			BankId bankid= (BankId) opContext.getFromContextData(EBankingConstants.BANK_ID);
			
			CUSRInoutVO cusrInoutvo = (CUSRInoutVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.CUSRInoutVO);
			
				IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CUSRInfoService"));
				cusrInoutvo = (CUSRInoutVO) serviceCStub.callService(opContext, cusrInoutvo, new FEBAUnboundString("fetchPrimaryInfo"));
				cusrInfo = cusrInoutvo.getCusrOutputVO();
			
			UserPrincipal principalId = null;
			
			principalId = cusrInfo.getUserPrincipal();
			YNFlag unifiedLoginUser = cusrInfo.getIsUnifiedLoginUser();
			
			/****************** If the data to be taken from cusr info */

			userProfileDetailsVO.setBankId(bankid.getValue());
			userProfileDetailsVO.setOrgId(cusrInfo.getOrgId().getValue());
			userProfileDetailsVO.setUserId(cusrInfo.getUserId().getValue());
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getUserType().getValue())) {
				userProfileDetailsVO.setUserType(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.USER_TYPE, cusrInfo.getUserType().getValue()));
			}
			userProfileDetailsVO.setCustomerId(cusrInfo.getCustId().getValue());
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getSalutation().getValue())) {
				userProfileDetailsVO.setSalutation(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.ONLINE_REG_SALUTATION_STATUS, cusrInfo.getSalutation().getValue()));
			}
			userProfileDetailsVO.setFirstName(cusrInfo.getCFName().getValue());
			userProfileDetailsVO.setMiddleName(cusrInfo.getCMName().getValue());
			userProfileDetailsVO.setLastName(cusrInfo.getCLName().getValue());
			if (RestCommonUtils.isNotNullorEmpty(principalId.getValue())) {
				userProfileDetailsVO.setPrincipalId(principalId.getValue());
			}
			userProfileDetailsVO.setPrimaryBranchId(cusrInfo.getPBranchId().getValue());

			userProfileDetailsVO.setPrimaryAccountId(cusrInfo.getPrimAcid().getValue());

			userProfileDetailsVO.setLastTransactionDate(cusrInfo.getLastTxnDate().getValue());
			userProfileDetailsVO.setLoginDate(cusrInfo.getLoginDate().getValue());
			userProfileDetailsVO.setLogoffDate(cusrInfo.getLogoffDate().getValue());
			userProfileDetailsVO.setSessionId(cusrInfo.getSessionId().getValue());
			userProfileDetailsVO.setMobileNumber(cusrInfo.getCMPhoneNo().getValue());
			userProfileDetailsVO.setPhoneNumber(cusrInfo.getCPhoneNo().getValue());
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getMultiCrnTxnAllowed().toString())) {
				userProfileDetailsVO.setMultiCurrencyTxnAllowed(RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.YES_NO_FLAG,
						cusrInfo.getMultiCrnTxnAllowed().toString()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getCGender().toString())) {
				userProfileDetailsVO.setGender(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.GENDER, cusrInfo.getCGender().toString()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getMaritalStatus().getValue())) {
				userProfileDetailsVO.setMaritalStatus(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.MARITAL_STATUS, cusrInfo.getMaritalStatus().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getCategoryCode().getValue())) {
				userProfileDetailsVO.setCategoryCode(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.CATEGORY_CODE, cusrInfo.getCategoryCode().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getLimitScheme().getValue())) {
				userProfileDetailsVO.setLimitScheme(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.LIMIT_SCHEME, cusrInfo.getLimitScheme().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getCResStatus().getValue())) {
				userProfileDetailsVO.setCustomerResidentialStatus(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.RESIDENTIAL_STATUS, cusrInfo.getCResStatus().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getLoginAllowed().toString())) {
				userProfileDetailsVO.setLoginAllowed(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.YES_NO_FLAG, cusrInfo.getLoginAllowed().toString()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getTransactionAllowed().toString())) {
				userProfileDetailsVO.setTransactionAllowed(RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.YES_NO_FLAG,
						cusrInfo.getTransactionAllowed().toString()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getRangeLimitScheme().getValue())) {
				userProfileDetailsVO.setRangeLimitScheme(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.RANGE_LIMIT_SCHEME, cusrInfo.getRangeLimitScheme().getValue()));
			}
			userProfileDetailsVO.setUserNickName(cusrInfo.getNickName().getValue());
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getAccFmt().getValue())) {
				userProfileDetailsVO.setAccountFormat(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.ACCOUNT_FORMAT, cusrInfo.getAccFmt().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getSegmentName().getValue())) {
				userProfileDetailsVO.setSegmentName(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.SEGMENT_NAME, cusrInfo.getSegmentName().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getAccessScheme().getValue())) {
				userProfileDetailsVO.setAccessScheme(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.ACCESS_SCHEME, cusrInfo.getAccessScheme().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getTranAuthScheme().getValue())) {
				userProfileDetailsVO.setTransactionAuthorizationScheme(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.TXN_AUTH_SCHEME, cusrInfo.getTranAuthScheme().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getLoginChannel().toString())) {
				userProfileDetailsVO.setLoginChannel(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.LOGIN_CHANNEL, cusrInfo.getLoginChannel().toString()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getLastUnsuccessfulLgnChannel().toString())) {
				userProfileDetailsVO.setLastUnsuccessfullLoginChannel(RestCommonUtils.getCodeReferences(opContext,
						RestResourceConstants.LOGIN_CHANNEL, cusrInfo.getLastUnsuccessfulLgnChannel().toString()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getCalType().getValue())) {
				userProfileDetailsVO.setCalendarType(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.CAL_TYPE, cusrInfo.getCalType().getValue()));
			}
			userProfileDetailsVO.setLastUnsuccessfullLoginTime(cusrInfo.getLastUnsuccessfulLoginTime().getValue());
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getDtFmt().getValue())) {
				userProfileDetailsVO.setDateFormat(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.DATEFORMAT, cusrInfo.getDtFmt().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getLangId().getValue())) {
				userProfileDetailsVO.setLanguage(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.LANGUAGE, cusrInfo.getLangId().getValue()));
			}
			if (RestCommonUtils.isNotNullorEmpty(cusrInfo.getDivAccInd().toString())) {
				userProfileDetailsVO.setDivisionAccessIndicator(
						RestCommonUtils.getCodeReferences(opContext, RestResourceConstants.DIV_INDICATOR, cusrInfo.getDivAccInd().toString()));
			}
			if (RestCommonUtils.isNotNullorEmpty(unifiedLoginUser.toString())) {
				userProfileDetailsVO.setUnifiedLoginUser(
						RestCommonUtils.getCodeReferences(opContext,  RestResourceConstants.YES_NO_FLAG, unifiedLoginUser.toString()));
			}

			userProfileDetailsVO.setPrimaryDivisionId(cusrInfo.getPDivId().getValue());
			
			String installationProductId = PropertyUtil.getProperty(CustomResourceConstants.INSTALLATION_PRODUCT_ID, opContext); 
			
			if(installationProductId.equalsIgnoreCase(CustomResourceConstants.PINANG)){
				userProfileDetailsVO.setEmailId(cusrInfo.getCEmailId().getValue());
				userProfileDetailsVO.setCollectabilityStatus("CollectabilityStatus");
			}else if(installationProductId.equalsIgnoreCase(CustomResourceConstants.SAHABAT)){
				CustomPayrollAccountFetchDetailsVO payrollDetailsVO =  fetchPayrollAccountDetails(CustomResourceConstants.LOAN_APP_STATUS_LOAN_CREATED, userProfileDetailsVO.getUserId(), opContext);
				userProfileDetailsVO.setEmailId(cusrInfo.getCEmailId().getValue());
				userProfileDetailsVO.setPayrollAccountNumber(payrollDetailsVO.getPayrollAccountNum().getValue());
			}
			RestResourceManager.updateHeaderFooterResponseData(requestVO, request, response, 1);
			
		} catch (BusinessConfirmation e) {
			LogManager.logDebug(e.getMessage());
		}catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e,
					new ArrayList<RestResponseErrorCodeMapping>());
			
		}
		return Response.ok().entity(userProfileDetailsVO).build();
	}
	public CustomPayrollAccountFetchDetailsVO fetchPayrollAccountDetails(String applicationStatus, String userId,  IOpContext context)
			throws FEBAException {
		CustomPayrollAccountFetchDetailsVO payrollAccountDetails = null;

		CustomPayrollAccountFetchEnquiryVO enquiryVO = (CustomPayrollAccountFetchEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomPayrollAccountFetchEnquiryVO);
		if(null!=applicationStatus){
			enquiryVO.getCriteria().setApplicationStatus(applicationStatus);
		}
		if(null!=userId){
			enquiryVO.getCriteria().setUserId(userId);
		}
		final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomPayrollAccountDetailsService"));
		enquiryVO = (CustomPayrollAccountFetchEnquiryVO)serviceCStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		FEBAArrayList<CustomPayrollAccountFetchDetailsVO> detailsListVO = enquiryVO.getResultList();

		if(detailsListVO.size()==1){
			payrollAccountDetails = detailsListVO.get(0);
		}else{
			LogManager.logDebug("More than one payroll account fetched from DB. Number of values fetched - "+detailsListVO.size()+1);
		}

		return payrollAccountDetails;
	}
}
