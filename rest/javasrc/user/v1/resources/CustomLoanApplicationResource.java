package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomApplicantDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomApplicantInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomApplicantKtpAddressDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomApplicationStatusVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCommonCodeDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomContactInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocumentInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomEmploymentDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomEmploymentInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationCreationOutputVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationCreationResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationMasterDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationRequestVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomPayrollInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomPayrollSessionDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomResponsePayrollDetailsVO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CPITTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CPITInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicantKtpDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoreCalculateVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanAppIdEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnApplicantDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnContactDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnEmploymentDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomOCRUploadVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomRestAPILimitInOutVO;
import com.infosys.custom.ebanking.user.util.CustomDocumentUploadUtil;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.pojo.CommonInputRetailHeaderVO;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.FEBAStringBuilder;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FileSequenceNumber;
import com.infosys.feba.framework.types.primitives.Flag;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FileDownloadDetailsVO;
import com.infosys.fentbase.core.cache.impl.StateCodesFilter;
import com.infosys.fentbase.tao.FDTTTAO;
import com.infosys.fentbase.tao.info.FDTTInfo;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.primitives.InterestRate;
import com.infosys.fentbase.types.valueobjects.StateCodeCacheVO;
import com.infosys.fentbase.types.valueobjects.StateCodeVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Application")
@Path("/v1/banks/{bankid}/users/{userid}/custom/applications/")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })
public class CustomLoanApplicationResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest servletrequest;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInformation;

	public static final String OP_CONTEXT = "OP_CONTEXT";
	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";
	public static final String LOAN_APPLICATION_ID_SERVICE = "CustomLoanApplicationIdService";
	public static final String COMMON_CODE_CACHE = "CommonCodeCache";
	public static final String CODE_TYPE_CM_CODE = "CODE_TYPE=ASTG|CM_CODE=";
	public static final String HOME_CUR_CODE = "HOME_CUR_CODE";
	public static final String CREATE = "create";
	public static final String MODIFY = "modify";
	public static final String CODE_TYPE = "CODE_TYPE=";
	public static final String CM_CODE = "|CM_CODE=";
	public static final String OCR_VER_FAIL = "OCR_VER_FAIL";
	public static final String BANK_ID="BANK_ID";
	public static final String CR_SCORE_SUB = "CR_SCORE_SUB";
	public static final String CR_SCORE_APR = "CR_SCORE_APR";
	public static final String KTP_SAVED = "KTP_SAVED";
	public static final String PER_SAVED = "PER_SAVED";
	public static final String CON_SAVED = "CON_SAVED";
	public static final String PAY_SAVED = "PAY_SAVED";
	public static final String CR_SCORE_REJ = "CR_SCORE_REJ";
	public static final String PAYROLL_REJ = "PAYROLL_REJ";
	public static final String PAYROLL_APP = "PAYROLL_APP";
	public static final String APP_CREATED = "APP_CREATED";


	final IClientStub restAPILimitService = ServiceUtil.getService(new FEBAUnboundString("CustomRestAPILimitService"));

	
	@POST
	@ApiOperation(value = "Create a Loan Application", response = CustomLoanApplicationResponseVO.class)
	@MethodInfo(uri = "/v1/banks/{bankid}/users/{userid}/custom/applications")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Loan Application is created succcessfully", response = CustomLoanApplicationResponseVO.class) })
	public Response createRequest(@ApiParam(value = "user id", required = true) @PathParam("userid") String userId,
			CustomLoanApplicationMasterDetailsVO customLoanApplnRefVO) {

		int responseStatus = 201;
		CustomLoanApplicationMasterDetailsVO requestData = null;
		CustomLoanApplicationRequestVO requestVO = new CustomLoanApplicationRequestVO();
		CustomLoanApplicationCreationOutputVO outputVO = new CustomLoanApplicationCreationOutputVO();
		CustomApplicationStatusVO applnStatusVO = new CustomApplicationStatusVO();
		CustomLoanApplicationCreationResponseVO response = new CustomLoanApplicationCreationResponseVO();
		String applicationStatus = null;
		FEBAUnboundChar insertFlag = new FEBAUnboundChar('I');
		FEBATransactionContext objTxnContext = null;
		try {
			IOpContext opContext = (IOpContext) servletrequest.getAttribute(OP_CONTEXT);
			isUnAuthenticatedRequest();
			populateHeaderFromHttpHeader(requestVO, opContext);

			if (customLoanApplnRefVO != null) {
				requestData = customLoanApplnRefVO;
			}

			CommonInputRetailHeaderVO headerDetails = requestVO.getHeader();

			if (headerDetails != null && headerDetails.getAuthorizationDetails() != null
					&& (RestCommonUtils.isNotNullorEmpty(headerDetails.getAuthorizationDetails().getPrimaryAccessCode())
							|| (RestCommonUtils.isNotNullorEmpty(
									headerDetails.getAuthorizationDetails().getSecondaryAccessCode())))) {
				throw new BusinessException(opContext, EBIncidenceCodes.UNSUPPORTED_AUTHORIZATION_MODE,
						"Transaction password not configured for the request type",
						EBankingErrorCodes.UNSUPPORTED_AUTHORIZATION_MODE);

			} else {
				objTxnContext = RestCommonUtils.getTransactionContext(opContext);
				checkDuplicateLoanRecord(opContext, PAYROLL_APP);

				// Added Service Call for Rest API Tracking Util
				CustomRestAPILimitInOutVO inOutVO = (CustomRestAPILimitInOutVO) FEBAAVOFactory
						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomRestAPILimitInOutVO");
				inOutVO.setApplicationKey("CREATE_APP");
				restAPILimitService.callService(opContext, inOutVO, new FEBAUnboundString("checkInvocation"));
				// Added Service Call for Rest API Tracking Util

				// mapping main application details:: start
				CustomLoanApplicationInputDetailsVO applicationDetails = requestData.getApplicationDetails();
				CustomPayrollSessionDetailsVO payrollSessionDetails = requestData.getPayrollSessionDetails();
				// added for customer address saving - start
				CustomApplicantKtpAddressDetailsVO ktpAddressDetails = requestData.getKtpAddressDetails();
				// added for customer address saving - end

				if (applicationDetails == null || payrollSessionDetails == null) {
					throw new BusinessException(true, opContext, "Application details mandatory", "", null, 211053,
							null);
				}

				CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());

				CustomLoanApplnPayrollAccntDetailsVO payrollSessionDetailsVO = (CustomLoanApplnPayrollAccntDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnPayrollAccntDetailsVO.class.getName());

				CustomLoanApplnMasterDetailsVO alreadyProcessedVO = isWhiteListRequest(opContext);
				if (alreadyProcessedVO != null) {
					if (payrollSessionDetails != null) {
						applicationDetails.setApplicationId(alreadyProcessedVO.getApplicationId().getValue());
						payrollSessionDetails.setApplicationId(alreadyProcessedVO.getApplicationId().getValue());
						payrollSessionDetailsVO.setBankCode(applicationDetails.getBankCode());
						mapInputPayrollDetails(payrollSessionDetails, payrollSessionDetailsVO, opContext, insertFlag);
						applicationStatus = payrollSessionDetailsVO.getApplicationStatus().getValue();
					}
				} else {

					if (applicationDetails != null && payrollSessionDetails != null) {
						applicationStatus = APP_CREATED;
					}

					loanApplnDetailsVO.setApplicationStatus(applicationStatus);
					mapLoanApplicationDetails(applicationDetails, loanApplnDetailsVO, opContext);
					String remarks = "";
					new CustomLoanHistoryUpdateUtil().updateHistory(loanApplnDetailsVO.getApplicationId(),
							applicationStatus, remarks, objTxnContext);

					if (payrollSessionDetails != null) {
						payrollSessionDetails.setApplicationId(applicationDetails.getApplicationId());
						payrollSessionDetailsVO.setBankCode(applicationDetails.getBankCode());
						mapInputPayrollDetails(payrollSessionDetails, payrollSessionDetailsVO, opContext, insertFlag);
						applicationStatus = payrollSessionDetailsVO.getApplicationStatus().getValue();
					}

				}

				outputVO.setLastActionDate(loanApplnDetailsVO.getRCreTime().getValue());
				outputVO.setApplicationId(applicationDetails.getApplicationId());

				applnStatusVO.setCodeType("ASTG");
				applnStatusVO.setCmCode(applicationStatus);
				String statusDesc = AppDataManager
						.getValue(opContext, COMMON_CODE_CACHE, CODE_TYPE_CM_CODE + applicationStatus).toString();
				applnStatusVO.setCodeDescription(statusDesc);
				outputVO.setApplicationStatus(applnStatusVO);
				response.setData(outputVO);
			}

		} catch (Exception e) {
			LogManager.logError(objTxnContext, e);
			RestResourceManager.handleFatalErrorOutput(servletrequest, httpResponse, e, restResponseErrorCodeMapping());
		} finally {
			if (objTxnContext != null) {
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					LogManager.logError(objTxnContext, e);
				}
			}
		}

		RestResourceManager.updateHeaderFooterResponseData(response, servletrequest, httpResponse, 0);

		return Response.status(responseStatus).entity(response).build();
	}
	
	/**
	 * This method is used to create a Loan Application.
	 *
	 * @return Response which is having the CustomLoanApplicationMasterDetailsVO
	 *         as entity.
	 */
	@POST
	@Path("/{applicationId}")
	@ApiOperation(value = "Create a Loan Application", response = CustomLoanApplicationResponseVO.class)
	@MethodInfo(uri = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Loan Application is created succcessfully", response = CustomLoanApplicationResponseVO.class) })
	public Response createSecondApplyRequest(@ApiParam(value = "user id", required = true) @PathParam("userid") String userId,
			CustomLoanApplicationMasterDetailsVO customLoanApplnRefVO,
			@ApiParam(value = "application id", required = true) @PathParam("applicationId") String applicationId) {

		int responseStatus = 201;
		CustomLoanApplicationMasterDetailsVO requestData = null;
		CustomLoanApplicationRequestVO requestVO = new CustomLoanApplicationRequestVO();
		CustomLoanApplicationCreationOutputVO outputVO = new CustomLoanApplicationCreationOutputVO();
		CustomApplicationStatusVO applnStatusVO = new CustomApplicationStatusVO();
		CustomLoanApplicationCreationResponseVO response = new CustomLoanApplicationCreationResponseVO();
		String applicationStatus = null;
		FEBAUnboundChar insertFlag = new FEBAUnboundChar('I');
		FEBATransactionContext objTxnContext = null;
		try {
			IOpContext opContext = (IOpContext) servletrequest.getAttribute(OP_CONTEXT);
			isUnAuthenticatedRequest();
			populateHeaderFromHttpHeader(requestVO, opContext);

			if (customLoanApplnRefVO != null) {
				requestData = customLoanApplnRefVO;
			}

			CommonInputRetailHeaderVO headerDetails = requestVO.getHeader();

			if (headerDetails != null && headerDetails.getAuthorizationDetails() != null
					&& (RestCommonUtils.isNotNullorEmpty(headerDetails.getAuthorizationDetails().getPrimaryAccessCode())
							|| (RestCommonUtils.isNotNullorEmpty(
									headerDetails.getAuthorizationDetails().getSecondaryAccessCode())))) {
				throw new BusinessException(opContext, EBIncidenceCodes.UNSUPPORTED_AUTHORIZATION_MODE,
						"Transaction password not configured for the request type",
						EBankingErrorCodes.UNSUPPORTED_AUTHORIZATION_MODE);

			} else {
				objTxnContext = RestCommonUtils.getTransactionContext(opContext);
				
//				dedupcheck not required as it ai already done in payroll_app
//				checkDuplicateLoanRecord(opContext,KTP_SAVED);

				// Added Service Call for Rest API Tracking Util
//				CustomRestAPILimitInOutVO inOutVO = (CustomRestAPILimitInOutVO) FEBAAVOFactory
//						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomRestAPILimitInOutVO");
//				inOutVO.setApplicationKey("CREATE_APP");
//				restAPILimitService.callService(opContext, inOutVO, new FEBAUnboundString("checkInvocation"));
				// Added Service Call for Rest API Tracking Util

				// mapping main application details:: start
				CustomLoanApplicationInputDetailsVO applicationDetails = requestData.getApplicationDetails();
				applicationDetails.setApplicationId(Long.valueOf(applicationId));
				CustomPayrollInputDetailsVO payrollDetails = requestData.getPayrollDetails();
				CustomDocumentInputDetailsVO documentDetails = requestData.getDocumentDetails();
				CustomApplicantInputDetailsVO applicantDetails = requestData.getApplicantInformation();
				CustomContactInputDetailsVO contactDetails = requestData.getContactInformation();
				CustomEmploymentInputDetailsVO employmentDetails = requestData.getEmploymentInformation();
				CustomPayrollSessionDetailsVO payrollSessionDetails = requestData.getPayrollSessionDetails();
				// added for customer address saving - start
				CustomApplicantKtpAddressDetailsVO ktpAddressDetails = requestData.getKtpAddressDetails();
				// added for customer address saving - end

				if ( documentDetails == null || (payrollSessionDetails == null && payrollDetails == null)) {
					throw new BusinessException(true, opContext, "Application details mandatory", "", null, 211053,
							null);
				}

				CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
				if (documentDetails != null) {
					applicationStatus = KTP_SAVED;
				}
				if (applicantDetails != null) {
					applicationStatus = PER_SAVED;
				}
				if (payrollDetails != null) {
					applicationStatus = PAY_SAVED;
				}
				if (contactDetails != null) {
					applicationStatus = CON_SAVED;
				}

				if (applicationDetails != null && applicationDetails.getIsSubmitted() != null) {
					if (applicationDetails.getIsSubmitted().equals(new Flag("Y"))) {
						applicationStatus = CR_SCORE_SUB;
					}
				}
								
				loanApplnDetailsVO.setApplicationStatus(applicationStatus);
				loanApplnDetailsVO.setApplicationId(applicationId);
				String remarks = "";
				loanApplnDetailsVO.setApplicationStatus(applicationStatus);
				
				new CustomLoanHistoryUpdateUtil().updateHistory(loanApplnDetailsVO.getApplicationId(),
						applicationStatus, remarks, objTxnContext);
				
				CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnDocumentsDetailsVO.class.getName());
				if (documentDetails != null) {
					documentDetails.setApplicationId(applicationDetails.getApplicationId());
					//Added indicator to skip OCR check during second loan application
					mapDocumentDetails(servletrequest, documentDetails, docDetailsVO, opContext,true);
				}
				
				CustomLoanApplnApplicantDetailsVO applicantDetailsVO = (CustomLoanApplnApplicantDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnApplicantDetailsVO.class.getName());

				if (applicantDetails != null) {
					applicantDetails.setApplicationId(applicationDetails.getApplicationId());
					mapApplicantDetails(applicantDetails, applicantDetailsVO, opContext, insertFlag);
				}

				editLoanApplicationDetails(applicationDetails, loanApplnDetailsVO, opContext, applicationId);
//				CustomLoanApplnPayrollAccntDetailsVO payrollSessionDetailsVO = (CustomLoanApplnPayrollAccntDetailsVO) FEBAAVOFactory
//						.createInstance(CustomLoanApplnPayrollAccntDetailsVO.class.getName());
//				
//				if (payrollSessionDetails != null) {
//					payrollSessionDetails.setApplicationId(applicationDetails.getApplicationId());
//					mapInputPayrollDetails(payrollSessionDetails, payrollSessionDetailsVO, opContext, insertFlag);
//				}
//				
				CustomLoanApplnEmploymentDetailsVO emplDetailsVO = (CustomLoanApplnEmploymentDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnEmploymentDetailsVO.class.getName());

				if (employmentDetails != null) {
					employmentDetails.setApplicationId(applicationDetails.getApplicationId());
					mapEmploymentDetails(employmentDetails, emplDetailsVO, opContext, insertFlag);
				}
				
				// added for customer address saving - start
				CustomApplicantKtpDetailsVO applicantKtpDetailsVO = (CustomApplicantKtpDetailsVO) FEBAAVOFactory
						.createInstance(CustomApplicantKtpDetailsVO.class.getName());
				
				if(ktpAddressDetails !=null) {
					ktpAddressDetails.setApplicationId(applicationDetails.getApplicationId());
					ktpAddressDetails.setBankCode(applicationDetails.getBankCode());
					mapKtpAddressDetails(ktpAddressDetails, applicantKtpDetailsVO, opContext, insertFlag);
				}
				// added for customer address saving - end
				
				CustomLoanApplnPayrollAccntDetailsVO payrollDetailsVO = (CustomLoanApplnPayrollAccntDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnPayrollAccntDetailsVO.class.getName());
				
				if (payrollDetails != null) {

					payrollDetails.setApplicationId(applicationDetails.getApplicationId());
					mapLoanPayrollDetails(payrollDetails, payrollDetailsVO, opContext, insertFlag);
				}

				CustomLoanApplnContactDetailsVO contactDetailsVO = (CustomLoanApplnContactDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnContactDetailsVO.class.getName());

				if (contactDetails != null) {
					contactDetails.setApplicationId(applicationDetails.getApplicationId());
					mapContactDetails(contactDetails, contactDetailsVO, opContext, insertFlag);
				}

				outputVO.setLastActionDate(loanApplnDetailsVO.getRCreTime().getValue());
				outputVO.setApplicationId(applicationDetails.getApplicationId());

				if (applicationDetails != null && applicationDetails.getIsSubmitted() != null) {
					if (applicationDetails.getIsSubmitted().equals(new Flag("Y"))) {
						CustomCreditScoreCalculateVO creditScoreCalculateVO = (CustomCreditScoreCalculateVO) FEBAAVOFactory
								.createInstance(CustomCreditScoreCalculateVO.class.getName());
						creditScoreCalculateVO
						.setApplicationId(new ApplicationNumber(applicationDetails.getApplicationId()));
						creditScoreCalculateVO.setUserId(userId);
						final IClientStub customCreditScoreService = ServiceUtil
								.getService(new FEBAUnboundString("CustomCreditScoreService"));
						customCreditScoreService.callService(opContext, creditScoreCalculateVO,
								new FEBAUnboundString("calculate"));
						loanApplnDetailsVO.setUid(creditScoreCalculateVO.getUid());
						loanApplnDetailsVO.setStatusDesc(creditScoreCalculateVO.getDesc());
						loanApplnDetailsVO.setApplicationStatus(creditScoreCalculateVO.getApplicationStatus());
						// Added for BRI Agro payroll
						if(creditScoreCalculateVO.getApplicationStatus().getValue().equals(CR_SCORE_SUB)){
							applicationStatus = CR_SCORE_SUB;
						}else if(creditScoreCalculateVO.getApplicationStatus().getValue().equals(PAYROLL_REJ)){
							applicationStatus = PAYROLL_REJ;
						}

						CLATInfo clatinfo = CLATTAO.select(objTxnContext,
								new BankId(opContext.getFromContextData(BANK_ID).toString()),
								new ApplicationNumber(applicationDetails.getApplicationId()));

						outputVO.setLastActionDate(clatinfo.getCookie().getRModTime().getValue());
						if (applicationStatus.equals(CR_SCORE_SUB)) {
							Date currentTime = new java.util.Date(System.currentTimeMillis());
							Date rModTime = clatinfo.getCookie().getRModTime().getValue();
							long diff = currentTime.getTime() - rModTime.getTime();

							long days = TimeUnit.MILLISECONDS.toDays(diff);
							long remainingHoursInMillis = diff - TimeUnit.DAYS.toMillis(days);
							long hours = TimeUnit.MILLISECONDS.toHours(remainingHoursInMillis);
							long remainingMinutesInMillis = remainingHoursInMillis - TimeUnit.HOURS.toMillis(hours);
							long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingMinutesInMillis);

							long timeLeftForScoreCompletion = 0;

							String creditScoreCalTimeInterval = PropertyUtil.getProperty("CREDIT_SCR_CAL_TIME_INTRVL",
									opContext);
							timeLeftForScoreCompletion = Long.valueOf(creditScoreCalTimeInterval) - minutes;
							outputVO.setTimeLeftForScoreCompletion(timeLeftForScoreCompletion);

						}
						// Added for BRI AGRO payroll.			
						if (applicationStatus.equals(PAYROLL_REJ)) {
							updatePayrollRejStatus(loanApplnDetailsVO,opContext);
							throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.PAYROLL_REJECTION,
									"Verified payroll data is not found.", null, CustomEBankingErrorCodes.PAYROLL_REJECTION, null);
						}
					}
				}

				applnStatusVO.setCodeType("ASTG");
				applnStatusVO.setCmCode(applicationStatus);
				String statusDesc = AppDataManager
						.getValue(opContext, COMMON_CODE_CACHE, CODE_TYPE_CM_CODE + applicationStatus).toString();
				applnStatusVO.setCodeDescription(statusDesc);
				outputVO.setApplicationStatus(applnStatusVO);
				response.setData(outputVO);
			}

		} catch (Exception e) {
			LogManager.logError(objTxnContext, e);
			RestResourceManager.handleFatalErrorOutput(servletrequest, httpResponse, e, restResponseErrorCodeMapping());
		} finally {
			if (objTxnContext != null) {
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					LogManager.logError(objTxnContext, e);
				}
			}
		}

		RestResourceManager.updateHeaderFooterResponseData(response, servletrequest, httpResponse, 0);

		return Response.status(responseStatus).entity(response).build();
	}

	private void mapKtpAddressDetails(CustomApplicantKtpAddressDetailsVO ktpAddressDetails,
			CustomApplicantKtpDetailsVO applicantKtpDetailsVO, IOpContext opContext, FEBAUnboundChar flag) 
					throws CriticalException, BusinessException, BusinessConfirmation, FEBATypeSystemException {
		
		applicantKtpDetailsVO.setApplicationId(new ApplicationNumber(ktpAddressDetails.getApplicationId()));
		applicantKtpDetailsVO.setBankCode(ktpAddressDetails.getBankCode());
				
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getAddress())) {
			applicantKtpDetailsVO.setAddressLine1(ktpAddressDetails.getAddress());
		}
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getAddress2())) {
			applicantKtpDetailsVO.setAddressLine2(ktpAddressDetails.getAddress2());
		}
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getAddress3())) {
			applicantKtpDetailsVO.setAddressLine3(ktpAddressDetails.getAddress3());
		}
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getAddress4())) {
			applicantKtpDetailsVO.setAddressLine4(ktpAddressDetails.getAddress4());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getAgama())) {
			applicantKtpDetailsVO.setReligion(ktpAddressDetails.getAgama());
		}

		if (RestCommonUtils.isNotNull(ktpAddressDetails.getBirthDate())) {
			applicantKtpDetailsVO.setDateOfBirth(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(ktpAddressDetails.getBirthDate()).getTimestampValue())));
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getCifNo())) {
			applicantKtpDetailsVO.setLegacyCifNo(ktpAddressDetails.getCifNo());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getCity())) {
			applicantKtpDetailsVO.setCity(ktpAddressDetails.getCity());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getIdNo())) {
			applicantKtpDetailsVO.setNationalId(ktpAddressDetails.getIdNo());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getIdType())) {
			applicantKtpDetailsVO.setNationalIdType(ktpAddressDetails.getIdType());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getNama())) {
			applicantKtpDetailsVO.setName(ktpAddressDetails.getNama());
		}

		if (RestCommonUtils.isNotNull(ktpAddressDetails.getPlaceOfBirth())) {
			applicantKtpDetailsVO.setPlaceOfBirth(ktpAddressDetails.getPlaceOfBirth());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getProvince())) {
			applicantKtpDetailsVO.setState(ktpAddressDetails.getProvince());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getSex())) {
			applicantKtpDetailsVO.setGender(ktpAddressDetails.getSex());
		}
		
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getZipCode())) {
				applicantKtpDetailsVO.setPostalCode(ktpAddressDetails.getZipCode().toString());
		}
		if (RestCommonUtils.isNotNull(ktpAddressDetails.getIsKTPAddess())) {
			applicantKtpDetailsVO.setIsKTPAddress(ktpAddressDetails.getIsKTPAddess().toString());
	}
		final IClientStub customApplicantService = ServiceUtil
				.getService(new FEBAUnboundString("CustomApplicantKtpDetailsService"));

		if (flag.equals(new FEBAUnboundChar('I'))) {
			customApplicantService.callService(opContext, applicantKtpDetailsVO, new FEBAUnboundString(CREATE));
		} else if (flag.equals(new FEBAUnboundChar('E'))) {
			customApplicantService.callService(opContext, applicantKtpDetailsVO, new FEBAUnboundString(MODIFY));
		}		
	}

	protected static void mapLoanApplicationDetails(CustomLoanApplicationInputDetailsVO applicationDetails,
			CustomLoanApplnMasterDetailsVO loanApplnDetailsVO, IOpContext opContext)
					throws CriticalException, BusinessException, BusinessConfirmation {
		loanApplnDetailsVO.setProductType(applicationDetails.getProductType());
		loanApplnDetailsVO.setProductCode(applicationDetails.getProductCode());
		loanApplnDetailsVO.setProductCategory(applicationDetails.getProductCategory());
		loanApplnDetailsVO.setProductSubCategory(applicationDetails.getProductSubCategory());
		loanApplnDetailsVO.getRequestedLoanAmt().setAmountValue(applicationDetails.getRequestedLoanAmount());
		loanApplnDetailsVO.getRequestedLoanAmt()
		.setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));

		loanApplnDetailsVO.setRequestedTenor(new FEBAUnboundInt(applicationDetails.getRequestedTenor()));
		loanApplnDetailsVO.setLoanIntRate(new InterestRate(applicationDetails.getInterestRate()));

		loanApplnDetailsVO.getMonthlyInstallment().setAmountValue(applicationDetails.getMonthlyInstallment());
		loanApplnDetailsVO.getMonthlyInstallment()
		.setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
		if (RestCommonUtils.isNotNull(applicationDetails.getLegacyCif())) {
			loanApplnDetailsVO.setLegacyCif(applicationDetails.getLegacyCif());
		}
		
		// Added for BRI AGRO
		loanApplnDetailsVO.setBankCode(applicationDetails.getBankCode());
		
		if (RestCommonUtils.isNotNull(applicationDetails.getKtpNum())) {
			loanApplnDetailsVO.setKtpId(applicationDetails.getKtpNum());
		}
		// Added for PINANG Pay later - start
		if (RestCommonUtils.isNotNullorEmpty(applicationDetails.getSchemeCode())) {
			loanApplnDetailsVO.setSchemeCode(applicationDetails.getSchemeCode());
		}else{
			loanApplnDetailsVO.setSchemeCode("PNANG"); //Added for production issue - requested by bank. 
		}
		if (RestCommonUtils.isNotNull(applicationDetails.getPartnerId())) {
			loanApplnDetailsVO.setPartnerId(applicationDetails.getPartnerId());
		}
		// Added for PINANG Pay later - end
		final IClientStub customLoanApplicationService = ServiceUtil
				.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));

		customLoanApplicationService.callService(opContext, loanApplnDetailsVO, new FEBAUnboundString(CREATE));
		// mapping main application details:: end
		applicationDetails.setApplicationId(new Long(loanApplnDetailsVO.getApplicationId().toString()));
	}

	protected static void mapLoanPayrollDetails(CustomPayrollInputDetailsVO payrollDetails,
			CustomLoanApplnPayrollAccntDetailsVO payrollDetailsVO, IOpContext opContext , FEBAUnboundChar flag)
					throws CriticalException, BusinessException, BusinessConfirmation {
		
		System.out.println("flag flag::"+flag);
		
		String pinangOrSahabat = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", opContext);
		payrollDetailsVO.setApplicationId(new ApplicationNumber(payrollDetails.getApplicationId()));
		payrollDetailsVO.setPayrollAccountId(payrollDetails.getPayrollAccount());
		if (RestCommonUtils.isNotNullorEmpty(payrollDetails.getPayrollAccountBranch())) {
			payrollDetailsVO.setPayrollAccountBranch(payrollDetails.getPayrollAccountBranch());
		}
		if (RestCommonUtils.isNotNull(payrollDetails.getAgentCode())) {
			payrollDetailsVO.setAgentCode(payrollDetails.getAgentCode());
		}
		if (RestCommonUtils.isNotNull(payrollDetails.getReferralCode())) {
			payrollDetailsVO.setRefferalCode(payrollDetails.getReferralCode());
		}
		if (RestCommonUtils.isNotNull(payrollDetails.getPayrollDate())) {
			payrollDetailsVO.setAccountPayrollDate(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(payrollDetails.getPayrollDate()).getTimestampValue())));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getSavingAmount())) {
			payrollDetailsVO.getSavingAmount().setAmountValue(payrollDetails.getSavingAmount());
			payrollDetailsVO.getSavingAmount().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
		}

		if (pinangOrSahabat.equals(CustomEBConstants.SAHABAT)) {

			if (RestCommonUtils.isNotNull(payrollDetails.getCreditFre3())) {
				payrollDetailsVO.setCreFre3(new FEBAUnboundInt(payrollDetails.getCreditFre3()));
			}

			if (String.valueOf(payrollDetails.getCreditFre6()) != null) {
				payrollDetailsVO.setCreFre6(new FEBAUnboundInt(payrollDetails.getCreditFre6()));
			}
			if (String.valueOf(payrollDetails.getCreditFre12()) != null) {
				payrollDetailsVO.setCreFre12(new FEBAUnboundInt(payrollDetails.getCreditFre12()));
			}
			if (String.valueOf(payrollDetails.getDebitFre3()) != null) {
				payrollDetailsVO.setDebFre3(new FEBAUnboundInt(payrollDetails.getDebitFre3()));
			}
			if (String.valueOf(payrollDetails.getDebitFre6()) != null) {
				payrollDetailsVO.setDebFre6(new FEBAUnboundInt(payrollDetails.getDebitFre6()));
			}
			if (String.valueOf(payrollDetails.getDebitFre12()) != null) {
				payrollDetailsVO.setDebFre12(new FEBAUnboundInt(payrollDetails.getDebitFre12()));
			}

			if (String.valueOf(payrollDetails.getCreditTotal3()) != null) {
				payrollDetailsVO.getCreTot3().setAmountValue(payrollDetails.getCreditTotal3());
				payrollDetailsVO.getCreTot3().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (String.valueOf(payrollDetails.getCreditTotal6()) != null) {
				payrollDetailsVO.getCreTot6().setAmountValue(payrollDetails.getCreditTotal6());
				payrollDetailsVO.getCreTot6().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (String.valueOf(payrollDetails.getCreditTotal12()) != null) {
				payrollDetailsVO.getCreTot12().setAmountValue(payrollDetails.getCreditTotal12());
				payrollDetailsVO.getCreTot12().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}

			if (String.valueOf(payrollDetails.getDebitTotal3()) != null) {
				payrollDetailsVO.getDebTot3().setAmountValue(payrollDetails.getDebitTotal3());
				payrollDetailsVO.getDebTot3().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}

			if (String.valueOf(payrollDetails.getDebitTotal6()) != null) {
				payrollDetailsVO.getDebTot6().setAmountValue(payrollDetails.getDebitTotal6());
				payrollDetailsVO.getDebTot6().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}

			if (String.valueOf(payrollDetails.getDebitTotal12()) != null) {
				payrollDetailsVO.getDebTot12().setAmountValue(payrollDetails.getDebitTotal12());
				payrollDetailsVO.getDebTot12().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}

			if (String.valueOf(payrollDetails.getCreditAvg3()) != null) {
				payrollDetailsVO.getCreAvg3().setAmountValue(payrollDetails.getCreditAvg3());
				payrollDetailsVO.getCreAvg3().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (String.valueOf(payrollDetails.getCreditAvg6()) != null) {
				payrollDetailsVO.getCreAvg6().setAmountValue(payrollDetails.getCreditAvg6());
				payrollDetailsVO.getCreAvg6().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));

			}
			if (String.valueOf(payrollDetails.getCreditAvg12()) != null) {
				payrollDetailsVO.getCreAvg12().setAmountValue(payrollDetails.getCreditAvg12());
				payrollDetailsVO.getCreAvg12().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (String.valueOf(payrollDetails.getDebitAvg3()) != null) {
				payrollDetailsVO.getDebAvg3().setAmountValue(payrollDetails.getDebitAvg3());
				payrollDetailsVO.getDebAvg3().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (String.valueOf(payrollDetails.getDebitAvg6()) != null) {
				payrollDetailsVO.getDebAvg6().setAmountValue(payrollDetails.getDebitAvg6());
				payrollDetailsVO.getDebAvg6().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (String.valueOf(payrollDetails.getDebitAvg12()) != null) {
				payrollDetailsVO.getDebAvg12().setAmountValue(payrollDetails.getDebitAvg12());
				payrollDetailsVO.getDebAvg12().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (String.valueOf(payrollDetails.getNetBanking()) != null) {
				payrollDetailsVO.setNetBanking(getYNFlag(payrollDetails.getNetBanking(), true));
			}
			if (String.valueOf(payrollDetails.getMobileBanking()) != null) {
				payrollDetailsVO.setMobileBanking(getYNFlag(payrollDetails.getMobileBanking(), true));
			}
			if (String.valueOf(payrollDetails.getPrimaryPhone()) != null) {
				payrollDetailsVO.setPrimaryPhone(getYNFlag(payrollDetails.getPrimaryPhone(), true));
			}
			if (String.valueOf(payrollDetails.getSecPhone()) != null) {
				payrollDetailsVO.setSecondaryPhone(getYNFlag(payrollDetails.getSecPhone(), true));
			}
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getSavingAccOwnshpSts())) {
			payrollDetailsVO.setSavingAccountOwnershipStatus(getYNFlag(payrollDetails.getSavingAccOwnshpSts(), true));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getSavingAccOwnshpDate())) {
			payrollDetailsVO.setSavingAccountOwnershipDate(new FEBADate(DateUtil
					.stripTimeComponent(new FEBADate(payrollDetails.getSavingAccOwnshpDate()).getTimestampValue())));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getCompanyName())) {
			payrollDetailsVO.setCompanyName(payrollDetails.getCompanyName());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getPersonelName())) {
			payrollDetailsVO.setPersonelName(payrollDetails.getPersonelName());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getPersonelNumber())) {
			payrollDetailsVO.setPersonelNumber(Long.toString(payrollDetails.getPersonelNumber()));
		}

		/*
		 * Added fields to CLPA for
		 */

		if (String.valueOf(payrollDetails.getNetIncomeWL()) != null) {
			payrollDetailsVO.getNetIncomeWL().setAmountValue(payrollDetails.getNetIncomeWL());
			payrollDetailsVO.getNetIncomeWL().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getNameWL())) {
			payrollDetailsVO.setNameWL(payrollDetails.getNameWL());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getBirthDateWL())) {
			payrollDetailsVO.setBirthDateWL(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(payrollDetails.getBirthDateWL()).getTimestampValue())));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getEmpEndDateWL())) {
			payrollDetailsVO.setEmpEndDateWL(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(payrollDetails.getEmpEndDateWL()).getTimestampValue())));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getEmpStartDateWL())) {
			payrollDetailsVO.setEmpStartDateWL(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(payrollDetails.getEmpStartDateWL()).getTimestampValue())));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getGenderWL())) {
			payrollDetailsVO.setGenderWL(payrollDetails.getGenderWL());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getPlaceOfBirthWL())) {
			payrollDetailsVO.setPlaceOfBirthWL(payrollDetails.getPlaceOfBirthWL());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getAddressWL())) {
			payrollDetailsVO.setAddressWL(payrollDetails.getAddressWL());
		}
		
		// Added for BRI AGRO start 
		if (RestCommonUtils.isNotNull(payrollDetails.getEmployeeStatus())) {
			payrollDetailsVO.setEmployeeStatus(payrollDetails.getEmployeeStatus());
		}
		
		if (RestCommonUtils.isNotNull(payrollDetails.getExpenditureAmount())) {
			payrollDetailsVO.getExpenditureAmount().setAmountValue(payrollDetails.getExpenditureAmount());
			payrollDetailsVO.getExpenditureAmount()
			.setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
		}
		// Added for BRI AGRO end 
		
		//non payroll NPWP addition--Start
		if (RestCommonUtils.isNotNull(payrollDetails.getNPWP())) {
			payrollDetailsVO.setNPWP(payrollDetails.getNPWP());
		}
		//non payroll NPWP addition--End
		//non payroll company Address addition--Start
		if (RestCommonUtils.isNotNull(payrollDetails.getCompanyAddress())) {
			payrollDetailsVO.setCompanyAddress(payrollDetails.getCompanyAddress());
		}
		//non payroll company Address addition--End
		
		// Added for PINANG Web view start
		if (RestCommonUtils.isNotNull(payrollDetails.getWorkAddress())) {
			payrollDetailsVO.setWorkAddress(payrollDetails.getWorkAddress());
		}
		// Added for PINANG Web view end

		final IClientStub customLoanPayrollService = ServiceUtil
				.getService(new FEBAUnboundString("CustomLoanApplnPayrollAccntService"));
		
		if (flag.equals(new FEBAUnboundChar('I'))) {
			customLoanPayrollService.callService(opContext, payrollDetailsVO, new FEBAUnboundString(CREATE));
		} else if (flag.equals(new FEBAUnboundChar('E'))) {
			customLoanPayrollService.callService(opContext, payrollDetailsVO, new FEBAUnboundString(MODIFY));
		}

		

	}

	protected static void mapInputPayrollDetails(CustomPayrollSessionDetailsVO payrollDetails,
			CustomLoanApplnPayrollAccntDetailsVO payrollDetailsVO, IOpContext opContext , FEBAUnboundChar flag)
					throws CriticalException, BusinessException, BusinessConfirmation {
		
		payrollDetailsVO.setApplicationId(new ApplicationNumber(payrollDetails.getApplicationId()));
		payrollDetailsVO.setPayrollAccountId(payrollDetails.getPayrollAccount());
		if (RestCommonUtils.isNotNullorEmpty(payrollDetails.getPayrollAccountBranch())) {
			payrollDetailsVO.setPayrollAccountBranch(payrollDetails.getPayrollAccountBranch());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getSavingAmount())) {
			payrollDetailsVO.getSavingAmount().setAmountValue(payrollDetails.getSavingAmount());
			payrollDetailsVO.getSavingAmount().setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getNameWL())) {
			payrollDetailsVO.setNameWL(payrollDetails.getNameWL());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getBirthDateWL())) {
			payrollDetailsVO.setBirthDateWL(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(payrollDetails.getBirthDateWL()).getTimestampValue())));
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getGenderWL())) {
			payrollDetailsVO.setGenderWL(payrollDetails.getGenderWL());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getPlaceOfBirthWL())) {
			payrollDetailsVO.setPlaceOfBirthWL(payrollDetails.getPlaceOfBirthWL());
		}

		if (RestCommonUtils.isNotNull(payrollDetails.getAddressWL())) {
			payrollDetailsVO.setAddressWL(payrollDetails.getAddressWL());
		}
		
		final IClientStub customLoanPayrollService = ServiceUtil
				.getService(new FEBAUnboundString("CustomCaptureApplnPayrollAccntService"));
		
		if (flag.equals(new FEBAUnboundChar('I'))) {
			customLoanPayrollService.callService(opContext, payrollDetailsVO, new FEBAUnboundString(CREATE));
		} else if (flag.equals(new FEBAUnboundChar('E'))) {
			customLoanPayrollService.callService(opContext, payrollDetailsVO, new FEBAUnboundString(MODIFY));
		}

	}

	private static YNFlag getYNFlag(char flag, boolean defaultY) {
		return defaultY ? new YNFlag('Y' == flag ? 'Y' : 'N') : new YNFlag('N' == flag ? 'N' : 'Y');
	}

	//Modified method signature to skip OCR check during second loan application
	protected static void mapDocumentDetails(HttpServletRequest request, CustomDocumentInputDetailsVO documentDetails,
			CustomLoanApplnDocumentsDetailsVO docDetailsVO, IOpContext opContext, boolean isSecondApply) throws Exception {

		FEBATransactionContext txnContext = null;
		
		try {
			String ocrCheckrequired = PropertyUtil.getProperty("OCR_CHECK_REQUIRED", opContext);
			
			//Added to skip OCR check during second loan application :: START
			if(isSecondApply) {
				ocrCheckrequired = "N";
			}
			//Added to skip OCR check during second loan application :: END
			System.out.println("ocrCheckrequired::"+ocrCheckrequired);

			final String OCR_VO = "com.infosys.custom.ebanking.types.valueobjects.CustomOCRUploadVO";

			FEBAUnboundString file = new FEBAUnboundString(documentDetails.getFile());
			String inputKTP = documentDetails.getDocumentCode();
			System.out.println("file.getLength() - "+file.getLength());
			System.out.println("inputKTP:: - "+inputKTP);
			documentDetails = new CustomDocumentUploadUtil()
					.createFileInSharedDirAndUploadEncryptedFile(documentDetails, opContext, request);

			String outputKTP = "";
			if (null != ocrCheckrequired && ocrCheckrequired.equalsIgnoreCase("Y")) {
				CustomOCRUploadVO ocrVO = (CustomOCRUploadVO) FEBAAVOFactory.createInstance(OCR_VO);
				ocrVO.setFile(documentDetails.getFile());
				// Added for AGRO payroll OCR fix
				ocrVO.setInputNik(inputKTP);
				final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomOCRService"));
				serviceStub.callService(opContext, ocrVO, new FEBAUnboundString("gVCheck"));

				outputKTP = ocrVO.getSuccess().toString();
			} else {
				outputKTP = inputKTP;
			}
			txnContext = RestCommonUtils.getTransactionContext(opContext);
			if ((outputKTP != null && outputKTP != "" && inputKTP.equals(outputKTP))) {

				docDetailsVO.setApplicationId(new ApplicationNumber(documentDetails.getApplicationId()));
				docDetailsVO.setDocType(documentDetails.getDocumentType());
				docDetailsVO.setDocCode(documentDetails.getDocumentCode());
				docDetailsVO.setDocKey(documentDetails.getDocKey());
				docDetailsVO.setDocStorePath(documentDetails.getFileUploadPath());
				docDetailsVO.setFileSeqNo(documentDetails.getFileSeqNo());
				final IClientStub customDocDetailsService = ServiceUtil
						.getService(new FEBAUnboundString("CustomLoanApplicationDocDetailsService"));

				customDocDetailsService.callService(opContext, docDetailsVO, new FEBAUnboundString("createOrUpdate"));

				CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
				loanApplnDetailsVO.setApplicationId(documentDetails.getApplicationId().toString());
				loanApplnDetailsVO.setIsKTPVerified("Y");
				updateKTPStatus(loanApplnDetailsVO,opContext);

			} else if (isOCRFailedAllowedToPass(opContext,new ApplicationNumber(documentDetails.getApplicationId()))){

				docDetailsVO.setApplicationId(new ApplicationNumber(documentDetails.getApplicationId()));
				docDetailsVO.setDocType(documentDetails.getDocumentType());
				docDetailsVO.setDocCode(documentDetails.getDocumentCode());
				docDetailsVO.setDocKey(documentDetails.getDocKey());
				docDetailsVO.setDocStorePath(documentDetails.getFileUploadPath());
				docDetailsVO.setFileSeqNo(documentDetails.getFileSeqNo());
				final IClientStub customDocDetailsService = ServiceUtil
						.getService(new FEBAUnboundString("CustomLoanApplicationDocDetailsService"));

				customDocDetailsService.callService(opContext, docDetailsVO, new FEBAUnboundString("createOrUpdate"));

			}
			else {
				throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.KTP_NUMBER_MISMATCH,
						"KTP Number is incorrect.", null, CustomEBankingErrorCodes.KTP_NUMBER_MISMATCH, null);
			}
		} catch (Exception ee) {

			LogManager.logError(opContext, ee);
			throw ee;
		}finally{
			if(txnContext != null){
				try {
					txnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}
	}


	protected static void mapApplicantDetails(CustomApplicantInputDetailsVO applicantDetails,
			CustomLoanApplnApplicantDetailsVO applicantDetailsVO, IOpContext opContext, FEBAUnboundChar flag)
					throws CriticalException, BusinessException, BusinessConfirmation {
		applicantDetailsVO.setApplicationId(new ApplicationNumber(applicantDetails.getApplicationId()));
		if (RestCommonUtils.isNotNull(applicantDetails.getMobileNo())) {
			applicantDetailsVO.setMobileNum(applicantDetails.getMobileNo().toString());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getEmailId())) {
			applicantDetailsVO.setEmailId(applicantDetails.getEmailId());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getName())) {
			applicantDetailsVO.setName(applicantDetails.getName());
		}

		if (RestCommonUtils.isNotNull(applicantDetails.getMobileNo())) {
			applicantDetailsVO.setMobileNum(applicantDetails.getMobileNo().toString());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getGender())) {
			applicantDetailsVO.setGender(applicantDetails.getGender());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getPlaceOfBirth())) {
			applicantDetailsVO.setPlaceOfBirth(applicantDetails.getPlaceOfBirth());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getDateOfBirth())) {
			applicantDetailsVO.setDateOfBirth(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(applicantDetails.getDateOfBirth()).getTimestampValue())));
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getNationalId())) {
			applicantDetailsVO.setNationalId(applicantDetails.getNationalId());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getLastEducation())) {
			applicantDetailsVO.setLastEducation(applicantDetails.getLastEducation());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getCreditCardIssuer())) {
			applicantDetailsVO.setCardIssuerBank(applicantDetails.getCreditCardIssuer());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getAddressLine1())) {
			applicantDetailsVO.setAddressLine1(applicantDetails.getAddressLine1());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getAddressLine2())) {
			applicantDetailsVO.setAddressLine2(applicantDetails.getAddressLine2());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getAddressLine3())) {
			applicantDetailsVO.setAddressLine3(applicantDetails.getAddressLine3());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getAddressLine4())) {
			applicantDetailsVO.setAddressLine4(applicantDetails.getAddressLine4());
		}

		if (RestCommonUtils.isNotNull(applicantDetails.getAddressLine5())) {
			applicantDetailsVO.setAddressLine5(applicantDetails.getAddressLine5());
			IFEBAType postalCode = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
					"CODE_TYPE=CPMP|CM_CODE=" + applicantDetails.getAddressLine5());
			if(null!= postalCode){
				applicantDetailsVO.setPostalCode(postalCode.toString());
			}
		} else {
			if (RestCommonUtils.isNotNull(applicantDetails.getPostalCode())) {
				applicantDetailsVO.setPostalCode(applicantDetails.getPostalCode().toString());
			}
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getProvince())) {
			applicantDetailsVO.setState(applicantDetails.getProvince());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getCity())) {
			applicantDetailsVO.setCity(applicantDetails.getCity());
		}

		if (RestCommonUtils.isNotNull(applicantDetails.getHomeOwnershipStatus())) {
			applicantDetailsVO.setHomeOwnershipStatus(applicantDetails.getHomeOwnershipStatus());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getHomeOwnershipDuration())) {
			applicantDetailsVO
			.setHomeOwnershipDuration(new FEBAUnboundInt(applicantDetails.getHomeOwnershipDuration()));
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getHomePhoneNumber())) {
			applicantDetailsVO.setHomePhoneNum(applicantDetails.getHomePhoneNumber().toString());
		}
		if (RestCommonUtils.isNotNull(applicantDetails.getReligion())) {
			applicantDetailsVO.setReligion(applicantDetails.getReligion());
		}

		if (applicantDetails.getIsAddressSameAsKTP() != null) {
			if (applicantDetails.getIsAddressSameAsKTP().equals("Y")
					|| applicantDetails.getIsAddressSameAsKTP().equals("N")) {
				applicantDetailsVO.setIsAddressSameAsKTP(applicantDetails.getIsAddressSameAsKTP());
			}
		}

		final IClientStub customApplicantService = ServiceUtil
				.getService(new FEBAUnboundString("CustomLoanApplicationApplicantDetailsService"));

		if (flag.equals(new FEBAUnboundChar('I'))) {
			customApplicantService.callService(opContext, applicantDetailsVO, new FEBAUnboundString(CREATE));
		} else if (flag.equals(new FEBAUnboundChar('E'))) {
			customApplicantService.callService(opContext, applicantDetailsVO, new FEBAUnboundString(MODIFY));
		}
	}

	protected static void mapContactDetails(CustomContactInputDetailsVO contactDetails,
			CustomLoanApplnContactDetailsVO contactDetailsVO, IOpContext opContext, FEBAUnboundChar flag)
					throws CriticalException, BusinessException, BusinessConfirmation {
		contactDetailsVO.setApplicationId(new ApplicationNumber(contactDetails.getApplicationId()));
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getMotherName())) {
			contactDetailsVO.setMotherName(contactDetails.getMotherName());
		}
		if (RestCommonUtils.isNotNull(contactDetails.getNoOfDependent())) {
			contactDetailsVO.setNoOfDependents(String.valueOf(contactDetails.getNoOfDependent()));
		}
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getMaritalStatus())) {
			contactDetailsVO.setMaritalStatus(contactDetails.getMaritalStatus());
		}
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getSpouseId())) {
			contactDetailsVO.setSpouseId(contactDetails.getSpouseId());
		}		
		//added for partner nik and name -start
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getPartnerNikId())) {
			contactDetailsVO.setPartnerNikId(contactDetails.getPartnerNikId());
		}
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getPartnerName())) {
			contactDetailsVO.setPartnerName(contactDetails.getPartnerName());
		}
		//added for partner nik and name -End
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getContactName())) {
			contactDetailsVO.setEmergencyContact(contactDetails.getContactName());
		}
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getRelationShip())) {
			contactDetailsVO.setEmergencyContactRelation(contactDetails.getRelationShip());
		}
		if (RestCommonUtils.isNotNull(contactDetails.getContactNumber())) {
			contactDetailsVO.setEmergencyContactPhoneNum(contactDetails.getContactNumber().toString());
		}
		if (RestCommonUtils.isNotNullorEmpty(contactDetails.getContactAddress())) {
			contactDetailsVO.setEmergencyContactAddress(contactDetails.getContactAddress());
		}
		if (RestCommonUtils.isNotNull(contactDetails.getContactPostalCode())) {
			contactDetailsVO.setEmergencyContactPostalCode(contactDetails.getContactPostalCode().toString());
		}

		final IClientStub customContactService = ServiceUtil
				.getService(new FEBAUnboundString("CustomLoanApplicationContactDetailsService"));
		if (flag.equals(new FEBAUnboundChar('I'))) {
			customContactService.callService(opContext, contactDetailsVO, new FEBAUnboundString(CREATE));
		} else if (flag.equals(new FEBAUnboundChar('E'))) {
			customContactService.callService(opContext, contactDetailsVO, new FEBAUnboundString(MODIFY));
		}
	}

	protected static void mapEmploymentDetails(CustomEmploymentInputDetailsVO employmentDetails,
			CustomLoanApplnEmploymentDetailsVO employmentDetailsVO, IOpContext opContext, FEBAUnboundChar flag)
					throws CriticalException, BusinessException, BusinessConfirmation {
		employmentDetailsVO.setApplicationId(new ApplicationNumber(employmentDetails.getApplicationId()));
		if (RestCommonUtils.isNotNullorEmpty(employmentDetails.getNpwp())) {
			employmentDetailsVO.setTaxId(employmentDetails.getNpwp());
		}
		if (RestCommonUtils.isNotNull(employmentDetails.getMonthlyIncome())) {
			employmentDetailsVO.getMonthlyIncome()
			.setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			employmentDetailsVO.getMonthlyIncome().setAmountValue(employmentDetails.getMonthlyIncome());
		}
		if (RestCommonUtils.isNotNull(employmentDetails.getMonthlyExpenditure())) {
			employmentDetailsVO.getMonthlyExpense().setAmountValue(employmentDetails.getMonthlyExpenditure());
			employmentDetailsVO.getMonthlyExpense()
			.setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
		}
		if (RestCommonUtils.isNotNullorEmpty(employmentDetails.getFinancialSource())) {
			employmentDetailsVO.setFinancialSource(employmentDetails.getFinancialSource());
		}
		if (RestCommonUtils.isNotNullorEmpty(employmentDetails.getWorkType())) {
			employmentDetailsVO.setWorkType(employmentDetails.getWorkType());
		}
		if (RestCommonUtils.isNotNullorEmpty(employmentDetails.getEmployerName())) {
			employmentDetailsVO.setEmployerName(employmentDetails.getEmployerName());
		}
		if (RestCommonUtils.isNotNullorEmpty(employmentDetails.getEmployerStatus())) {
			employmentDetailsVO.setEmploymentStatus(employmentDetails.getEmployerStatus());
		}
		if (RestCommonUtils.isNotNull(employmentDetails.getEmpStartDate())) {
			employmentDetailsVO.setEmploymentStartDate(new FEBADate(DateUtil
					.stripTimeComponent(new FEBADate(employmentDetails.getEmpStartDate()).getTimestampValue())));
		}
		if (RestCommonUtils.isNotNull(employmentDetails.getEmpEndDate())) {
			employmentDetailsVO.setEmploymentEndDate(new FEBADate(
					DateUtil.stripTimeComponent(new FEBADate(employmentDetails.getEmpEndDate()).getTimestampValue())));
		}

		final IClientStub customEmploymentService = ServiceUtil
				.getService(new FEBAUnboundString("CustomLoanApplicationEmploymentDetailsService"));
		if (flag.equals(new FEBAUnboundChar('I'))) {
			customEmploymentService.callService(opContext, employmentDetailsVO, new FEBAUnboundString(CREATE));
		} else if (flag.equals(new FEBAUnboundChar('E'))) {
			customEmploymentService.callService(opContext, employmentDetailsVO, new FEBAUnboundString(MODIFY));
		}

	}

	/**
	 * This method is used to edit counterparty details based on counterparty id
	 * passed.
	 *
	 * @param reference_Id
	 * @return Response
	 */
	@PUT
	@Path("/{applicationId}")
	@ApiOperation(value = "Edit loan application details", response = CustomLoanApplicationResponseVO.class)
	@MethodInfo(uri = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}", auditFields = {
	"applicationId:applicationId" })
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Application details are modified successfully ", response = CustomLoanApplicationResponseVO.class) })
	public Response editLoanApplicationRequest(
			@ApiParam(value = "user id", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "application id", required = true) @PathParam("applicationId") String applicationId,
			CustomLoanApplicationMasterDetailsVO inputVO) {
		int responseStatus = 201;
		CustomLoanApplicationMasterDetailsVO requestData = new CustomLoanApplicationMasterDetailsVO();
		CustomLoanApplicationRequestVO requestVO = new CustomLoanApplicationRequestVO();
		CustomLoanApplicationCreationOutputVO outputVO = new CustomLoanApplicationCreationOutputVO();
		CustomApplicationStatusVO applnStatusVO = new CustomApplicationStatusVO();
		CustomLoanApplicationCreationResponseVO response = new CustomLoanApplicationCreationResponseVO();
		FEBAUnboundChar flag = null;
		FEBATransactionContext objTxnContext = null;
		
		try {

			isUnAuthenticatedRequest();

			if (requestVO != null) {
				requestData = inputVO;
			}

			IOpContext opContext = (IOpContext) servletrequest.getAttribute(OP_CONTEXT);

			populateHeaderFromHttpHeader(requestVO, opContext);
			String pinangOrSahabat = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", opContext);
			CommonInputRetailHeaderVO headerDetails = requestVO.getHeader();

			CustomLoanApplicationInputDetailsVO applicationDetails = requestData.getApplicationDetails();
			CustomDocumentInputDetailsVO documentDetails = requestData.getDocumentDetails();
			CustomApplicantInputDetailsVO applicantDetails = requestData.getApplicantInformation();
			CustomContactInputDetailsVO contactDetails = requestData.getContactInformation();
			CustomEmploymentInputDetailsVO employmentDetails = requestData.getEmploymentInformation();
			CustomPayrollInputDetailsVO payrollDetails = requestData.getPayrollDetails();
			// added for customer address saving - start
			CustomApplicantKtpAddressDetailsVO ktpAddressDetails = requestData.getKtpAddressDetails();
			// added for customer address saving - end
			
			if (headerDetails != null && headerDetails.getAuthorizationDetails() != null
					&& (RestCommonUtils.isNotNullorEmpty(headerDetails.getAuthorizationDetails().getPrimaryAccessCode())
							|| (RestCommonUtils.isNotNullorEmpty(
									headerDetails.getAuthorizationDetails().getSecondaryAccessCode())))) {
				throw new BusinessException(opContext, EBIncidenceCodes.UNSUPPORTED_AUTHORIZATION_MODE,
						"Transaction password not configured for the request type",
						EBankingErrorCodes.UNSUPPORTED_AUTHORIZATION_MODE);

			} else {
				// calling inquiry service
				CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
						.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
				enquiryVO.getCriteria().setApplicationId(applicationId);
				final IClientStub customLoanInquiryService = ServiceUtil
						.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));

				customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("fetchForInquiry"));
				CustomLoanApplnMasterDetailsVO detailsVO = (CustomLoanApplnMasterDetailsVO) enquiryVO.getResultList()
						.get(0);
				
				// Start logic to check If customer has already LOAN_PAID status then set isSecond Apply Flag value to True.
				// Added for Non Payroll - second apply
				CustomLoanApplicationEnquiryVO enquiryVO1 = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
						.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
				enquiryVO1.getCriteria().setApplicationStatus("LOAN_PAID");
				customLoanInquiryService.callService(opContext, enquiryVO1, new FEBAUnboundString("fetchForInquiry"));
				
				boolean isSecondApply = false;
				if(enquiryVO1.getResultList() !=null && enquiryVO1.getResultList().size() != 0){
					for(int i=0; i<enquiryVO1.getResultList().size(); i++){
						CustomLoanApplnMasterDetailsVO detailsVO1 = (CustomLoanApplnMasterDetailsVO) enquiryVO1.getResultList()
								.get(i);
							if(FEBATypesUtility.isNotNullOrBlank(detailsVO1.getApplicationStatus())){
								System.out.println("isSecondApply::Application Status::"+detailsVO1.getApplicationStatus());
								if(detailsVO1.getApplicationStatus().getValue().equals("LOAN_PAID")){
									System.out.println("isSecondApply::LOAN PAID status::"+detailsVO1.getApplicationStatus());
									isSecondApply = true;
									break;
								}
						}
					}
				}
				// end logic here
				System.out.println("isSecondApply::"+isSecondApply);
				String applicationStatus = detailsVO.getApplicationStatus().toString();
				String newApplicationStatus = null;
				String status = null;
				try {
					status = AppDataManager
							.getValue(opContext, COMMON_CODE_CACHE, "CODE_TYPE=ASVS|CM_CODE=" + applicationStatus)
							.toString();
				} catch (Exception e) {	
					LogManager.logError(null, e);
				}
				
				
				objTxnContext = RestCommonUtils.getTransactionContext(opContext);

				if (documentDetails != null) {
					CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory
							.createInstance(CustomLoanApplnDocumentsDetailsVO.class.getName());
					documentDetails.setApplicationId(Long.valueOf(applicationId));
					//Added indicator for regulating OCR check during loan application
					mapDocumentDetails(servletrequest, documentDetails, docDetailsVO, opContext, isSecondApply);
					if (status != null && Integer.valueOf(status) < 1) {
						newApplicationStatus = KTP_SAVED;
					}
				}
				if (status != null) {
					if (applicantDetails != null && applicationStatus != null) {
						CustomLoanApplnApplicantDetailsVO applicantDetailsVO = (CustomLoanApplnApplicantDetailsVO) FEBAAVOFactory
								.createInstance(CustomLoanApplnApplicantDetailsVO.class.getName());
						applicantDetails.setApplicationId(Long.valueOf(applicationId));
						if (Integer.valueOf(status) < 2) {
							flag = new FEBAUnboundChar('I');
							mapApplicantDetails(applicantDetails, applicantDetailsVO, opContext, flag);
							newApplicationStatus = PER_SAVED;
						} else {
							flag = new FEBAUnboundChar('E');
							mapApplicantDetails(applicantDetails, applicantDetailsVO, opContext, flag);
						}
					}
					
					// Changes for BRI AGRO payroll start
					if (payrollDetails != null && applicationStatus != null) {
						CustomLoanApplnPayrollAccntDetailsVO payrollDetailsVO = (CustomLoanApplnPayrollAccntDetailsVO) FEBAAVOFactory
								.createInstance(CustomLoanApplnPayrollAccntDetailsVO.class.getName());
						payrollDetails.setApplicationId(Long.valueOf(applicationId));
						
						if (Integer.valueOf(status) < 3) {
							flag = new FEBAUnboundChar('I');
							mapLoanPayrollDetails(payrollDetails, payrollDetailsVO, opContext, flag);
							newApplicationStatus = PAY_SAVED;
						} else {
								flag = new FEBAUnboundChar('E');
								mapLoanPayrollDetails(payrollDetails, payrollDetailsVO, opContext , flag);
						}
					}
					// Changes for BRI AGRO payroll end
					
					if (contactDetails != null && applicationStatus != null) {
						CustomLoanApplnContactDetailsVO contactDetailsVO = (CustomLoanApplnContactDetailsVO) FEBAAVOFactory
								.createInstance(CustomLoanApplnContactDetailsVO.class.getName());
						contactDetails.setApplicationId(Long.valueOf(applicationId));
						
						if (Integer.valueOf(status) < 4) {
							flag = new FEBAUnboundChar('I');
							mapContactDetails(contactDetails, contactDetailsVO, opContext, flag);
							newApplicationStatus = CON_SAVED;
						} else {
							flag = new FEBAUnboundChar('E');
							mapContactDetails(contactDetails, contactDetailsVO, opContext, flag);
						}
					}

					if (employmentDetails != null && applicationStatus != null) {
						CustomLoanApplnEmploymentDetailsVO emplDetailsVO = (CustomLoanApplnEmploymentDetailsVO) FEBAAVOFactory
								.createInstance(CustomLoanApplnEmploymentDetailsVO.class.getName());
						employmentDetails.setApplicationId(Long.valueOf(applicationId));
						if (Integer.valueOf(status) < 4 && pinangOrSahabat.equals(CustomEBConstants.SAHABAT)) {
							flag = new FEBAUnboundChar('I');
							mapEmploymentDetails(employmentDetails, emplDetailsVO, opContext, flag);
							newApplicationStatus = "EMP_SAVED";
						}
						if (Integer.valueOf(status) < 2 && pinangOrSahabat.equals(CustomEBConstants.PINANG)) {
							flag = new FEBAUnboundChar('I');
							mapEmploymentDetails(employmentDetails, emplDetailsVO, opContext, flag);
						} else {
							flag = new FEBAUnboundChar('E');
							mapEmploymentDetails(employmentDetails, emplDetailsVO, opContext, flag);
						}
					}
					
					// added for customer address saving - start
					if(ktpAddressDetails !=null && applicationStatus != null){
						CustomApplicantKtpDetailsVO applicantKtpDetailsVO = (CustomApplicantKtpDetailsVO) FEBAAVOFactory
								.createInstance(CustomApplicantKtpDetailsVO.class.getName());
						ktpAddressDetails.setApplicationId(Long.valueOf(applicationId));
						ktpAddressDetails.setBankCode(detailsVO.getBankCode().getValue());
						System.out.println("Integer.valueOf(status):::"+Integer.valueOf(status));

						if (Integer.valueOf(status) < 2 && pinangOrSahabat.equals(CustomEBConstants.PINANG)) {
							flag = new FEBAUnboundChar('I');
							mapKtpAddressDetails(ktpAddressDetails, applicantKtpDetailsVO, opContext, flag);
						} else {
							flag = new FEBAUnboundChar('E');
							mapKtpAddressDetails(ktpAddressDetails, applicantKtpDetailsVO, opContext, flag);
						}
					}	
					// added for customer address saving - end

				}
				CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());

				if (applicationDetails != null && applicationDetails.getNewStatus() != null
						&& applicationDetails.getNewStatus().equals("USR_REJECT")) {
					if (applicationStatus.equalsIgnoreCase(CR_SCORE_APR)) {
						newApplicationStatus = applicationDetails.getNewStatus();
					}
				}

				if (applicationDetails != null && applicationDetails.getNewStatus() != null
						&& applicationDetails.getNewStatus().equals("DISB_ACC_CONF")) {
					if (pinangOrSahabat.equals(CustomEBConstants.PINANG)
							&& applicationStatus.equalsIgnoreCase(CR_SCORE_APR)) {
						newApplicationStatus = applicationDetails.getNewStatus();
					} else if (pinangOrSahabat.equals(CustomEBConstants.SAHABAT)
							&& applicationDetails.getNewStatus() != null
							&& applicationStatus.equalsIgnoreCase("ACT_LINK_CONF")) {
						newApplicationStatus = applicationDetails.getNewStatus();
					}
				}

				
				String remarks = "";
				
				if (applicationDetails != null && applicationDetails.getIsSubmitted() != null) {
					if (applicationDetails.getIsSubmitted().equals(new Flag("Y"))) {
						CustomCreditScoreCalculateVO creditScoreCalculateVO = (CustomCreditScoreCalculateVO) FEBAAVOFactory
								.createInstance(CustomCreditScoreCalculateVO.class.getName());
						creditScoreCalculateVO.setApplicationId(new ApplicationNumber(applicationId));
						creditScoreCalculateVO.setUserId(userId);
						final IClientStub customCreditScoreService = ServiceUtil
								.getService(new FEBAUnboundString("CustomCreditScoreService"));
						customCreditScoreService.callService(opContext, creditScoreCalculateVO,
								new FEBAUnboundString("calculate"));
																		
						loanApplnDetailsVO.setUid(creditScoreCalculateVO.getUid());
						loanApplnDetailsVO.setStatusDesc(creditScoreCalculateVO.getDesc());
						loanApplnDetailsVO.setApplicationStatus(creditScoreCalculateVO.getApplicationStatus());
												
						// Added for BRI Agro payroll
						if(creditScoreCalculateVO.getApplicationStatus().getValue().equals(CR_SCORE_SUB)){
							newApplicationStatus = CR_SCORE_SUB;
						}else if(creditScoreCalculateVO.getApplicationStatus().getValue().equals(PAYROLL_REJ)){
							newApplicationStatus = PAYROLL_REJ;
						}
						
					}
				}

				if (applicationStatus.equalsIgnoreCase("LOAN_CREATED")) {
					if (applicationDetails.getFeedback() != null) {
						loanApplnDetailsVO.setFeedback(applicationDetails.getFeedback());
					}
					if (applicationDetails.getRemarks() != null) {
						loanApplnDetailsVO.setRemarks(applicationDetails.getRemarks());
					}
				}
				
				System.out.println("newApplicationStatus::"+newApplicationStatus);
				
				if (newApplicationStatus != null) {
					loanApplnDetailsVO.setApplicationStatus(newApplicationStatus);
					applicationStatus = newApplicationStatus;
					editLoanApplicationDetails(applicationDetails, loanApplnDetailsVO, opContext, applicationId);
					
										
					if(loanApplnDetailsVO.getApplicationStatus().getValue().equals(PAYROLL_REJ)){
						remarks = loanApplnDetailsVO.getStatusDesc().getValue();
					}
					
					new CustomLoanHistoryUpdateUtil().updateHistory(new ApplicationNumber(applicationId),
							applicationStatus, remarks, objTxnContext);
				} else if (applicationDetails != null) {
					// call edit impl for application details
					editLoanApplicationDetails(applicationDetails, loanApplnDetailsVO, opContext, applicationId);
				}

				CLATInfo clatinfo = CLATTAO.select(objTxnContext,
						new BankId(opContext.getFromContextData(BANK_ID).toString()),
						new ApplicationNumber(applicationId));

				outputVO.setApplicationId(Long.valueOf(applicationId));
				outputVO.setLastActionDate(clatinfo.getCookie().getRModTime().getValue());
				applnStatusVO.setCodeType("ASTG");
				if (null != applicationStatus) {
					applnStatusVO.setCmCode(applicationStatus);
					String statusDesc = AppDataManager
							.getValue(opContext, COMMON_CODE_CACHE, CODE_TYPE_CM_CODE + applicationStatus).toString();
					applnStatusVO.setCodeDescription(statusDesc);
				}
				outputVO.setApplicationStatus(applnStatusVO);

				if (applicationStatus.equals(CR_SCORE_SUB)) {
					Date currentTime = new java.util.Date(System.currentTimeMillis());
					Date rModTime = clatinfo.getCookie().getRModTime().getValue();
					long diff = currentTime.getTime() - rModTime.getTime();

					long days = TimeUnit.MILLISECONDS.toDays(diff);
					long remainingHoursInMillis = diff - TimeUnit.DAYS.toMillis(days);
					long hours = TimeUnit.MILLISECONDS.toHours(remainingHoursInMillis);
					long remainingMinutesInMillis = remainingHoursInMillis - TimeUnit.HOURS.toMillis(hours);
					long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingMinutesInMillis);

					long timeLeftForScoreCompletion = 0;

					String creditScoreCalTimeInterval = PropertyUtil.getProperty("CREDIT_SCR_CAL_TIME_INTRVL",
							opContext);
					timeLeftForScoreCompletion = Long.valueOf(creditScoreCalTimeInterval) - minutes;
					outputVO.setTimeLeftForScoreCompletion(timeLeftForScoreCompletion);

				}
					// Added for BRI AGRO payroll.			
					if (applicationStatus.equals(PAYROLL_REJ)) {
						
						throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.PAYROLL_REJECTION,
								"Verified payroll data is not found.", null, CustomEBankingErrorCodes.PAYROLL_REJECTION, null);
					}
				
				response.setData(outputVO);
			}

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(servletrequest, httpResponse, e, restResponseErrorCodeMapping());
		} finally {
			if (objTxnContext != null) {
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}

		RestResourceManager.updateHeaderFooterResponseData(response, servletrequest, httpResponse, 0);

		if (responseStatus == 401) {
			return Response.status(responseStatus).entity(response).build();
		} else {
			return Response.accepted(null).entity(response).build();
		}

	}

	protected static void editLoanApplicationDetails(CustomLoanApplicationInputDetailsVO applicationDetails,
			CustomLoanApplnMasterDetailsVO loanApplnDetailsVO, IOpContext opContext, String applicationId)
					throws CriticalException, BusinessException, BusinessConfirmation {
		if (applicationDetails != null) {

			if (RestCommonUtils.isNotNullorEmpty(applicationDetails.getProductType())) {
				loanApplnDetailsVO.setProductType(applicationDetails.getProductType());
			}
			if (RestCommonUtils.isNotNullorEmpty(applicationDetails.getProductCode())) {
				loanApplnDetailsVO.setProductCode(applicationDetails.getProductCode());
			}
			if (RestCommonUtils.isNotNullorEmpty(applicationDetails.getProductCategory())) {
				loanApplnDetailsVO.setProductCategory(applicationDetails.getProductCategory());
			}
			if (RestCommonUtils.isNotNullorEmpty(applicationDetails.getProductSubCategory())) {
				loanApplnDetailsVO.setProductSubCategory(applicationDetails.getProductSubCategory());
			}
			if (RestCommonUtils.isNotNull(applicationDetails.getRequestedLoanAmount())) {
				loanApplnDetailsVO.getRequestedLoanAmt().setAmountValue(applicationDetails.getRequestedLoanAmount());
				loanApplnDetailsVO.getRequestedLoanAmt()
				.setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}

			if (RestCommonUtils.isNotNull(applicationDetails.getRequestedTenor())) {
				loanApplnDetailsVO.setRequestedTenor(new FEBAUnboundInt(applicationDetails.getRequestedTenor()));
			}
			if (RestCommonUtils.isNotNull(applicationDetails.getInterestRate())) {
				loanApplnDetailsVO.setLoanIntRate(new InterestRate(applicationDetails.getInterestRate()));
			}

			if (RestCommonUtils.isNotNull(applicationDetails.getMonthlyInstallment())) {
				loanApplnDetailsVO.getMonthlyInstallment().setAmountValue(applicationDetails.getMonthlyInstallment());
				loanApplnDetailsVO.getMonthlyInstallment()
				.setCurrencyCodeValue(PropertyUtil.getProperty(HOME_CUR_CODE, opContext));
			}
			if (RestCommonUtils.isNotNullorEmpty(applicationDetails.getLegacyCif())) {
				loanApplnDetailsVO.setLegacyCif(applicationDetails.getLegacyCif());
			}
			
			/* Commented for Production Issue. As requested by Bank.
			if (RestCommonUtils.isNotNullorEmpty(applicationDetails.getSchemeCode())) {
				loanApplnDetailsVO.setSchemeCode(applicationDetails.getSchemeCode());
			}else{
				loanApplnDetailsVO.setSchemeCode("PNANG");  
			}
			*/

		}

		loanApplnDetailsVO.setApplicationId(new ApplicationNumber(applicationId));
		final IClientStub customLoanApplicationService = ServiceUtil
				.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));

		customLoanApplicationService.callService(opContext, loanApplnDetailsVO, new FEBAUnboundString(MODIFY));
		// mapping main application details:: end

	}

	protected static void updateApplicationStatus(IOpContext opContext, String applicationId, String applicationStatus)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
		loanApplnDetailsVO.setApplicationId(new ApplicationNumber(applicationId));
		loanApplnDetailsVO.setIsKTPVerified("N");
		final IClientStub customLoanApplicationService = ServiceUtil
				.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
		loanApplnDetailsVO.setApplicationStatus(new CommonCode(applicationStatus));
		customLoanApplicationService.callService(opContext, loanApplnDetailsVO, new FEBAUnboundString(MODIFY));
		// mapping main application details:: end

	}
	protected static void updateApplicationStatus(IOpContext opContext, String applicationId, String applicationStatus,String statusDesc)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
		loanApplnDetailsVO.setApplicationId(new ApplicationNumber(applicationId));
		loanApplnDetailsVO.setIsKTPVerified("N");
		loanApplnDetailsVO.setStatusDesc(statusDesc);
		final IClientStub customLoanApplicationService = ServiceUtil
				.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
		loanApplnDetailsVO.setApplicationStatus(new CommonCode(applicationStatus));
		customLoanApplicationService.callService(opContext, loanApplnDetailsVO, new FEBAUnboundString(MODIFY));
		// mapping main application details:: end

	}

	@GET
	@Path("/{applicationId}")
	@MethodInfo(uri = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}", auditFields = {
	"applicationId: Application Id" })
	@ApiOperation(value = "Fetch the Application details", response = CustomLoanApplicationResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successful Retrieval", response = CustomLoanApplicationResponseVO.class) })
	public Response fetchDetails(
			@ApiParam(value = "Application Id input", required = true) @PathParam("applicationId") String applicationId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId) {
		CustomLoanApplicationResponseVO responseVO = new CustomLoanApplicationResponseVO();
		CustomLoanApplicationMasterDetailsVO outputVO = new CustomLoanApplicationMasterDetailsVO();
		CustomCommonCodeDetailsVO applnStatusVO = new CustomCommonCodeDetailsVO();
		CustomLoanApplicationInputDetailsVO applicationDetails = new CustomLoanApplicationInputDetailsVO();
		CustomDocumentInputDetailsVO documentDetails = new CustomDocumentInputDetailsVO();
		CustomPayrollInputDetailsVO payrollDetails = new CustomPayrollInputDetailsVO();
		CustomResponsePayrollDetailsVO respPayrollDetails = new CustomResponsePayrollDetailsVO();
		CustomApplicantDetailsVO applicantDetails = new CustomApplicantDetailsVO();
		CustomContactInputDetailsVO contactDetails = new CustomContactInputDetailsVO();
		CustomEmploymentDetailsVO employmentDetails = new CustomEmploymentDetailsVO();
		try {
			isUnAuthenticatedRequest();
			IOpContext opContext = (IOpContext) servletrequest.getAttribute(OP_CONTEXT);
			
			// Changes for PENTEST Findings by bank - start
			CustomLoanAppIdEnquiryVO enquiryVO = (CustomLoanAppIdEnquiryVO) FEBAAVOFactory
					.createInstance(CustomLoanAppIdEnquiryVO.class.getName());
			enquiryVO.getCriteria().setApplicationId(new ApplicationNumber(applicationId));
			enquiryVO.getCriteria().setUserId(userId);
			final IClientStub customAppService = ServiceUtil
					.getService(new FEBAUnboundString(LOAN_APPLICATION_ID_SERVICE));
			customAppService.callService(opContext, enquiryVO, new FEBAUnboundString("validateAppId"));
			
			if(enquiryVO.getDetails().getIsLoanAppIdMatch().getValue() == 'N'){
				throw new BusinessException(opContext, "LOANAPPLN001", "The application Id is not available for the user in loan master table.", EBankingErrorCodes.NO_RECORDS_FOUND);
			}

			// Changes for PENTEST Findings by bank - end
			
			CustomLoanApplicationDetailsVO detailsVO = (CustomLoanApplicationDetailsVO) FEBAAVOFactory
					.createInstance(CustomLoanApplicationDetailsVO.class.getName());
			detailsVO.getLoanApplnMasterDetails().setApplicationId(new ApplicationNumber(applicationId));
			final IClientStub customApplicantService = ServiceUtil
					.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
			customApplicantService.callService(opContext, detailsVO, new FEBAUnboundString("fetchLoanDetails"));
			if (detailsVO.getLoanApplnMasterDetails() != null) {
				getLoanApplicationDetails(applicationDetails, detailsVO.getLoanApplnMasterDetails());
				applicationDetails.setLastActionDate(detailsVO.getLoanApplnMasterDetails().getRModTime().getValue());
				applnStatusVO.setCodeType("ASTG");
				applnStatusVO.setCmCode(detailsVO.getLoanApplnMasterDetails().getApplicationStatus().toString());
				String statusDesc = AppDataManager
						.getValue(opContext, COMMON_CODE_CACHE,
								CODE_TYPE_CM_CODE + detailsVO.getLoanApplnMasterDetails().getApplicationStatus())
						.toString();
				applnStatusVO.setCodeDescription(statusDesc);
				applicationDetails.setApplicationStatus(applnStatusVO);
				outputVO.setApplicationDetails(applicationDetails);
			}
			
			if (RestCommonUtils.isNotNull(detailsVO.getLoanDocumentDetails())) {
				if (!detailsVO.getLoanDocumentDetails().getFileSeqNo().getValue().equalsIgnoreCase("")) {
					getDocumentDetails(opContext, documentDetails, detailsVO.getLoanDocumentDetails());
					outputVO.setDocumentDetails(documentDetails);
				}
			}
			
			System.out.println("Fetch Details :: detailsVO.getIsPayrollDetNull():: "+detailsVO.getIsPayrollDetNull());
			// Added for AGRO Payroll
			if (!detailsVO.getIsPayrollDetNull().toString().equals("Y")) {
				getPayrollDetails(payrollDetails, detailsVO.getPayrollAccntDetails());
				outputVO.setPayrollDetails(payrollDetails);
			}else{
				
				getInputPayrollDetails(respPayrollDetails,  applicationId, opContext);
				outputVO.setRespPayrollDetails(respPayrollDetails);
			}
			
			if (!detailsVO.getIsApplicantDetNull().toString().equals("Y")) {
				getApplicantDetails(opContext, applicantDetails, detailsVO.getLoanApplicantDetails());
				outputVO.setApplicantDetails(applicantDetails);
			}
			if (!detailsVO.getIsContactDetNull().toString().equals("Y")) {
				getContactDetails(opContext, contactDetails, detailsVO.getLoanApplicantContactDetails());
				outputVO.setContactInformation(contactDetails);
			}
			if (!detailsVO.getIsEmploymentDetNull().toString().equals("Y")) {
				getEmploymentDetails(opContext, employmentDetails, detailsVO.getLoanApplicantEmploymentDetails());
				outputVO.setEmploymentDetails(employmentDetails);
			}

			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, servletrequest, 0);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logError(null, e);
			RestResourceManager.handleFatalErrorOutput(servletrequest, response, e, restResponseErrorCodeMapping());
		}

		return Response.ok().entity(outputVO).build();
	}

	protected static void getLoanApplicationDetails(CustomLoanApplicationInputDetailsVO applicationDetails,
			CustomLoanApplnMasterDetailsVO loanApplnDetailsVO) {
		applicationDetails.setProductType(loanApplnDetailsVO.getProductType().toString());
		applicationDetails.setProductCode(loanApplnDetailsVO.getProductCode().toString());
		applicationDetails.setProductCategory(loanApplnDetailsVO.getProductCategory().toString());
		applicationDetails.setProductSubCategory(loanApplnDetailsVO.getProductSubCategory().toString());
		applicationDetails.setRequestedLoanAmount(loanApplnDetailsVO.getRequestedLoanAmt().getAmountValue());
		applicationDetails.setRequestedTenor(loanApplnDetailsVO.getRequestedTenor().getValue());
		applicationDetails.setInterestRate(loanApplnDetailsVO.getLoanIntRate().getValue());

		applicationDetails.setMonthlyInstallment(loanApplnDetailsVO.getMonthlyInstallment().getAmountValue());

		applicationDetails.setLegacyCif(loanApplnDetailsVO.getLegacyCif().toString());
		

		if (FEBATypesUtility.isNotNullOrBlank(loanApplnDetailsVO.getBankCode())) {
			applicationDetails.setBankCode(loanApplnDetailsVO.getBankCode().getValue());
		}
		// added for PINANG pay later start
		if (FEBATypesUtility.isNotNullOrBlank(loanApplnDetailsVO.getPartnerId())) {
			applicationDetails.setPartnerId(loanApplnDetailsVO.getPartnerId().getValue());
		}
		if (FEBATypesUtility.isNotNullOrBlank(loanApplnDetailsVO.getSchemeCode())) {
			applicationDetails.setSchemeCode(loanApplnDetailsVO.getSchemeCode().getValue());
		}
		// added for PINANG pay later end
	}

	protected static void getPayrollDetails(CustomPayrollInputDetailsVO payrollDetails,
			CustomLoanApplnPayrollAccntDetailsVO payrollDetailsVO) {

		payrollDetails.setPayrollAccount(payrollDetailsVO.getPayrollAccountId().toString());
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getPayrollAccountBranch())) {
			payrollDetails.setPayrollAccountBranch(payrollDetailsVO.getPayrollAccountBranch().toString());
		}
		payrollDetails.setAgentCode(payrollDetailsVO.getAgentCode().toString());
		payrollDetails.setPayrollDate(payrollDetailsVO.getAccountPayrollDate().getValue());
		payrollDetails.setReferralCode(payrollDetailsVO.getRefferalCode().toString());
		payrollDetails.setSavingAmount(payrollDetailsVO.getSavingAmount().getAmountValue());

		if (FEBATypesUtility.isNotBlankInt(payrollDetailsVO.getCreFre3())) {
			payrollDetails.setCreditFre3(payrollDetailsVO.getCreFre3().getValue());
		}

		if (FEBATypesUtility.isNotBlankInt(payrollDetailsVO.getCreFre6())) {
			payrollDetails.setCreditFre6(payrollDetailsVO.getCreFre6().getValue());
		}
		if (FEBATypesUtility.isNotBlankInt(payrollDetailsVO.getCreFre12())) {
			payrollDetails.setCreditFre12(payrollDetailsVO.getCreFre12().getValue());
		}
		if (FEBATypesUtility.isNotBlankInt(payrollDetailsVO.getDebFre3())) {
			payrollDetails.setDebitFre3(payrollDetailsVO.getDebFre3().getValue());
		}
		if (FEBATypesUtility.isNotBlankInt(payrollDetailsVO.getDebFre6())) {
			payrollDetails.setDebitFre6(payrollDetailsVO.getDebFre6().getValue());
		}
		if (FEBATypesUtility.isNotBlankInt(payrollDetailsVO.getDebFre12())) {
			payrollDetails.setDebitFre12(payrollDetailsVO.getDebFre12().getValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getCreTot3().getAmount())) {
			payrollDetails.setCreditTotal3(payrollDetailsVO.getCreTot3().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getCreTot6().getAmount())) {
			payrollDetails.setCreditTotal6(payrollDetailsVO.getCreTot6().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getCreTot12().getAmount())) {
			payrollDetails.setCreditTotal12(payrollDetailsVO.getCreTot12().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getDebTot3().getAmount())) {
			payrollDetails.setDebitTotal3(payrollDetailsVO.getDebTot3().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getDebTot6().getAmount())) {
			payrollDetails.setDebitTotal6(payrollDetailsVO.getDebTot6().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getDebTot12().getAmount())) {
			payrollDetails.setDebitTotal12(payrollDetailsVO.getDebTot12().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getCreAvg3().getAmount())) {
			payrollDetails.setCreditAvg3(payrollDetailsVO.getCreAvg3().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getCreAvg6().getAmount())) {
			payrollDetails.setCreditAvg6(payrollDetailsVO.getCreAvg6().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getCreAvg12().getAmount())) {
			payrollDetails.setCreditAvg12(payrollDetailsVO.getCreAvg12().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getDebAvg3().getAmount())) {
			payrollDetails.setDebitAvg3(payrollDetailsVO.getDebAvg3().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getDebAvg6().getAmount())) {
			payrollDetails.setDebitAvg6(payrollDetailsVO.getDebAvg6().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getDebAvg12().getAmount())) {
			payrollDetails.setDebitAvg12(payrollDetailsVO.getDebAvg12().getAmountValue());
		}

		if (FEBATypesUtility.isNotBlankChar(payrollDetailsVO.getNetBanking())) {
			payrollDetails.setNetBanking(payrollDetailsVO.getNetBanking().getValue());
		}

		if (FEBATypesUtility.isNotBlankChar(payrollDetailsVO.getMobileBanking())) {
			payrollDetails.setMobileBanking(payrollDetailsVO.getMobileBanking().getValue());
		}

		if (FEBATypesUtility.isNotBlankChar(payrollDetailsVO.getPrimaryPhone())) {
			payrollDetails.setPrimaryPhone(payrollDetailsVO.getPrimaryPhone().getValue());
		}

		if (FEBATypesUtility.isNotBlankChar(payrollDetailsVO.getSecondaryPhone())) {
			payrollDetails.setSecPhone(payrollDetailsVO.getSecondaryPhone().getValue());
		}

		if (FEBATypesUtility.isNotBlankChar(payrollDetailsVO.getSavingAccountOwnershipStatus())) {
			payrollDetails.setSavingAccOwnshpSts(payrollDetailsVO.getSavingAccountOwnershipStatus().getValue());
		}

		if (FEBATypesUtility.isNotBlankDate(payrollDetailsVO.getSavingAccountOwnershipDate())) {
			payrollDetails.setSavingAccOwnshpDate(payrollDetailsVO.getSavingAccountOwnershipDate().getValue());
		}

		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getCompanyName())) {
			payrollDetails.setCompanyName(payrollDetailsVO.getCompanyName().toString());
		}

		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getPersonelName())) {
			payrollDetails.setPersonelName(payrollDetailsVO.getPersonelName().toString());
		}

		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getPersonelNumber())) {
			payrollDetails.setPersonelNumber(Long.valueOf(payrollDetailsVO.getPersonelNumber().toString()));
		}

		if (String.valueOf(payrollDetails.getPersonelNumber()) != null) {
			payrollDetailsVO.setPersonelNumber(Long.toString(payrollDetails.getPersonelNumber()));
		}

		/*
		 * Added Fields for WL data
		 */
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getNameWL())) {
			payrollDetails.setNameWL(payrollDetailsVO.getNameWL().toString());
		}
		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getNetIncomeWL().getAmount())) {
			payrollDetails.setNetIncomeWL(payrollDetailsVO.getNetIncomeWL().getAmountValue());
		}
		if (FEBATypesUtility.isNotBlankDate(payrollDetailsVO.getBirthDateWL())) {
			payrollDetails.setBirthDateWL(payrollDetailsVO.getBirthDateWL().getValue());
		}
		if (FEBATypesUtility.isNotBlankDate(payrollDetailsVO.getEmpEndDateWL())) {
			payrollDetails.setEmpEndDateWL(payrollDetailsVO.getEmpEndDateWL().getValue());
		}
		if (FEBATypesUtility.isNotBlankDate(payrollDetailsVO.getEmpStartDateWL())) {
			payrollDetails.setEmpStartDateWL(payrollDetailsVO.getEmpStartDateWL().getValue());
		}
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getGenderWL())) {
			payrollDetails.setGenderWL(payrollDetailsVO.getGenderWL().getValue());
		}
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getAddressWL())) {
			payrollDetails.setAddressWL(payrollDetailsVO.getAddressWL().getValue());
		}
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getPlaceOfBirthWL())) {
			payrollDetails.setPlaceOfBirthWL(payrollDetailsVO.getPlaceOfBirthWL().getValue());
		}
		// Added for BRI Agro payroll
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getEmployeeStatus())) {
			payrollDetails.setEmployeeStatus(payrollDetailsVO.getEmployeeStatus().getValue());
		}
		if (FEBATypesUtility.isNotBlankDouble(payrollDetailsVO.getExpenditureAmount().getAmount())) {
			payrollDetails.setExpenditureAmount(payrollDetailsVO.getExpenditureAmount().getAmountValue());
		}	
		//non payroll NPWP addition--Start		
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getNPWP())) {
			payrollDetails.setNPWP(payrollDetailsVO.getNPWP().toString());
		}
		//non payroll NPWP addition--End
		//non payroll company Address addition--Start		
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getCompanyAddress())) {
			payrollDetails.setCompanyAddress(payrollDetailsVO.getCompanyAddress().toString());
		}
		//non payroll company Address addition--End
		// Added for PINANG WEBVIEW start
		if (FEBATypesUtility.isNotNullOrBlank(payrollDetailsVO.getWorkAddress())) {
			payrollDetails.setWorkAddress(payrollDetailsVO.getWorkAddress().getValue());
		}
		// Added for PINANG WEBVIEW end
	}
		

	
	protected static void getInputPayrollDetails(CustomResponsePayrollDetailsVO payrollDetails, String applicationId, IOpContext opContext) throws CriticalException {
		
		CPITInfo cpitInfo = null;
		FEBATransactionContext objTxnContext = null;
		
		objTxnContext = RestCommonUtils.getTransactionContext(opContext);
		
		try {

			cpitInfo = CPITTAO.select(objTxnContext, objTxnContext.getBankId(),new ApplicationNumber(applicationId));
			
			System.out.println("Fetch Details ::getInputPayrollDetails:: cpitInfo:: "+cpitInfo);

			if (cpitInfo != null) {
					
				payrollDetails.setPayrollAccount(cpitInfo.getPayrollAccountNum().toString());
				if (FEBATypesUtility.isNotNullOrBlank(cpitInfo.getPayrollAccBranch())) {
					payrollDetails.setPayrollAccountBranch(cpitInfo.getPayrollAccBranch().toString());
				}
				payrollDetails.setSavingAmount(cpitInfo.getSavingAmount().getAmountValue());

				if (FEBATypesUtility.isNotNullOrBlank(cpitInfo.getNameWl())) {
					payrollDetails.setNameWL(cpitInfo.getNameWl().toString());
				}
				if (FEBATypesUtility.isNotBlankDate(cpitInfo.getBirthDateWl())) {
					payrollDetails.setBirthDateWL(cpitInfo.getBirthDateWl().getValue());
				}
				if (FEBATypesUtility.isNotNullOrBlank(cpitInfo.getGenderWl())) {
					payrollDetails.setGenderWL(cpitInfo.getGenderWl().getValue());
				}
				if (FEBATypesUtility.isNotNullOrBlank(cpitInfo.getAddressWl())) {
					payrollDetails.setAddressWL(cpitInfo.getAddressWl().getValue());
				}
				if (FEBATypesUtility.isNotNullOrBlank(cpitInfo.getPlaceOfBirthWl())) {
					payrollDetails.setPlaceOfBirthWL(cpitInfo.getPlaceOfBirthWl().getValue());
				}
				
				System.out.println("Fetch Details ::getInputPayrollDetails:: payrollDetails:: "+payrollDetails);
				
			}

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
			LogManager.logError(null, e);
		}finally{
			if(objTxnContext != null){
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	protected static void getDocumentDetails(IOpContext opContext, CustomDocumentInputDetailsVO documentDetails,
			CustomLoanApplnDocumentsDetailsVO documentDetailsVO) throws CriticalException, FEBATableOperatorException {

		FEBATransactionContext objTxnContext = null;
		documentDetails.setDocumentCode(documentDetailsVO.getDocCode().toString());
		documentDetails.setDocumentType(documentDetailsVO.getDocType().toString());
		documentDetails.setDocKey(documentDetailsVO.getDocKey().toString());
		documentDetails.setFileSeqNo(documentDetailsVO.getFileSeqNo().toString());
		documentDetails.setFileUploadPath(documentDetailsVO.getDocStorePath().toString());

		FEBAArrayList list = null;
		StringBuilder file = new StringBuilder();
		try {
			objTxnContext = RestCommonUtils.getTransactionContext(opContext);
			FDTTInfo fdttInfo = FDTTTAO.select(objTxnContext, documentDetailsVO.getFileSeqNo(),
					objTxnContext.getBankId());
			documentDetails.setFileName(fdttInfo.getFileName().toString());

			list = fetchFileChunksFromTable((FEBATransactionContext) objTxnContext, objTxnContext.getBankId(),
					documentDetailsVO.getFileSeqNo());
			int size = list.size();
			for (int i = 0; i < size; i++) {

				FileDownloadDetailsVO rowData = (FileDownloadDetailsVO) list.get(i);

				FEBABinary fetchedData = rowData.getDownloadedFile();

				file = file.append(new String(fetchedData.getValue()));

			}
		} catch (BusinessException e1) {
			LogManager.logError(null, e1);
		} finally {
			if (objTxnContext != null) {
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}
		documentDetails.setFile(file.toString());

	}

	private static FEBAArrayList fetchFileChunksFromTable(FEBATransactionContext context, BankId bankId,
			FileSequenceNumber sequenceNumber) throws BusinessException {

		String sQueryIdentifier = "FileDownload";

		QueryOperator queryOperator = QueryOperator.openHandle((FEBATransactionContext) context, sQueryIdentifier);

		queryOperator.associate("FILE_SEQ_NO", sequenceNumber);

		queryOperator.associate(BANK_ID, bankId);

		try {

			return queryOperator.fetchList((FEBATransactionContext) context);

		}

		catch (DALException e) {

			throw new BusinessException(context, FEBAIncidenceCodes.FU_FUCT_FETCH_FAILED,
					ErrorCodes.FU_FUCT_FETCH_FAILED, e);

		}

	}

	protected static void getApplicantDetails(IOpContext opContext, CustomApplicantDetailsVO applicantDetails,
			CustomLoanApplnApplicantDetailsVO applicantDetailsVO) throws CriticalException {
		CustomCommonCodeDetailsVO genderVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO lastEducationVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO creditCardIssuerVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO addrLine4VO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO addrLine5VO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO cityVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO provinceVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO homeOwnershipStatusVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO homeOwnershipDurationVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO religionVO = new CustomCommonCodeDetailsVO();
		try {
			applicantDetails.setEmailId(applicantDetailsVO.getEmailId().toString());
			applicantDetails.setName(applicantDetailsVO.getName().toString());
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getMobileNum())) {
				applicantDetails.setMobileNo("0"+applicantDetailsVO.getMobileNum().toString());
			}
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getGender())) {
				genderVO.setCodeType("GEN");
				genderVO.setCmCode(applicantDetailsVO.getGender().toString());
				String genderDesc = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
						"CODE_TYPE=GEN|CM_CODE=" + applicantDetailsVO.getGender()).toString();
				genderVO.setCodeDescription(genderDesc);
				applicantDetails.setGender(genderVO);
			}
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getPlaceOfBirth())) {
				applicantDetails.setPlaceOfBirth(applicantDetailsVO.getPlaceOfBirth().toString());
			}

			if (RestCommonUtils.isNotNull(applicantDetailsVO.getDateOfBirth())) {
				applicantDetails.setDateOfBirth(applicantDetailsVO.getDateOfBirth().getValue());
			}

			if (RestCommonUtils.isNotNull(applicantDetailsVO.getDateOfBirth())) {
				applicantDetails.setNationalId(applicantDetailsVO.getNationalId().toString());
			}

			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getLastEducation())) {
				lastEducationVO.setCodeType("EDU");
				lastEducationVO.setCmCode(applicantDetailsVO.getLastEducation().toString());
				String lastEducationDesc = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
						"CODE_TYPE=EDU|CM_CODE=" + applicantDetailsVO.getLastEducation()).toString();
				lastEducationVO.setCodeDescription(lastEducationDesc);
				applicantDetails.setLastEducation(lastEducationVO);
			}
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getCardIssuerBank())) {
				creditCardIssuerVO.setCodeType("CCBK");
				creditCardIssuerVO.setCmCode(applicantDetailsVO.getCardIssuerBank().toString());
				String creditCardIssuerDesc = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
						"CODE_TYPE=CCBK|CM_CODE=" + applicantDetailsVO.getCardIssuerBank()).toString();
				creditCardIssuerVO.setCodeDescription(creditCardIssuerDesc);
				applicantDetails.setCreditCardIssuer(creditCardIssuerVO);
			}
			applicantDetails.setAddressLine1(applicantDetailsVO.getAddressLine1().toString());
			applicantDetails.setAddressLine2(applicantDetailsVO.getAddressLine2().toString());
			applicantDetails.setAddressLine3(applicantDetailsVO.getAddressLine3().toString());

			FEBATransactionContext objTxnContext = RestCommonUtils.getTransactionContext(opContext);
			FEBAStringBuilder stateDesc = new FEBAStringBuilder("");
			FEBAArrayList filteredStcdList = new FEBAArrayList();
			StateCodesFilter stcdFilter = new StateCodesFilter();
			StateCodeCacheVO stateCodeCacheVO = (StateCodeCacheVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.StateCodeCacheVO);
			stateCodeCacheVO.getCriteria().setStateCode(applicantDetailsVO.getState().toString());
			stateCodeCacheVO.getCriteria().setCountry("ID");
			stateCodeCacheVO.getCriteria().setLanguageID(objTxnContext.getLangId());

			try {
				filteredStcdList = stcdFilter.getFilteredStcdList(objTxnContext, stateCodeCacheVO);
			} catch (CriticalException e) {
				LogManager.logError(null, e);
			} finally {
				if (objTxnContext != null) {
					try {
						objTxnContext.cleanup();
					} catch (CriticalException e) {
						e.printStackTrace();
					}
				}
			}

			if (filteredStcdList.get(0) != null) {
				stateDesc.append(((StateCodeVO) filteredStcdList.get(0)).getStateDescription().getValue());
			}

			provinceVO.setCodeType("ID");
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getState())) {
				provinceVO.setCmCode(applicantDetailsVO.getState().toString());
				provinceVO.setCodeDescription(stateDesc.toString());
				applicantDetails.setProvinceDetails(provinceVO);
			}

			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getState())
					&& FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getCity())) {
				cityVO.setCodeType(applicantDetailsVO.getState().toString());
				cityVO.setCmCode(applicantDetailsVO.getCity().toString());
				IFEBAType cityDesc = AppDataManager.getValue(opContext, COMMON_CODE_CACHE, CODE_TYPE
						+ applicantDetailsVO.getState().toString() + CM_CODE + applicantDetailsVO.getCity().toString());
				if (null != cityDesc) {
					cityVO.setCodeDescription(cityDesc.toString());
					applicantDetails.setCity(cityVO);
				}

			}
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getCity())
					&& FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getAddressLine4())) {
				addrLine4VO.setCodeType(applicantDetailsVO.getCity().toString());
				addrLine4VO.setCmCode(applicantDetailsVO.getAddressLine4().toString());
				IFEBAType addrLine4Desc = AppDataManager.getValue(opContext, COMMON_CODE_CACHE, CODE_TYPE
						+ applicantDetailsVO.getCity().toString() + CM_CODE + applicantDetailsVO.getAddressLine4());
				if (null != addrLine4Desc) {
					addrLine4VO.setCodeDescription(addrLine4Desc.toString());
					applicantDetails.setAddressLine4(addrLine4VO);
				}
			}
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getAddressLine4())
					&& FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getAddressLine5())) {
				addrLine5VO.setCodeType(applicantDetailsVO.getAddressLine4().toString());
				addrLine5VO.setCmCode(applicantDetailsVO.getAddressLine5().toString());
				IFEBAType addrLine5DescString = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
						CODE_TYPE + applicantDetailsVO.getAddressLine4().toString() + CM_CODE
						+ applicantDetailsVO.getAddressLine5());
				if (null != addrLine5DescString) {
					addrLine5VO.setCodeDescription(addrLine5DescString.toString());
					applicantDetails.setAddressLine5(addrLine5VO);
				}
			}

			applicantDetails.setPostalCode(Long.valueOf(applicantDetailsVO.getPostalCode().toString()));

			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getHomeOwnershipStatus())) {
				homeOwnershipStatusVO.setCodeType("RES");
				homeOwnershipStatusVO.setCmCode(applicantDetailsVO.getHomeOwnershipStatus().toString());

				String homeOwnershipStatus = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
						"CODE_TYPE=RES|CM_CODE=" + applicantDetailsVO.getHomeOwnershipStatus()).toString();
				homeOwnershipStatusVO.setCodeDescription(homeOwnershipStatus);
				applicantDetails.setHomeOwnershipStatus(homeOwnershipStatusVO);
			}
			if (null != applicantDetailsVO.getHomeOwnershipDuration()) {
				homeOwnershipDurationVO.setCodeType("OCTM");
				homeOwnershipDurationVO.setCmCode(applicantDetailsVO.getHomeOwnershipDuration().toString());
				String homeOwnershipDuration = AppDataManager
						.getValue(opContext, COMMON_CODE_CACHE,
								"CODE_TYPE=OCTM|CM_CODE=" + "0" + applicantDetailsVO.getHomeOwnershipDuration())
						.toString();
				homeOwnershipDurationVO.setCodeDescription(homeOwnershipDuration);
				applicantDetails.setHomeOwnershipDuration(homeOwnershipDurationVO);
			}
			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getHomePhoneNum())) {
				applicantDetails.setHomePhoneNumber("0"+applicantDetailsVO.getHomePhoneNum().toString());
			}

			if (FEBATypesUtility.isNotNullOrBlank(applicantDetailsVO.getReligion())) {
				religionVO.setCodeType("RELI");
				religionVO.setCmCode(applicantDetailsVO.getReligion().toString());
				String religion = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
						"CODE_TYPE=RELI|CM_CODE=" + applicantDetailsVO.getReligion()).toString();
				religionVO.setCodeDescription(religion);
				applicantDetails.setReligion(religionVO);
			}

		} catch (Exception exe) {
			LogManager.logError(opContext, exe);
			throw exe;
		}
	}

	protected static void getContactDetails(IOpContext opcontext, CustomContactInputDetailsVO contactDetails,
			CustomLoanApplnContactDetailsVO contactDetailsVO) {
		contactDetails.setMotherName(contactDetailsVO.getMotherName().toString());
		contactDetails.setNoOfDependent(Integer.valueOf(contactDetailsVO.getNoOfDependents().toString()));
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getMaritalStatus())) {
			contactDetails.setMaritalStatusReference(RestCommonUtils.getCodeReferences(opcontext, "MST",
					contactDetailsVO.getMaritalStatus().toString()));
		}
		if (FEBATypesUtility.isNotBlankLong(contactDetailsVO.getSpouseId())) {
			contactDetails.setSpouseId(contactDetailsVO.getSpouseId().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getEmergencyContact())) {
			contactDetails.setContactName(contactDetailsVO.getEmergencyContact().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getEmergencyContactRelation())) {
			contactDetails.setRelationshipReference(RestCommonUtils.getCodeReferences(opcontext, "RLT",
					contactDetailsVO.getEmergencyContactRelation().toString()));
		}
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getEmergencyContactPhoneNum())) {
			contactDetails.setContactNumber(Long.valueOf(contactDetailsVO.getEmergencyContactPhoneNum().toString()));
		}
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getEmergencyContactAddress())) {
			contactDetails.setContactAddress(contactDetailsVO.getEmergencyContactAddress().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getEmergencyContactPostalCode())) {
			contactDetails
			.setContactPostalCode(Long.valueOf(contactDetailsVO.getEmergencyContactPostalCode().toString()));
		}
		//added for partner nik and name -start
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getPartnerNikId())) {
			contactDetails
			.setPartnerNikId(contactDetailsVO.getPartnerNikId().getValue());
		}
		if (FEBATypesUtility.isNotNullOrBlank(contactDetailsVO.getPartnerName())) {
			contactDetails
			.setPartnerName(contactDetailsVO.getPartnerName().getValue());
		}
		//added for partner nik and name -end

	}

	protected static void getEmploymentDetails(IOpContext opContext, CustomEmploymentDetailsVO employmentDetails,
			CustomLoanApplnEmploymentDetailsVO employmentDetailsVO) throws CriticalException {
		CustomCommonCodeDetailsVO finSourceVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO workTypeVO = new CustomCommonCodeDetailsVO();
		CustomCommonCodeDetailsVO empStatusVO = new CustomCommonCodeDetailsVO();

		employmentDetails.setNpwp(employmentDetailsVO.getTaxId().toString());
		employmentDetails.setMonthlyIncome(employmentDetailsVO.getMonthlyIncome().getAmountValue());
		employmentDetails.setMonthlyExpenditure(employmentDetailsVO.getMonthlyExpense().getAmountValue());

		if (FEBATypesUtility.isNotNullOrBlank(employmentDetailsVO.getFinancialSource())) {
			finSourceVO.setCodeType("INSC");
			finSourceVO.setCmCode(employmentDetailsVO.getFinancialSource().toString());
			String finSource = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
					"CODE_TYPE=INSC|CM_CODE=" + employmentDetailsVO.getFinancialSource()).toString();
			finSourceVO.setCodeDescription(finSource);

			employmentDetails.setFinancialSource(finSourceVO);
		}
		if (FEBATypesUtility.isNotNullOrBlank(employmentDetailsVO.getWorkType())) {
			workTypeVO.setCodeType("TYOW");
			workTypeVO.setCmCode(employmentDetailsVO.getWorkType().toString());
			String workType = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
					"CODE_TYPE=TYOW|CM_CODE=" + employmentDetailsVO.getWorkType()).toString();
			workTypeVO.setCodeDescription(workType);

			employmentDetails.setWorkType(workTypeVO);
		}
		employmentDetails.setEmployerName(employmentDetailsVO.getEmployerName().toString());
		if (FEBATypesUtility.isNotNullOrBlank(employmentDetailsVO.getEmploymentStatus())) {
			empStatusVO.setCodeType("EMPS");
			empStatusVO.setCmCode(employmentDetailsVO.getEmploymentStatus().toString());
			String empStatus = AppDataManager.getValue(opContext, COMMON_CODE_CACHE,
					"CODE_TYPE=EMPS|CM_CODE=" + employmentDetailsVO.getEmploymentStatus()).toString();
			empStatusVO.setCodeDescription(empStatus);

			employmentDetails.setEmployerStatus(empStatusVO);
		}
		employmentDetails.setEmpStartDate(employmentDetailsVO.getEmploymentStartDate().getValue());
		employmentDetails.setEmpEndDate(employmentDetailsVO.getEmploymentEndDate().getValue());

	}

	/*protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		return new ArrayList<>();

	}*/
	
	private List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(EBankingErrorCodes.NO_RECORDS_FOUND, "BE", 400));

		return restResponceErrorCode;
	}
	protected static boolean isOCRFailedAllowedToPass(IOpContext opContext,ApplicationNumber applicationId ) throws FEBAException {
		String allowedCounter = PropertyUtil.getProperty("OCR_VERIFICATION_FAIL_ALLOWED_COUNTER", opContext);
		int count=2;
		if(null!=allowedCounter){
			count=Integer.valueOf(allowedCounter);
		}
		boolean flag=false;
		int numberOfRecordsFetched=0;
		CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
				.createInstance(CustomLoanApplicationEnquiryVO.class.getName());

		enquiryVO.getCriteria().setApplicationId(applicationId);

		enquiryVO.getCriteria().setUserId(opContext.getFromContextData("USER_ID").toString());

		final IClientStub customLoanInquiryService = ServiceUtil
				.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
		customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("fetchOCR"));
		CustomLoanApplnMasterDetailsVO details = (CustomLoanApplnMasterDetailsVO) enquiryVO.getResultList().get(0);
		if (details.getApplicationStatus().getValue().equalsIgnoreCase(PAYROLL_APP)) {
			String statusDesc=details.getStatusDesc().getValue();
			if (statusDesc.contains("OCR_VER_FAIL")) {
				numberOfRecordsFetched =Integer.valueOf(statusDesc.split("\\|")[1]);
				if(numberOfRecordsFetched>=count-1){
					flag= true;
				}
			}
		}
		
	if (!flag) {
		numberOfRecordsFetched=numberOfRecordsFetched+1;
		updateApplicationStatus(opContext, applicationId.toString(), PAYROLL_APP,OCR_VER_FAIL+"|"+numberOfRecordsFetched);
		
		new CustomLoanHistoryUpdateUtil().updateHistory(
				applicationId, OCR_VER_FAIL+"|"+numberOfRecordsFetched,
				"OCR Verification Failed", RestCommonUtils.getTransactionContext(opContext));

	}
		return flag;
	}
	protected static void checkDuplicateLoanRecord(IOpContext opContext, String applicationStatus)
			throws FEBAException {
		int numberOfRecordsFetched=0;
		CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
				.createInstance(CustomLoanApplicationEnquiryVO.class.getName());

		enquiryVO.getCriteria().setApplicationStatus(applicationStatus);

		enquiryVO.getCriteria().setUserId(opContext.getFromContextData("USER_ID").toString());

		final IClientStub customLoanInquiryService = ServiceUtil
				.getService(new FEBAUnboundString("CustomApplicationService"));
		customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("deDupCheck"));
		numberOfRecordsFetched = enquiryVO.getResultList().size();
		
		FEBAArrayList<CustomLoanApplnMasterDetailsVO> result=enquiryVO.getResultList();
		Boolean error=false;
		for (CustomLoanApplnMasterDetailsVO customLoanApplnMasterDetailsVO : result) {
			if (customLoanApplnMasterDetailsVO.getApplicationStatus().equals(new CommonCode(applicationStatus))) {
				error=true;
			}
		}
		if (error) {
			throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.KTP_NUMBER_MISMATCH,
					"Multiple Loan Applications Present", null, CustomEBankingErrorCodes.KTP_NUMBER_MISMATCH, null);
		}
	}
	
	protected static CustomLoanApplnMasterDetailsVO isWhiteListRequest(IOpContext opContext) throws FEBAException {
		int numberOfRecordsFetched = 0;
		CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
				.createInstance(CustomLoanApplicationEnquiryVO.class.getName());

		enquiryVO.getCriteria().setApplicationStatus(APP_CREATED);

		enquiryVO.getCriteria().setUserId(opContext.getFromContextData("USER_ID").toString());

		final IClientStub customLoanInquiryService = ServiceUtil
				.getService(new FEBAUnboundString("CustomApplicationService"));
		customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("deDupCheck"));
		numberOfRecordsFetched = enquiryVO.getResultList().size();
		if (numberOfRecordsFetched == 1) {
			CustomLoanApplnMasterDetailsVO obj = (CustomLoanApplnMasterDetailsVO) enquiryVO.getResultList().get(0);
			return obj;
		}
		return null;
	}
	
	private static void updateKTPStatus(CustomLoanApplnMasterDetailsVO enqVO,
			IOpContext context) throws FEBAException{

		ServiceUtil.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE)).callService(context, enqVO, new FEBAUnboundString(MODIFY));

	}
	
	private static void updatePayrollRejStatus(CustomLoanApplnMasterDetailsVO enqVO,
			IOpContext context) throws FEBAException{
		ServiceUtil.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE)).callService(context, enqVO, new FEBAUnboundString(MODIFY));

	}
}
