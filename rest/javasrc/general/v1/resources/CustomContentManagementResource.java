package com.infosys.custom.ebanking.rest.general.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomContentManagementReferencesResponseVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomContentManagementReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentManagementDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentManagementEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.security.Base64EncoderDecoder;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ServiceInfo(moduleName = "contentmanagement")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Content Management")
public class CustomContentManagementResource extends ResourceAuthenticator{
	@Context
	HttpServletRequest request;



	@Context
	UriInfo uriInfo;

	@Override
	/**
	 * Authenticates if the bankid who is trying to access the resource is same as the corpid and bankid which is associated with the context.
	 *
	 * @return boolean if the bankid is authenticated it returns false
	 * @throws Exception
	 */
	protected boolean isUnAuthenticatedRequest() throws Exception {


		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankId = pathParams.getFirst("bankid");

		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);

		
		boolean isInValid;

		if (bank.getValue().equals(bankId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.",
					FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}


	@GET
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_CONTENT_MANAGEMENT_URL, auditFields = {"contentType : Content Management content Type input"
	})
	@ApiOperation(value="Fetch Content Management details based on content type" , response=CustomContentManagementReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code=200, message= "Successfull Retrieval of Content Management Details" , response = CustomContentManagementReferencesResponseVO.class),
			@ApiResponse(code=404, message= "Content Management Details do not exist", response = CustomContentManagementReferencesResponseVO.class),
			@ApiResponse(code=400, message= "Content Type is mandatory" , response = CustomContentManagementReferencesResponseVO.class),
			@ApiResponse(code=400, message= "Content Type is invalid" , response = CustomContentManagementReferencesResponseVO.class)

	})
	public Response fetchRequest(
			@ApiParam(value = "Content Management content type input" ,required=true) @QueryParam("contentType") String contentType
			){
		CustomContentManagementReferencesResponseVO responseVO = new CustomContentManagementReferencesResponseVO();
		try {
			isUnAuthenticatedRequest();
			IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
			List<CustomContentManagementReferencesVO> outputVO = fetchContentManagementDetails(contentType, context);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO	, request,response, 0);
			
			

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request,response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	public List<CustomContentManagementReferencesVO> fetchContentManagementDetails(String contentType, IOpContext context)
			throws  FEBAException {
		List<CustomContentManagementReferencesVO> outputVO = null;

		CustomContentManagementEnquiryVO enquiryVO = (CustomContentManagementEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomContentManagementEnquiryVO);
		if(null!=contentType){
			enquiryVO.getCriteria().setContentType(contentType);
		}

		outputVO = new ArrayList<>();
		final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomContentManagementService"));
		enquiryVO = (CustomContentManagementEnquiryVO)serviceCStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		FEBAArrayList<CustomContentManagementDetailsVO> detailsListVO = enquiryVO.getResultList();

		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomContentManagementDetailsVO detailsVO = detailsListVO.get(i);
			CustomContentManagementReferencesVO contentManagementReferenceVO = new CustomContentManagementReferencesVO();
			
			String conType = detailsVO.getContentType().getValue();
			FEBABinary fetchedData = detailsVO.getProfilePhoto();
			byte[] decodedData = Base64EncoderDecoder.decode(new String(
					fetchedData.getValue()));
			byte[] biggerArray = decodedData;
			fetchedData = new FEBABinary(biggerArray);
			switch (conType) {
			case CustomResourceConstants.LOAN_CONTENT_TYPE_SLP:
				contentManagementReferenceVO.setHeader(detailsVO.getHeader().getValue());
				contentManagementReferenceVO.setDescription(detailsVO.getDescription().getValue());
				contentManagementReferenceVO.setImagePath(fetchedData.toString());
				break;
			case CustomResourceConstants.LOAN_CONTENT_TYPE_PROMO:
				contentManagementReferenceVO.setHeader(detailsVO.getHeader().getValue());
				contentManagementReferenceVO.setDescription(detailsVO.getDescription().getValue());
				contentManagementReferenceVO.setTextType(detailsVO.getTextType().getValue());
				contentManagementReferenceVO.setImagePath(fetchedData.toString());
				break;
			case CustomResourceConstants.LOAN_CONTENT_TYPE_MER:
				contentManagementReferenceVO.setMerchantId(detailsVO.getMerchantId().getValue());
				contentManagementReferenceVO.setAndAppURL(detailsVO.getAndAppURL().getValue());
				contentManagementReferenceVO.setPlayStoreURL(detailsVO.getPlayStoreURL().getValue());
				contentManagementReferenceVO.setIosAppURL(detailsVO.getIosAppURL().getValue());
				contentManagementReferenceVO.setIosStoreURL(detailsVO.getIosStoreURL().getValue());
				contentManagementReferenceVO.setImagePath(fetchedData.toString());
				
				break;
				default:
				break;
			}
			outputVO.add(contentManagementReferenceVO);
		}
		return outputVO;
	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();

		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211030, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211031, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));

		
		return restResponceErrorCode;
	}

}

