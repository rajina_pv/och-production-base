package com.infosys.custom.ebanking.rest.general.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomCreditScoringCallbackReferencesResponseVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomCreditScoringCallbackReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoringCallbackEnquiryVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBACurrencyCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Api(value = "Credit Scoring Callback")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/v1/banks/{bankId}/custom/applications/{appId}/creditscoreupdate")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })

public class CustomCreditScoringCallbackResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request1;
	@Context
	HttpServletResponse httpResponse;
	
	public static final String HOME_CUR_CODE = "HOME_CUR_CODE";


	@POST
	@ApiOperation(value = "Credit scoring callback", response = CustomCreditScoringCallbackReferencesResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.CREDIT_SCORING_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Credit Scoring Callback", response = CustomCreditScoringCallbackReferencesResponseVO.class),
			@ApiResponse(code = 500, message = "Table update failed") })
	public Response hitCallback(CustomCreditScoringCallbackReferencesVO customCreditScoringCallbackVO,
			@ApiParam(value = "bank id", required = true) @PathParam("bankId") String bankId,
			@ApiParam(value = "app id", required = true) @PathParam("appId") String appId) {
		CustomCreditScoringCallbackReferencesResponseVO responseVO = new CustomCreditScoringCallbackReferencesResponseVO();
		try {
			CustomCreditScoringCallbackEnquiryVO enquiryVO = getCustomCreditScoringDetails(
					customCreditScoringCallbackVO);
			enquiryVO.getCriteria().setApplicationId(appId);
			responseVO = getSingleResponse(enquiryVO, "requestPayload");
			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request1, 0);
		} catch (Exception e) {
			
			LogManager.logError(null, e);
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();

	}

	private CustomCreditScoringCallbackEnquiryVO getCustomCreditScoringDetails(
			CustomCreditScoringCallbackReferencesVO callbackReferencesVO) {
		IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
		CustomCreditScoringCallbackEnquiryVO enquiryVO = (CustomCreditScoringCallbackEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomCreditScoringCallbackEnquiryVO);
		CustomCreditScoringCallbackCriteriaVO criteriaVO = (CustomCreditScoringCallbackCriteriaVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomCreditScoringCallbackCriteriaVO);
		criteriaVO.setStatus(callbackReferencesVO.getStatus());
		criteriaVO.setUid(callbackReferencesVO.getUid());
		if (RestCommonUtils.isNotNullorEmpty(callbackReferencesVO.getScore())) {
			criteriaVO.setScore(callbackReferencesVO.getScore());
		}
		if (RestCommonUtils.isNotNullorEmpty(callbackReferencesVO.getGrade())) {
			criteriaVO.setGrade(callbackReferencesVO.getGrade());
		}

		FEBAAmount plafond1 = new FEBAAmount();
		String homeCurrency = PropertyUtil.getProperty("HOME_CUR_CODE", context);
		if (callbackReferencesVO.getApproval()!=null) {
			if (callbackReferencesVO.getApproval().size()==2) {
				if (FEBATypesUtility.isNotNullOrBlank(new FEBAUnboundString(callbackReferencesVO.getApproval().get(0).getPlafond()))) {
					plafond1.setAmountValue(new Double(callbackReferencesVO.getApproval().get(0).getPlafond()));
				}
				plafond1.setCurrencyCode(new FEBACurrencyCode(homeCurrency));
				criteriaVO.setPlafond1(plafond1);

				FEBAAmount plafond2 = new FEBAAmount();
				if (FEBATypesUtility.isNotNullOrBlank(new FEBAUnboundString(callbackReferencesVO.getApproval().get(1).getPlafond()))) {
					plafond2.setAmountValue(new Double(callbackReferencesVO.getApproval().get(1).getPlafond()));
				}
				plafond2.setCurrencyCode(new FEBACurrencyCode(homeCurrency));
				criteriaVO.setPlafond2(plafond2);

				FEBAUnboundInt tenor1 = new FEBAUnboundInt();
				if (FEBATypesUtility.isNotNullOrBlank(new FEBAUnboundString(callbackReferencesVO.getApproval().get(0).getTenor()))) {
					tenor1.set(callbackReferencesVO.getApproval().get(0).getTenor());
				}
				
				FEBAUnboundInt tenor2 = new FEBAUnboundInt();
				if (FEBATypesUtility.isNotNullOrBlank(new FEBAUnboundString(callbackReferencesVO.getApproval().get(1).getTenor()))) {
					tenor2.set(callbackReferencesVO.getApproval().get(1).getTenor());
				}
				criteriaVO.setTenor1(tenor1);
				criteriaVO.setTenor2(tenor2);
			}		
			if (callbackReferencesVO.getApproval().size()==1) {
				if (FEBATypesUtility.isNotNullOrBlank(new FEBAUnboundString(callbackReferencesVO.getApproval().get(0).getPlafond()))) {
					plafond1.setAmountValue(new Double(callbackReferencesVO.getApproval().get(0).getPlafond()));
				}
				plafond1.setCurrencyCode(new FEBACurrencyCode(homeCurrency));
				criteriaVO.setPlafond1(plafond1);

				FEBAAmount plafond2 = new FEBAAmount();
				plafond2.setCurrencyCode(new FEBACurrencyCode(homeCurrency));
				criteriaVO.setPlafond2(plafond2);

				FEBAUnboundInt tenor1 = new FEBAUnboundInt();
				if (FEBATypesUtility.isNotNullOrBlank(new FEBAUnboundString(callbackReferencesVO.getApproval().get(0).getTenor()))) {
					tenor1.set(callbackReferencesVO.getApproval().get(0).getTenor());
				}
				
				FEBAUnboundInt tenor2 = new FEBAUnboundInt();
				criteriaVO.setTenor1(tenor1);
				criteriaVO.setTenor2(tenor2);
			}
		}

		FEBAUnboundInt tenorMax = new FEBAUnboundInt();
		if (RestCommonUtils.isNotNullorEmpty(callbackReferencesVO.getTenor_max())) {
			tenorMax.set(callbackReferencesVO.getTenor_max());
		}

		criteriaVO.setTenormax(tenorMax);
		criteriaVO.setDesc(callbackReferencesVO.getDesc());
		
		
		
		// Added for Missing Simpanan case.
		FEBAUnboundInt respCode = new FEBAUnboundInt();
		if (RestCommonUtils.isNotNullorEmpty(callbackReferencesVO.getResp_code())) {
			respCode.set(callbackReferencesVO.getResp_code());
		}
		criteriaVO.setCrRespCode(respCode);
		
		enquiryVO.setCriteria(criteriaVO);
		return enquiryVO;
	}

	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211052, "BE", 500));
	//	restResponceErrorCode.add(new RestResponseErrorCodeMapping(211305, "BE", 400));
		return restResponceErrorCode;
	}

	private CustomCreditScoringCallbackReferencesResponseVO getSingleResponse(
			CustomCreditScoringCallbackEnquiryVO enquiryVO, String method) throws FEBAException {
		IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
		final IClientStub serviceCStub = ServiceUtil
				.getService(new FEBAUnboundString("CustomCreditScoringCallbackService"));
		return getResponsePOJO((CustomCreditScoringCallbackEnquiryVO) serviceCStub.callService(context, enquiryVO,
				new FEBAUnboundString(method)));
	}

	private CustomCreditScoringCallbackReferencesResponseVO getResponsePOJO(
			CustomCreditScoringCallbackEnquiryVO enquiryVO) {
		CustomCreditScoringCallbackReferencesResponseVO callbackReferencesResponseVO = new CustomCreditScoringCallbackReferencesResponseVO();
		CustomCreditScoringCallbackReferencesVO callbackReferencesVO = new CustomCreditScoringCallbackReferencesVO();
		CustomCreditScoringCallbackDetailsVO detailsVO = enquiryVO.getDetails();

		List<CustomCreditScoringCallbackReferencesVO> outputVO = null;
		enquiryVO.setDetails(detailsVO);
		if (detailsVO.getStatus().toString().equals("200")) {
			callbackReferencesVO.setStatusCode("0000");
			callbackReferencesVO.setApplicationStatus(detailsVO.getApplicationStatus().toString());
		}
		outputVO = new ArrayList<>();
		outputVO.add(callbackReferencesVO);
		callbackReferencesResponseVO.setData(outputVO);
		return callbackReferencesResponseVO;
	}
}
