package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomContentManagementReferencesResponseVO extends RestHeaderFooterHandler{
	private List<CustomContentManagementReferencesVO> data;
	
	public List<CustomContentManagementReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomContentManagementReferencesVO> data) {
		this.data = data;
	}
}
