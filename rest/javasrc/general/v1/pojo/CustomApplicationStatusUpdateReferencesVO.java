package com.infosys.custom.ebanking.rest.general.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Application Status Update")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomApplicationStatusUpdateReferencesVO {

	@ApiModelProperty(value = "status")
	private String status;

	@ApiModelProperty(value = "uid")
	private String uid;

	@ApiModelProperty(value = "statusCode")
	private String statusCode;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public CustomApplicationStatusUpdateReferencesVO() {
		super();
	}

	public CustomApplicationStatusUpdateReferencesVO(String status, String uid, String score, String bankId,
			String userId, String applicationId, String statusCode) {
		super();
		this.status = status;
		this.uid = uid;
		this.statusCode = statusCode;
	}
}
