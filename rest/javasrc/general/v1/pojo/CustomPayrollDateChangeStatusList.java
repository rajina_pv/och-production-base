package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class CustomPayrollDateChangeStatusList {

	@ApiModelProperty(value = "payroll date update status list")
	private List<CustomPayrollDateChangeStatusDetail> statusList;

	public List<CustomPayrollDateChangeStatusDetail> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<CustomPayrollDateChangeStatusDetail> statusList) {
		this.statusList = statusList;
	}
}
