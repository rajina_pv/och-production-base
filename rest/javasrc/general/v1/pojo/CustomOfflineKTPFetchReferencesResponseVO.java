package com.infosys.custom.ebanking.rest.general.v1.pojo;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomOfflineKTPFetchReferencesResponseVO extends RestHeaderFooterHandler {
	private CustomOfflineKTPFetchReferencesVO data;

	public CustomOfflineKTPFetchReferencesVO getData() {
		return data;
	}

	public void setData(CustomOfflineKTPFetchReferencesVO data) {
		this.data = data;
	}

	
}
