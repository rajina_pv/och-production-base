package com.infosys.custom.ebanking.rest.servicerequest.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.servicerequest.v1.pojo.CustomLoanPaymentDetailsInputVO;
import com.infosys.custom.ebanking.rest.servicerequest.v1.pojo.CustomLoanPaymentDetailsResponseVO;
import com.infosys.custom.ebanking.rest.servicerequest.v1.pojo.CustomLoanPaymentRequestVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanPayoffInOutVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "RQST")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Payment")
public class CustomLoanPaymentResource extends ResourceAuthenticator  {

	RestCommonSuccessMessageHandler headerResponse = new RestCommonSuccessMessageHandler();
	@Context
	protected HttpServletRequest request;
	
	private static final String Payoffprocess = "payoffprocess";
	final IClientStub CustomLoanPaymentService = ServiceUtil.getService(new FEBAUnboundString("CustomLoanPaymentService"));
	
	@POST
	@ApiOperation(value = "Service request to break a fixed deposit",response= CustomLoanPaymentDetailsResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.LOAN_PAYMENT_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successful submit the  loan payment request"),
			@ApiResponse(code = 400, message = "Invalid Input Details"),
			@ApiResponse(code = 500, message = "Internal server error", response = CustomLoanPaymentDetailsResponseVO.class )})
	public Response createRequest(	@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid,CustomLoanPaymentDetailsInputVO inputDetails) {
		CustomLoanPaymentDetailsResponseVO wrapperOutput = new CustomLoanPaymentDetailsResponseVO();
		CustomLoanPaymentRequestVO requestVO = new CustomLoanPaymentRequestVO();

		try {
			isUnAuthenticatedRequest();
			
			IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
			CustomLoanPayoffInOutVO enquiryVO = (CustomLoanPayoffInOutVO) FEBAAVOFactory
					.createInstance(CustomLoanPayoffInOutVO.class.getName());
			enquiryVO.setLoanAccountID(inputDetails.getAccountId());
			enquiryVO.setBankId(new BankId(bankid));
			enquiryVO.setUserId(userid);
			CustomLoanPaymentService.callService(opcontext, enquiryVO, new FEBAUnboundString(Payoffprocess));
			RestResourceManager.updateHeaderFooterResponseData(wrapperOutput, request, response, 0);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(wrapperOutput).build();
		/****************** end code *****************/
		
	}
	
	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211150, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211151, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211152, "BE", 400));
		return restResponceErrorCode;
	}
}
	
