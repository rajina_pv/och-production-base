package com.infosys.custom.ebanking.rest.servicerequest.v1.resources;


import java.util.Iterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.servicerequest.v1.pojo.CustomLoanPaymentDetailsResponseVO;
import com.infosys.custom.ebanking.rest.servicerequest.v1.pojo.CustomMerchantPaymentDetailsInOutVO;
import com.infosys.custom.ebanking.rest.servicerequest.v1.pojo.CustomMerchantPaymentDetailsResponseVO;
import com.infosys.custom.ebanking.rest.servicerequest.v1.pojo.CustomMerchantPaymentRequestVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.retail.servicerequest.v1.resources.AbstractServiceRetailRequest;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.TypesUtil;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.formsgroup.processor.FEBATypeFactory;
import com.infosys.feba.framework.interceptor.rest.pojo.AuthorizationInputVO;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.lists.FEBAListIterator;
import com.infosys.feba.framework.types.primitives.ActionID;
import com.infosys.feba.framework.types.primitives.FEBAOperatingMode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FormRequestId;
import com.infosys.feba.framework.types.primitives.IFEBAPrimitive;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.util.FormConfigCacheUtil;
import com.infosys.fentbase.types.primitives.SectionId;
import com.infosys.fentbase.types.valueobjects.FormFieldConfigVO;
import com.infosys.fentbase.types.valueobjects.FormManagementVO;
import com.infosys.fentbase.types.valueobjects.NameValuePairVO;
import com.infosys.fentbase.types.valueobjects.SBMTDetailsVO;
import com.infosys.fentbase.types.valueobjects.SectionConfigVO;
import com.infosys.fentbase.types.valueobjects.SectionDataVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "RQST")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Merchant Payment")
public class CustomMerchantPaymentResource extends AbstractServiceRetailRequest<CustomMerchantPaymentDetailsInOutVO> {
	RestHeaderFooterHandler restHeaderFooterHandler;
	
	@Override
	@POST
	@ApiOperation(value = "Service request to make merchant payment",response= CustomLoanPaymentDetailsResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.MERCHANT_PAYMENT_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successful submit the  merchant payment request"),
			@ApiResponse(code = 400, message = "Invalid Input Details"),
			@ApiResponse(code = 500, message = "Internal server error", response = CustomLoanPaymentDetailsResponseVO.class )})
	public Response createRequest(CustomMerchantPaymentDetailsInOutVO inOutVO) {
		int responseStatus = 201;
		CustomMerchantPaymentDetailsResponseVO wrapperOutput = new CustomMerchantPaymentDetailsResponseVO();
		CustomMerchantPaymentRequestVO requestVO = new CustomMerchantPaymentRequestVO();
		
		try {
			isUnAuthenticatedRequest();
			Header header = new Header();

			IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
			populateHeaderFromHttpHeader(requestVO,opcontext);
			String homeCurrency = PropertyUtil.getProperty(EBankingConstants.HOME_CURRENCY,opcontext);
			
			String errorField = "";
			if(!RestCommonUtils.isNotNull(inOutVO.getMerchantId())) {
				errorField = "merchantId";
			}else if(!RestCommonUtils.isNotNull(inOutVO.getMerchantRefId())) {
				errorField = "merchantRefId";			
			}else if(!RestCommonUtils.isNotNull(inOutVO.getNetPurchaseAmount())) {
				errorField = "netPurchaseAmount";				
			}else if(!RestCommonUtils.isNotNull(inOutVO.getFullPurchaseAmount())) {
				errorField = "fullPurchaseAmount";
			}
			if(!"".equalsIgnoreCase(errorField)){
				throw new BusinessException(true, opcontext, "MRC001",			  
						"Mandatory Field Not passed",null, 211090,null, new AdditionalParam("FIELD_ID",new FEBAUnboundString(errorField)));
			}
			
			if (RestCommonUtils.isNotNull(inOutVO.getFeeAmount())) {
				RestCommonUtils.validateCurrency(opcontext,homeCurrency,inOutVO.getFeeAmount().toString());
			}
			if (RestCommonUtils.isNotNull(inOutVO.getFullPurchaseAmount())) {
				RestCommonUtils.validateCurrency(opcontext,homeCurrency,inOutVO.getFullPurchaseAmount().toString());
			}
			if (RestCommonUtils.isNotNull(inOutVO.getNetPurchaseAmount())) {
				RestCommonUtils.validateCurrency(opcontext,homeCurrency,inOutVO.getNetPurchaseAmount().toString());
			}
			if (RestCommonUtils.isNotNull(inOutVO.getTotalPurchaseAmount())) {
				RestCommonUtils.validateCurrency(opcontext,homeCurrency,inOutVO.getTotalPurchaseAmount().toString());
			}
			
			FEBAHashList commonCodeList =  RestCommonUtils.getList(opcontext, CustomResourceConstants.LOAN_TENURE_CODE_TYPE);
			CommonCodeVO commonCodeVO=null;
			Boolean isValidTenure = false;

			for (Object obj : commonCodeList) {
				commonCodeVO = (CommonCodeVO) obj;
				if (commonCodeVO.getCommonCode().toString().trim()
						.equalsIgnoreCase(inOutVO.getSelectedTenor())){
					isValidTenure=true;
					break;
				}
			}
			if(!isValidTenure){
				throw new BusinessException(
						true,
						opcontext,
						CustomEBankingIncidenceCodes.CLNSIM_INVALID_LOAN_TENURE,
						"Tenure is invalid",
						null,
						CustomEBankingErrorCodes.INVALID_LOAN_TENURE,
						null);
			}
			
			SBMTDetailsVO sbmtinfo = getSBMTInfo(opcontext, "MPA");

			FormManagementVO fmVo = (FormManagementVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.FormManagementVO);

			fmVo.setFormRequestId(new FormRequestId("MPA_INITIATE"));

			
			
			fmVo = convertInputDetailToFormManagementVO(opcontext, inOutVO, sbmtinfo, fmVo);
			
			fmVo = validateFormManagementDetails(opcontext, fmVo);
			
			System.out.println("After validating fmVO");

			if (requestVO!=null && requestVO.getHeader() != null && requestVO.getHeader().getAuthorizationDetails() != null &&
					RestCommonUtils
						.isNotNullorEmpty(requestVO.getHeader().getAuthorizationDetails().getUserRemarks())) {
					setRemarksInContext(requestVO.getHeader().getAuthorizationDetails().getUserRemarks(), opcontext);

			}
			
			if ((sbmtinfo.getAuthflg().getValue() != 'Y') && validateChannelContext(requestVO,opcontext)) {
				throw new BusinessException(opcontext, EBIncidenceCodes.UNSUPPORTED_AUTHORIZATION_MODE,
						"Transaction password not configured for the request type",
						EBankingErrorCodes.UNSUPPORTED_AUTHORIZATION_MODE);

			} else if (sbmtinfo.getAuthflg().getValue() == 'Y' && validateChannelContext(requestVO,opcontext)) {
				fmVo = submitServiceRequest(opcontext, requestVO.getHeader().getAuthorizationDetails(), fmVo,
						sbmtinfo.getTransactionType().getValue());
				FEBAUnboundString txnRefNumber =getTrnId(fmVo,opcontext);

				if(null!=txnRefNumber)
					inOutVO.setTxnRefNumber(txnRefNumber.toString());;
					wrapperOutput.setData(inOutVO);
			} else {
				System.out.println(" authorization details not present");
				AuthorizationInputVO authmodeDetails = RestCommonUtils
						.transactionAuthorizationModeDetails(sbmtinfo.getTransactionType().getValue(), opcontext);
				if (sbmtinfo.getAuthflg().getValue() == 'Y' && (authmodeDetails.getFirstAuthorizationMode() != null
						|| authmodeDetails.getSecondAuthorizationMode() != null
						|| authmodeDetails.getUserIdRequired() == 'Y')) {
					header.setAuthorization(authmodeDetails);
					wrapperOutput.setHeader(header);
					wrapperOutput.setData(inOutVO);
					responseStatus = 401;
				} else {
					fmVo = submitServiceRequest(opcontext, fmVo, sbmtinfo.getTransactionType().getValue());
					FEBAUnboundString txnRefNumber =getTrnId(fmVo,opcontext);
					if(null!=txnRefNumber){
						inOutVO.setTxnRefNumber(txnRefNumber.toString());
					}
					wrapperOutput.setData(inOutVO);
				}
			}
			RestResourceManager.updateHeaderFooterResponseData(wrapperOutput, request, httpResponse, 0);

		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request, httpResponse, e, restResponseErrorCodeMapping());
		}
		return Response.status(responseStatus).entity(wrapperOutput).build();
		/****************** end code *****************/
		
	}
	
	private FEBAUnboundString getTrnId(FormManagementVO formManagementVO,
			IOpContext opcontext) throws  IllegalAccessException, IllegalArgumentException {

		FEBAHashList sectionList = formManagementVO.getSectionsList();

		CustomMerchantPaymentDetailsInOutVO inOutVO = new CustomMerchantPaymentDetailsInOutVO();
		Iterator keySet = sectionList.getKeySet();
		// Iterate and build all the sections together.
		while (keySet.hasNext()) {
			String hashKey = keySet.next().toString();
			SectionDataVO sectionDataVO = (SectionDataVO) sectionList.get(hashKey);
			FEBAListIterator basicFieldsIterator = sectionDataVO.getBasicFormFieldsList().iterator();
			while (basicFieldsIterator.hasNext()) {
				NameValuePairVO nameValuePairVO = (NameValuePairVO) basicFieldsIterator.next();
				IFEBAPrimitive formFieldValue = nameValuePairVO.getFormFieldValue();

					String formField = nameValuePairVO.getFormFieldId().toString();
					String fmvoField = "TRN_ID";
					if (formField.equals(fmvoField) && formFieldValue != null && formFieldValue.toString() != "") {
						return (FEBAUnboundString)formFieldValue;
					}
			}
		}
		return null;
	}

	private FormManagementVO convertInputDetailToFormManagementVO(IOpContext opcontext,
			CustomMerchantPaymentDetailsInOutVO inputDetail, SBMTDetailsVO sbmtinfo, FormManagementVO formManagementVO)
					throws CriticalException {
		FEBAHashList sectionList = FormConfigCacheUtil.getSectionList(formManagementVO.getFormRequestId(), opcontext);

		Iterator keySet = sectionList.getKeySet();
		// Iterate and build all the sections together.

		while (keySet.hasNext()) {
			SectionId sectionId = new SectionId();
			String hashKey = keySet.next().toString();
			sectionId.set(hashKey);

			SectionConfigVO sectionConfigVO = (SectionConfigVO) sectionList.get(hashKey);
			addSection(sectionConfigVO, sectionId, formManagementVO, inputDetail, sbmtinfo, opcontext);
		}

		formManagementVO.setCurrentSectionId(EBankingConstants.NO_SECTION);
		formManagementVO.setCorpId(opcontext.getFromContextData("CORPORATE_ID").toString());

		if (sbmtinfo.getRequestmodeflag().toString().equals("B") || sbmtinfo.getRequestmodeflag().toString().equals("O")) {
			formManagementVO.setFormRequestActionId(new ActionID("SUBMIT_TO_HOST"));

		} else {
			formManagementVO.setFormRequestActionId(new ActionID("SUBMIT_TO_RM"));
		}

		return formManagementVO;

	}


	private void addSection(SectionConfigVO sectionConfigVO, SectionId sectionId, FormManagementVO formManagementVO,
			CustomMerchantPaymentDetailsInOutVO requestData, SBMTDetailsVO sbmtinfo, IOpContext opcontext) throws CriticalException {

		SectionDataVO sectionDataVO = (SectionDataVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.SectionDataVO);

		sectionDataVO.setEditFlg(FBAConstants.NO);
		sectionDataVO.setSelectedIndexEditRowIndicator(new FEBAUnboundInt(0));
		// Call method to add basic type fields in VO and cache
		addBasicFields(sectionDataVO, sectionConfigVO, requestData, sbmtinfo, opcontext);
		// set the section id in the sectionDataVO
		sectionDataVO.setSectionId(sectionId);

		// Add the SectionDataVO to the FormManagementVO
		formManagementVO.getSectionsList().putObject(sectionId.getValue(), sectionDataVO);

	}

	private void addBasicFields(SectionDataVO sectionDataVO, SectionConfigVO sectionConfigVO,
			CustomMerchantPaymentDetailsInOutVO requestData, SBMTDetailsVO sbmtinfo, IOpContext opcontext) throws CriticalException {
		String homeCurrency = PropertyUtil.getProperty("HOME_CUR_CODE",opcontext);
		// Identifier to depict whether the field in array or basic type
		final boolean isArrayType = false;

		FEBAHashList<FormFieldConfigVO> basicFieldsConfigList = sectionConfigVO.getBasicFieldList();

		// Check whether the basicFieldsConfigList is null
		if (basicFieldsConfigList == null) {
			// return to the calling method
			return;
		}
		Iterator basicFieldsConfigItr = basicFieldsConfigList.iterator();
	
		
		while (basicFieldsConfigItr.hasNext()) {
			Object fieldValue = null;
			FormFieldConfigVO formFieldConfigVO = (FormFieldConfigVO) basicFieldsConfigItr.next();

			NameValuePairVO nameValuePairVO = (NameValuePairVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.NameValuePairVO);
			IFEBAPrimitive data = null;
			String packageName = "";
			if (formFieldConfigVO.getFormFieldType().getValue() != null) {
				String sPackageName = TypesUtil.getPackageName(formFieldConfigVO.getFormFieldType().getValue());
				if (sPackageName != null) {
					packageName = sPackageName.trim() + "." + formFieldConfigVO.getFormFieldType().getValue().trim();
				}
			}
			data = (IFEBAPrimitive) FEBATypeFactory.getPrimitiveInstance(packageName);

			if (requestData != null ) {
				if (formFieldConfigVO.getFormFieldId().getValue().equals("MERCHANT_ID")
						&& RestCommonUtils.isNotNullorEmpty(requestData.getMerchantId())) {
					data.set(requestData.getMerchantId());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("MERCHANT_REF_ID")
						&& RestCommonUtils.isNotNullorEmpty(requestData.getMerchantRefId())) {
					data.set(requestData.getMerchantRefId());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("PROMO_CODE")
						&& RestCommonUtils.isNotNullorEmpty(requestData.getPromoCode())) {
					data.set(requestData.getPromoCode());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("SELECTED_TENOR")
						&& RestCommonUtils.isNotNullorEmpty(requestData.getSelectedTenor())) {
					data.set(requestData.getSelectedTenor());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("NET_PURCHASE_AMOUNT")
						&& RestCommonUtils.isNotNull(requestData.getNetPurchaseAmount())) {
					data.set(homeCurrency + "|" +requestData.getNetPurchaseAmount().toString());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("FULL_PURCHASE_AMOUNT")
						&& RestCommonUtils.isNotNull(requestData.getFullPurchaseAmount())) {
					data.set(homeCurrency + "|" +requestData.getFullPurchaseAmount().toString());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("FEE_AMOUNT")
						&& RestCommonUtils.isNotNull(requestData.getFeeAmount())) {
					data.set(homeCurrency + "|" +requestData.getFeeAmount().toString());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("TOTAL_TXN_AMOUNT")
						&& RestCommonUtils.isNotNull(requestData.getTotalPurchaseAmount())) {
					data.set(homeCurrency + "|" +requestData.getTotalPurchaseAmount().toString());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("PAYMENT_MODE")
						&& RestCommonUtils.isNotNullorEmpty(requestData.getPaymentMode())) {
					System.out.println("inside payment");
					data.set(requestData.getPaymentMode());
				}
				if (formFieldConfigVO.getFormFieldId().getValue().equals("PAYMENT_MODE")
						&& !RestCommonUtils.isNotNullorEmpty(requestData.getPaymentMode())) {
					System.out.println("inside purchase");
					data.set("PURCHASE");
				}
			}

			if (formFieldConfigVO.getFormFieldId().getValue().equals("AUTHORIZATION")) {
				data.set(sbmtinfo.getAuthflg());
			} else if (formFieldConfigVO.getFormFieldId().getValue().equals("RMAPPROVAL")) {
				data.set(sbmtinfo.getRmapprovalrequired());
			} else if (formFieldConfigVO.getFormFieldId().getValue().equals("REQUESTTYPE")) {
				data.set(sbmtinfo.getRequestmodeflag());
			} else if (formFieldConfigVO.getFormFieldId().getValue().equals("REQUEST_MODE")) {
				if (sbmtinfo.getRequestmodeflag().toString().equals("B")
						|| sbmtinfo.getRequestmodeflag().toString().equals("O")) {

					data.set("O");
				} else {

					data.set("F");
				}
			} else if (formFieldConfigVO.getFormFieldId().getValue().equals("ENABLED_ONLINE")
					|| formFieldConfigVO.getFormFieldId().getValue().equals("PRODUCT_ID")
					|| formFieldConfigVO.getFormFieldId().getValue().equals("REQSTID")
					|| formFieldConfigVO.getFormFieldId().getValue().equals("BRANCH_ID")) {
				data.set(formFieldConfigVO.getDefaultInitializeValue().getValue());
			}

			nameValuePairVO.setFormFieldId(formFieldConfigVO.getFormFieldId());
			FEBAUnboundString dummyvalue = new FEBAUnboundString();
			if (data != null) {
				nameValuePairVO.setFormFieldValue(data);
			} else {
				nameValuePairVO.setFormFieldValue(dummyvalue);
			}
			sectionDataVO.getBasicFormFieldsList().putObject(formFieldConfigVO.getFormFieldId().getValue(),
					nameValuePairVO);

		}
	}

	private FormManagementVO validateFormManagementDetails(IOpContext opcontext, FormManagementVO fmVo)
			throws CriticalException, BusinessException, BusinessConfirmation {
		opcontext.setOperatingMode(new FEBAOperatingMode("V"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("FormDataValidateSaveWFService"));
		return (FormManagementVO) serviceCStub.callService(opcontext, fmVo, new FEBAUnboundString("process"));

	}
	
	
	@Override
	public Response fetchRequest(String arg0) {
		return null;
	}
}
