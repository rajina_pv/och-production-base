package com.infosys.custom.ebanking.rest.servicerequest.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomMerchantInquiryOutputVO {
	
	@ApiModelProperty(value = "status")
	private String status;
	
	@ApiModelProperty(value = "The transaction date")
	private Date txnDate;
	
	@ApiModelProperty(value = "The transaction id")
	private String txnId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

}
