package com.infosys.custom.ebanking.rest.common.v1;

import java.util.Set;

import com.infosys.custom.ebanking.rest.general.v1.resources.CustomApplicationStatusUpdateResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomBanksListFetchForForgotPasswordResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomChangePayrollDateResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomContentManagementResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomCreditScoringCallbackResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomOfflineKTPFetchResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomOfflineKTPVerificationResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomStandardTextMaintenanceResource;
import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomLoanPaymentResource;
import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomMerchantInquiryResource;
import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomMerchantPaymentResource;
import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomMerchantRefundResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomATMInquiryCallResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomAccountListInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomAppStatusNotificationMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomBankCodeFetchResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomBanksListInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomCompanyDetailsManagerResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomCreateLoanResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomDeviceRegUpdateHPNumberResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomDeviceRegistrationRetailResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomDocTextMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomFetchLoanHistoryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomFetchLoanRepaymentScheduleResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomFetchLoanSchemesResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomForgotPasswordResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomGenerateOtpManagerResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanApplicationDocumentListResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanApplicationListInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanApplicationResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanDetailsUpdateResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanLimitInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanProductMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanSimulationResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomNotificationMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomOnlineRegistrationRetailResources;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomPasswordRetailResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomPrivyMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomReUploadKtpResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomStandardTextResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomUserProfileDetailsResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomValidatePhotoMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomVerifyOtpManagerResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomVerifyPayrollResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomVidaLivenessResource;
import com.infosys.ebanking.rest.common.v1.IRestResourceConfiguration;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomFileAttachmentResource;

public class CustomResourceConfiguration implements IRestResourceConfiguration {

	@Override
	public void resourceConfigurationReader(Set<Class<?>> classes) {
		classes.add(CustomUserResource.class);
		classes.add(CustomAdminResource.class);
		classes.add(CustomOnlineRegistrationRetailResources.class);
		classes.add(CustomLoanApplicationResource.class);
		classes.add(CustomPrivyMaintenanceResource.class);
		classes.add(CustomDocTextMaintenanceResource.class);
		classes.add(CustomATMInquiryCallResource.class);
		classes.add(CustomCreditScoringCallbackResource.class);
		classes.add(CustomContentManagementResource.class);
		classes.add(CustomStandardTextMaintenanceResource.class);
		classes.add(CustomLoanProductMaintenanceResource.class);
		classes.add(CustomLoanSimulationResource.class);
		classes.add(CustomCompanyDetailsManagerResource.class);
		classes.add(CustomLoanApplicationListInquiryResource.class);
		classes.add(CustomGenerateOtpManagerResource.class);
		classes.add(CustomVerifyOtpManagerResource.class);
		classes.add(CustomLoanInquiryResource.class);
		classes.add(CustomStandardTextResource.class);
		classes.add(CustomPasswordRetailResource.class);
		classes.add(CustomValidatePhotoMaintenanceResource.class);
		classes.add(CustomUserProfileDetailsResource.class);
		classes.add(CustomNotificationMaintenanceResource.class);
		classes.add(CustomFetchLoanRepaymentScheduleResource.class);
		classes.add(CustomDeviceRegistrationRetailResource.class);
		classes.add(CustomDeviceRegUpdateHPNumberResource.class);
		classes.add(CustomFetchLoanHistoryResource.class);
		classes.add(CustomLoanPaymentResource.class);
		classes.add(CustomLoanApplicationDocumentListResource.class);
		classes.add(CustomCreateLoanResource.class);
		classes.add(CustomMerchantPaymentResource.class);
		classes.add(CustomForgotPasswordResource.class);
		classes.add(CustomAdminResource.class);
		classes.add(CustomAccountListInquiryResource.class);
		classes.add(CustomLoanLimitInquiryResource.class);
		classes.add(CustomMerchantRefundResource.class);
		classes.add(CustomApplicationStatusUpdateResource.class);
		classes.add(CustomMerchantInquiryResource.class);
		classes.add(CustomOfflineKTPVerificationResource.class);
		classes.add(CustomLoanDetailsUpdateResource.class);
		classes.add(CustomBanksListInquiryResource.class);
		classes.add(CustomBankCodeFetchResource.class);
		classes.add(CustomBanksListFetchForForgotPasswordResource.class);
		classes.add(CustomReUploadKtpResource.class);
		classes.add(CustomOfflineKTPFetchResource.class);
		classes.add(CustomVerifyPayrollResource.class);
		classes.add(CustomAppStatusNotificationMaintenanceResource.class);
		classes.add(CustomFileAttachmentResource.class);
		classes.add(CustomChangePayrollDateResource.class);
		classes.add(CustomFetchLoanSchemesResource.class);
		// Added for VIDA liveness start
		classes.add(CustomVidaLivenessResource.class);
		// Added for VIDA liveness end
	}

}
