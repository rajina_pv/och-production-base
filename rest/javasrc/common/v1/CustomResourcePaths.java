package com.infosys.custom.ebanking.rest.common.v1;

public class CustomResourcePaths {

	private CustomResourcePaths() {
	}

	/***************** Custom Resource Paths - Start *****************/
	public static final String CUSTOM = "/custom";
	public static final String STANDARD_TEXT_MAINTENANCE_URL = "/v1/banks/{bankid}/custom/standard-text";
	public static final String LOAN_PRODUCT_MAINTENANCE_URL = "/v1/banks/{bankid}/users/{userid}/custom/loanproducts";
	public static final String LOAN_PRODUCT_SIMULATION_URL = "/v1/banks/{bankid}/users/{userid}/custom/loansimulation";
	public static final String CUSTOM_CONTENT_MANAGEMENT_URL = "/v1/banks/{bankid}/custom/contents";
	public static final String CUSTOM_USER_COMPANY_DETAILS = "/v1/banks/{bankid}/users/{userid}/custom/companydetails";
	public static final String PRIVY_MAINTENANCE_URL = "/v1/banks/{bankid}/users/{userid}/custom/privyregistrations";
	public static final String DOC_MAINTENANCE_URL = "/v1/banks/{bankid}/users/{userid}/applications/{applicationId}/custom/doctext/{documentId}";
	public static final String CAMS_CALL_URL = "/v1/banks/{bankid}/users/{userid}/custom/atminquiry";
	public static final String CREDIT_SCORING_URL = "/v1/banks/{bankId}/custom/applications/{appId}/creditscoreupdate";
	public static final String OTP_GENERATION_URL = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/generateOtp";
	public static final String OTP_VERIFICATION_URL = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/verifyOTP";
	public static final String LOAN_INQ_URL = "/v1/banks/{bankid}/users/{userid}/custom/loans";
	public static final String HELP_TOPIC_INQ = "/v1/banks/{bankid}/users/{userid}/custom/helptopics";
	public static final String CUSTOM_CHANGE_PASS_RETAIL_URL = "/v1/banks/{bankid}/users/{userid}/custom/password";
	public static final String PHOTO_MAINTENANCE_URL = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/valdiatePhoto";
	public static final String CUSTOM_USER_PROFILE_DETAILS = "/v1/banks/{bankid}/users/{userid}/custom/userprofile";
	public static final String CUSTOM_NOTIFICATION_UPDATE_RESOURCE = "/v1/banks/{bankid}/users/{userid}/custom/notifications/{notificationId}";
	public static final String LOAN_REPAYMENT_SCHEUDLE_LIST_URL = "/v1/banks/{bankid}/users/{userid}/custom/loans/{accountId}";
	public static final String LOAN_HISTORY_URL = "/v1/banks/{bankid}/users/{userid}/custom/loanHist/{accountId}";
	public static final String CUSTOM_NOTIFICATION_LIST_RESOURCE = "/v1/banks/{bankid}/users/{userid}/custom/notifications";
	public static final String LOAN_PAYMENT_URL = "/v1/banks/{bankid}/users/{userid}/custom/servicerequests/loan-payment";
	public static final String CUSTOM_APPLICATION_DOCUMENT_LIST_RESOURCE = "/v1/banks/{bankid}/users/{userid}/custom/applications/document-list";
	public static final String CUSTOM_CREATE_LOAN_RESOURCE = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/createloan";
	public static final String CUSTOM_ACCOUNT_LIST_INQUIRY="/v1/banks/{bankid}/users/{userid}/custom/loanaccounts";
	public static final String MERCHANT_PAYMENT_URL = "/v1/banks/{bankid}/users/{userid}/custom/servicerequests/make-purchase";
	public static final String LIMIT_INQUIRY_URL = "/v1/banks/{bankid}/users/{userid}/custom/limitInquiry";
	public static final String STMT_INQ_URL = "/v1/banks/{bankid}/users/{userid}/custom/stmt";
	public static final String MERCHANT_REFUND_URL = "/v1/banks/{bankid}/custom/servicerequests/make-refund";
	public static final String APPLICATION_STATUS_UPDATE_URL = "/v1/banks/{bankId}/custom/applicationStatusUpdate";
	public static final String MERCHANT_INQUIRY_URL = "/v1/banks/{bankid}/custom/servicerequests/merchant-inquiry";
	public static final String OFFLINE_KTP_VERIFICATION_URL = "/v1/banks/{bankId}/custom/applications/{appId}/verifyKTP";
	public static final String CUSTOM_LOAN_DETAILS_UPDATE_URL = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/updateLoanDetails";
	public static final String BANK_CODES_MAINTENANCE_URL = "/v1/banks/{bankid}/users/{userid}/custom/bankcodes/{productCode}";
	public static final String BANK_CODE_FETCH_URL = "/v1/banks/{bankid}/users/{userid}/custom/bankcode/{applicationId}";
	public static final String BANK_CODES_FETCH_FOR_FP_URL = "/v1/banks/{bankid}/custom/bankcodes/{productCode}";
	public static final String CUSTOM_REUPLOAD_KTP_URL = "/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/reuploadktp";
	public static final String OFFLINE_KTP_FETCH_URL = "/v1/banks/{bankId}/custom/applications/{appId}/fetchKTP";
	public static final String VERIFY_PAYROLL_ACCT_URL = "/v1/banks/{bankid}/users/{userid}/custom/verifyPayroll";
	public static final String CUSTOM_APP_STATUS_NOTIFICATION_LIST_RESOURCE = "/v1/banks/{bankid}/users/{userid}/custom/appstatusnotifications";
	public static final String CUSTOM_APP_STATUS_NOTIFICATION_UPDATE_RESOURCE = "/v1/banks/{bankid}/users/{userid}/custom/appstatusnotifications/{notificationId}";


	public static final String CHANGE_PAYROLL_DATE_URL = "/v1/banks/{bankId}/custom/applications/changepayrolldate";
	public static final String CHANGE_PAYROLL_DATE_STATUS_URL = "/v1/banks/{bankId}/custom/applications/changepayrolldate/status";
	// added for PINANG pay later start
	public static final String LOAN_SCHEME_CODES_FETCH_URL = "/v1/banks/{bankid}/users/{userid}/custom/loanSchemeCodes";
	// added for PINANG pay later end
	// Added for VIDA Liveness start
	public static final String VIDA_LIVENESS_URL = "/v1/banks/{bankid}/users/{userid}/custom/face/liveness";
	// Added for VIDA Liveness end
	

}
