/*
 * Created on Feb 13, 2009
 * COPYRIGHT NOTICE:
 * Copyright (c) 2004 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */

package com.infosys.custom.reports.jasper.export;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.PdfExporterConfiguration;
import net.sf.jasperreports.export.PdfReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.engine.JRPropertiesUtil;

import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.IContext;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.reports.constants.ReportConstants;
import com.infosys.feba.framework.reports.core.IFEBAReportExport;
import com.infosys.feba.framework.reports.core.IFEBAReportInfo;
import com.infosys.feba.framework.reports.util.InfoAccessFactory;
import com.infosys.feba.framework.types.valueobjects.ReportInputOutputVO;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author sunny_gujjaru, Deepthi_k01 Created : Feb 13, 2009 This class exports
 *         the report into an PDF file or stream.
 */
public class CustomPDFReportExport implements IFEBAReportExport {

	/* Context variable passed from service/ batch framework. */
	private IContext context;

	/**
	 * Method Name : export This method streams/saves the report in a PDF file
	 * 
	 * @author sunny_gujjaru, Deepthi_k01
	 * @since FEBA 2.0
	 * @param input
	 *            - Jasper Print object.
	 * @param out
	 *            - Output File Path : if saveReport = SAVE_REPORT_TO_FILE -
	 *            Output Stream : if saveReport = DOWNLOAD_REPORT
	 * @param context
	 *            - Context that is passed for exceptions.
	 * @param saveReport
	 *            - boolean flag that decides if the report should be saved or
	 *            streamed.
	 * @throws CriticalException
	 */
	public final void exportReport(Object input, Object out, IContext context,
			boolean saveReport) throws CriticalException {
		exportReport(input, out, context, saveReport, null);
	}

	public final void exportReport(Object input, Object out, IContext context,
			boolean saveReport, ReportInputOutputVO reportInputOutputVO)
			throws CriticalException {
		LogManager.logDebug(context, "PDFReportExport : exportReport : start.");

		/* set the context since method. */
		this.context = context;

		final JasperPrint jasperPrint = (JasperPrint) input;

		/*
		 * Call the PDF exporter class which will format the report print object
		 * to PDF format.
		 */

		final JRPdfExporter PdfExporter = new JRPdfExporter();
		
		SimplePdfExporterConfiguration pdfExporterConfiguration = new SimplePdfExporterConfiguration();

		/* set export parameter - Jasper print object */
		//PdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		PdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		PdfExporter.setConfiguration(pdfExporterConfiguration);
		
		/* reportInfo class gets the password specific to a user. */
		final IFEBAReportInfo reportInfo;
		String pwdClass = null;
		if (null != reportInputOutputVO
				&& FEBAConstants.YES.equalsIgnoreCase(reportInputOutputVO
						.getIsPasswordProtectedDoc().toString())) {
			pwdClass = getReportInfoIdentifierClass(reportInputOutputVO);
		}

		try {
			reportInfo = InfoAccessFactory.getReportInfoClass(pwdClass);
		} catch (InstantiationException e) {
			LogManager.logError(context, e);

			throw new CriticalException(context,
					FEBAIncidenceCodes.INSTANTIATION_EXCEPTION,
					"Unable to instantiate the report class",
					ErrorCodes.INSTANTIATIONEXCEPTION, e);
		} catch (IllegalAccessException e) {
			LogManager.logError(context, e);

			throw new CriticalException(context,
					FEBAIncidenceCodes.ILLEGAL_ACCESS, "Illegal Access",
					ErrorCodes.ILLEGAL_ACCESS, e);
		}

		/*
		 * password should change every time for every report/ atleast for every
		 * user. So, we are getting that from reportInfo object
		 */
		String password = "";
		if (null != reportInputOutputVO) {
			password = reportInfo.getPassword(context, reportInputOutputVO)
					.getValue();
		} else {
			password = reportInfo.getPassword(null).getValue();
		}

		/*
		 * Added for Defect ID 192475 - To set permission to enable
		 * 1)print icon in PDF Report
		 * 2)annotations like Hightlight Text and Add Sticky Notes options in PDF report
		 */
		setPdfPermission(pdfExporterConfiguration);	
		
		//pdfExporterConfiguration.
		/* Encrypt the pdf stream/file data . */
		pdfExporterConfiguration.setEncrypted(Boolean.TRUE);
		
		//PdfExporter.setParameter(JRPdfExporterParameter.IS_ENCRYPTED,
		//		Boolean.TRUE);
		pdfExporterConfiguration.set128BitKey(Boolean.TRUE);
		//PdfExporter.setParameter(JRPdfExporterParameter.IS_128_BIT_KEY,
		//		Boolean.TRUE);

		/* Set the password and permissions for the pdf to be created. */
		/* Added for RTL Enhancements */
		//PdfExporter
		//		.setParameter(JRPdfExporterParameter.USER_PASSWORD, password);
		pdfExporterConfiguration.setUserPassword(password);
		JRPropertiesUtil.getInstance(DefaultJasperReportsContext.getInstance()).setProperty("net.sf.jasperreports.default.pdf.encoding",
				"Identity-H");
		JRPropertiesUtil.getInstance(DefaultJasperReportsContext.getInstance()).setProperty(
				"net.sf.jasperreports.default.pdf.font.name",
				FEBAConstants.FEBA_SYSTEM_ROOT_PATH
						+ "/"
						+ PropertyUtil.getProperty(
								ReportConstants.PDF_FONT_NAME, this.context));
		JRPropertiesUtil.getInstance(DefaultJasperReportsContext.getInstance()).setProperty("net.sf.jasperreports.default.pdf.embedded",
				Boolean.TRUE.toString());
		//PdfExporter.setParameter(JRExporterParameter.CHARACTER_ENCODING,
		//		"UTF-8");
		
		/* Added for RTL Enhancements */

		if (saveReport) {
			/* Logic has been moved to JasperReportingEngine saveReport method */
			final String outputFile = (String) out;
			
			//PdfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,
			//		outputFile);
			PdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new java.io.File(outputFile))); 
		} else {
			/* Set parameter - output Stream */
			//PdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
			//		(OutputStream) out);
			/* Set parameter - output Stream */
			
			PdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput((OutputStream)out));
		}

		try {
			/* Call export method of the corresponding export class. */
			PdfExporter.exportReport();
		}

		catch (JRException jrexception) {
			LogManager.logError(context, jrexception);

			throw new CriticalException(context,
					FEBAIncidenceCodes.UNABLE_TO_FORMAT_REPORT,
					"Unable to format the report",
					ErrorCodes.UNABLE_TO_FORMAT_REPORT, jrexception);
		}
		LogManager.logDebug(context, "PDFReportExport : export : PDF Report "
				+ " is created successfully");
	}

	// Get the fully qualified class name
	public String getReportInfoIdentifierClass(ReportInputOutputVO reportVO) {
		String REPORT_CLASS_LOCATION = PropertyUtil.getProperty(
				"REPORTS_INFO_CLASS_LOCATION", this.context);
		String reportClassName = REPORT_CLASS_LOCATION
				+ FEBAConstants.TAG_SEPARATOR + ReportConstants.REPORTS_INFO
				+ reportVO.getReportInfoIdentifier() + ReportConstants.GEN_PWD;
		// Set the Qualified class name back to
		return reportClassName;
	}
	
	/**
	 * Method to set permission in PDF exporter
	 *  
	 * @param PdfExporter
	 */
	public void setPdfPermission(SimplePdfExporterConfiguration PdfExporter) {
		int opt = 0;

		//Added for enabling print icon in PDF Report		
	    if (PropertyUtil.getProperty(FEBAConstants.PRINT_ALLOWED_IN_PDF_REPORT,
					context).equalsIgnoreCase(FEBAConstants.YES)) {
	    	opt = opt | PdfWriter.ALLOW_PRINTING;
	    }

	    //Added for enabling annotations like Hightlight Text and Add Sticky Notes options in PDF report
	    if (PropertyUtil.getProperty(FEBAConstants.MODIFY_ANNOTATIONS_ALLOWED_IN_PDF,
					context).equalsIgnoreCase(FEBAConstants.YES)) {
	    	opt = opt | PdfWriter.ALLOW_MODIFY_ANNOTATIONS;
	    }

	    if (PropertyUtil.getProperty("ALLOW_SCREENREADERS",
				context).equalsIgnoreCase(FEBAConstants.YES)) {
	    	opt = opt | PdfWriter.ALLOW_SCREENREADERS;
	    }

	    if (PropertyUtil.getProperty("ALLOW_FILL_IN",
				context).equalsIgnoreCase(FEBAConstants.YES)) {
	    	opt = opt | PdfWriter.ALLOW_FILL_IN;
	    }

	    if (PropertyUtil.getProperty("SIGNATURE_APPEND_ONLY",
				context).equalsIgnoreCase(FEBAConstants.YES)) {
	    	opt = opt | PdfWriter.SIGNATURE_APPEND_ONLY;
	    }	    
	    
	    //Set permission in PDF exporter
	    PdfExporter.setPermissions(opt);
	}

}
